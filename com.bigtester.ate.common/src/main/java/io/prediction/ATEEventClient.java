/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2017, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package io.prediction;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.StringUtils;
import org.asynchttpclient.Request;
import org.asynchttpclient.RequestBuilder;
import org.joda.time.DateTime;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import io.prediction.Event;
import io.prediction.EventClient;
import io.prediction.FutureAPIResponse;

// TODO: Auto-generated Javadoc
/**
 * This class ATEEventClient defines ....
 * @author Peidong Hu
 *
 */
public class ATEEventClient extends EventClient {
	protected String accessKey;

	/**
	 * @param accessKey
	 * @param eventURL
	 */
	public ATEEventClient(String accessKey, String eventURL) {
		super(accessKey, eventURL);
		this.accessKey = accessKey;

	}
	public ATEEventClient(String accessKey, String eventURL, int threadLimit) {
		super(accessKey, null, eventURL, threadLimit);
		this.accessKey = accessKey;

	}
	public ATEEventClient(String accessKey, String eventURL, String channelId, int threadLimit) {
		super(accessKey, channelId, eventURL, threadLimit);
		this.accessKey = accessKey;

	}
	/**
     * Sends an asynchronous get event request to the API.
     *
     * @param eid ID of the event to get
     */
    public FutureAPIResponse getEventsAsFuture() throws IOException {
        Request request = (new RequestBuilder("GET"))
            .setUrl(apiUrl + "/events.json?accessKey=" + accessKey + (this.getChannelName().isPresent()?"&channel=" + this.getChannelName().get():"")+ "&limit=100000")
            .build();
        return new FutureAPIResponse(client.executeRequest(request, getHandler()));
    }

    public FutureAPIResponse getEventsAsFutureForAut(String autName) throws IOException {
        Request request = (new RequestBuilder("GET"))
            .setUrl(apiUrl + "/events.json?accessKey=" + accessKey + (StringUtils.isEmpty(autName) ? "" : "&event=" + autName) + (this.getChannelName().isPresent()?"&channel=" + this.getChannelName().get():"") + "&limit=100000")
            .build();
        return new FutureAPIResponse(client.executeRequest(request, getHandler()));
    }

    public List<Event> getEventsForAut(String autName)
            throws ExecutionException, InterruptedException, IOException {
        return getEvents(getEventsAsFutureForAut(autName));
    }

    public List<AteEvent> getAteEventsForAut(String autName)
            throws ExecutionException, InterruptedException, IOException {
        return getAteEvents(getEventsAsFutureForAut(autName));
    }

    /**
     * Sends a synchronous get event request to the API.
     *
     * @param eid ID of the event to get
     *
     * @throws ExecutionException indicates an error in the HTTP backend
     * @throws InterruptedException indicates an interruption during the HTTP operation
     * @throws IOException indicates an error from the API response
     */
    public List<Event> getEvents()
            throws ExecutionException, InterruptedException, IOException {
        return getEvents(getEventsAsFuture());
    }
    public List<Event> getEvents(FutureAPIResponse response)
            throws ExecutionException, InterruptedException, IOException {
        int status = response.get().getStatus();
        String message = response.get().getMessage();

        if (status == BaseClient.HTTP_OK) {
            // handle DateTime separately
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(DateTime.class, new DateTimeAdapter());
            Gson gson = gsonBuilder.create();
            Type listType = new TypeToken<ArrayList<Event>>(){}.getType();
            return gson.fromJson(message,listType);
        } else {
            throw new IOException(message);
        }
    }
    public List<AteEvent> getAteEvents(FutureAPIResponse response)
            throws ExecutionException, InterruptedException, IOException {
        int status = response.get().getStatus();
        String message = response.get().getMessage();

        if (status == BaseClient.HTTP_OK) {
            // handle DateTime separately
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(DateTime.class, new DateTimeAdapter());
            Gson gson = gsonBuilder.create();
            Type listType = new TypeToken<ArrayList<AteEvent>>(){}.getType();
            return gson.fromJson(message,listType);
        } else {
            throw new IOException(message);
        }
    }
}
