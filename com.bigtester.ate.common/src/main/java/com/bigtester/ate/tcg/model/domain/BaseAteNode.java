/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model.domain;
 
import java.util.Collection;
import java.util.Iterator;

import org.eclipse.jdt.annotation.Nullable;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;

import com.bigtester.ate.tcg.model.relationship.InTestCase;
import com.bigtester.ate.tcg.utils.GlobalUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;




/**
 * This class AbstractAteNode defines ....
 * @author Peidong Hu
 *
 */
@NodeEntity
abstract public class BaseAteNode implements INonPointerAssignment{
	
	/** The id. */
	@GraphId
	@Nullable
	private Long graphId; //NOPMD
	
	/** The node label name. */
	@Property
	private String nodeLabelName;
		
	/**
	 * Instantiates a new base ate node.
	 *
	 * @param nodeLblName the node label name
	 */
	public BaseAteNode(String nodeLblName) {
		nodeLabelName = nodeLblName;
	}
	@JsonIgnore
	abstract public String getIdentifierString();
	
	public void assignedFrom(INonPointerAssignment value) {
		if (value instanceof BaseAteNode) {
			if (((BaseAteNode) value).getGraphId()!=null && ((BaseAteNode) value).getGraphId()!=0)
				this.graphId = ((BaseAteNode) value).getGraphId();
			this.nodeLabelName = ((BaseAteNode) value).getNodeLabelName();
				
		}
		
	}
	/**
	 * @return the nodeLabelName
	 */
	public String getNodeLabelName() {
		return nodeLabelName;
	}

	/**
	 * @param nodeLabelName the nodeLabelName to set
	 */
	public void setNodeLabelName(String nodeLabelName) {
		this.nodeLabelName = nodeLabelName;
	}
	
	/**
	 * Non pointer assign.
	 *
	 * @param value the value
	 * @param assignee the assignee
	 */
	protected <T> void nonPointerAssign(Collection<T> value, Collection<T> assignee) {
//		if (value == assignee) return;
//		if (assignee.isEmpty()) {
//			assignee.addAll(value);
//		} else {
//			boolean nonINonPointerAssignmentExists = false;
//			Iterator<Object> itr = (Iterator<Object>) value.iterator();
//			while (itr.hasNext()) {
//				Object temp = itr.next();
//				if (temp instanceof INonPointerAssignment) {
//					INonPointerAssignment record = (INonPointerAssignment) temp;
//					Long graphId = record.getGraphId(); 
//					Iterator<Object> itr2 = (Iterator<Object>) assignee.iterator();
//					
//					boolean exist = false;
//					INonPointerAssignment record2;
//					while (itr2.hasNext()) {
//						Object temp2 = itr2.next();
//						if (temp2 instanceof INonPointerAssignment) {
//							record2 = (INonPointerAssignment) temp2;
//							
//							if (graphId.equals(record2.getGraphId())) {
//								exist = true;
//								record2.assignedFrom(record);
//								break;
//							}
//						} else {
//							nonINonPointerAssignmentExists = true;
//							break;
//						}
//					}
//					if (nonINonPointerAssignmentExists) {
//						break;
//					} else if (!exist ) {
//						assignee.add((T) record);
//					}
//					
//				} else {
//					nonINonPointerAssignmentExists = true;
//					break;
//				}
//			}
//			if (nonINonPointerAssignmentExists) {
//				assignee.addAll(value);
//			}
//		}
		this.nonPointerAssign(value, assignee, false);
	}

	/**
	 * Non pointer assign.
	 *
	 * @param <T> the generic type
	 * @param value the value
	 * @param assignee the assignee
	 * @param replace the replace
	 */
	protected <T> void nonPointerAssign(Collection<T> value, Collection<T> assignee, boolean replace) {
		if (value == assignee) return;
		//System.out.println();
		if (assignee.isEmpty()) {
			assignee.addAll(value);
		} else {
			if (replace) {
				assignee.removeAll(assignee);
			}
			boolean nonINonPointerAssignmentExists = false;
			Iterator<Object> itr = (Iterator<Object>) value.iterator();
			while (itr.hasNext()) {
				Object temp = itr.next();
				if (temp instanceof INonPointerAssignment) {
					INonPointerAssignment record = (INonPointerAssignment) temp;
					Long graphId = record.getGraphId(); 
					Iterator<Object> itr2 = (Iterator<Object>) assignee.iterator();
					
					boolean exist = false;
					INonPointerAssignment record2;
					while (itr2.hasNext()) {
						Object temp2 = itr2.next();
						if (temp2 instanceof INonPointerAssignment) {
							record2 = (INonPointerAssignment) temp2;
							boolean labelSameRecord = false;
							boolean isSameHtmlNode = false;
							if (record instanceof INameLabelPredictableObjectNonPointerAssignment && record2 instanceof INameLabelPredictableObjectNonPointerAssignment) {
								labelSameRecord = ((INameLabelPredictableObjectNonPointerAssignment) record).getComparableLabelName().equalsIgnoreCase(
										((INameLabelPredictableObjectNonPointerAssignment) record2).getComparableLabelName());
								isSameHtmlNode = GlobalUtils.isSameHtmlCodeWithoutAteGuid(((INameLabelPredictableObjectNonPointerAssignment) record).getComparableHtmlCode(), ((INameLabelPredictableObjectNonPointerAssignment) record2).getComparableHtmlCode());
								
							}
							if (graphId!=null && graphId.equals(record2.getGraphId()) && graphId!=0 || (graphId==null || graphId==0 || record2.getGraphId()==null || record2.getGraphId() == 0 ) && labelSameRecord && isSameHtmlNode) {
								exist = true;
								record2.assignedFrom(record);
								break;
							}
						} else {
							nonINonPointerAssignmentExists = true;
							break;
						}
					}
					if (nonINonPointerAssignmentExists) {
						break;
					} else if (!exist ) {
						assignee.add((T) record);
					}
					
				} else {
					nonINonPointerAssignmentExists = true;
					break;
				}
			}
			if (nonINonPointerAssignmentExists) {
				assignee.addAll(value);
			}
		}
	}

	/**
	 * Non pointer assign.
	 *
	 * @param value the value
	 * @param assignee the assignee
	 */
	protected void nonPointerAssign(INonPointerAssignment value, INonPointerAssignment assignee) {
			assignee.assignedFrom(value);
	}


	/**
	 * @return the graphId
	 */
	public Long getGraphId() {
			//if (graphId==null) return 0l;
			return graphId;
	}


	/**
	 * @param graphId the graphId to set
	 */
	public void setGraphId(Long graphId) {
		this.graphId = graphId;
	}

	@Override
	public boolean equals(@Nullable Object obj) {
	    if (obj == null) {
	        return false;
	    }
	    if (!BaseAteNode.class.isAssignableFrom(obj.getClass())) {
	        return false;
	    }
	    final BaseAteNode other = (BaseAteNode) obj;
	    if (this.getGraphId()!=null && this.getGraphId()!=0 && other.getGraphId()!=null && other.getGraphId()!=0){
	    		if (!this.getGraphId().equals(other.getGraphId())) 
	    			return false;
	    } else {
		    if (!this.getIdentifierString().equals(other.getIdentifierString()) ) {
		        return false;
		    }
	    }
	    return true;
	}

	@Override
	public int hashCode() {
	    int hash = 3;
	    //hash = 53 * hash + (this.startNode != null ? this.startNode.hashCode() : 0);
	    hash = 53 * hash + (this.getIdentifierString() != null ? this.getIdentifierString().hashCode() : 0);
	    return hash;
	}

}
