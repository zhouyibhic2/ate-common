/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.runner.model;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.collections.map.LinkedMap;
import org.bigtester.ate.model.casestep.IStepJumpingEnclosedContainer;
import org.bigtester.ate.model.data.exception.RuntimeDataException;
import org.bigtester.ate.model.page.atewebdriver.IMyWebDriver;
import org.bigtester.ate.model.page.atewebdriver.exception.BrowserUnexpectedException;
import org.bigtester.ate.model.page.elementfind.IElementFind;
import org.bigtester.ate.model.page.exception.PageValidationException;
import org.bigtester.ate.model.page.exception.StepExecutionException;
import org.eclipse.jdt.annotation.Nullable;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import com.bigtester.ate.tcg.model.ScreenNamePredictStrategy;
import com.bigtester.ate.tcg.model.domain.HTMLSource;
import com.bigtester.ate.tcg.model.domain.ScreenJumperElementTrainingRecord;
import com.bigtester.ate.tcg.model.domain.UserInputTrainingRecord;
import com.bigtester.ate.tcg.model.ws.api.ScreenStepsAdvice;
import com.bigtester.ate.tcg.runner.JsInjector;
import com.bigtester.ate.tcg.runner.TebloudRunnerProperties;
import com.bigtester.ate.tcg.runner.screenobject.handler.IFilePickerDialogHandler;
import com.bigtester.ate.tcg.runner.screenobject.handler.IWebElementHandler;

// TODO: Auto-generated Javadoc
/**
 * This class ITestStepExecutionContext defines ....
 *
 * @author Peidong Hu
 *
 */
public interface ITestStepExecutionContext {
	IMyWebDriver getMyWebDriver();

	// <String, StepAdvice>
	List<List<ScreenStepsAdvice>> getExecutedStepAdvices();

	Optional<ScreenStepsAdvice> getLastExecutedStepAdvice();

	void appendExecutedStepAdvice(ScreenStepsAdvice newAdvice);

	IElementFind getElementFinder(Class<?> elementFinderClass);

	Optional<ScreenStepsAdvice> getPreviousScreenLastStepAdvice(String currentScreenName);

	long getSuccessCount();

	void setSuccessCount(long count);

	TebloudRunnerProperties getRunnerProperties();

	// Map<ScreenNamePredictStrategy, ScreenStepsAdvice>
	// getStepAdvice(DevToolMessage message, ScreenStepsAdvice previousAdvice);

	Map<ScreenNamePredictStrategy, ScreenStepsAdvice> getCurrentExecutingScreenStepAdvices();

	boolean isSameScreenNamedAdvices(Map<ScreenNamePredictStrategy, ScreenStepsAdvice> screen1advices,
			Map<ScreenNamePredictStrategy, ScreenStepsAdvice> screen2advices);

	DevToolMessage downloadScreenMessage();

	int caculateDomRelativeDistanceByGuid(String guid1, String guid2);

	int caculateDomRelativeDistanceByXpath(String xpath1, String xpath2);
	// boolean isAlreadyExecutedUitr(UserInputTrainingRecord uitr, ScreenStepsAdvice
	// stepAdvice);

	String generateStepNaming(String screenName, String elementLabelName, String actionName, String value,
			String stepWebElementGuid);

	Map<String, String> parseStepNaming(String stepNaming);

	List<IWebElementHandler> getWebElementHandlers();

	List<IFilePickerDialogHandler> getFilePickerSystemDialogHandler();

	boolean isInDeedLoopOfScreenJumping();

	boolean isValidStepPath(String domainName, String previousScreenName, String jumperName, String nextScreenName);

	Optional<ScreenStepsAdvice> isValidStepPath(ScreenStepsAdvice currentExecutingAdvice,
			ScreenJumperElementTrainingRecord jumper,
			Map<ScreenNamePredictStrategy, ScreenStepsAdvice> nextExecutingAdvices);

	Map<ScreenNamePredictStrategy, ScreenStepsAdvice> testScreen(JsInjector injector,
			Map<ScreenNamePredictStrategy, ScreenStepsAdvice> stepAdvices, DevToolMessage message) throws Throwable;

	// integration with ate xml engine
	public void doStep(@Nullable IStepJumpingEnclosedContainer jumpingContainer)
			throws StepExecutionException, PageValidationException, RuntimeDataException;

	boolean isExactSameScreenAdvices(Map<ScreenNamePredictStrategy, ScreenStepsAdvice> stepAdvices1,
			Map<ScreenNamePredictStrategy, ScreenStepsAdvice> stepAdvices2);

	/**
	 * @param testCaseName
	 * @param message
	 * @param previousStepAdvice
	 * @return
	 */
	Map<ScreenNamePredictStrategy, ScreenStepsAdvice> getStepAdvice(String testCaseName, DevToolMessage message,
			ScreenStepsAdvice previousStepAdvice);
	IElementFind getElementFinder();
	WebElement findElement(String finderIdentifier) throws NoSuchElementException, BrowserUnexpectedException;
	WebElement findFollowingInputFileWebElement(String finderIdentifier) throws NoSuchElementException, BrowserUnexpectedException;
	WebElement findPrecedingInputFileWebElement(String finderIdentifier) throws NoSuchElementException, BrowserUnexpectedException;
	int caculateDomRelativeDistanceForFollowingFileInput(String domFinderId);
	int caculateDomRelativeDistanceForPrecedingFileInput(String domFinderId);
	String getTestCaseName();
}
