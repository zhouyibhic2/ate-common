/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.service.repository;

import java.util.Map;

import org.eclipse.jdt.annotation.Nullable;
import org.neo4j.ogm.model.Result;
import org.springframework.data.neo4j.annotation.Depth;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bigtester.ate.tcg.model.domain.Neo4jScreenNode;

// TODO: Auto-generated Javadoc
/**
 * This class ScreenNodeRepo defines ....
 * @author Peidong Hu
 *
 */
@Repository
public interface ScreenNodeRepo extends Neo4jRepository<Neo4jScreenNode, Long> {

	/**
	 * Gets the neo4j screen node by url and name.
	 *
	 * @return the neo4j screen node by url and name
	 */
	@Nullable
	Neo4jScreenNode getNeo4jScreenNodeByUrlAndName(String screenUrl, String screenName, @Depth int depth);

	/**
	 * Gets the neo4j screen node by name.
	 *
	 * @param screenName the screen name
	 * @return the neo4j screen node by name
	 */
	@Depth(2)
	Iterable<Neo4jScreenNode> getNeo4jScreenNodeByName(String screenName);

	/**
	 * Gets the distinct screen jumper label names.
	 *
	 * @param screenName the screen name
	 * @return the distinct screen jumper label names
	 */
	// @Query("MATCH (screen:Neo4jScreenNode{name:{screenName}})-[jumper_r:PREDICTED_USER_SCREENJUMP_ELEMENT_RESULTS]-(jumper)-[inTest:IN_TESTCASE]-() where jumper.actionTrigger = true RETURN distinct jumper.inputLabelName")
	@Query("MATCH (screen:Neo4jScreenNode{name:{screenName}})-[jumper_r:PREDICTED_USER_SCREENJUMP_ELEMENT_RESULTS]-(jumper)-[inTest:IN_TESTCASE]-(), (jumper)-[:PREDICTED_FIELD_NAME]-(label) "
			+ "where jumper.actionTrigger = true RETURN distinct label.value as lName union " +
			"MATCH (screen:Neo4jScreenNode{name:{screenName}})-[jumper_r:PREDICTED_USER_SCREENJUMP_ELEMENT_RESULTS]-(jumper)-[inTest:IN_TESTCASE]-() "
					+ "where jumper.actionTrigger = true RETURN distinct jumper.inputLabelName as lName")
	Iterable<String> getDistinctScreenJumperLabelNames(
			@Param("screenName") String screenName);

	@Query("MATCH (screen:Neo4jScreenNode{name:{screenName}})-[jumper_r:PREDICTED_USER_SCREEN_ELEMENT_CHANGE_RESULTS]-(jumper)-[inTest:IN_TESTCASE]-(), (jumper)-[:PREDICTED_FIELD_NAME]-(label) "
			+ "where jumper.actionTrigger = true RETURN distinct label.value as lName union " +
			"MATCH (screen:Neo4jScreenNode{name:{screenName}})-[jumper_r:PREDICTED_USER_SCREEN_ELEMENT_CHANGE_RESULTS]-(jumper)-[inTest:IN_TESTCASE]-() "
					+ "where jumper.actionTrigger = true RETURN distinct jumper.inputLabelName as lName")
	Iterable<String> getDistinctScreenElementChangerLabelNames(
			@Param("screenName") String screenName);

	@Query("MATCH (screen:Neo4jScreenNode{name:{screenName}})-[jumper_r:PREDICTED_USER_IN_SCREENJUMP_ELEMENT_RESULTS]-(jumper)-[inTest:IN_TESTCASE]-(), (jumper)-[:PREDICTED_FIELD_NAME]-(label) "
			+ "where jumper.actionTrigger = true RETURN distinct label.value as lName union " +
			"MATCH (screen:Neo4jScreenNode{name:{screenName}})-[jumper_r:PREDICTED_USER_IN_SCREENJUMP_ELEMENT_RESULTS]-(jumper)-[inTest:IN_TESTCASE]-() "
			+ "where jumper.actionTrigger = true RETURN distinct jumper.inputLabelName as lName" )
	Iterable<String> getDistinctInScreenJumperLabelNames(
			@Param("screenName") String screenName);

	@Query("MATCH (screen:Neo4jScreenNode{name:{screenName}})-[ui_r:PREDICTED_USER_INPUT_RESULTS]-(ui)-[inTest:IN_TESTCASE]-(), (ui)-[:PREDICTED_FIELD_NAME]-(label) "
			+ "where ui.actionTrigger = false RETURN distinct label.value as lName union " +
			"MATCH (screen:Neo4jScreenNode{name:{screenName}})-[ui_r:PREDICTED_USER_INPUT_RESULTS]-(ui)-[inTest:IN_TESTCASE]-() "
			+ "where ui.actionTrigger = false RETURN distinct ui.inputLabelName as lName")
	Iterable<String> getDistinctNonActionUserInputLabelNames(
			@Param("screenName") String screenName);

	@Query("match (n:WebElementTrainingRecord)-[r]-(m:Neo4jScreenNode)-[r2:SOURCING_DOMS]-(dom) with n, count(dom) as domCount  match (n:WebElementTrainingRecord)-[r]-(m:Neo4jScreenNode)-[r2:SOURCING_DOMS]-(dom) where domCount=1 and r is not null and r2 is not null  return min(length(n.inputMLHtmlCode)*1.0/length(dom.domDoc))")
	Double getMinMlHtmlCodeVsDomDocPercentileLengthRate();

	@Query("match (n:WebElementTrainingRecord)-[r]-(m:Neo4jScreenNode)-[r2:SOURCING_DOMS]-(dom) with n, count(dom) as domCount  match (n:WebElementTrainingRecord)-[r]-(m:Neo4jScreenNode)-[r2:SOURCING_DOMS]-(dom) where domCount=1 and r is not null and r2 is not null  return max(length(n.inputMLHtmlCode)*1.0/length(dom.domDoc))")
	Double getMaxMlHtmlCodeVsDomDocPercentileLengthRate();

	@Query("match (n:WebElementTrainingRecord)-[r]-(m:Neo4jScreenNode)-[r2:SOURCING_DOMS]-(dom) with n, count(dom) as domCount  match (n:WebElementTrainingRecord)-[r]-(m:Neo4jScreenNode)-[r2:SOURCING_DOMS]-(dom) where domCount=1 and r is not null and r2 is not null  return percentileDisc(length(n.inputMLHtmlCode)*1.0/length(dom.domDoc), {percentRate})")
	Double getMlHtmlCodeVsDomDocPercentileLengthRate(
			@Param("percentRate") double percentRate);


	@Query("match (n:WebElementTrainingRecord)-[r:COMPUTED_CSS_SIZE]-(m) where UPPER(n.inputLabelName)={label} return min(m.uitrSize)*1.0")
	Double getMinElementSize(@Param("label") String uitrLabel);

	@Query("match (n:WebElementTrainingRecord)-[r:COMPUTED_CSS_SIZE]-(m) where UPPER(n.inputLabelName)={label} return max(m.uitrSize)*1.0")
	Double getMaxElementSize(@Param("label") String uitrLabel);

	@Query("match (n:WebElementTrainingRecord)-[r:COMPUTED_CSS_SIZE]-(m) where UPPER(n.inputLabelName)={label} return percentileDisc(m.uitrSize*1.0, {percentRate})")
	Double getElementSizePercentileDist(@Param("label") String uitrLabel, @Param("percentRate") double percentRate);

	/**
	 * Gets the distinct user input label names.
	 *
	 * @param screenName
	 *            the screen name
	 * @return the distinct user input label names
	 */
	@Query("MATCH (screen:Neo4jScreenNode{name:{screenName}})-[userinput_r:PREDICTED_USER_INPUT_RESULTS]-(userinput)-[inTest:IN_TESTCASE]-(), (userinput)-[:PREDICTED_FIELD_NAME]-(label) RETURN distinct label.value")
	Iterable<String> getDistinctUserInputLabelNames(
			@Param("screenName") String screenName);

	@Query("MATCH (screen:Neo4jScreenNode{name:{previousScreenName}})-[jumper_r:PREDICTED_USER_SCREENJUMP_ELEMENT_RESULTS]-(jumper:ScreenJumperElementTrainingRecord)-[r2:STEP_OUT]->(nextScreen:Neo4jScreenNode{name:{nextScreenName}}), "
			+ "(nextScreen)-[inTest:IN_TESTCASE]-(tc:TestCase)-[inTest2:IN_TESTCASE]-(screen) "
			+ " match (jumper)-[inTest3:IN_TESTCASE]-(tc:TestCase{name:{testCaseName}})-[inTest4:IN_TESTCASE]-(nextScreen) where jumper.inputLabelName=~ {jumperName} return nextScreen.name")
	Iterable<String> getNextScreenNameInStepPath(
			@Param("previousScreenName") String previousScreenName
			, @Param("nextScreenName") String nextScreenName
			, @Param("jumperName") String jumperName
			, @Param("testCaseName") String testCaseName);


	@Query("MATCH (m:TebloudUserZone{zoneDomainName:{domainName}})-[d:CONTAINS_SCREEN]->(screen:Neo4jScreenNode{name:{screenName}})-[:PREDICTED_USER_SCREENJUMP_ELEMENT_RESULTS|PREDICTED_USER_IN_SCREENJUMP_ELEMENT_RESULTS|PREDICTED_USER_SCREEN_ELEMENT_CHANGE_RESULTS|PREDICTED_USER_INPUT_RESULTS]-(userinput)-[inTest:IN_TESTCASE]-(testcase:TestCase{name:{testCaseName}}), (userinput)-[:PREDICTED_FIELD_NAME]-(label) RETURN distinct label.value")
	Iterable<String> getDistinctUserInputLabelNamesInTestcaseOfDomain(
			@Param("domainName") String domainName,
			@Param("testCaseName") String testCaseName,
			@Param("screenName") String screenName);

	/**
	 * Gets the distinct end screen names in test case.
	 *
	 * @return the distinct end screen names in test case
	 */
	@Query("MATCH (screen:Neo4jScreenNode) where not ((screen:Neo4jScreenNode)-[:PREDICTED_USER_SCREENJUMP_ELEMENT_RESULTS|PREDICTED_USER_IN_SCREENJUMP_ELEMENT_RESULTS|PREDICTED_USER_SCREEN_ELEMENT_CHANGE_RESULTS|PREDICTED_USER_INPUT_RESULTS]-()-[:IN_TESTCASE]-())  RETURN distinct screen.name")
	Iterable<String> getDistinctEndScreenNamesInTestCase();

	@Query("MATCH (screen:Neo4jScreenNode)-[:IN_TESTCASE]-()  RETURN distinct screen.name")
	Iterable<String> getDistinctScreenNamesInTestCase();

	@Query("MATCH (suite:TestSuite)  RETURN distinct suite.name")
	Iterable<String> getDistinctTestSuiteNames();

	@Query("match (n:Neo4jScreenNode) where n.name is not null and n.inputMLHtmlWords is not null return n.name as label, n.inputMLHtmlWords as trainingWords")
	Iterable<Map<String, String>> findAllScreenLabelAndTrainingWords();

	@Query("match (x:Neo4jScreenNode)-[xr]-(n:UserInputTrainingRecord)-[r:CHILD_POSITION_UITR_IN_GROUP]->(m) match (n)-[l:USER_VALUES]-(q) where id(x) = {screenNodeId} delete r")
	Result deleteFieldGroupingRel(@Param("screenNodeId") Long screenNodeId);

	@Query("match (n:Neo4jScreenNode)-[r]-(m:TebloudUserZone) where n.name={screenName} and n.url={screenUrl} and m.zoneDomainName={domainName} return id(n)")
	Iterable<Long> getNeo4jScreenNodeIdByUrlNameAndDomainName(@Param("screenUrl") String screenUrl, @Param("screenName") String screenName, @Param("domainName") String domainName);

}
