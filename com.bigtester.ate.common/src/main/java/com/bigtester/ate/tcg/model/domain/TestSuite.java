/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model.domain;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.annotation.Nullable;
import org.neo4j.ogm.annotation.GraphId;
//import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import com.bigtester.ate.tcg.model.relationship.Relations;
import com.fasterxml.jackson.annotation.JsonIgnore;

// TODO: Auto-generated Javadoc
/**
 * This class TestIndustry defines ....
 * @author Peidong Hu
 *
 */
@NodeEntity
public class TestSuite extends BaseAteNode implements INonPointerAssignment{
	
	 
	
	/** The name. */
	//@Index
	private String name;
	
	@JsonIgnore
	/** The industry categories. */
	@Relationship(type=Relations.SUB_SUITE_OF,  direction=Relationship.INCOMING)
	private Set<TestSuite> subTestSuites= new HashSet<TestSuite>();
	
	//relationship is handled by test case hostingTestSuites field
	//private Set<TestCase> testCases = new HashSet<TestCase>();

//	/** The parent industry category. */
//	@Relationship(type=Relations.PARENT_TEST_SUITE)
//	@Nullable 
//	private TestSuite parentTestSuite;

	/**
	 * Instantiates a new test suite. Constructor for web service call
	 */
	public TestSuite() {
		super("TestSuite");
		this.name = "";
	}
	/**
	 * Instantiates a new test industry.
	 *
	 * @param code the code
	 * @param name the name
	 */
	public TestSuite(String name) {
		super("TestSuite");
		this.name = name;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the gid
	 */
	@Nullable
	public Long getGid() {
		return getGraphId();
	}

	/**
	 * @param gid the gid to set
	 */
	public void setGid(Long gid) {
		this.setGraphId(gid);
	}




//	/**
//	 * @return the parentTestSuite
//	 */
//	@Nullable
//	public TestSuite getParentTestSuite() {
//		return parentTestSuite;
//	}
//
//
//	/**
//	 * @param parentTestSuite the parentTestSuite to set
//	 */
//	public void setParentTestSuite(TestSuite parentTestSuite) {
//		this.parentTestSuite = parentTestSuite;
//	}


	/**
	 * @return the subTestSuites
	 */
	public Set<TestSuite> getSubTestSuites() {
		return subTestSuites;
	}


	/**
	 * @param subTestSuites the subTestSuites to set
	 */
	@Relationship(type=Relations.SUB_SUITE_OF,  direction=Relationship.INCOMING)
	public void setSubTestSuites(Set<TestSuite> subTestSuites) {
		this.nonPointerAssign(subTestSuites, this.subTestSuites);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void assignedFrom(INonPointerAssignment value) {
		if (value instanceof TestSuite) {
			TestSuite record = (TestSuite) value;
			this.setName(record.getName());
			Set<TestSuite> temp = record.getSubTestSuites();
			if (!temp.isEmpty()) {
				this.setSubTestSuites(temp);
				//this.nonPointerAssign(temp, this.getSubTestSuites());
			}
//			Set<TestCase> tempCases = record.getTestCases();
//			if (!tempCases.isEmpty()) {
//				this.setTestCases(tempCases);
//			}
//			
			 
		} 
	}
	 
//	public Set<TestCase> getTestCases() {
//		return testCases;
//	}
//	public void setTestCases(Set<TestCase> testCases) {
//		this.nonPointerAssign(testCases, this.testCases);
//	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getIdentifierString() {
		 
		return getName();
	}
}
