/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.service;

 
import com.bigtester.ate.tcg.model.IntermediateResult;
import com.bigtester.ate.tcg.model.domain.Neo4jScreenNode;
import com.bigtester.ate.tcg.model.domain.TestCase;
import com.bigtester.ate.tcg.model.domain.UserInputTrainingRecord;
  
// TODO: Auto-generated Javadoc
/**
 * This class IScreenNodeCrud defines ....
 * @author Peidong Hu
 *
 */
public interface IScreenNodeCrud {
   
	boolean deleteInTestCaseRelationship(Long graphId);
 
   
 	/**
	 * Create or update.
	 *
	 * @param intermediateResult
	 *            the intermediate result
	 * @return the neo4j screen node
	 */
	Neo4jScreenNode createOrUpdate(IntermediateResult intermediateResult, boolean commit);
 
	/**
	 * Update.
	 *
	 * @param screenNode the screen node
	 * @return the neo4j screen node
	 */
	Neo4jScreenNode update(Neo4jScreenNode screenNode);
 
	/**
	 * Update test case relationships.
	 *
	 * @param screenNode the screen node
	 * @param intermediateResult the intermediate result
	 * @param commit the commit
	 * @return the neo4j screen node
	 */
	Neo4jScreenNode updateTestCaseRelationships(IntermediateResult intermediateResult,
			boolean commit);
 
		 
 	/**
	 * Stepped into.
	 *
	 * @param endNode
	 *            the end node
	 * @param uitrId
	 *            the uitr id
	 * @return the step into
	 */
	void createOrUpdateStepout(Neo4jScreenNode startNode,
			Neo4jScreenNode endNode, IntermediateResult iResult);
 
	//MATCH (screen:Neo4jScreenNode{name:'jobDescApplyPage'})-[jumper_r:PREDICTED_USER_SCREENJUMP_ELEMENT_RESULTS]-(jumper)-[inTest:IN_TESTCASE]-() RETURN distinct screen.name, jumper.inputLabelName
	/**
	 * Find distinct screen jumper label names in test case.
	 *
	 * @param screenName the screen name
	 * @param testCase the test case
	 * @return the iterable
	 */
	Iterable<String> findDistinctScreenJumperLabelNamesInTestCase(String screenName, TestCase testCase);
 	
	/**
	 * Match screen jumper label names in test case.
	 *
	 * @param screenName the screen name
	 * @param labelNameToMatch the label name to match
	 * @param testCase the test case
	 * @return true, if successful
	 */
	boolean matchScreenJumperLabelNamesInTestCase(String screenName, final String labelNameToMatch, TestCase testCase);
 	
	boolean matchNonActionUserInputLabelNamesInTestCase(String screenName, final String labelNameToMatch, TestCase testCase);
 	/**
	 * Match screen element changer label names in test case.
	 *
	 * @param screenName the screen name
	 * @param labelNameToMatch the label name to match
	 * @param testCase the test case
	 * @return true, if successful
	 */
	boolean matchScreenElementChangerLabelNamesInTestCase(String screenName, final String labelNameToMatch, TestCase testCase);
 	
	/**
	 * Match in screen jumper label names in test case.
	 *
	 * @param screenName the screen name
	 * @param labelNameToMatch the label name to match
	 * @param testCase the test case
	 * @return true, if successful
	 */
	boolean matchInScreenJumperLabelNamesInTestCase(String screenName, final String labelNameToMatch, TestCase testCase);
 	
	/**
	 * Find distinct user input label names in test case.
	 *
	 * @param screenName the screen name
	 * @param testCase the test case
	 * @return the iterable
	 */
	Iterable<String> findDistinctUserInputLabelNamesInTestCase(String screenName, TestCase testCase);
 	
	/**
	 * Find distinct end screen names in test case.
	 *
	 * @param testCase the test case
	 * @return the iterable
	 */
	Iterable<String> findDistinctEndScreenNamesInTestCase(TestCase testCase);
  
	
	Double getMlHtmlCodeVsDomDocPercentileLengthRate(double rateRange);
	Double getMinMlHtmlCodeVsDomDocPercentileLengthRate();
	Double getMaxMlHtmlCodeVsDomDocPercentileLengthRate();
	Double getMinElementSize(String elementLabel);
	Double getMaxElementSize(String elementLabel);
	Double getElementSizePercentile(String elementLabel, double rateRange);
	boolean deleteFieldGroupRel(Long screenNodeId);

}
