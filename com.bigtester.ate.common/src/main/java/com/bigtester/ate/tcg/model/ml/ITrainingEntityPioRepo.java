/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model.ml;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord;
import com.bigtester.tebloud.machine.fasttext.config.TrainingParams;
import com.bigtester.tebloud.tcg.config.RunningModeProperties.RunningMode;

import io.prediction.AteEvent;

// TODO: Auto-generated Javadoc
/**
 * This class PredictionIOTrainer defines ....
 *
 * @author Peidong Hu
 *
 */

public interface ITrainingEntityPioRepo extends ITebloudMachine{


	public String sentUitrTrainingEntity(WebElementTrainingRecord record, String runningMode)
			throws ExecutionException, InterruptedException, IOException ;
	public static class MyPair
	{
	    private final String key;
	    private final String value;

	    public MyPair(String aKey, String aValue)
	    {
	        key   = aKey;
	        value = aValue;
	    }

	    public String key()   { return key; }
	    public String value() { return value; }
	}

	public List<MyPair> queryAllUitrTrainingEntitiesForAut()
			throws ExecutionException, InterruptedException, IOException ;
	public List<AteEvent> queryAllDecisionTreeEventsForAut()
			throws ExecutionException, InterruptedException, IOException;
	public String deleteUitrDecisionTrainingEntity(String entityId)
			throws ExecutionException, InterruptedException, IOException ;

	public String deleteUitrTrainingEntity(String entityId)
			throws ExecutionException, InterruptedException, IOException ;
	public boolean deleteUitrTrainingEvent(String eId)
			throws ExecutionException, InterruptedException, IOException;
	public List<AteEvent> queryAllEventsForAut()
			throws ExecutionException, InterruptedException, IOException;
	public String deleteScreenNameTrainingEntity(String entityId)
			throws ExecutionException, InterruptedException, IOException ;
	public boolean deleteUitrDecisionTrainingEvent(String eId)
			throws ExecutionException, InterruptedException, IOException;

	public String sentUitrDecisionTrainingEntityToAppEngine(WebElementTrainingRecord uitr, String screenName, boolean userTrainedRecord, String testSuiteName, String subSuiteName, String testCaseName)
			throws ExecutionException, InterruptedException, IOException ;

	public String trainScreenNameForAppEngine(String screenNameTrainingWords,
			String screenName) throws ExecutionException, InterruptedException,
			IOException ;

	public String trainLabelWordsForAppEngine(String trainingWords,
			String label) throws ExecutionException, InterruptedException,
			IOException ;
	public String trainRemoteFastTextModel(String autName, TrainingParams trainingParams);

}
