/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2017, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model.ml;

import org.bytedeco.javacpp.PointerPointer;

import com.github.jfasttext.FastTextWrapper;
import com.github.jfasttext.FastTextWrapper.FastTextApi;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PreDestroy;

public class AteFastText implements IAteFastText{

    private FastTextWrapper.FastTextApi fta;

    public AteFastText() {
        fta = new FastTextWrapper.FastTextApi();
    }

    @PreDestroy
    public void clearModelCache() {
        this.unloadModel();
        FastTextApi.deallocateReferences();
        //FastTextApi.free(fta);
        fta.close();
    }
    public void runCmd(String[] args) {
        // Prepend "fasttext" to the argument list so that it is compatible with C++'s main()
        String[] cArgs = new String[args.length + 1];
        cArgs[0] = "fasttext";
        System.arraycopy(args, 0, cArgs, 1, args.length);
        fta.runCmd(cArgs.length, new PointerPointer(cArgs));
    }

    public void loadModel(String modelFile) {
        fta.loadModel(modelFile);
    }

    public void unloadModel() {
        fta.unloadModel();
    }

    public void test(String testFile) {
        test(testFile, 1);
    }

    public void test(String testFile, int k) {
        if (k <= 0) {
            throw new IllegalArgumentException("k must be positive");
        }
        fta.test(testFile, k);
    }

    public String predict(String text){
        return predict(text, 1).get(0);  // NULL pointer exception?
    }

    public List<String> predict(String text, int k) {
        if (k <= 0) {
            throw new IllegalArgumentException("k must be positive");
        }
        FastTextWrapper.StringVector sv = fta.predict(text, k);
        List<String> predictions = new ArrayList<>();
        for (int i = 0; i < sv.size(); i++) {
            predictions.add(sv.get(i).getString());
        }
        return predictions;
    }

    public IAteFastText.ProbLabel predictProba(String text){
        return predictProba(text, 1).get(0);  // NULL pointer exception?
    }



    public List<Float> getVector(String word) {
        FastTextWrapper.RealVector rv = fta.getVector(word);
        List<Float> wordVec = new ArrayList<>();
        for (int i = 0; i < rv.size(); i++) {
            wordVec.add(rv.get(i));
        }
        return wordVec;
    }

    public int getNWords() {
        return fta.getNWords();
    }

    public List<String> getWords() {
        return stringVec2Strings(fta.getWords());
    }

    public int getNLabels() {
        return fta.getNLabels();
    }

    public List<String> getLabels() {
        return stringVec2Strings(fta.getWords());
    }

    public double getLr() {
        return fta.getLr();
    }

    public int getLrUpdateRate() {
        return fta.getLrUpdateRate();
    }

    public int getDim() {
        return fta.getDim();
    }

    public int getContextWindowSize() {
        return fta.getContextWindowSize();
    }

    public int getEpoch() {
        return fta.getEpoch();
    }

    public int getMinCount() {
        return fta.getMinCount();
    }

    public int getMinCountLabel() {
        return fta.getMinCountLabel();
    }

    public int getNSampledNegatives() {
        return fta.getNSampledNegatives();
    }

    public int getWordNgrams() {
        return fta.getWordNgrams();
    }

    public String getLossName() {
        return fta.getLossName().getString();
    }

    public String getModelName() {
        return fta.getModelName().getString();
    }

    public int getBucket() {
        return fta.getBucket();
    }

    public int getMinn() {
        return fta.getMinn();
    }

    public int getMaxn() {
        return fta.getMaxn();
    }

    public double getSamplingThreshold() {
        return fta.getSamplingThreshold();
    }

    public String getLabelPrefix() {
        return fta.getLabelPrefix().getString();
    }

    public String getPretrainedVectorsFileName() {
        return fta.getPretrainedVectorsFileName().getString();
    }

    private static List<String> stringVec2Strings(FastTextWrapper.StringVector sv) {
        List<String> strings = new ArrayList<>();
        for (int i = 0; i < sv.size(); i++) {
            strings.add(sv.get(i).getString());
        }
        return strings;
    }



    public static void main(String[] args) {
        AteFastText jft = new AteFastText();
        jft.runCmd(args);
    }

    public List<ProbLabel> predictProba(String text, int k) {
        if (k <= 0) {
            throw new IllegalArgumentException("k must be positive");
        }
        FastTextWrapper.FloatStringPairVector fspv = fta.predictProba(text, k);
        List<ProbLabel> probaPredictions = new ArrayList<>();
        for (int i = 0; i < fspv.size(); i++) {
            float logProb = fspv.first(i);
            String label = fspv.second(i).getString();
            probaPredictions.add(new ProbLabel(logProb, label));
        }
        return probaPredictions;
    }
}
