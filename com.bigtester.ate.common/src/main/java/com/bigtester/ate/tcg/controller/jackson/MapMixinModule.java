/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2017, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.controller.jackson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.introspect.AnnotatedClass;
import com.fasterxml.jackson.databind.introspect.BasicBeanDescription;
import com.fasterxml.jackson.databind.introspect.BasicClassIntrospector;
import com.fasterxml.jackson.databind.introspect.BeanPropertyDefinition;
import com.fasterxml.jackson.databind.introspect.ClassIntrospector;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.introspect.ObjectIdInfo;
import com.fasterxml.jackson.databind.introspect.POJOPropertiesCollector;
import com.fasterxml.jackson.databind.type.MapLikeType;
import com.google.common.collect.Multimap;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

public class MapMixinModule extends
		com.fasterxml.jackson.databind.module.SimpleModule {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3481569752079620282L;

	static class MyJacksonAnnotationIntrospector extends AnnotationIntrospector {
/**
		 * 
		 */
		private static final long serialVersionUID = -6143558939059540163L;

		//        @Override
//        public boolean _isIgnorable(final Annotated ann) {
//        	boolean retVal = super._isIgnorable(ann);
//        	if (!retVal) {
//            if (ann.getRawType() == HashMap.class || ann.getRawType() == Map.class) {
//                ann.
//            	return new ObjectIdInfo(
//                        PropertyName.construct("@id", null),
//                        null,
//                        JSOGGenerator.class,
//                        null);
//            }
//        	}
//            return retVal;
//        }
        @Override
        public ObjectIdInfo findObjectIdInfo(final Annotated ann) {
            if (ann.getType().isMapLikeType()) {
            	
                return new ObjectIdInfo(
                        PropertyName.construct("@id", null),
                        null,
                        JSOGGenerator.class,
                        null);
            }
            return super.findObjectIdInfo(ann);
        }

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Version version() {
			// TODO Auto-generated method stub
			return null;
		}

		
    }
	
	public static class MyClassIntrospector extends BasicClassIntrospector {

		/**
		 * 
		 */
		private static final long serialVersionUID = -6341709581828635356L;

		public static class MyBeanDesc extends BasicBeanDescription{

			/**
			 * @param config
			 * @param type
			 * @param classDef
			 * @param props
			 */
			public MyBeanDesc(MapperConfig<?> config, JavaType type,
					AnnotatedClass classDef, List<BeanPropertyDefinition> props) {
				super(config, type, classDef, props);
				super._objectIdInfo = new ObjectIdInfo(
                        PropertyName.construct("@id", null),
                        null,
                        JSOGGenerator.class,
                        null);
			}
		    
			
			public static MyBeanDesc forOtherUse(MapperConfig<?> config,
		            JavaType type, AnnotatedClass ac)
		    {
		        return new MyBeanDesc(config, type,
		                ac, Collections.<BeanPropertyDefinition>emptyList());
		    }
			
			
		}
		
		public MyBeanDesc _findStdJdkCollectionDesc(MapperConfig<?> cfg, JavaType type) {
			if (_isStdJDKCollection(type)) {
	            AnnotatedClass ac = AnnotatedClass.construct(type, cfg);
	            return MyBeanDesc.forOtherUse(cfg, type, ac);
	        }
	        return null;
		}
		/**
		 * {@inheritDoc}
		 */
		@Override
		public BasicBeanDescription forSerialization(SerializationConfig cfg,
				JavaType type, MixInResolver r) {
			// TODO Auto-generated method stub
			return super.forSerialization(cfg, type, r);
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public BasicBeanDescription forDeserialization(DeserializationConfig cfg,
				JavaType type, MixInResolver r) {
			return super.forDeserialization(cfg, type, r);
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public BasicBeanDescription forDeserializationWithBuilder(
				DeserializationConfig cfg, JavaType type, MixInResolver r) {
			// TODO Auto-generated method stub
			return super.forDeserializationWithBuilder(cfg, type, r);
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public BasicBeanDescription forCreation(DeserializationConfig cfg,
				JavaType type, MixInResolver r) {
			BasicBeanDescription desc = _findStdTypeDesc(type);
	        if (desc == null) {

	            // As per [Databind#550], skip full introspection for some of standard
	            // structured types as well
	            desc = this._findStdJdkCollectionDesc(cfg, type);
	            if (desc == null) {
	                desc = BasicBeanDescription.forDeserialization(
	            		collectProperties(cfg, type, r, false, "set"));
	            }
	        }
	        // should this be cached for FCA?
	        return desc;
//			if (type.isMapLikeType()) {
//				return _findStdJdkCollectionDesc(cfg, type);
//			}
			//return null;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public BasicBeanDescription forClassAnnotations(MapperConfig<?> cfg,
				JavaType type, MixInResolver r) {
			// TODO Auto-generated method stub
			return super.forClassAnnotations(cfg, type, r);
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public BasicBeanDescription forDirectClassAnnotations(MapperConfig<?> cfg,
				JavaType type, MixInResolver r) {
			// TODO Auto-generated method stub
			return super.forDirectClassAnnotations(cfg, type, r);
		}
		
	}

	
	public MapMixinModule() {
		super("ModuleName", new Version(0, 0, 1, null));
	}

	@Override
	public void setupModule(SetupContext context) {
		context.setMixInAnnotations(Map.class, MapMixIn.class);
		context.setMixInAnnotations(List.class, MapMixIn.class);
		context.setMixInAnnotations(LinkedHashMap.class, LinkedHashMapMixin.class);
		context.setMixInAnnotations(ArrayList.class, LinkedHashMapMixin.class);
		//context.appendAnnotationIntrospector(new MyJacksonAnnotationIntrospector());
		context.setClassIntrospector(new MyClassIntrospector());
		// and so on
	}
}