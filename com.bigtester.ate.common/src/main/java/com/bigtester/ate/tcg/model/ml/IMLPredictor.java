/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model.ml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.w3c.dom.Document;
import com.bigtester.ate.tcg.controller.WebFormUserInputsCollectorHtmlTerms;
import com.bigtester.ate.tcg.model.ScreenNamePredictResult;
import com.bigtester.ate.tcg.model.ScreenNamePredictStrategy;
import com.bigtester.ate.tcg.model.domain.ComputedCssSize;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord;


// TODO: Auto-generated Javadoc
/**
 * This class PredictionIOTrainer defines ....
 *
 * @author Peidong Hu
 *
 */

public interface IMLPredictor extends ITebloudMachine{


	public double predictUitrRunDecision(
			WebElementTrainingRecord uitr, String screenName, String testSuiteName, String subSuiteName, String testCaseName) throws ExecutionException,
			InterruptedException, IOException;


	// /**
	// * To double.
	// *
	// * @param str the str
	// * @return the double
	// */
	// private static Double toDouble(String str) {
	// Double retVal;
	// Integer index = categories.indexOf(str);
	// if (index.equals(-1)) {
	// categories.add(str);
	// retVal = ((Integer) categories.indexOf(str)).doubleValue();
	// } else {
	// retVal = index.doubleValue();
	// }
	// if (null == retVal) retVal = 0.0;
	// return retVal;
	// }

	/**
	 * Quert entity.
	 *
	 * @param record
	 *            the record
	 * @return the user input training record
	 * @throws ExecutionException
	 *             the execution exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public WebElementTrainingRecord predictEntity(
			WebElementTrainingRecord record, String runningMode) throws ExecutionException,
			InterruptedException, IOException ;

	public WebElementTrainingRecord pioPredictAlgrithm(
			WebElementTrainingRecord record) throws ExecutionException,
			InterruptedException, IOException;

	public WebElementTrainingRecord fastTextPredict(WebElementTrainingRecord record, String runningMode) ;

	public Map<String, ScreenNamePredictResult> predictScreenName(List<Document> pageFrames, ScreenNamePredictStrategy screenPredictStrategy)
			throws ExecutionException, InterruptedException, IOException ;




}
