/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2017, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model.ml;

import java.util.Iterator;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord;
import com.bigtester.ate.tcg.utils.GlobalUtils;
import com.bigtester.tebloud.tcg.config.RunningModeProperties;
import com.bigtester.tebloud.tcg.config.RunningModeProperties.RunningMode;
import com.google.common.base.Preconditions;

// TODO: Auto-generated Javadoc
/**
 * This class TebloudMachine defines ....
 * @author Peidong Hu
 *
 */
public abstract class TebloudMachine {
	public static String MONITORED_NODE_HTML_CODE = "action=edit";
	public static String MONITORED_LABEL = "DRAFTPOSTLINK";
	public static int MONITORED_NODE_LENGTH = 600;

	/** The aut name. this should be unique as each aut's identifier*/
	private String autName;
	private IAteFastText ateFastText;
	//private RunningModeProperties runningModeProperties;
	/**
	 * @return the autName
	 */
	public String getAutName() {
		return autName;
	}

	/**
	 * @param autName the autName to set
	 */
	public void setAutName(String autName) {
		this.autName = autName;
	}

	public TebloudMachine(String autName, IAteFastText ateFastText) { //, RunningModeProperties runningModeProperties) {
		Preconditions.checkArgument(!StringUtils.isEmpty(autName));
		Preconditions.checkNotNull(ateFastText);
		this.autName = autName;
		this.setAteFastText(ateFastText);
		//this.runningModeProperties = runningModeProperties;
	}

	/**
	 * @return the ateFastText
	 */
	public IAteFastText getAteFastText() {
		return ateFastText;
	}

	/**
	 * @param ateFastText the ateFastText to set
	 */
	public void setAteFastText(IAteFastText ateFastText) {
		this.ateFastText = ateFastText;
	}

	public String parseMLText(WebElementTrainingRecord record, String runningMode) {
		if (runningMode.equals(RunningMode.USECASE_APP.toString())) {
			return GlobalUtils.removeTagAndAttributeNamesAndDigits(GlobalUtils.replaceGuidId(record.getInputMLHtmlCode()));
		} else {
			Set<String> textNodeWords = record.parseTextNodeWords();

			String retVal = GlobalUtils.removeAteAttributesValues(GlobalUtils.replaceGuidId(record.getInputMLHtmlCode())).replaceAll("[^A-Za-z0-9]", " ");

			Iterator<String> itr = textNodeWords.iterator();
			while (itr.hasNext()) {
				String textWord = itr.next();
				retVal.replaceAll(textWord, "(" + textWord + ")");
			}
			return retVal;

		}
	}

}
