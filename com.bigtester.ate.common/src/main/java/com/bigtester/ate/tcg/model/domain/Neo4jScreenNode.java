/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.jdt.annotation.Nullable;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import com.bigtester.ate.tcg.model.ATENeo4jNodeComparision;
import com.bigtester.ate.tcg.model.IntermediateResult;
import com.bigtester.ate.tcg.model.ScreenNamePredictStrategy;
import com.bigtester.ate.tcg.model.relationship.Relations;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

// TODO: Auto-generated Javadoc
/**
 * This class Neo4jScreenNode defines ....
 * 
 * @author Peidong Hu
 *
 */
@NodeEntity
@JsonIdentityInfo(generator=JSOGGenerator.class )

public class Neo4jScreenNode extends AbstractScreenNode implements
		ATENeo4jNodeComparision,
		INameLabelPredictableObjectNonPointerAssignment, INonPointerAssignment {

	public enum ScreenType {
		HTML, WINDOWFILEPICKER
	}
	/** The sourcing doms. */
	@Relationship(type = Relations.SOURCING_DOMS)
	private List<HTMLSource> sourcingDoms = new ArrayList<HTMLSource>();

	/** The inscreen jumper click uitrs. */
	@Relationship(type = Relations.PREDICTED_USER_IN_SCREENJUMP_ELEMENT_RESULTS)
	private List<InScreenJumperTrainingRecord> clickUitrs = new ArrayList<InScreenJumperTrainingRecord>();

	/** The screen jumper action uitrs. */
	@Relationship(type = Relations.PREDICTED_USER_SCREENJUMP_ELEMENT_RESULTS)
	 
	private List<ScreenJumperElementTrainingRecord> actionUitrs = new ArrayList<ScreenJumperElementTrainingRecord>();

	/** The screen element change uitrs. */
	@Relationship(type = Relations.PREDICTED_USER_SCREEN_ELEMENT_CHANGE_RESULTS)
	private List<ScreenElementChangeUITR> screenElementChangeUitrs = new ArrayList<ScreenElementChangeUITR>();
	
	private String screenTrainedEntityId="";
	private Set<String> uitrTrainedEntityIds = new HashSet<String>();
	
	private Set<String> decisionUitrTrainedEntityIds = new HashSet<String>();
	
	private ScreenType screenType = ScreenType.HTML;

	private ScreenNamePredictStrategy screenNamePredictStrategy = ScreenNamePredictStrategy.PREDICTED_BY_VISIBLE_ELEMENT_TEXT;
	
	/** The top document width. */
	private int topDocumentWidth = 0;
	
	/** The top document height. */
	private int topDocumentHeight = 0;
	
	private int screenWidth = 0;
	
	private boolean expectedScreen = true;

	// /** The testcases. */
	// @Relationship(type = Relations.IN_TESTCASE)
	// private Collection<TestCase> testcases = new HashSet<TestCase>();

	/**
	 * Instantiates a new neo4j screen node. no-arg constructor for restful
	 * call.
	 */
	public Neo4jScreenNode() {
		super("Neo4jScreenNode");
	}

	public void removeDuplidatedUitrs(IntermediateResult iResult) {//, CRUDService) {
		//add code to remove the additional labels from neo4j db nodes
		iResult.getScreenNode().getActionUitrs().forEach(uitr->{
			this.getClickUitrs().removeAll((this.getClickUitrs().stream().filter(clickUitr->clickUitr.getGraphId()!=null && clickUitr.getGraphId().equals(uitr.getGraphId()))).collect(Collectors.toList()));
			this.getUserInputUitrs().removeAll(this.getUserInputUitrs().stream().filter(clickUitr->clickUitr.getGraphId()!=null && clickUitr.getGraphId().equals(uitr.getGraphId())).collect(Collectors.toList()));
			this.getScreenElementChangeUitrs().removeAll(this.getScreenElementChangeUitrs().stream().filter(clickUitr->clickUitr.getGraphId()!=null && clickUitr.getGraphId().equals(uitr.getGraphId())).collect(Collectors.toList()));
		});
		iResult.getScreenNode().getUserInputUitrs().forEach(uitr->{
			this.getClickUitrs().removeAll((this.getClickUitrs().stream().filter(clickUitr->clickUitr.getGraphId()!=null && clickUitr.getGraphId().equals(uitr.getGraphId()))).collect(Collectors.toList()));
			this.getActionUitrs().removeAll(this.getUserInputUitrs().stream().filter(clickUitr->clickUitr.getGraphId()!=null && clickUitr.getGraphId().equals(uitr.getGraphId())).collect(Collectors.toList()));
			this.getScreenElementChangeUitrs().removeAll(this.getScreenElementChangeUitrs().stream().filter(clickUitr->clickUitr.getGraphId()!=null && clickUitr.getGraphId().equals(uitr.getGraphId())).collect(Collectors.toList()));
		});
		iResult.getScreenNode().getScreenElementChangeUitrs().forEach(uitr->{
			this.getClickUitrs().removeAll((this.getClickUitrs().stream().filter(clickUitr->clickUitr.getGraphId()!=null && clickUitr.getGraphId().equals(uitr.getGraphId()))).collect(Collectors.toList()));
			this.getUserInputUitrs().removeAll(this.getUserInputUitrs().stream().filter(clickUitr->clickUitr.getGraphId()!=null && clickUitr.getGraphId().equals(uitr.getGraphId())).collect(Collectors.toList()));
			this.getActionUitrs().removeAll(this.getScreenElementChangeUitrs().stream().filter(clickUitr->clickUitr.getGraphId()!=null && clickUitr.getGraphId().equals(uitr.getGraphId())).collect(Collectors.toList()));
		});
		iResult.getScreenNode().getClickUitrs().forEach(uitr->{
			this.getActionUitrs().removeAll((this.getClickUitrs().stream().filter(clickUitr->clickUitr.getGraphId()!=null && clickUitr.getGraphId().equals(uitr.getGraphId()))).collect(Collectors.toList()));
			this.getUserInputUitrs().removeAll(this.getUserInputUitrs().stream().filter(clickUitr->clickUitr.getGraphId()!=null && clickUitr.getGraphId().equals(uitr.getGraphId())).collect(Collectors.toList()));
			this.getScreenElementChangeUitrs().removeAll(this.getScreenElementChangeUitrs().stream().filter(clickUitr->clickUitr.getGraphId()!=null && clickUitr.getGraphId().equals(uitr.getGraphId())).collect(Collectors.toList()));
		});
	}
	
	public void populateCorrectValueforFieldBelongsToTestCaseFromDBInfo(String testCaseName) {
		this.getUserInputUitrs().forEach(uitr -> {
			if (uitr.getTestcases().isEmpty() || uitr.getTestcases().stream().filter(tc->tc.getEndNode().getName().equalsIgnoreCase(testCaseName)).count()==0) {
				uitr.setBelongToCurrentTestCase(false);
			} else {
				uitr.setBelongToCurrentTestCase(true);
			}
		});
		this.getActionUitrs().forEach(uitr -> {
			if (uitr.getTestcases().isEmpty()|| uitr.getTestcases().stream().filter(tc->tc.getEndNode().getName().equalsIgnoreCase(testCaseName)).count()==0) {
				uitr.setBelongToCurrentTestCase(false);
			} else {
				uitr.setBelongToCurrentTestCase(true);
			}
		});
		this.getClickUitrs().forEach(uitr -> {
			if (uitr.getTestcases().isEmpty()|| uitr.getTestcases().stream().filter(tc->tc.getEndNode().getName().equalsIgnoreCase(testCaseName)).count()==0) {
				uitr.setBelongToCurrentTestCase(false);
			} else {
				uitr.setBelongToCurrentTestCase(true);
			}
		});
		this.getScreenElementChangeUitrs().forEach(uitr -> {
			if (uitr.getTestcases().isEmpty()|| uitr.getTestcases().stream().filter(tc->tc.getEndNode().getName().equalsIgnoreCase(testCaseName)).count()==0) {
				uitr.setBelongToCurrentTestCase(false);
			} else {
				uitr.setBelongToCurrentTestCase(true);
			}
		});
	}

	public void populateCorrectValueforFieldBelongsToTestCaseFromIntermediateResult(
			IntermediateResult inter) {
		this.getUserInputUitrs().forEach(
				uitr -> {
					Set<WebElementTrainingRecord> interUi = inter.getScreenNode()
							.getUserInputUitrs()
							.stream()
							.filter(interUitr -> interUitr
									.getGraphId()!=null && interUitr
									.getGraphId().equals( 
											uitr.getGraphId()))
							.collect(Collectors.toSet());
					if (interUi.iterator().hasNext())
						uitr.setBelongToCurrentTestCase(interUi.iterator().next().isBelongToCurrentTestCase());
					else {
						interUi = inter.getScreenNode()
							.getUserInputUitrs()
							.stream()
							.filter(interUitr -> interUitr
									.getComparableHtmlCode().equalsIgnoreCase(
											uitr.getComparableHtmlCode()))
							.collect(Collectors.toSet());
						if (interUi.iterator().hasNext())
							uitr.setBelongToCurrentTestCase(interUi.iterator().next().isBelongToCurrentTestCase());
					}
					
				});
		this.getActionUitrs().forEach(uitr -> {

			List<WebElementTrainingRecord> interUi = inter.getScreenNode()
					.getActionUitrs()
					.stream()
					.filter(interUitr -> interUitr
							.getGraphId()!=null && interUitr
							.getGraphId().equals( 
									uitr.getGraphId()))
					.collect(Collectors.toList());
			if (interUi.iterator().hasNext())
				uitr.setBelongToCurrentTestCase(interUi.iterator().next().isBelongToCurrentTestCase());
			else {
				interUi = inter.getScreenNode()
					.getActionUitrs()
					.stream()
					.filter(interUitr -> interUitr
							.getComparableHtmlCode().equalsIgnoreCase(
									uitr.getComparableHtmlCode()))
					.collect(Collectors.toList());
				if (interUi.iterator().hasNext())
					uitr.setBelongToCurrentTestCase(interUi.iterator().next().isBelongToCurrentTestCase());
			}
			
		});
		this.getClickUitrs().forEach(uitr -> {
			
			Set<WebElementTrainingRecord> interUi = inter.getScreenNode().getClickUitrs()
					
					.stream()
					.filter(interUitr -> interUitr
							.getGraphId()!=null && interUitr
							.getGraphId().equals(
							uitr.getGraphId()))
					.collect(Collectors.toSet());
			if (interUi.iterator().hasNext())
				uitr.setBelongToCurrentTestCase(interUi.iterator().next().isBelongToCurrentTestCase());
			else {
				interUi = inter.getScreenNode()
					.getClickUitrs()
					.stream()
					.filter(interUitr -> interUitr
							.getComparableHtmlCode().equalsIgnoreCase(
									uitr.getComparableHtmlCode()))
					.collect(Collectors.toSet());
			if (interUi.iterator().hasNext())
			uitr.setBelongToCurrentTestCase(interUi.iterator().next().isBelongToCurrentTestCase());
			}
			
		});
		this.getScreenElementChangeUitrs().forEach(uitr -> {
			
			Set<WebElementTrainingRecord> interUi = inter.getScreenNode()
					.getScreenElementChangeUitrs()
					.stream()
					.filter(interUitr -> interUitr
							.getGraphId()!=null && interUitr
							.getGraphId().equals( 
							uitr.getGraphId()))
					.collect(Collectors.toSet());
			if (interUi.iterator().hasNext())
				uitr.setBelongToCurrentTestCase(interUi.iterator().next().isBelongToCurrentTestCase());
			else {
				interUi = inter.getScreenNode()
					.getScreenElementChangeUitrs()
					.stream()
					.filter(interUitr -> interUitr
							.getComparableHtmlCode().equalsIgnoreCase(
									uitr.getComparableHtmlCode()))
					.collect(Collectors.toSet());
				if (interUi.iterator().hasNext())
					uitr.setBelongToCurrentTestCase(interUi.iterator().next().isBelongToCurrentTestCase());
			}
		});
	}

	/**
	 * Instantiates a new neo4j screen node.
	 *
	 * @param nodeLabelName
	 *            the node label name
	 */
	public Neo4jScreenNode(String nodeLabelName) {
		super(nodeLabelName);
	}

	/**
	 * Instantiates a new neo4j screen node.
	 *
	 * @param name
	 *            the name
	 * @param url
	 *            the url
	 * @param iResult
	 *            the i result
	 */
	public Neo4jScreenNode(String name, String url, IntermediateResult iResult) {
		// this.name = name;
		// this.url = url;
		super(name, url, "Neo4jScreenNode");
		this.sourcingDoms = iResult.getScreenNode().getSourcingDoms();
		setUserInputUitrs(iResult.getScreenNode().getUserInputUitrs());
		this.clickUitrs = iResult.getScreenNode().getClickUitrs();
		this.actionUitrs = iResult.getScreenNode().getActionUitrs();
		this.screenElementChangeUitrs = iResult.getScreenNode().getScreenElementChangeUitrs();
		this.setInputMLHtmlWords(iResult.getScreenNode().getInputMLHtmlWords());
		this.setScreenTrainedEntityId(iResult.getScreenNode().getScreenTrainedEntityId());
		this.setUitrTrainedEntityIds(iResult.getScreenNode().getUitrTrainedEntityIds());
		this.setDecisionUitrTrainedEntityIds(iResult.getScreenNode().getDecisionUitrTrainedEntityIds());
	}

	/**
	 * @return the sourcingDoms
	 */
	public List<HTMLSource> getSourcingDoms() {
		return sourcingDoms;
	}

	/**
	 * @param sourcingDoms
	 *            the sourcingDoms to set
	 */
	public void setSourcingDoms(List<HTMLSource> sourcingDoms) {
		this.nonPointerAssign(sourcingDoms, this.sourcingDoms);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean sameNode(@Nullable Object obj) {
		boolean retVal = false;// NOPMD
		if (obj instanceof Neo4jScreenNode) {
			retVal = ((Neo4jScreenNode) obj).getName() == this.getName()
					&& ((Neo4jScreenNode) obj).getUrl() == this.getUrl();
		}
		return retVal;
	}

	/**
	 * @return the actionUitrs
	 */
	 
	public List<ScreenJumperElementTrainingRecord> getActionUitrs() {
		return actionUitrs;
	}

	/**
	 * @param actionUitrs
	 *            the actionUitrs to set, replace false by default
	 */
	 
	public void setActionUitrs(
			List<ScreenJumperElementTrainingRecord> actionUitrs) {

		super.nonPointerAssign(actionUitrs, this.actionUitrs);
	}

	/**
	 * Sets the action uitrs.
	 *
	 * @param actionUitrs
	 *            the action uitrs
	 * @param replace
	 *            the replace
	 */
	// TODO refactor the other operations in this class
	public void setActionUitrs(
			Set<ScreenJumperElementTrainingRecord> actionUitrs, boolean replace) {
		super.nonPointerAssign(actionUitrs, this.actionUitrs, replace);
	}

	/**
	 * @return the screenElementChangeUitrs
	 */
	public List<ScreenElementChangeUITR> getScreenElementChangeUitrs() {
		return screenElementChangeUitrs;
	}

	/**
	 * @param screenElementChangeUitrs
	 *            the screenElementChangeUitrs to set
	 */
	public void setScreenElementChangeUitrs(
			List<ScreenElementChangeUITR> screenElementChangeUitrs) {
		this.nonPointerAssign(screenElementChangeUitrs,
				this.screenElementChangeUitrs);
	}

	/**
	 * @return the clickUitrs
	 */
	public List<InScreenJumperTrainingRecord> getClickUitrs() {
		return clickUitrs;
	}

	/**
	 * @param clickUitrs
	 *            the clickUitrs to set
	 */
	public void setClickUitrs(List<InScreenJumperTrainingRecord> clickUitrs) {
		this.nonPointerAssign(clickUitrs, this.clickUitrs);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void assignedFrom(INonPointerAssignment value) {
		if (value instanceof Neo4jScreenNode) {
			Neo4jScreenNode record = (Neo4jScreenNode) value;
			this.setActionUitrs(record.getActionUitrs());
			this.setScreenElementChangeUitrs(record
					.getScreenElementChangeUitrs());
			this.setClickUitrs(record.getClickUitrs());
			this.setName(record.getName());
			InScreenJumperTrainingRecord temp = record
					.getPreviousScreenTriggerClickUitr();
			if (temp != null) {
				this.setPreviousScreenTriggerClickUitr(temp);
			}
			this.setSourcingDoms(record.getSourcingDoms());
			this.setTestcases(record.getTestcases());
			this.setUrl(record.getUrl());
			this.setUserInputUitrs(record.getUserInputUitrs());

		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getComparableLabelName() {
		return this.getName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getComparableHtmlCode() {
		return "<url>" + this.getUrl() + "</url>";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getIdentifierString() { 
		return this.getName() + this.getUrl();
	}

	/**
	 * @return the screenTrainedEntityId
	 */
	public String getScreenTrainedEntityId() {
		return screenTrainedEntityId;
	}

	/**
	 * @param screenTrainedEntityId the screenTrainedEntityId to set
	 */
	public void setScreenTrainedEntityId(String screenTrainedEntityId) {
		this.screenTrainedEntityId = screenTrainedEntityId;
	}

	/**
	 * @return the uitrTrainedEntityIds
	 */
	public Set<String> getUitrTrainedEntityIds() {
		return uitrTrainedEntityIds;
	}

	/**
	 * @param uitrTrainedEntityIds the uitrTrainedEntityIds to set
	 */
	public void setUitrTrainedEntityIds(Set<String> uitrTrainedEntityIds) {
		this.uitrTrainedEntityIds = uitrTrainedEntityIds;
	}

	/**
	 * @return the decisionUitrTrainedEntityIds
	 */
	public Set<String> getDecisionUitrTrainedEntityIds() {
		return decisionUitrTrainedEntityIds;
	}

	/**
	 * @param decisionUitrTrainedEntityIds the decisionUitrTrainedEntityIds to set
	 */
	public void setDecisionUitrTrainedEntityIds(
			Set<String> decisionUitrTrainedEntityIds) {
		this.decisionUitrTrainedEntityIds = decisionUitrTrainedEntityIds;
	}

	/**
	 * @return the screenType
	 */
	public ScreenType getScreenType() {
		return screenType;
	}

	/**
	 * @param screenType the screenType to set
	 */
	public void setScreenType(ScreenType screenType) {
		this.screenType = screenType;
	}

	/**
	 * @return the screenNamePredictStrategy
	 */
	public ScreenNamePredictStrategy getScreenNamePredictStrategy() {
		return screenNamePredictStrategy;
	}

	/**
	 * @param screenNamePredictStrategy the screenNamePredictStrategy to set
	 */
	public void setScreenNamePredictStrategy(ScreenNamePredictStrategy screenNamePredictStrategy) {
		this.screenNamePredictStrategy = screenNamePredictStrategy;
	}

	/**
	 * @return the topDocumentWidth
	 */
	public int getTopDocumentWidth() {
		return topDocumentWidth;
	}

	/**
	 * @param topDocumentWidth the topDocumentWidth to set
	 */
	public void setTopDocumentWidth(int topDocumentWidth) {
		this.topDocumentWidth = topDocumentWidth;
	}

	/**
	 * @return the topDocumentHeight
	 */
	public int getTopDocumentHeight() {
		return topDocumentHeight;
	}

	/**
	 * @param topDocumentHeight the topDocumentHeight to set
	 */
	public void setTopDocumentHeight(int topDocumentHeight) {
		this.topDocumentHeight = topDocumentHeight;
	}

	/**
	 * @return the screenWidth
	 */
	public int getScreenWidth() {
		return screenWidth;
	}

	/**
	 * @param screenWidth the screenWidth to set
	 */
	public void setScreenWidth(int screenWidth) {
		this.screenWidth = screenWidth;
	}

	/**
	 * @return the expectedScreen
	 */
	public boolean isExpectedScreen() {
		return expectedScreen;
	}

	/**
	 * @param expectedScreen the expectedScreen to set
	 */
	public void setExpectedScreen(boolean expectedScreen) {
		this.expectedScreen = expectedScreen;
	}

	 

}
