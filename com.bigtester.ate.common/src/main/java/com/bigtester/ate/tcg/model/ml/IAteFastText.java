/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2017, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model.ml;

import org.bytedeco.javacpp.PointerPointer;

import com.github.jfasttext.FastTextWrapper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public interface IAteFastText {


    public void runCmd(String[] args) ;

    public void loadModel(String modelFile) ;

    public void unloadModel() ;
/*
    public void test(String testFile) ;

    public void test(String testFile, int k);

    public String predict(String text);

    public List<String> predict(String text, int k);
   */
    public ProbLabel predictProba(String text);


   /* public List<ProbLabel> predictProba(String text, int k);

    public List<Float> getVector(String word);

    public int getNWords() ;

    public List<String> getWords();

    public int getNLabels() ;

    public List<String> getLabels();

    public double getLr() ;

    public int getLrUpdateRate();
    public int getDim() ;

    public int getContextWindowSize();
    public int getEpoch() ;

    public int getMinCount();

    public int getMinCountLabel() ;

    public int getNSampledNegatives();

    public int getWordNgrams() ;

    public String getLossName() ;

    public String getModelName() ;

    public int getBucket();

    public int getMinn() ;

    public int getMaxn() ;

    public double getSamplingThreshold();

    public String getLabelPrefix();

    public String getPretrainedVectorsFileName();

*/
    public class ProbLabel implements Serializable{
        /**
		 *
		 */
		private static final long serialVersionUID = -8363461689466120650L;
		public float logProb;
        public String label;
        public ProbLabel(float logProb, String label) {
            this.logProb = logProb;
            this.label = label;
        }
        @Override
        public String toString() {
            return String.format("logProb = %f, label = %s", logProb, label);
        }
    }

}
