/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model.ml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bigtester.ate.tcg.controller.WebFormUserInputsCollectorHtmlTerms;
import com.bigtester.ate.tcg.model.ScreenNamePredictResult;
import com.bigtester.ate.tcg.model.ScreenNamePredictStrategy;
import com.bigtester.ate.tcg.model.domain.ComputedCssSize;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord;
import com.bigtester.ate.tcg.model.ml.FastTextTrainer;
import com.bigtester.ate.tcg.utils.GlobalUtils;
import com.bigtester.tebloud.tcg.config.RunningModeProperties.RunningMode;
import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import io.prediction.ATEEventClient;
import io.prediction.EngineClient;
import io.prediction.EventClient;

import org.w3c.dom.Attr;

// TODO: Auto-generated Javadoc
/**
 * This class PredictionIOTrainer defines ....
 *
 * @author Peidong Hu
 *
 */
//@Component
@PropertySource("classpath:application.properties")

public class MLPredictor extends TebloudMachine implements IMLPredictor{

	protected static Log _log = LogFactoryUtil.getLog(MLPredictor.class);

	//for home desktop development

	public static String PIO_SERVER_DOMAIN;// = "pio-ate-v-ate-tcg-prediction-data";
	//for home desktop development
	//public static final String EVENT_SERVER_PORT = "7070";
	//for home desktop development
	public static final String LABEL_ENGINE_SERVER_PORT = "8000";
	//for home desktop dev
	public static final String DECISION_ENGINE_SERVER_PORT = "8010";

	public static final String SCREENNAME_ENGINE_SERVER_PORT = "8110";

	private EngineClient pioDecisionTreeEngineClient;
	private EngineClient pioScreenNameEngineClient;
	//private ATEEventClient pioUitrStoreEventClient;


	/** The Constant SAMPLETEXTCLASSIFIERACCESSKEY. */
	public static final String SAMPLETEXTCLASSIFIERACCESSKEY = "JPEaFI2xyLqpz0TGmMc156wqDl2tJ21PK6yHtMvCyev4aI5ATVnUoKTqmg3kCUBs";

	/** The Constant DECISION_TREE_CLASSIFIER_ACCESSKEY. */
	//public static final String DECISION_TREE_CLASSIFIER_ACCESSKEY = "o82oGn6EqKSfo6K5gjzIf6RTq54uLwwwGdsDRsUrOseeeJJ27HZoCzCHXWV2m7Yb";
	//for home desktop development, TestApp
	public static final String DECISION_TREE_CLASSIFIER_ACCESSKEY = "KfF59ZIqk8g32U2hxAPWqvuyvxlfn9d2puBaScKvY9KcNXsFWwuoHBfQiCzlpKrW";

	public static final String SCREENNAME_CLASSIFIER_ACCESSKEY = "P7qijFYit5SXg5yWRNTQzN3U1ACYTghuqExYZNsK8B7P0ha7S1KigDDP9PoYHKxu";

	/** The Constant EVENTSERVERURL, use the docker0 virtual ip */
	// public static final String EVENTSERVERURL = "http://172.16.173.50:7070";
	//public static  String EVENTSERVERURL;// = "http://"+EVENT_SERVER_DOMAIN+":" + EVENT_SERVER_PORT;

	/** The Constant ENGINESERVERURL. */
	// public static final String ENGINESERVERURL = "http://172.16.173.50:8000";
	public static  String LABELENGINESERVERURL;// = "http://"+EVENT_SERVER_DOMAIN+":" + LABEL_ENGINE_SERVER_PORT;

	public static  String DECISION_ENGINE_SERVERURL;// = "http://"+EVENT_SERVER_DOMAIN+":" + DECISION_ENGINE_SERVER_PORT;

	public static  String SCREENNAME_ENGINE_SERVERURL;// = "http://"+EVENT_SERVER_DOMAIN+":" + DECISION_ENGINE_SERVER_PORT;

	public static boolean FAST_TEXT_TRAINER_ENABLED;


	/** The categories. */
	public static List<String> categories = new ArrayList<String>();

	/**
	 * Instantiates a new prediction io trainer.
	 * https://stackoverflow.com/questions/35108778/spring-bean-with-runtime-constructor-arguments
	 */
	public  MLPredictor(String autName, IAteFastText fastText) {
		super(autName, fastText);

	};


	@PostConstruct
    public void init(){
		//EVENTSERVERURL = "http://"+EVENT_SERVER_DOMAIN+":" + EVENT_SERVER_PORT;

		/** The Constant ENGINESERVERURL. */
		// public static final String ENGINESERVERURL = "http://172.16.173.50:8000";
		LABELENGINESERVERURL = "http://"+PIO_SERVER_DOMAIN+":" + LABEL_ENGINE_SERVER_PORT;

		DECISION_ENGINE_SERVERURL = "http://"+PIO_SERVER_DOMAIN+":" + DECISION_ENGINE_SERVER_PORT;

		SCREENNAME_ENGINE_SERVERURL = "http://"+PIO_SERVER_DOMAIN+":" + SCREENNAME_ENGINE_SERVER_PORT;
		this.pioDecisionTreeEngineClient = new EngineClient(DECISION_ENGINE_SERVERURL, 8, 100, 1000);
		this.pioScreenNameEngineClient = new EngineClient(SCREENNAME_ENGINE_SERVERURL);

    }
	@PreDestroy
	public void tearDown() throws IOException {
		//_log.info("pio client connections closed");
		pioScreenNameEngineClient.close();
		pioDecisionTreeEngineClient.close();
		//this.pioUitrStoreEventClient.close();
	}
	@Value("${event_server_domain}")
	public void setEvent_server_domain(String event_server_domain) {
		PIO_SERVER_DOMAIN = event_server_domain;
	}

	@Value("${fast_text_trainer_enabled}")
	public void setFast_text_trainer_enabled(boolean fast_text_trainer_enabled) {
		FAST_TEXT_TRAINER_ENABLED = fast_text_trainer_enabled;
	}

	public double predictUitrRunDecision(
			WebElementTrainingRecord uitr, String screenName, String testSuiteName, String subSuiteName, String testCaseName) throws ExecutionException,
			InterruptedException, IOException {


		List<String> properties = new ArrayList<String>();
		ComputedCssSize uitrDimension = uitr.getComputedCssSizes().iterator().next();
		properties.add(
				String.valueOf(
				uitrDimension
						.getUitrWidth()));
		properties.add(
				String.valueOf(
				uitrDimension
						.getUitrHeight()));
		double xRatio = (double) (Math.round(1000*
				((uitrDimension
				.getLeftTopCornerX()-uitrDimension
				.getTopDocumentWidth()/2)
				/ uitrDimension
						.getTopDocumentWidth())));
		if(xRatio<-1000) xRatio = -1000;
		if(xRatio>1000) xRatio = 1000;
		properties.add(
				String.valueOf(xRatio));

		double yRatio = (double) (Math.round (1000 *
				(uitrDimension
				.getLeftTopCornerY()-uitrDimension
				.getTopDocumentHeight()/2)
				/ uitrDimension
						.getTopDocumentHeight()));
		if (yRatio<-1000) xRatio = -1000;
		if (yRatio>1000) xRatio = 1000;
		properties.add(
				String.valueOf(yRatio));
		properties.add(
				String.valueOf(
						uitr.calculateAteCollectableTagsCount(Arrays
								.asList(WebFormUserInputsCollectorHtmlTerms.USER_COLLECTABLE_TAGS_EXCEPT_ALINK))));


		properties.add(
				String.valueOf(uitr.getComputedCssSizes().iterator().next().getScreenWidth()));
//		properties.put("testCaseName", uitr
//				.getTestcases().iterator().next().getEndNode()
//				.getName());
		properties.add(
				screenName);
		properties.add(
				uitr.getInputLabelName());
		properties.add(testSuiteName);
		properties.add(subSuiteName);
		properties.add(testCaseName);

		Map<String, Object> features = ImmutableMap.<String, Object> of(
				"features", properties);

		JsonObject jObj = this.pioDecisionTreeEngineClient.sendQuery(features);
		//client.close();
		Double cat = jObj.get("value").getAsDouble();
		//System.out.println(cat + "->" + properties.stream().collect(Collectors.joining(",")));
		if (null == cat)
			cat = 0d;

		return cat;
	}


	private static boolean getNodeProcessedOrNot(Node node) {
		NamedNodeMap attrs = node.getAttributes();
		boolean processed = false;
		for (int index2 = 0; index2<attrs.getLength(); index2++) {
			Attr attr = (Attr) attrs.item(index2);

			if (attr.getName().equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.PREVIOUS_PROCESSED_ATTR_NAME)
					&& attr.getValue().equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE)) {
				processed = true;
				break;
			}
		}
		return processed;
	}
	private static String getUnprocessedTextContent(Node element) {

		if (!getNodeProcessedOrNot(element)){
			return element.getTextContent();
		} else {
			NodeList childrenNodes = element.getChildNodes();
			String childText = " ";
			for (int index=0; index<childrenNodes.getLength(); index++) {
				NamedNodeMap attrs;
				if (childrenNodes.item(index) instanceof Element) {
					attrs = childrenNodes.item(index).getAttributes();
				} else {
					attrs = element.getAttributes();
				}
				boolean processed = false;
				for (int index2 = 0; index2<attrs.getLength(); index2++) {
					Attr attr = (Attr) attrs.item(index2);

					if (attr.getName().equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.PREVIOUS_PROCESSED_ATTR_NAME)
							&& attr.getValue().equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE)) {
						processed = true;
						break;
					}
				}
				if (!processed)
					childText = childText + " " + childrenNodes.item(index).getTextContent();
				else if (childrenNodes.item(index) instanceof Element){
					childText = childText + " " + getUnprocessedTextContent(childrenNodes.item(index));
				}

			}
			return childText;
		}

	}

private static String getTextContent(Node element, boolean removeProcessedElementText) {

		if ((!removeProcessedElementText || !getNodeProcessedOrNot(element))
//				&& !(element.getNodeName().equalsIgnoreCase("script")
//				|| element.getNodeName().equalsIgnoreCase("style"))
				){
			return element.getTextContent();
		} else {
			NodeList childrenNodes = element.getChildNodes();
			String childText = " ";
			for (int index=0; index<childrenNodes.getLength(); index++) {
				NamedNodeMap attrs;
				if (childrenNodes.item(index) instanceof Element) {
					attrs = childrenNodes.item(index).getAttributes();
				} else {
					attrs = element.getAttributes();
				}
				boolean processed = false;
				for (int index2 = 0; index2<attrs.getLength(); index2++) {
					Attr attr = (Attr) attrs.item(index2);

					if (attr.getName().equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.PREVIOUS_PROCESSED_ATTR_NAME)
							&& attr.getValue().equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE)) {
						processed = true;
						break;
					}
				}
				if (!processed)
					childText = childText + " " + childrenNodes.item(index).getTextContent();
				else if (childrenNodes.item(index) instanceof Element){
					childText = childText + " " + getUnprocessedTextContent(childrenNodes.item(index));
				}

			}
			return childText;
		}

	}

private static String removeInvisibleElementsAndGetText(Document html, ScreenNamePredictStrategy predictingStrategy) {
	Document doc = html;
		doc = GlobalUtils.removeScriptStylesAndInvisiables(doc);
		doc = GlobalUtils.addSpacesIntoTextNodes(doc);
		String ret="";
		if (predictingStrategy==ScreenNamePredictStrategy.PREDICTED_BY_VISIBLE_AND_UNPROCESSED_ELEMENT_TEXT)
			ret = getTextContent(GlobalUtils.getBodyEquevelantElement(doc), true);//.getTextContent();
		else
			ret = getTextContent(GlobalUtils.getBodyEquevelantElement(doc), false);//.getTextContent();
		ret = ret.trim().replaceAll("[\\t\\n\\r]", " ")
				.replaceAll(" +", " ");

		if (ret == null)
			ret = "";
		return ret;

}



	// /**
	// * To double.
	// *
	// * @param str the str
	// * @return the double
	// */
	// private static Double toDouble(String str) {
	// Double retVal;
	// Integer index = categories.indexOf(str);
	// if (index.equals(-1)) {
	// categories.add(str);
	// retVal = ((Integer) categories.indexOf(str)).doubleValue();
	// } else {
	// retVal = index.doubleValue();
	// }
	// if (null == retVal) retVal = 0.0;
	// return retVal;
	// }

	/**
	 * Quert entity.
	 *
	 * @param record
	 *            the record
	 * @return the user input training record
	 * @throws ExecutionException
	 *             the execution exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public WebElementTrainingRecord predictEntity(
			WebElementTrainingRecord record, String runningMode) throws ExecutionException,
			InterruptedException, IOException {
		if (FAST_TEXT_TRAINER_ENABLED)
			return fastTextPredict(record, runningMode);
		else
			return pioPredictAlgrithm(record);

	}
	public WebElementTrainingRecord pioPredictAlgrithm(
			WebElementTrainingRecord record) throws ExecutionException,
			InterruptedException, IOException {


		EngineClient client = new EngineClient(LABELENGINESERVERURL);

		JsonObject jObj = client.sendQuery(ImmutableMap.<String, Object> of(
				"sentence", GlobalUtils.removeAllDigits(GlobalUtils.removeGuidId(record.getInputMLHtmlWords())).replaceAll("[<>\\_'\",!;\\.\\-:\\)\\(]", " ").replaceAll("\\s+", " ")));

		client.close();
		String cat = jObj.get("interest").getAsString();
		if (null == cat)
			cat = "";
		record.getPioPredictLabelResult().setValue(cat);
		record.setInputLabelName(cat);
		Double con = jObj.get("confidence").getAsDouble();
		if (null == con)
			con = 0.0;
		record.setPioPredictConfidence(con);

		return record;

	}

	public WebElementTrainingRecord fastTextPredict(WebElementTrainingRecord record, String runningMode) {



		  // Do label prediction
		String text = this.parseMLText(record, runningMode);
		  try {
			  IAteFastText.ProbLabel probLabel;
			  if (getAteFastText() instanceof IRemoteTebloudFastText)
				  probLabel = ((IRemoteTebloudFastText)getAteFastText()).predictProba(this.getAutName(), text);
			  else {
				   probLabel = (getAteFastText()).predictProba(text);
			  }
			  if ((text.contains(MONITORED_NODE_HTML_CODE) && probLabel.label.equalsIgnoreCase(MONITORED_LABEL)
					  && text.length()<MONITORED_NODE_LENGTH) || StringUtils.isEmpty(MONITORED_LABEL)) {
				  _log.info("ProbLabel1 at fasttext prediction" + probLabel.label +":" + probLabel.logProb);
				  _log.info("fasttext predicting text: " + text);
			  }

			  String cat = probLabel.label.replace(FastTextTrainer.__label__, "");
				if (null == cat)
					cat = "";
				record.getPioPredictLabelResult().setValue(cat);
				record.setInputLabelName(cat);
				Double con = Math.exp(probLabel.logProb);
				if (null == con)
					con = 0.0;
				record.setPioPredictConfidence(con);
		  } catch (IndexOutOfBoundsException exp) {
			  //null result predicted,

			  record.getPioPredictLabelResult().setValue("notInterested");
				record.setInputLabelName("notInterested");
				record.setPioPredictConfidence(1.0);
		  }
		  //_log.info("fasttext predicting result : " + record.getInputLabelName() + " text: " + text);


			return record;
//		  System.out.printf("\nThe label of '%s' is '%s' with probability %f\n",
//		          text, probLabel.label, Math.exp(probLabel.logProb));
	  }

	public Map<String, ScreenNamePredictResult> predictScreenName(List<Document> pageFrames, ScreenNamePredictStrategy screenPredictStrategy)
			throws ExecutionException, InterruptedException, IOException {
		StringBuilder tmp = new StringBuilder("");
		//TODO
		//re-write getDocText();
		for (java.util.Iterator<Document> itr = pageFrames.iterator(); itr
				.hasNext();) {
			tmp.append(" "); // NOPMD
			String visibleDocText = removeInvisibleElementsAndGetText(itr.next()
					, screenPredictStrategy);
			//tmp.append(itr.next().getDocText());
			tmp.append(visibleDocText);
		}
		JsonObject jObj = this.pioScreenNameEngineClient.sendQuery(ImmutableMap.<String, Object> of(
				"sentence", tmp.toString()));
		//client.close();
		String cat = jObj.get("interest").getAsString();
		if (null == cat)
			cat = "";

		Double con = jObj.get("confidence").getAsDouble();
		if (null == con)
			con = 0.0;
		// Double con = 0.0;
		Map<String, ScreenNamePredictResult> retVal = new ConcurrentHashMap<String, ScreenNamePredictResult>();// NOPMD



		retVal.put(cat, new ScreenNamePredictResult(tmp.toString(),con, cat));
		return retVal;
	}



}
