/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model.relationship;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.annotation.Nullable;
import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

import com.bigtester.ate.tcg.model.ATENeo4jNodeComparision;
import com.bigtester.ate.tcg.model.domain.BaseAteNode;
import com.bigtester.ate.tcg.model.domain.BaseAteTestEntityNode;
import com.bigtester.ate.tcg.model.domain.INonPointerAssignment;
import com.bigtester.ate.tcg.model.domain.Neo4jScreenNode;
import com.bigtester.ate.tcg.model.domain.TestCase;
import com.bigtester.ate.tcg.utils.MapLongSerializer;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

// TODO: Auto-generated Javadoc
/**
 * This class StepInto defines ....
 * @author Peidong Hu
 *
 */
@RelationshipEntity (type=Relations.IN_TESTCASE)
@JsonIdentityInfo(generator=JSOGGenerator.class )
public class InTestCase implements INonPointerAssignment {
	
	/** The node id. */
	@Nullable 
	@GraphId
	private Long nodeId;
	
	/** The start node. */
	@StartNode
	 @JsonBackReference 
	private BaseAteTestEntityNode startNode;
	
	/** The end node. */
	@EndNode
	 
	private TestCase endNode;
	
	public InTestCase() {
		super();
	}
	
	/** The step weight. 
	 *  stepWeight is dynamically changed based on the current execution test case. 
	 *  ML will predict weight value according several factors including, 
	 *  current execution test case, test suite
	 * */
	@Property
	@JsonSerialize(using=MapLongSerializer.class)
	private Set<Long> contextScreenGraphIds = new HashSet<Long>();

	/**
	 * Instantiates a new step into.
	 *
	 * @param startNode the start node
	 * @param endNode the end node
	 * @param uitrId the uitr id
	 */
	public InTestCase(BaseAteTestEntityNode startNode, TestCase endNode, Neo4jScreenNode contextScreen) {
		this.startNode = startNode;
		this.endNode = endNode;
		 
		this.contextScreenGraphIds.add(contextScreen.getGraphId());
		
	}
	
	/**
	 * Instantiates a new step into.
	 *
	 * @param startNode the start node
	 * @param endNode the end node
	 * @param uitrId the uitr id
	 */
	public InTestCase(BaseAteTestEntityNode startNode, TestCase endNode) {
		this.startNode = startNode;
		this.endNode = endNode;

	}
	
	
	 
	/**
	 * @return the startNode
	 */
	@Nullable
	@JsonBackReference
	public BaseAteTestEntityNode getStartNode() {
		return startNode;
	}

	/**
	 * @param startNode the startNode to set
	 */
	@JsonBackReference
	public void setStartNode(BaseAteTestEntityNode startNode) {
		this.startNode = startNode;
	}

	/**
	 * @return the endNode
	 */
	@Nullable
	public TestCase getEndNode() {
		return endNode;
	}

	/**
	 * @param endNode the endNode to set
	 */
	public void setEndNode(TestCase endNode) {
		this.endNode = endNode;
	}

	/**
	 * @return the nodeId
	 */
	@Nullable
	public Long getNodeId() {
		return nodeId;
	}

	/**
	 * @param nodeId the nodeId to set
	 */
	public void setNodeId(Long nodeId) {
		this.nodeId = nodeId;
	}

  
	
	@Override
	public boolean equals(@Nullable Object obj) {
	    if (obj == null) {
	        return false;
	    }
	    if (!InTestCase.class.isAssignableFrom(obj.getClass())) {
	        return false;
	    }
	    final InTestCase other = (InTestCase) obj;
	    
	    if ((this.startNode == null) ? (other.startNode != null) : !this.startNode.equals(other.startNode)) {
	        return false;
	    }
	    if ((this.endNode == null) ? (other.endNode != null) : !this.endNode.equals(other.endNode)) {
	        return false;
	    }
	    return true;
	}

	@Override
	public int hashCode() {
	    int hash = 3;
	    hash = 53 * hash + (this.startNode != null ? this.startNode.hashCode() : 0);
	    hash = 53 * hash + (this.endNode != null ? this.endNode.hashCode() : 0);
	    return hash;
	}

	/**
	 * @return the contextScreenGraphIds
	 */
	public Set<Long> getContextScreenGraphIds() {
		return contextScreenGraphIds;
	}

	/**
	 * @param contextScreenGraphIds the contextScreenGraphIds to set
	 */
	public void setContextScreenGraphIds(Set<Long> contextScreenGraphIds) {
		this.contextScreenGraphIds = contextScreenGraphIds;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void assignedFrom(INonPointerAssignment value) {
		if (value instanceof InTestCase) {
			InTestCase record = (InTestCase) value;
			//this.setHostingTestSuite(record.getHostingTestSuites());
			this.setStartNode(record.getStartNode());
			this.setEndNode(((InTestCase) value).getEndNode());
			//this.setTestCaseNodeRelationships(record.getTestCaseNodeRelationships());
			 
		} 
		
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long getGraphId() {
		if (this.nodeId==null) return 0l; 
		return this.nodeId;
	}

	

}
