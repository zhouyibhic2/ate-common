/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model;

import java.util.ArrayList;

import java.util.List;
import org.eclipse.jdt.annotation.Nullable;

import com.bigtester.ate.tcg.model.domain.IndustryCategory;
import com.bigtester.ate.tcg.model.domain.Neo4jScreenNode;
import com.bigtester.ate.tcg.model.domain.InScreenJumperTrainingRecord;
import com.bigtester.ate.tcg.model.domain.TestSuite;

// TODO: Auto-generated Javadoc
//import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord.UserInputType;

// TODO: Auto-generated Javadoc
/**
 * This class IntermediateResult is the frontend response holder.
 *
 * @author Peidong Hu
 */
//@JsonIdentityInfo(generator=JSOGGenerator.class, scope=IntermediateResult.class )
public class IntermediateResult {

	private Long userId;
	/** The screen triggered by previous screen uitr id. */
	private Long screenTriggeredByPreviousScreenUitrId = 0l;

	/** The in screen jump. */
	private boolean inScreenJump;

	/** The same page update. */
	private boolean samePageUpdate;


	/** The force create new screen. */
	private boolean forceCreateNewScreen= false;

	/** The previous screen trigger uitrs. */
	@Nullable
	private InScreenJumperTrainingRecord previousScreenTriggerInScreenJumperUitr;//NOPMD


	/** The test suites map. */
	private List<TestSuite> testSuitesMap = new ArrayList<TestSuite>();

	/** The industry categories map. */
	private List<IndustryCategory> industryCategoriesMap = new ArrayList<IndustryCategory>();

	/** The test case name. */
	private String testCaseName="";

	/** The domain name. */

	private String domainName="";

	/** The domain protocol. */
	private String domainProtocol="";

	/** The domain port. */
	private String domainPort="";

	/** The last screen node. */
	@Nullable
	private IntermediateResult lastScreenNodeIntermediateResult=null;

	/** The screen node. */
	private Neo4jScreenNode screenNode = new Neo4jScreenNode();
	/**
	 * Instantiates a new intermediate result.
	 */
	public IntermediateResult() { //NOPMD
		//For spring web services, an empty constructor has to be placed in models receiving web request.
		super();
	}

	/**
	 * Gets the test suites map.
	 *
	 * @return the testSuitesMap
	 */
	public List<TestSuite> getTestSuitesMap() {
		return testSuitesMap;
	}

	/**
	 * Sets the test suites map.
	 *
	 * @param testSuitesMap the testSuitesMap to set
	 */
	public void setTestSuitesMap(List<TestSuite> testSuitesMap) {
		this.testSuitesMap = testSuitesMap;
	}

	/**
	 * Gets the industry categories map.
	 *
	 * @return the industryCategoriesMap
	 */
	public List<IndustryCategory> getIndustryCategoriesMap() {
		return industryCategoriesMap;
	}

	/**
	 * Sets the industry categories map.
	 *
	 * @param industryCategoriesMap the industryCategoriesMap to set
	 */
	public void setIndustryCategoriesMap(
			List<IndustryCategory> industryCategoriesMap) {
		this.industryCategoriesMap = industryCategoriesMap;
	}

	/**
	 * Gets the test case name.
	 *
	 * @return the testCaseName
	 */
	public String getTestCaseName() {
		return testCaseName;
	}

	/**
	 * Sets the test case name.
	 *
	 * @param testCaseName the testCaseName to set
	 */
	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}

	/**
	 * Gets the domain name.
	 *
	 * @return the domainName
	 */
	public String getDomainName() {
		return domainName;
	}

	/**
	 * Sets the domain name.
	 *
	 * @param domainName the domainName to set
	 */
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	/**
	 * Gets the last screen node intermediate result.
	 *
	 * @return the lastScreenNodeIntermediateResult
	 */
	@Nullable
	public IntermediateResult getLastScreenNodeIntermediateResult() {
		return lastScreenNodeIntermediateResult;
	}

	/**
	 * Sets the last screen node intermediate result.
	 *
	 * @param lastScreenNodeIntermediateResult the lastScreenNodeIntermediateResult to set
	 */
	public void setLastScreenNodeIntermediateResult(
			IntermediateResult lastScreenNodeIntermediateResult) {
		this.lastScreenNodeIntermediateResult = lastScreenNodeIntermediateResult;
	}

	/**
	 * Gets the previous screen trigger in screen jumper uitr.
	 *
	 * @return the previousScreenTriggerClickUitr
	 */
	@Nullable
	public InScreenJumperTrainingRecord getPreviousScreenTriggerInScreenJumperUitr() {
		return previousScreenTriggerInScreenJumperUitr;
	}

	/**
	 * Sets the previous screen trigger in screen jumper uitr.
	 *
	 * @param previousScreenTriggerInScreenJumperUitr the previousScreenTriggerClickUitr to set
	 */
	public void setPreviousScreenTriggerInScreenJumperUitr(
			InScreenJumperTrainingRecord previousScreenTriggerInScreenJumperUitr) {
		this.previousScreenTriggerInScreenJumperUitr = previousScreenTriggerInScreenJumperUitr;
	}

	/**
	 * Checks if is same page update.
	 *
	 * @return the samePageUpdate
	 */
	public boolean isSamePageUpdate() {
		return samePageUpdate;
	}

	/**
	 * Sets the same page update.
	 *
	 * @param samePageUpdate the samePageUpdate to set
	 */
	public void setSamePageUpdate(boolean samePageUpdate) {
		this.samePageUpdate = samePageUpdate;
	}

	/**
	 * Checks if is in screen jump.
	 *
	 * @return the inScreenJump
	 */
	public boolean isInScreenJump() {
		return inScreenJump;
	}

	/**
	 * Sets the in screen jump.
	 *
	 * @param inScreenJump the inScreenJump to set
	 */
	public void setInScreenJump(boolean inScreenJump) {
		this.inScreenJump = inScreenJump;
	}

	/**
	 * Gets the screen triggered by previous screen uitr id.
	 *
	 * @return the screenTriggeredByPreviousScreenUitrId
	 */
	public Long getScreenTriggeredByPreviousScreenUitrId() {
		return screenTriggeredByPreviousScreenUitrId;
	}

	/**
	 * Sets the screen triggered by previous screen uitr id.
	 *
	 * @param screenTriggeredByPreviousScreenUitrId the screenTriggeredByPreviousScreenUitrId to set
	 */
	public void setScreenTriggeredByPreviousScreenUitrId(
			Long screenTriggeredByPreviousScreenUitrId) {
		this.screenTriggeredByPreviousScreenUitrId = screenTriggeredByPreviousScreenUitrId;
	}

	/**
	 * Gets the screen node.
	 *
	 * @return the screenNode
	 */
	public Neo4jScreenNode getScreenNode() {
		return screenNode;
	}

	/**
	 * Sets the screen node.
	 *
	 * @param screenNode the screenNode to set
	 */
	public void setScreenNode(Neo4jScreenNode screenNode) {
		this.screenNode = screenNode;
	}

	/**
	 * Gets the domain protocol.
	 *
	 * @return the domainProtocol
	 */
	public String getDomainProtocol() {
		return domainProtocol;
	}

	/**
	 * Sets the domain protocol.
	 *
	 * @param domainProtocol the domainProtocol to set
	 */
	public void setDomainProtocol(String domainProtocol) {
		this.domainProtocol = domainProtocol;
	}

	/**
	 * Gets the domain port.
	 *
	 * @return the domainPort
	 */
	public String getDomainPort() {
		return domainPort;
	}

	/**
	 * Sets the domain port.
	 *
	 * @param domainPort the domainPort to set
	 */
	public void setDomainPort(String domainPort) {
		this.domainPort = domainPort;
	}

	/**
	 * Checks if is force create new screen.
	 *
	 * @return the forceCreateNewScreen
	 */
	public boolean isForceCreateNewScreen() {
		return forceCreateNewScreen;
	}

	/**
	 * Sets the force create new screen.
	 *
	 * @param forceCreateNewScreen the forceCreateNewScreen to set
	 */
	public void setForceCreateNewScreen(boolean forceCreateNewScreen) {
		this.forceCreateNewScreen = forceCreateNewScreen;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}



}
