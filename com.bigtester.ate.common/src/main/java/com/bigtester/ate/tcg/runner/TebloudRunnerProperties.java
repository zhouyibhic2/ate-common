/**
 *
 */
package com.bigtester.ate.tcg.runner;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


/**
 * @author peidong
 *
 */
@Component

public class TebloudRunnerProperties {
	@Value("${enableDemoCode:false}")
	private boolean enableDemoCode;

	@Value("${pauseScreenName:loginScreen}")
	private String pauseScreenName;

	@Value("${pauseUitrsCount:2}")
	private int pauseUitrsCount;

	@Value("${demoConfidenceThredthold:0.6}")
	private float demoConfidenceThredthold;

	/**
	 * @return the enableDemoCode
	 */
	public boolean isEnableDemoCode() {
		return enableDemoCode;
	}

	/**
	 * @param enableDemoCode the enableDemoCode to set
	 */
	public void setEnableDemoCode(boolean enableDemoCode) {
		this.enableDemoCode = enableDemoCode;
	}

	/**
	 * @return the pauseScreenName
	 */
	public String getPauseScreenName() {
		return pauseScreenName;
	}

	/**
	 * @param pauseScreenName the pauseScreenName to set
	 */
	public void setPauseScreenName(String pauseScreenName) {
		this.pauseScreenName = pauseScreenName;
	}

	/**
	 * @return the pauseUitrsCount
	 */
	public int getPauseUitrsCount() {
		return pauseUitrsCount;
	}

	/**
	 * @param pauseUitrsCount the pauseUitrsCount to set
	 */
	public void setPauseUitrsCount(int pauseUitrsCount) {
		this.pauseUitrsCount = pauseUitrsCount;
	}

	/**
	 * @return the demoConfidenceThredthold
	 */
	public float getDemoConfidenceThredthold() {
		return demoConfidenceThredthold;
	}

	/**
	 * @param demoConfidenceThredthold the demoConfidenceThredthold to set
	 */
	public void setDemoConfidenceThredthold(float demoConfidenceThredthold) {
		this.demoConfidenceThredthold = demoConfidenceThredthold;
	}



}
