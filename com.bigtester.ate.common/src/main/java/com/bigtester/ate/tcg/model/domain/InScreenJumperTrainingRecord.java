/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model.domain;

import java.util.Set;

import org.eclipse.jdt.annotation.Nullable;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord.UserInputType;
import com.bigtester.ate.tcg.model.relationship.Relations;
import com.bigtester.ate.tcg.model.relationship.StepOut;
import com.bigtester.ate.tcg.model.relationship.StepOut.StepOutTriggerType;
import com.bigtester.ate.tcg.runner.model.ITestStepRunner;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

// TODO: Auto-generated Javadoc
/**
 * This class TrainingRecord defines ....
 * @author Peidong Hu
 *
 */

/**
 * @author ashraf
 *
 */
@NodeEntity
@JsonIdentityInfo(generator=JSOGGenerator.class )
public class InScreenJumperTrainingRecord extends UserInputTrainingRecord implements ITestStepRunner, INameLabelPredictableObjectNonPointerAssignment, INonPointerAssignment{
//	/** The step outs. */
//	@JsonIgnore
//	@Relationship(type = Relations.STEP_OUT, direction = Relationship.OUTGOING)
//	@Nullable
//	private AbstractScreenNode stepOut;

//	/** The action trigger. */
//	private transient boolean actionTrigger;

	
	/** The click times. */
	private int clickTimes;
	
	/**
	 * Instantiates a new screen user click input training record.
	 */
	public InScreenJumperTrainingRecord() {
		super("ScreenUserClickInputTrainingRecord");
	}
	
	/**
	 * Instantiates a new screen user click input training record.
	 *
	 * @param uitr the uitr
	 */
	public InScreenJumperTrainingRecord(UserInputTrainingRecord uitr) {
		super("ScreenUserClickInputTrainingRecord", uitr);
		this.setUserInputType(UserInputType.INSCREENJUMPER);
//		this.setId(uitr.getId());
//		this.setInputLabelName(uitr.getInputLabelName());
//		this.setInputMLHtmlCode(uitr.getInputMLHtmlCode());
//		this.setPioPredictConfidence(uitr.getPioPredictConfidence());
//		this.setTestcases(uitr.getTestcases());
//		this.setTrainedResult(uitr.getTrainedResult());
//		this.setUserInputType(UserInputType.INSCREENJUMPER);
//		this.setUserValues(uitr.getUserValues());
//		this.setPioPredictLabelResult(uitr.getPioPredictLabelResult());
//		this.setBelongToCurrentTestCase(uitr.isBelongToCurrentTestCase());
//		this.setElementCoreHtmlCode(uitr.getElementCoreHtmlCode());
//		this.setElementCoreHtmlCodeWithoutGuidValue(uitr.getElementCoreHtmlCodeWithoutGuidValue());
//		this.setTriggeredBy(uitr.getTriggeredBy());
	}
	public InScreenJumperTrainingRecord(WebElementTrainingRecord uitr) {
		super("ScreenUserClickInputTrainingRecord", uitr);
		this.setUserInputType(UserInputType.INSCREENJUMPER);
//		this.setId(uitr.getId());
//		this.setInputLabelName(uitr.getInputLabelName());
//		this.setInputMLHtmlCode(uitr.getInputMLHtmlCode());
//		this.setPioPredictConfidence(uitr.getPioPredictConfidence());
//		this.setTestcases(uitr.getTestcases());
//		this.setTrainedResult(uitr.getTrainedResult());
//		this.setUserInputType(UserInputType.INSCREENJUMPER);
//		this.setUserValues(uitr.getUserValues());
//		this.setPioPredictLabelResult(uitr.getPioPredictLabelResult());
//		this.setBelongToCurrentTestCase(uitr.isBelongToCurrentTestCase());
//		this.setElementCoreHtmlCode(uitr.getElementCoreHtmlCode());
//		this.setElementCoreHtmlCodeWithoutGuidValue(uitr.getElementCoreHtmlCodeWithoutGuidValue());
//		this.setTriggeredBy(uitr.getTriggeredBy());
	}
//	/**
//	 * @return the stepOut
//	 */
//	@Nullable
//	public AbstractScreenNode getStepOut() {
//		return stepOut;
//	}
//	/**
//	 * @param stepOut the stepOut to set
//	 */
//	public void setStepOut(AbstractScreenNode stepOut) {
//		if (this.stepOut != null) {
//			
//			this.nonPointerAssign(stepOut, this.stepOut);
//		} else {
//			this.stepOut = stepOut;
//		}
//	}
	
	/**
	 * @return the clickTimes
	 */
	public int getClickTimes() {
		return clickTimes;
	}


//	/**
//	 * @return the actionTrigger
//	 */
//	public boolean isActionTrigger() {
//		return actionTrigger;
//	}
//
//	/**
//	 * @param actionTrigger the actionTrigger to set
//	 */
//	public void setActionTrigger(boolean actionTrigger) {
//		this.actionTrigger = actionTrigger;
//	}
/**
	 * @param clickTimes the clickTimes to set
	 */
	public void setClickTimes(int clickTimes) {
		this.clickTimes = clickTimes;
	}
	/**
	 * Adds the step out.
	 *
	 * @param screenNode the screen node
	 * @return the step out
	 */
	public StepOut addStepOut(Neo4jScreenNode screenNode) {
		return addStepOut(screenNode, StepOutTriggerType.INSCREENJUMPERSTEPOUT);
	}
/**
 * {@inheritDoc}
 */
@Override
public void assignedFrom(INonPointerAssignment value) {
	super.assignedFrom(value);
	if (value instanceof InScreenJumperTrainingRecord) {
		InScreenJumperTrainingRecord record = (InScreenJumperTrainingRecord) value;
		this.setActionTrigger(record.isActionTrigger());
		this.setBelongToCurrentTestCase(record.isBelongToCurrentTestCase());
		this.setInputLabelName(record.getInputLabelName());
		this.setInputMLHtmlCode(record.getInputMLHtmlCode());
		this.setPioPredictConfidence(record.getPioPredictConfidence());
		//this.getPioPredictLabelResult().assignedFrom(record.getPioPredictLabelResult());
		this.setPioPredictLabelResult(record.getPioPredictLabelResult()); 
		this.setTestcases(record.getTestcases());
		//this.nonPointerAssign(record.getTestcases(), this.getTestcases());
		this.setTrainedResult(record.getTrainedResult());
		this.setUserInputType(record.getUserInputType());
		//this.nonPointerAssign(record.getUserValues(), this.getUserValues());
		this.setUserValues(record.getUserValues());
		this.setComputedCssSizes(record.getComputedCssSizes());
		 this.setClickTimes(record.getClickTimes());
		 //this.nonPointerAssign(record.getStepOuts(), this.getStepOuts());
		 //this.setStepOuts(record.getStepOuts());
		 this.setElementCoreHtmlCode(record.getElementCoreHtmlCode());
		 this.setElementCoreHtmlCodeWithoutGuidValue(record.getElementCoreHtmlCodeWithoutGuidValue());
		 this.setTriggeredBy(record.getTriggeredBy());
		 
		this.setElementCoreHtmlNode(record.getElementCoreHtmlNode());
		this.setInputMLHtmlWords(record.getInputMLHtmlWords());
		this.setScreenUitrProbability(record.getScreenUitrProbability());
		this.setTriggeredBy(record.getTriggeredBy());
	} 
	
}

/**
 * {@inheritDoc}
 */
@Override
public String getComparableLabelName() {
	return this.getPioPredictLabelResult().getValue();
}
}
