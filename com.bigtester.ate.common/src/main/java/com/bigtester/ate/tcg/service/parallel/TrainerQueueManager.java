/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2017, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.service.parallel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;

import com.bigtester.ate.tcg.model.IntermediateResult;
import com.googlecode.concurrentlinkedhashmap.ConcurrentLinkedHashMap;
import com.googlecode.concurrentlinkedhashmap.ConcurrentLinkedHashMap.Builder;

// TODO: Auto-generated Javadoc
/**
 * This class TrainerQueueManager defines ....
 * @author Peidong Hu
 *
 */
public class TrainerQueueManager {
	final private ConcurrentLinkedHashMap<CompletableFuture<IntermediateResult>, IntermediateResult> futuresPool = new Builder<CompletableFuture<IntermediateResult>, IntermediateResult>().maximumWeightedCapacity(500)
	        .initialCapacity(100)
	        .concurrencyLevel(32).build();
	final private PausableThreadExecutor threadExecutor = new PausableThreadExecutor(10,  Executors.defaultThreadFactory());
	/**
	 * @return the futuresPool
	 */
	public Map<CompletableFuture<IntermediateResult>, IntermediateResult> getFuturesPool() {
		return futuresPool;
	}
	/**
	 * @return the threadExecutor
	 */
	public PausableThreadExecutor getThreadExecutor() {
		return threadExecutor;
	}
}
