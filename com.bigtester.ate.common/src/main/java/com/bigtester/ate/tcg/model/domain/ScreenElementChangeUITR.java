/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model.domain;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.bigtester.ate.model.page.atewebdriver.exception.BrowserUnexpectedException;
import org.bigtester.ate.model.page.elementfind.ElementFindByCss;
import org.bigtester.ate.systemlogger.LogbackWriter;
import org.neo4j.ogm.annotation.NodeEntity;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
//import org.sikuli.script.Match;

import com.bigtester.RunnerGlobalUtils;
import com.bigtester.ate.tcg.model.ScreenNamePredictStrategy;
import com.bigtester.ate.tcg.model.ws.api.ScreenStepsAdvice;
import com.bigtester.ate.tcg.runner.model.DevToolMessage;
import com.bigtester.ate.tcg.runner.model.ITestStepExecutionContext;
import com.bigtester.ate.tcg.runner.model.ITestStepRunner;
import com.bigtester.ate.tcg.runner.screenobject.handler.IFilePickerDialogHandler;
import com.bigtester.ate.tcg.runner.screenobject.handler.IWebElementHandler;
import com.bigtester.ate.tcg.runner.screenobject.handler.IWebElementHandler.ActionResult;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

/**
 * This class TrainingRecord defines ....
 * @author Peidong Hu
 *
 */

@NodeEntity
@JsonIdentityInfo(generator=JSOGGenerator.class )
public class ScreenElementChangeUITR extends UserInputTrainingRecord implements ITestStepRunner, INameLabelPredictableObjectNonPointerAssignment, INonPointerAssignment{

	/** The element changed screen. */
	//failed due to the angularjs can't handle the circular reference in ng-options track by clause.
	//refer to https://github.com/angular/angular.js/issues/13378
//	@Relationship(type = Relations.CHANGE_SCREEN_ELEMENT)
//	private Neo4jScreenNode elementChangedScreen;
	private Long elementChangedScreen;

	/**
	 * Instantiates a new screen user click input training record.
	 */
	public ScreenElementChangeUITR() {
		super("ScreenElementChangeUITR");
	}

	/**
	 * Instantiates a new screen user click input training record.
	 *
	 * @param uitr the uitr
	 */
	public ScreenElementChangeUITR(UserInputTrainingRecord uitr) {
		super("ScreenElementChangeUITR", uitr);
		this.setUserInputType(UserInputType.SCREENCHANGEUITR);
	}
	public ScreenElementChangeUITR(WebElementTrainingRecord uitr) {
		super("ScreenElementChangeUITR", uitr);
		this.setUserInputType(UserInputType.SCREENCHANGEUITR);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void assignedFrom(INonPointerAssignment value) {
		super.assignedFrom(value);
		if (value instanceof ScreenElementChangeUITR) {
			ScreenElementChangeUITR record = (ScreenElementChangeUITR) value;
			this.setElementChangedScreen(record.getElementChangedScreen());

		}
	}

	/**
	 * @return the elementChangedScreen
	 */
	public Long getElementChangedScreen() {
		return elementChangedScreen;
	}

	/**
	 * @param elementChangedScreen the elementChangedScreen to set
	 */
//	public void setElementChangedScreen(Neo4jScreenNode elementChangedScreen) {
//		if (this.elementChangedScreen == null) this.elementChangedScreen = elementChangedScreen;
//		else
//		this.nonPointerAssign(elementChangedScreen, this.elementChangedScreen);
//	}
	public void setElementChangedScreen(Long elementChangedScreen) {
		this.elementChangedScreen = elementChangedScreen;
	}

//	protected void handleFilePickerSystemDialog(ITestStepExecutionContext executeEngine) {
//		doSystemDialogFilePick(executeEngine);
//	}

//	protected boolean doSystemDialogFilePick(ITestStepExecutionContext executeEngine) {
//		System.out.println("elementChanger java robot based click: " + this.getInputLabelName());
//		List<WebElement> fileInputs = executeEngine.getMyWebDriver().getWebDriverInstance().findElements(By.cssSelector("input[type='file']"));
//		//find empty file inputs
//		fileInputs = fileInputs.stream().filter(we->StringUtils.isEmpty(we.getAttribute("value"))).collect(Collectors.toList());
//		if (fileInputs.size()>0) {
//			//check if file picker system dialog appears.
//			List<Match> matchedPickerObjects = null;
//			for (int index=0; index<executeEngine.getFilePickerSystemDialogHandler().size(); index++) {
//				IFilePickerDialogHandler fileH = executeEngine.getFilePickerSystemDialogHandler().get(index);
//				matchedPickerObjects = fileH.match();
//				System.out.println("matched size: " + matchedPickerObjects.size());
//				if (matchedPickerObjects.size()>5) {
//					fileH.doAcceptableAction(matchedPickerObjects, "/home/peidong/testfile.pdf");
//					return true;
//				}
//			}
//			return false;
//		} else {
//			return false;
//		}
//	}
//
	protected ActionResult doFileInputInsideUitrHtmlCode(ITestStepExecutionContext executeEngine, ScreenStepsAdvice stepAdvice) {
		return executeUitrStep(executeEngine, stepAdvice,this.getUserValues().iterator().next().getValue(), this.getUserInputType());
	}

	protected ActionResult fileInputAction(WebElement fileInput, String input) {

		WebElement webE = fileInput;
		try{
//				if (!StringUtils.isEmpty(webE.getAttribute("value")))
//					return ActionResult.SUCCESS;
			if (!StringUtils.isEmpty(webE.getAttribute("value"))) {
                // "\u0008" - is backspace char
				return IWebElementHandler.ActionResult.SUCCESS;
            }
				webE.sendKeys(input);
				if (!StringUtils.isEmpty(webE.getAttribute("value")))
					return IWebElementHandler.ActionResult.SUCCESS;
				else
					return IWebElementHandler.ActionResult.VALUE_NOT_TYPED_IN_CORRECTLY;


		} catch (ElementNotVisibleException enve){
			return IWebElementHandler.ActionResult.NOT_VISIBLE_EXCEPTION;
		} catch (Throwable thr) {
			thr.printStackTrace();
			return ActionResult.FAILED_FOR_UNKNOWN_REASON;
		}
	}

//	protected Map<ScreenNamePredictStrategy, ScreenStepsAdvice> tryClickGuidElementInCoreHtmlCodeUntilSuccess(
//			String guid, String coreHtmlCode,
//			ITestStepExecutionContext executeEngine,
//			ScreenStepsAdvice stepAdvice, boolean tryFileInputSearching) {
//		//System.out.println("");
//		if (!this.getUserValues().isEmpty() && tryFileInputSearching) {
//			//might be file picker, need to run doFileInput
//			if (doFileInputInsideUitrHtmlCode(executeEngine, stepAdvice) == ActionResult.SUCCESS) {
//				return executeEngine.getCurrentExecutingScreenStepAdvices();
//			} else {
//				//the uitr core html doesn't have file input, we need to find the closest file input element and try
//				WebElement fileInput = null;
//				WebElement coreElement = null;
//				Optional<WebElement> followingFileInput = Optional.empty();
//				Optional<WebElement> precedingFileInput = Optional.empty();
//				try {
//					//assume that file pick input will be in the same frame as the core element. we only search in this particular frame.
//					coreElement = executeEngine.getElementFinder(ElementFindByCss.class).doFind(executeEngine.getMyWebDriver(),"[ate-guid='" + guid +"']");
//					followingFileInput = Optional.ofNullable(executeEngine.getMyWebDriver().getWebDriverInstance().findElement(By.xpath("(.//*[@ate-guid='" + guid +"']/following::input[@type='file'])[1]")));
//					//followingFileInput = Optional.of(executeEngine.getElementFinder(ElementFindByXpath.class).doFind(executeEngine.getMyWebDriver(), "(.//*[@ate-guid='" + guid +"']/following::input[@type='file'])[1]"));
//				} catch (NoSuchElementException | BrowserUnexpectedException exp) {
//
//				}
//				try {
//					if (coreElement == null) {
//						coreElement = executeEngine.getElementFinder(ElementFindByCss.class).doFind(executeEngine.getMyWebDriver(),"[ate-guid='" + guid +"']");
//					}
//					precedingFileInput = Optional.ofNullable(executeEngine.getMyWebDriver().getWebDriverInstance().findElement(By.xpath("(.//*[@ate-guid='" + guid +"']/preceding::input[@type='file'])[1]")));
//					//precedingFileInput = Optional.of(executeEngine.getElementFinder(ElementFindByXpath.class).doFind(executeEngine.getMyWebDriver(), "(.//*[@ate-guid='" + guid +"']/preceding::input[@type='file'])[1]"));
//				} catch (NoSuchElementException | BrowserUnexpectedException  exp) {
//
//				}
//				//List<WebElement> precedingFileInput = executeEngine.getMyWebDriver().getWebDriverInstance().findElements(By.xpath(("(.//*[@ate-guid='" + guid +"']/preceding::input[@type='file'])[1]")));
//				if (followingFileInput.isPresent() && precedingFileInput.isPresent()){
//
//					String followingFileInputGuid = followingFileInput.get().getAttribute("ate-guid");
//					String precedingFileInputGuid = precedingFileInput.get().getAttribute("ate-guid");
//					int followingFileInputDistance = executeEngine.caculateDomRelativeDistance(guid, followingFileInputGuid);
//					int precedingFileInputDistance = executeEngine.caculateDomRelativeDistance(guid, precedingFileInputGuid);
//					if (followingFileInputDistance < precedingFileInputDistance ) {
//						fileInput = followingFileInput.get();
//					} else {
//						fileInput = precedingFileInput.get();
//					}
//				} else if (precedingFileInput.isPresent()) {
//					fileInput = precedingFileInput.get();
//				} else if (followingFileInput.isPresent()){
//					fileInput = followingFileInput.get();
//				}
//				if (fileInput != null) {
//					if (fileInputAction(fileInput, this.getUserValues().iterator().next().getValue()) == ActionResult.SUCCESS) {
//						return executeEngine.getCurrentExecutingScreenStepAdvices();
//					}
//				}
//				return executeEngine.getCurrentExecutingScreenStepAdvices();
//			}
//		} else {
//		try{
//
//			executeEngine.getMyWebDriver().getWebDriverInstance().manage().window().maximize();
//
//			WebElement webE = executeEngine.getElementFinder(ElementFindByCss.class).doFind(executeEngine.getMyWebDriver(),"[ate-guid='" + guid +"']");
//			boolean exactSameScreen = false;
//			Map<ScreenNamePredictStrategy, ScreenStepsAdvice> newStepAdvices = new HashMap<ScreenNamePredictStrategy, ScreenStepsAdvice>();
//			if (webE.getSize().getWidth()/2 > 15 && webE.getSize().getHeight()/2>10) {
//				Actions builder = new Actions(executeEngine.getMyWebDriver().getWebDriverInstance());
//				builder.moveToElement(webE, webE.getSize().getWidth()/2, webE.getSize().getHeight()/2).click().build().perform();
//				LogbackWriter.writeAppInfo("elementChanger location based click: " + this.getInputLabelName() +
//						" ==htmlCode== " + this.getElementCoreHtmlCode() +" ==");
//
//
//				Thread.sleep(5000);
//				DevToolMessage newDevMsg = executeEngine
//						.downloadScreenMessage();
//				DevToolMessage previousDevMsg = stepAdvice.getDevToolMessage();
//				if (newDevMsg.equals(previousDevMsg)) {
//					exactSameScreen = true;
//				} else {
//
//					newStepAdvices = executeEngine.getStepAdvice(
//							newDevMsg,
//							executeEngine.getPreviousScreenLastStepAdvice(
//									stepAdvice.getScreenName()).orElse(null));
//					newStepAdvices.entrySet().forEach(entry->{
//						entry.getValue().setDevToolMessage(newDevMsg);
//					});;
//					//stepAdvice.setDevToolMessage(newDevMsg);
//					exactSameScreen = executeEngine.isExactSameScreenAdvices(
//							newStepAdvices, executeEngine
//									.getCurrentExecutingScreenStepAdvices());
//					if (exactSameScreen) {
//						stepAdvice.setDevToolMessage(newDevMsg);
//					}
//				}
//			} else {
//				exactSameScreen = true;
//			}
//			if(exactSameScreen) {
//				webE.click();
//				LogbackWriter.writeAppInfo("elementChanger webE based click: " + this.getInputLabelName());
//				Thread.sleep(5000);
//				DevToolMessage newDevMsg = executeEngine
//						.downloadScreenMessage();
//				DevToolMessage previousDevMsg = stepAdvice.getDevToolMessage();
//
//				if (newDevMsg.equals(previousDevMsg)) {
//					exactSameScreen = true;
//				} else {
//					newStepAdvices = executeEngine.getStepAdvice(
//							newDevMsg,
//							executeEngine.getPreviousScreenLastStepAdvice(
//									stepAdvice.getScreenName()).orElse(null));
//					newStepAdvices.entrySet().forEach(entry->{
//						entry.getValue().setDevToolMessage(newDevMsg);
//					});;
//					exactSameScreen = executeEngine.isExactSameScreenAdvices(
//							newStepAdvices, executeEngine
//									.getCurrentExecutingScreenStepAdvices());
//					if (exactSameScreen) {
//						stepAdvice.setDevToolMessage(newDevMsg);
//					}
//				}
//				if(exactSameScreen) {
//					String newGuid = RunnerGlobalUtils.parseNextAteGuid(coreHtmlCode, guid);
//					if (!newGuid.isEmpty() ) {
//						//stepAdvice.setExecutedJumperElementInThisScreen(Optional.of(uitr));
//						//dead code
//						return tryClickGuidElementInHtmlCodeUntilSuccess(
//								newGuid, coreHtmlCode,
//								executeEngine,
//								stepAdvice,false);
//					} else {
//						/*it ends up a wrong prediction of action trigger uitr or file picker*/
//						return executeEngine.getCurrentExecutingScreenStepAdvices();
//					}
//				}
//			}
//
//			return newStepAdvices;
//		} catch (Throwable niwde) {
//
//			if (niwde instanceof ElementNotVisibleException || niwde.getMessage().contains("is not clickable")) {
//				String newGuid = RunnerGlobalUtils.parseNextAteGuid(coreHtmlCode, guid);
//				if (!newGuid.isEmpty() ) {
//					//stepAdvice.setExecutedJumperElementInThisScreen(Optional.of(uitr));
//					//dead code
//					return tryClickGuidElementInHtmlCodeUntilSuccess(
//							newGuid, coreHtmlCode,
//							executeEngine,
//							stepAdvice, false);
//				} else {
//					/*it ends up a wrong prediction of action trigger uitr*/
//					/*handleFilePickerSystemDialog(executeEngine);*/
//					return executeEngine.getCurrentExecutingScreenStepAdvices();
//				}
//			}
//			niwde.printStackTrace();
//			return executeEngine.getCurrentExecutingScreenStepAdvices();
//		}
//		}
//
//	}

	protected Map<ScreenNamePredictStrategy, ScreenStepsAdvice> tryClickGuidElementInHtmlCodeUntilSuccess(
			String guid, String mlHtmlCode,
			ITestStepExecutionContext executeEngine,
			ScreenStepsAdvice stepAdvice, boolean tryFileInputSearching) {
		//System.out.println("");
		if (!this.getUserValues().isEmpty() && tryFileInputSearching) {
			//might be file picker, need to run doFileInput
			if (doFileInputInsideUitrHtmlCode(executeEngine, stepAdvice) == ActionResult.SUCCESS) {
				return executeEngine.getCurrentExecutingScreenStepAdvices();
			} else {
				//the uitr core html doesn't have file input, we need to find the closest file input element and try
				WebElement fileInput = null;
				WebElement coreElement = null;
				Optional<WebElement> followingFileInput = Optional.empty();
				Optional<WebElement> precedingFileInput = Optional.empty();
				try {
					//assume that file pick input will be in the same frame as the core element. we only search in this particular frame.
					//coreElement = executeEngine.getWebElementHandlers().get(0).findWebE(executeEngine.getMyWebDriver(), guid);
					coreElement = executeEngine.findElement(guid);
					//coreElement = executeEngine.getElementFinder(ElementFindByCss.class).doFind(executeEngine.getMyWebDriver(),"[ate-guid='" + guid +"']");
					//followingFileInput = Optional.ofNullable(executeEngine.getMyWebDriver().getWebDriverInstance().findElement(By.xpath(guid + "/following::input[@type='file'])[1]")));
					followingFileInput = Optional.ofNullable(executeEngine.findFollowingInputFileWebElement(guid));
					//followingFileInput = Optional.of(executeEngine.getElementFinder(ElementFindByXpath.class).doFind(executeEngine.getMyWebDriver(), "(.//*[@ate-guid='" + guid +"']/following::input[@type='file'])[1]"));
				} catch (NoSuchElementException | BrowserUnexpectedException  exp) {

				}
				try {
					if (coreElement == null) {
						coreElement = executeEngine.findElement(guid);
						//coreElement = executeEngine.getWebElementHandlers().get(0).findWebE(executeEngine.getMyWebDriver(), guid);
						//coreElement = executeEngine.getElementFinder(ElementFindByCss.class).doFind(executeEngine.getMyWebDriver(),"[ate-guid='" + guid +"']");
					}
					precedingFileInput = Optional.ofNullable(executeEngine.findPrecedingInputFileWebElement(guid));
					//precedingFileInput = Optional.ofNullable(executeEngine.getMyWebDriver().getWebDriverInstance().findElement(By.xpath(guid + "/preceding::input[@type='file'])[1]")));
					//precedingFileInput = Optional.of(executeEngine.getElementFinder(ElementFindByXpath.class).doFind(executeEngine.getMyWebDriver(), "(.//*[@ate-guid='" + guid +"']/preceding::input[@type='file'])[1]"));
				} catch (NoSuchElementException | BrowserUnexpectedException exp) {

				}
				//List<WebElement> precedingFileInput = executeEngine.getMyWebDriver().getWebDriverInstance().findElements(By.xpath(("(.//*[@ate-guid='" + guid +"']/preceding::input[@type='file'])[1]")));
				if (followingFileInput.isPresent() && precedingFileInput.isPresent()){

					//String followingFileInputGuid = followingFileInput.get().getAttribute("ate-guid");
					//String precedingFileInputGuid = precedingFileInput.get().getAttribute("ate-guid");
					int followingFileInputDistance = executeEngine.caculateDomRelativeDistanceForFollowingFileInput(guid);
					int precedingFileInputDistance = executeEngine.caculateDomRelativeDistanceForPrecedingFileInput(guid);
					if (followingFileInputDistance < precedingFileInputDistance ) {
						fileInput = followingFileInput.get();
					} else {
						fileInput = precedingFileInput.get();
					}
				} else if (precedingFileInput.isPresent()) {
					fileInput = precedingFileInput.get();
				} else if (followingFileInput.isPresent()){
					fileInput = followingFileInput.get();
				}
				if (fileInput != null) {
					if (fileInputAction(fileInput, this.getUserValues().iterator().next().getValue()) == ActionResult.SUCCESS) {
						return executeEngine.getCurrentExecutingScreenStepAdvices();
					}
				}
				return executeEngine.getCurrentExecutingScreenStepAdvices();
			}
		} else {
		try{
//			String shortcutGoToFullScreen = Keys.chord(Keys.F11);
//			executeEngine.getMyWebDriver().getWebDriverInstance().manage().window().
//			Thread.sleep(5000);
			executeEngine.getMyWebDriver().getWebDriverInstance().manage().window().maximize();
			//Long heightOffSet = (Long) ((JavascriptExecutor) executeEngine.getMyWebDriver().getWebDriverInstance()).executeScript("return window.outerHeight - window.innerHeight");
			//Long widthOffSet = (Long) ((JavascriptExecutor) executeEngine.getMyWebDriver().getWebDriverInstance()).executeScript("return window.outerWidth - window.innerWidth");
			//height.byteValue();
			//WebElement webE = executeEngine.getElementFinder(ElementFindByCss.class).doFind(executeEngine.getMyWebDriver(),"[ate-guid='" + guid +"']");

			WebElement webE = executeEngine.getWebElementHandlers().get(0).findWebE(executeEngine.getMyWebDriver(), guid);
			boolean exactSameScreen = false;
			Map<ScreenNamePredictStrategy, ScreenStepsAdvice> newStepAdvices = new HashMap<ScreenNamePredictStrategy, ScreenStepsAdvice>();
			if (webE.getSize().getWidth()/2 > 15 && webE.getSize().getHeight()/2>10) {
				Actions builder = new Actions(executeEngine.getMyWebDriver().getWebDriverInstance());
				builder.moveToElement(webE, webE.getSize().getWidth()/2, webE.getSize().getHeight()/2).click().build().perform();
				LogbackWriter.writeAppInfo("elementChanger location based click: " + this.getInputLabelName() +
						" ==htmlCode== " + this.getElementCoreHtmlCode() +" ==");

//			Point viewPortLocation = ((Locatable) webE).getCoordinates().inViewPort();
//			int x = viewPortLocation.getX();
//			  int y = viewPortLocation.getY();
//			  Robot bot = new Robot();
//
//			  executeEngine.getMyWebDriver().getMultiWindowsHandler().getMainWindowHandler();
//			    bot.mouseMove(x+widthOffSet.intValue() + webE.getSize().getWidth()/2, y + heightOffSet.intValue() + webE.getSize().getHeight()/2);
//			    bot.mousePress(InputEvent.BUTTON1_MASK);
//			    bot.mouseRelease(InputEvent.BUTTON1_MASK);
			//webE.click();
				Thread.sleep(5000);

			//stepAdvice.setExecutedJumperElementInThisScreen(executedJumperElementInThisScreen);(Optional.of(uitr));
//				if (doSystemDialogFilePick(executeEngine)) {
//					return executeEngine.getCurrentExecutingScreenStepAdvices();
//				}
				DevToolMessage newDevMsg = executeEngine
						.downloadScreenMessage();
				DevToolMessage previousDevMsg = stepAdvice.getDevToolMessage();
				if (newDevMsg.equals(previousDevMsg)) {
					exactSameScreen = true;
				} else {

					newStepAdvices = executeEngine.getStepAdvice(executeEngine.getTestCaseName(),
							newDevMsg,
							executeEngine.getPreviousScreenLastStepAdvice(
									stepAdvice.getScreenName()).orElse(null));
					newStepAdvices.entrySet().forEach(entry->{
						entry.getValue().setDevToolMessage(newDevMsg);
					});;
					//stepAdvice.setDevToolMessage(newDevMsg);
					exactSameScreen = executeEngine.isExactSameScreenAdvices(
							newStepAdvices, executeEngine
									.getCurrentExecutingScreenStepAdvices());
					if (exactSameScreen) {
						stepAdvice.setDevToolMessage(newDevMsg);
					}
				}
			} else {
				exactSameScreen = true;
			}
			if(exactSameScreen) {
				webE.click();
				LogbackWriter.writeAppInfo("elementChanger webE based click: " + this.getInputLabelName());
				Thread.sleep(5000);
//				if (doSystemDialogFilePick(executeEngine)) {
//					return executeEngine.getCurrentExecutingScreenStepAdvices();
//				}
				DevToolMessage newDevMsg = executeEngine
						.downloadScreenMessage();
				DevToolMessage previousDevMsg = stepAdvice.getDevToolMessage();

				if (newDevMsg.equals(previousDevMsg)) {
					exactSameScreen = true;
				} else {
					newStepAdvices = executeEngine.getStepAdvice(executeEngine.getTestCaseName(),
							newDevMsg,
							executeEngine.getPreviousScreenLastStepAdvice(
									stepAdvice.getScreenName()).orElse(null));
					newStepAdvices.entrySet().forEach(entry->{
						entry.getValue().setDevToolMessage(newDevMsg);
					});;
					exactSameScreen = executeEngine.isExactSameScreenAdvices(
							newStepAdvices, executeEngine
									.getCurrentExecutingScreenStepAdvices());
					if (exactSameScreen) {
						stepAdvice.setDevToolMessage(newDevMsg);
					}
				}
				if(exactSameScreen) {
					String newGuid = RunnerGlobalUtils.parseNextAteAttr(mlHtmlCode,guid, IWebElementHandler.ELEMENT_FIND_ATE_ATTRIBUE_NAME);
					if (!newGuid.isEmpty() ) {
						//stepAdvice.setExecutedJumperElementInThisScreen(Optional.of(uitr));
						//dead code
						return tryClickGuidElementInHtmlCodeUntilSuccess(
								newGuid, mlHtmlCode,
								executeEngine,
								stepAdvice,false);
					} else {
						//TODO deal with file upload dialog case. need to use user input handler to match
						//if this is a file upload before click the element changer, file upload will directly use sendKeys to give value.

						//this element changer is not predicted correctly, we need to try next guid and then try the archived changers
						//Check if there is file picker dialog poped up
						/*handleFilePickerSystemDialog(executeEngine);*/
		//				guid = RunnerGlobalUtils.parseNextAteGuid(mlHtmlCode, guid);
		//				if (!guid.isEmpty())
		//					return tryClickGuidElementInMachineLearningHtmlCodeUntilSuccess(
		//							guid, mlHtmlCode,
		//							executeEngine,
		//							stepAdvice);
						/*it ends up a wrong prediction of action trigger uitr or file picker*/
						return executeEngine.getCurrentExecutingScreenStepAdvices();
					}
				}
			} else if (!executeEngine.isSameScreenNamedAdvices(newStepAdvices, executeEngine.getCurrentExecutingScreenStepAdvices())) {
				//incorrectly clicked jumper, need to return previous screen
				executeEngine.getMyWebDriver().getWebDriverInstance().navigate().back();
				String newGuid = RunnerGlobalUtils.parseNextAteAttr(mlHtmlCode,guid, IWebElementHandler.ELEMENT_FIND_ATE_ATTRIBUE_NAME);
				if (!newGuid.isEmpty() ) {
					//stepAdvice.setExecutedJumperElementInThisScreen(Optional.of(uitr));
					//dead code
					return tryClickGuidElementInHtmlCodeUntilSuccess(
							newGuid, mlHtmlCode,
							executeEngine,
							stepAdvice,false);
				} else {
					//TODO deal with file upload dialog case. need to use user input handler to match
					//if this is a file upload before click the element changer, file upload will directly use sendKeys to give value.

					//this element changer is not predicted correctly, we need to try next guid and then try the archived changers
					//Check if there is file picker dialog poped up
					/*handleFilePickerSystemDialog(executeEngine);*/
	//				guid = RunnerGlobalUtils.parseNextAteGuid(mlHtmlCode, guid);
	//				if (!guid.isEmpty())
	//					return tryClickGuidElementInMachineLearningHtmlCodeUntilSuccess(
	//							guid, mlHtmlCode,
	//							executeEngine,
	//							stepAdvice);
					/*it ends up a wrong prediction of action trigger uitr or file picker*/
					return executeEngine.getCurrentExecutingScreenStepAdvices();
				}
			}

			return newStepAdvices;
		} catch (Throwable niwde) {

			if (niwde instanceof ElementNotVisibleException || niwde.getMessage().contains("is not clickable")) {
				String newGuid = RunnerGlobalUtils.parseNextAteAttr(mlHtmlCode, guid, IWebElementHandler.ELEMENT_FIND_ATE_ATTRIBUE_NAME);
				if (!newGuid.isEmpty() ) {
					//stepAdvice.setExecutedJumperElementInThisScreen(Optional.of(uitr));
					//dead code
					return tryClickGuidElementInHtmlCodeUntilSuccess(
							newGuid, mlHtmlCode,
							executeEngine,
							stepAdvice, false);
				} else {
					/*it ends up a wrong prediction of action trigger uitr*/
					/*handleFilePickerSystemDialog(executeEngine);*/
					return executeEngine.getCurrentExecutingScreenStepAdvices();
				}
			}
//			else {
//				try {
//					executeEngine.getMyWebDriver().getMultiWindowsHandler().closeAllWindowsExceptMainWindow();
//				} catch (BrowserUnexpectedException e) {
//					e.printStackTrace();
//				}
//
//			}
			niwde.printStackTrace();
			return executeEngine.getCurrentExecutingScreenStepAdvices();
		}
		}

	}
	public Map<ScreenNamePredictStrategy, ScreenStepsAdvice> executeStep(ITestStepExecutionContext executeEngine,  ScreenStepsAdvice stepAdvice) throws Throwable {

		//DevToolMessage message = stepAdvice.getDevToolMessage();
		UserInputTrainingRecord uitr = this;
		/*start with core html code,
		 * two scenarios are covered by following code
		 * 1) pioConfidence = 1, mlHtmlCode >= coreHtmlCode
		 * 2) pioConfidence <1, coreHtmlCode == mlHtmlCode*/
		if (this.getPioPredictConfidence().equals(1)) {
			String guid = RunnerGlobalUtils.parseFirstAteAttrByBoundary(uitr.getElementCoreHtmlCode(), IWebElementHandler.ELEMENT_FIND_ATE_ATTRIBUE_NAME);
			return this.tryClickGuidElementInHtmlCodeUntilSuccess(guid, this.getElementCoreHtmlCode(), executeEngine, stepAdvice, true);
		}

//		if (!this.getUserValues().isEmpty()) {
//			//might be file picker, need to run doFileInput
//			if (doFileInputInsideUitrHtmlCode(executeEngine, stepAdvice) == ActionResult.SUCCESS) {
//				return executeEngine.getCurrentExecutingScreenStepAdvices();
//			} else {
//				//the uitr core html doesn't have file input, we need to find the closest file input element and try
//				WebElement fileInput = null;
//				Optional<WebElement> followingFileInput = Optional.empty();
//				Optional<WebElement> precedingFileInput = Optional.empty();
//				try {
//					followingFileInput = Optional.of(executeEngine.getElementFinder(ElementFindByXpath.class).doFind(executeEngine.getMyWebDriver(), "(.//*[@ate-guid='" + guid +"']/following::input[@type='file'])[1]"));
//				} catch (NoSuchElementException | BrowserUnexpectedException exp) {
//
//				}
//				try {
//					precedingFileInput = Optional.of(executeEngine.getElementFinder(ElementFindByXpath.class).doFind(executeEngine.getMyWebDriver(), "(.//*[@ate-guid='" + guid +"']/preceding::input[@type='file'])[1]"));
//				} catch (NoSuchElementException | BrowserUnexpectedException exp) {
//
//				}
//				//List<WebElement> precedingFileInput = executeEngine.getMyWebDriver().getWebDriverInstance().findElements(By.xpath(("(.//*[@ate-guid='" + guid +"']/preceding::input[@type='file'])[1]")));
//				if (followingFileInput.isPresent() && precedingFileInput.isPresent()){
//
//					String followingFileInputGuid = followingFileInput.get().getAttribute("ate-guid");
//					String precedingFileInputGuid = precedingFileInput.get().getAttribute("ate-guid");
//					int followingFileInputDistance = executeEngine.caculateDomRelativeDistance(guid, followingFileInputGuid);
//					int precedingFileInputDistance = executeEngine.caculateDomRelativeDistance(guid, precedingFileInputGuid);
//					if (followingFileInputDistance < precedingFileInputDistance ) {
//						fileInput = followingFileInput.get();
//					} else {
//						fileInput = precedingFileInput.get();
//					}
//				} else if (precedingFileInput.isPresent()) {
//					fileInput = precedingFileInput.get();
//				} else if (followingFileInput.isPresent()){
//					fileInput = followingFileInput.get();
//				}
//				if (fileInput != null) {
//					if (fileInputAction(fileInput, this.getUserValues().iterator().next().getValue()) == ActionResult.SUCCESS) {
//						return executeEngine.getCurrentExecutingScreenStepAdvices();
//					}
//				}
//
//			}
//		}
		else {
			String guid = RunnerGlobalUtils.parseFirstAteAttrByBoundary(uitr.getElementCoreHtmlCode(), IWebElementHandler.ELEMENT_FIND_ATE_ATTRIBUE_NAME);//RunnerGlobalUtils.parseFirstAteGuidByBoundary(uitr.getInputMLHtmlCode());
			return this.tryClickGuidElementInHtmlCodeUntilSuccess(guid, this.getInputMLHtmlCode(), executeEngine, stepAdvice, true);
		}
//		try{
//			WebElement webE = executeEngine.getMyWebDriver().getWebDriverInstance().findElement(By.cssSelector("[ate-guid='" + guid +"']"));
//			webE.click();
//			Thread.sleep(5000);
//
//			//stepAdvice.setExecutedJumperElementInThisScreen(executedJumperElementInThisScreen);(Optional.of(uitr));
//
//			Map<ScreenNamePredictStrategy, ScreenStepsAdvice> newStepAdvices = executeEngine
//					.getStepAdvice(
//							executeEngine.downloadScreenMessage(),
//							executeEngine.getExecutedStepAdvices().get(
//									executeEngine.getExecutedStepAdvices()
//											.size() - 1));
////			String stepName = executeEngine.generateStepNaming(stepAdvice
////					.getScreenName(), uitr.getInputLabelName(), "click", "",
////					RunnerGlobalUtils.parseAteGuid(uitr
////							.getElementCoreHtmlCode()));
//			//executeEngine.getExecutedScreenHTMLSources().put(stepName, message.getPages());
//			//executeEngine.getExecutedScreenHTMLSources().put(stepName, stepAdvice);
//			//TODO add code to decide if this elementchanger prediction correct, if not will try the lower confidence sameLabeled elementChanger
//			List<String> allCurrentStepRunners = executeEngine
//					.getCurrentExecutingScreenStepAdvices()
//					.entrySet()
//					.stream()
//					.map(Map.Entry::getValue)
//					.collect(Collectors.toList())
//					.stream()
//					.map(ScreenStepsAdvice::getTestStepRunners)
//					.collect(Collectors.toList())
//					.stream()
//					.flatMap(Collection::stream)
//					.collect(Collectors.toList())
//					.stream()
//					.sorted(Comparator.comparing(runner -> runner
//							.getInputLabelName())).collect(Collectors.toList())
//							.stream().map(ITestStepRunner::getInputLabelName).collect(Collectors.toList());
//
//			List<String> allNewStepRunners = newStepAdvices
//					.entrySet()
//					.stream()
//					.map(Map.Entry::getValue)
//					.collect(Collectors.toList())
//					.stream()
//					.map(ScreenStepsAdvice::getTestStepRunners)
//					.collect(Collectors.toList())
//					.stream()
//					.flatMap(Collection::stream)
//					.collect(Collectors.toList())
//					.stream()
//					.sorted(Comparator.comparing(runner -> runner
//							.getInputLabelName())).collect(Collectors.toList())
//							.stream().map(ITestStepRunner::getInputLabelName).collect(Collectors.toList());
//
//			boolean sameScreen = allNewStepRunners.size() == allCurrentStepRunners.size() && allNewStepRunners.containsAll(allCurrentStepRunners);
//
//			if(sameScreen) {
//				//TODO deal with file upload dialog case. need to use user input handler to match
//				//if this is a file upload before click the element changer, file upload will directly use sendKeys to give value.
//				//this element changer is not predicted correctly, we need to try the archived changers
//			}
//
//			return newStepAdvices;
//		} catch (Throwable niwde) {
//
//			if (niwde instanceof ElementNotVisibleException) {
//				String newGuid = RunnerGlobalUtils.parsePreviousAteGuid(uitr.getInputMLHtmlCode(), guid);
//				if (!newGuid.isEmpty() && tryClickVisibleElementInMachineLearningHtmlCodeUntilSuccess(executeEngine.getMyWebDriver().getWebDriverInstance(), newGuid, uitr.getInputMLHtmlCode())) {
//					//stepAdvice.setExecutedJumperElementInThisScreen(Optional.of(uitr));
//
//					Map<ScreenNamePredictStrategy, ScreenStepsAdvice> newStepAdvices = executeEngine
//							.getStepAdvice(executeEngine.downloadScreenMessage(), stepAdvice);
//
//					return newStepAdvices;
//				} else {
//					throw niwde;
//				}
//			} else {
//				if (message != null) {
//					String msg1 = "Failed screen url is:  " + message.getDomainProtocol() +
//							"://" + message.getDomain() + message.getScreenUrl() + " with error: " + niwde.getMessage();
//					System.out.println(msg1);
//					LogbackWriter.writeAppInfo(msg1);
//				} else {
//					niwde.printStackTrace();
//				}
//				try {
//					executeEngine.getMyWebDriver().getMultiWindowsHandler().closeAllWindowsExceptMainWindow();
//				} catch (BrowserUnexpectedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//
//			}
//			throw niwde;
//		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getComparableLabelName() {
		return this.getPioPredictLabelResult().getValue();
	}



}
