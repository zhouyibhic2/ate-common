/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.runner;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Predicate;

import org.bigtester.ate.model.page.atewebdriver.IMyWebDriver;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.bigtester.RunnerGlobalUtils;
import com.bigtester.ate.tcg.runner.exception.NotInjectableWebDriverException;
import com.bigtester.ate.tcg.runner.model.DevToolMessage;

// TODO: Auto-generated Javadoc
/**
 * This class JsInjector defines ....
 *
 * @author Peidong Hu
 *
 */
public class JsInjector {

	/** The my web driver. */
	private final IMyWebDriver myWebDriver;

	private transient final JavascriptExecutor injectableWebDriver;

	/** The Constant JQUERY_JS_FILE_PATH. */
	private final static String JQUERY_JS_FILE_PATH = "js/jquery-3.0.0.min.js";

	/** The Constant JQUERY_EXISTS_FILE_PATH. */
	private final static String JQUERY_EXISTS_FILE_PATH = "js/messageback-jquery-existence.js";

	private final static String CHECK_PAGE_READY = "js/checkPageReady.js";

	/** The Constant MARK_PAGE_FILE_PATH. */
	private final static String MARK_PAGE_FILE_PATH = "js/messageback-mark-page-script.js";


	/** The Constant MARK_PAGE_FILE_PATH. */
	private final static String UNMARK_PAGE_FILE_PATH = "js/messageback-unmark-page-script.js";

	/** The Constant CALCULATE_HIRACHY_DISTANCE. */
	private final static String CALCULATE_HIRACHY_DISTANCE = "js/calculateHirachyLength.js";

	/**
	 * Instantiates a new js injector.
	 *
	 * @param myWebDriver
	 *            the my web driver
	 */
	public JsInjector(IMyWebDriver myWebDriver)
			throws NotInjectableWebDriverException {
		this.myWebDriver = myWebDriver;
		if (getMyWebDriver().getWebDriverInstance() instanceof JavascriptExecutor) {
			this.injectableWebDriver = (JavascriptExecutor) getMyWebDriver()
					.getWebDriverInstance();
		} else {
			throw new NotInjectableWebDriverException(
					"web driver not js injectable.");
		}
	}

	public boolean isJQueryExists() {
		boolean retVal = false;
		Optional<URL> urlJQueryExistCodeFilePath = Optional.ofNullable(this
				.getClass().getResource(JQUERY_EXISTS_FILE_PATH));

		String jQueryExistsCode = RunnerGlobalUtils
				.readStringFromURL(urlJQueryExistCodeFilePath.get());

		if ((Boolean) getInjectableWebDriver().executeScript(jQueryExistsCode)) {
			retVal = true;
		}
		return retVal;
	}

	/**
	 * Inject j query.
	 *
	 * @return true, if successful
	 */
	public boolean injectJQuery() {
		boolean retVal = false;
		//if (!isJQueryExists()) {
			Optional<URL> urlInjectJQueryCodeFilePath = Optional
					.ofNullable(this.getClass()
							.getResource(JQUERY_JS_FILE_PATH));
			String injectJQueryCode = RunnerGlobalUtils
					.readStringFromURL(urlInjectJQueryCodeFilePath.get());
			getInjectableWebDriver().executeScript(injectJQueryCode);
			//if (isJQueryExists())
				retVal = true;
			//else
			//	retVal = false;
		//}

		return retVal;
	}



	private void WaitForReady(JavascriptExecutor webDriver)
	{
		Optional<URL> urlJQueryExistCodeFilePath = Optional.ofNullable(this
				.getClass().getResource(CHECK_PAGE_READY));

		String checkPageReadyCode = RunnerGlobalUtils
				.readStringFromURL(urlJQueryExistCodeFilePath.get());

		FluentWait<JavascriptExecutor> wait = new FluentWait<JavascriptExecutor>( webDriver).withTimeout(30, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);;
	    Predicate<JavascriptExecutor> myPredicate = x ->(boolean)(x.
	            executeScript(checkPageReadyCode));

	    wait.until(myPredicate);
//	    {
//	        boolean isAjaxFinished = (boolean)getInjectableWebDriver().
//	            executeScript("return jQuery.active == 0");
//	        return isAjaxFinished;
//	    });
	}

	/**
	 * Inject page marker.
	 *
	 * @return true, if successful
	 */
	private String executePageMarker() {
		String retVal = "";
		//if (!isJQueryExists()) {
			//injectJQuery();
		//}
		//if (isJQueryExists()) {
			Optional<URL> urlInjectJQueryCodeFilePath = Optional
					.ofNullable(this.getClass()
							.getResource(JQUERY_JS_FILE_PATH));
			String injectJQueryCode = RunnerGlobalUtils
					.readStringFromURL(urlInjectJQueryCodeFilePath.get());
			Optional<URL> urlMarkPageJsCodeFilePath = Optional.ofNullable(this
					.getClass().getResource(MARK_PAGE_FILE_PATH));
			String injectMarkPageJsCode = RunnerGlobalUtils
					.readStringFromURL(urlMarkPageJsCodeFilePath.get());
			//if (this.isJQueryExists())
			WaitForReady(getInjectableWebDriver());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			retVal = (String) Optional.ofNullable(
					getInjectableWebDriver().executeScript(injectJQueryCode + "\n" +
							"ate_global_webdriver_executed_js = true; \n"
									+ injectMarkPageJsCode + "\n return getMarkedPagesInJson();")).orElse("");
			// retVal = (String) getInjectableWebDriver().executeScript(
			// injectMarkPageJsCode);

		//}
		return retVal;
	}

	/**
	 * Inject page marker.
	 *
	 * @return true, if successful
	 */
	public String executePageUnMarker() {
		String retVal = "";
		//if (!isJQueryExists()) {
			//injectJQuery();
		//}
		//if (isJQueryExists()) {
			Optional<URL> urlInjectJQueryCodeFilePath = Optional
					.ofNullable(this.getClass()
							.getResource(JQUERY_JS_FILE_PATH));
			String injectJQueryCode = RunnerGlobalUtils
					.readStringFromURL(urlInjectJQueryCodeFilePath.get());
			Optional<URL> urlMarkPageJsCodeFilePath = Optional.ofNullable(this
					.getClass().getResource(UNMARK_PAGE_FILE_PATH));
			String injectMarkPageJsCode = RunnerGlobalUtils
					.readStringFromURL(urlMarkPageJsCodeFilePath.get());
			//if (this.isJQueryExists())
			WaitForReady(getInjectableWebDriver());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			retVal = (String) Optional.ofNullable(
					getInjectableWebDriver().executeScript(injectJQueryCode + "\n" +
							"ate_global_webdriver_executed_js = true; \n"
									+ injectMarkPageJsCode + "\n return getMarkedPagesInJson();")).orElse("");
			// retVal = (String) getInjectableWebDriver().executeScript(
			// injectMarkPageJsCode);

		//}
		return retVal;
	}

	public int calculateWebElementCommonParentDepth(String guid1, String guid2) {
		int retVal = 0;
		//if (!isJQueryExists()) {
			//injectJQuery();
		//}
		//if (isJQueryExists()) {
			Optional<URL> urlInjectJQueryCodeFilePath = Optional
					.ofNullable(this.getClass()
							.getResource(JQUERY_JS_FILE_PATH));
			String injectJQueryCode = RunnerGlobalUtils
					.readStringFromURL(urlInjectJQueryCodeFilePath.get());
			Optional<URL> urlCalculateDistanceJsCodeFilePath = Optional.ofNullable(this
					.getClass().getResource(CALCULATE_HIRACHY_DISTANCE));
			String calculateDistanceJsCode = RunnerGlobalUtils
					.readStringFromURL(urlCalculateDistanceJsCodeFilePath.get());
			retVal = (int) Optional.ofNullable(
					getInjectableWebDriver().executeScript(injectJQueryCode + "\n" +
							"ate_global_webdriver_executed_js = true; \n"
									+ calculateDistanceJsCode + "\n return calculateDomDistance("+guid1+","+guid2+");")).orElse("");
			// retVal = (String) getInjectableWebDriver().executeScript(
			// injectMarkPageJsCode);

		//}
		return retVal;
	}

	public int calculateWebElementCommonParentDepthByXpath(String xpath1, String xpath2) {
		int retVal = 0;
		//if (!isJQueryExists()) {
			//injectJQuery();
		//}
		//if (isJQueryExists()) {
			Optional<URL> urlInjectJQueryCodeFilePath = Optional
					.ofNullable(this.getClass()
							.getResource(JQUERY_JS_FILE_PATH));
			String injectJQueryCode = RunnerGlobalUtils
					.readStringFromURL(urlInjectJQueryCodeFilePath.get());
			Optional<URL> urlCalculateDistanceJsCodeFilePath = Optional.ofNullable(this
					.getClass().getResource(CALCULATE_HIRACHY_DISTANCE));
			String calculateDistanceJsCode = RunnerGlobalUtils
					.readStringFromURL(urlCalculateDistanceJsCodeFilePath.get());
			retVal = (int) Optional.ofNullable(
					getInjectableWebDriver().executeScript(injectJQueryCode + "\n" +
							"ate_global_webdriver_executed_js = true; \n"
									+ calculateDistanceJsCode + "\n return calculateDomDistanceByXpath("+xpath1+","+xpath2+");")).orElse("");
			// retVal = (String) getInjectableWebDriver().executeScript(
			// injectMarkPageJsCode);

		//}
		return retVal;
	}


	/**
	 * Download frame pages.
	 *
	 * @return the dev tool message
	 */
	public Optional<DevToolMessage> downloadFramePages() {
		Optional<DevToolMessage> retVal = Optional.empty();
		String jsonStrDevToolMessage = "";
		if (!(jsonStrDevToolMessage = executePageMarker()).isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();

			// JSON from file to Object
			try {
				JsonNode jsonNode = mapper.readTree(jsonStrDevToolMessage);
				retVal = Optional.of(mapper.readValue(jsonNode.get("content"),
						DevToolMessage.class));
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return retVal;

	}

	/**
	 * @return the myWebDriver
	 */
	public IMyWebDriver getMyWebDriver() {
		return myWebDriver;
	}

	/**
	 * @return the injectableWebDriver
	 */
	public JavascriptExecutor getInjectableWebDriver() {
		return injectableWebDriver;
	}

}
