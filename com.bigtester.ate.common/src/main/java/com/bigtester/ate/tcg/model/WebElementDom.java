/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Optional;

import javax.xml.transform.TransformerException;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.bigtester.ate.tcg.controller.WebFormUserInputsCollectorHtmlTerms;
import com.bigtester.ate.tcg.controller.WebFormUserInputsCollectorHtmlTerms.HtmlInputType;
import com.bigtester.ate.tcg.controller.WebFormUserInputsCollectorHtmlTerms.UserCollectableHtmlTag;

import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord.UserInputType;
import com.bigtester.ate.tcg.utils.GlobalUtils;

// TODO: Auto-generated Javadoc
/**
 * This class WebElementDom defines ....
 * @author Peidong Hu
 *
 */
public class WebElementDom {
	public class InputType {
		final private WebFormUserInputsCollectorHtmlTerms.UserCollectableHtmlTag mainType;
		final private Optional<WebFormUserInputsCollectorHtmlTerms.HtmlInputType> subType;
		
		public InputType (WebFormUserInputsCollectorHtmlTerms.UserCollectableHtmlTag mainType, Optional<WebFormUserInputsCollectorHtmlTerms.HtmlInputType> subType) {
			this.mainType = mainType;
			this.subType = subType;
		}
		/**
		 * @return the subType
		 */
		public Optional<WebFormUserInputsCollectorHtmlTerms.HtmlInputType> getSubType() {
			return subType;
		}
		
		/**
		 * @return the mainType
		 */
		public WebFormUserInputsCollectorHtmlTerms.UserCollectableHtmlTag getMainType() {
			return mainType;
		}
		
	}
	private InputType coreNodeInputType;
	/** The dom node pointer. */
	private Node domNodePointer; //Dom node;
	
	/** The x path. */
	private String xPath;
	

	/** The dom node source. */
	private transient String domNodeSource = "";
	
	/**
	 * @return the coreNodeInputType
	 */
	public InputType getCoreNodeInputType() {
		if (coreNodeInputType==null) {
			coreNodeInputType = this.parseCoreNodeInputType();
			
		}
		return coreNodeInputType;
	}
	
	private InputType parseCoreNodeInputType() {
		UserCollectableHtmlTag mainType = UserCollectableHtmlTag.valueOf(this.domNodePointer.getNodeName().toUpperCase());
		Optional<HtmlInputType> subType = Optional.empty();
		if (this.domNodePointer.getNodeName().equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.UserCollectableHtmlTag.INPUT.toString())){
			String inputTypeValue = ((Element) this.domNodePointer).getAttribute("type").toUpperCase();
			if (inputTypeValue == null || inputTypeValue.isEmpty()) {
				subType = Optional.of(HtmlInputType.valueOf("TEXT"));
			} else {
				subType = Optional.of(HtmlInputType.valueOf(inputTypeValue));
			}
		} 
		
		return new InputType(mainType, subType);
		
	}


	/**
	 * Generate core dom node source.
	 *
	 * @return the string
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws TransformerException the transformer exception
	 */
	public String generateCoreDomNodeSource() throws IOException, TransformerException {
		String retVal;
		if (domNodeSource.isEmpty()) {
			StringBuffer temp = new StringBuffer("");
			
				if (null != domNodePointer) {
					ByteArrayOutputStream stringOutput = new ByteArrayOutputStream();
					GlobalUtils.printDocument(domNodePointer, stringOutput);
					//stringOutput.toString();
					temp.append(stringOutput.toString());
				}
			
			retVal = temp.toString();
			domNodeSource = retVal;
		} else
			retVal = domNodeSource;

		return GlobalUtils.removeAllXmlNamespace(retVal);
	}
	
	WebElementDom(Node domPointer) {
		this.domNodePointer = domPointer;
		xPath = "";
	}
	/**
	 * @return the domNodePointer
	 */
	public Node getDomNodePointer() {
		return domNodePointer;
	}
	/**
	 * @param domNodePointer the domNodePointer to set
	 */
	public void setDomNodePointer(Node domNodePointer) {
		this.domNodePointer = domNodePointer;
	}
	/**
	 * @return the xPath
	 */
	
	public String getXPath() {
		return xPath;
	}
	/**
	 * @param xPath the xPath to set
	 */
	public void setXPath(String xPath) {
		this.xPath = xPath;
	}
	
}
