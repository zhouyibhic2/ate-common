/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model.domain;

import static org.joox.JOOX.$;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.StreamSupport;

import javassist.bytecode.Descriptor.Iterator;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.WordUtils;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.jetty.util.ConcurrentHashSet;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.Relationship;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.bigtester.RunnerGlobalUtils;
import com.bigtester.ate.tcg.controller.WebFormUserInputsCollectorHtmlTerms;
import com.bigtester.ate.tcg.model.relationship.Relations;
import com.bigtester.ate.tcg.service.repository.PredictedFieldNameRepo;
import com.bigtester.ate.tcg.utils.GlobalUtils;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

// TODO: This class name is better to be changed to
//ScreenEntityTrainingRecord. The child of this class could
//have the filepicker's OK button or Cancel button
//and browser window close/min/max buttons. and other non-web page elements.

/**
 * This class TrainingRecord defines ....
 * @author Peidong Hu
 *
 */

/**
 * @author ashraf
 *
 */
@NodeEntity
@JsonIdentityInfo(generator=JSOGGenerator.class )
public class WebElementTrainingRecord extends BaseAteTestEntityNode{



	/**
	 * @return the id
	 */
	@Nullable
	public Long getId() {
		return getGraphId();
	}

	/** The triggered by. */
	@Relationship(type=Relations.TRIGGERED_BY,  direction = Relationship.INCOMING)
	@Nullable
	private ScreenElementChangeUITR triggeredBy;
	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(@Nullable Long id) {//NOPMD required by neo4j
		this.setGraphId(id);
	}
	/** The action trigger. */
	@Property
	private boolean actionTrigger;


	/** The belong to current test case. */
	private transient boolean belongToCurrentTestCase=true;

	/** The input ml html code changed by ui. */
	private transient boolean inputMLHtmlCodeChangedByUI=false;

	/** The input label name. */
	@Property

	private String inputLabelName; // htmlLabelContent

	/** The input ml html code. */
	@Property
	private String inputMLHtmlCode;

	@Property
	private String xpath;

	/** The element core html code. */
	@Property
	private String elementCoreHtmlCode = "";

	@JsonIgnore
	private transient Optional<Node> elementCoreHtmlNode = Optional.empty();

	/** The element core html code without guid value. */
	@Property
	private String elementCoreHtmlCodeWithoutGuidValue = "";

	/** The trained result. */
	@Property
	private String trainedResult; // "__ATE__Error___" or succeed with eventid


	/** The pio predict label result. */
	@Relationship(type=Relations.PREDICTED_FIELD_NAME)
	@JsonProperty
	private PredictedFieldName pioPredictLabelResult = new PredictedFieldName();
	///** The user value. */
	//private String userValue;

	/** The pio predict confidence. */
	@Property
	private Double pioPredictConfidence = - 1.0;

	/** The user values. */
	@Relationship(type=Relations.USER_VALUES)
	private Set<UserInputValue> userValues = new HashSet<UserInputValue>();

	/** The computed css sizes. */
	@Relationship(type=Relations.COMPUTED_CSS_SIZE)
	private Set<ComputedCssSize> computedCssSizes = new HashSet<ComputedCssSize>();

	/** The user input type. */
	@Property
	private UserInputType userInputType;

	private transient Set<UserInputType> alternativeUserInputTypes=new HashSet<UserInputType>();

	private transient long screenUitrProbability = 0;


	/**
	 * The Enum UserInputType. USERINPUT is pure user data input which doesn't
	 * trigger a new element or new page, it could be any type of html input
	 * element including text input, radio button, check box or selection
	 * dropdown. SREENJUMPER leads to a new screen InSCREENJUMPER triggers a in
	 * page elements changes, for example a new element added in this page
	 * (screen) The difference between screen jumper and inscreen jumper is
	 * really depends on how pio predicts. The phillosophy is that a minor
	 * change, not big enough to let pio recognized as a new screen.
	 *
	 * VERY IMPORTANT, pio training and prediction will use the combination of
	 * field name and userinputtype as the training/predict result. For example,
	 * a username field and userinput combination is the pio result, which is
	 * labaled as username_userinput. So the pio always returns result as,
	 * FieldName_UserInputType. Engine script needs to decouple the result to
	 * two variables, FieldName and UserInputType. This will improve the
	 * intelligence of the script to know what is expected after the action on a
	 * particular element, either a screen jump or in screen jump or nothing
	 * instead of a input
	 */
	public enum UserInputType {

		USERINPUT(1), SCREENJUMPER(4), INSCREENJUMPER(3), SCREENCHANGEUITR(2), unPredicted(5);
		private int value;

        private UserInputType(int value) {
                this.value = value;
        }
        public int getPriority() {
        	return value;
        }
	}

	// @Nullable
	// private boolean userFinalizedLabelResult; //user has revised the pio
	// predicted result if !=null.
	/**
	 * Instantiates a new user input training record.
	 */

	public WebElementTrainingRecord(String nodeLabelName) {
		super(nodeLabelName);
		inputLabelName = ""; // htmlLabelContent
		inputMLHtmlCode = "";
		trainedResult = ""; // "__ATE__Error___" or succeed with eventid
		userInputType = UserInputType.USERINPUT;
	}

	public WebElementTrainingRecord(String nodeLabelName, WebElementTrainingRecord uitr) {
		super(nodeLabelName);
		this.setId(uitr.getId());
		this.setActionTrigger(uitr.isActionTrigger());
		this.setInputLabelName(uitr.getInputLabelName());
		this.setInputMLHtmlCode(uitr.getInputMLHtmlCode());
		this.setPioPredictConfidence(uitr.getPioPredictConfidence());
		this.setTestcases(uitr.getTestcases());
		this.setTrainedResult(uitr.getTrainedResult());
		this.setUserInputType(UserInputType.USERINPUT);
		this.setUserValues(uitr.getUserValues());
		this.setComputedCssSizes(uitr.getComputedCssSizes());
		this.setPioPredictLabelResult(uitr.getPioPredictLabelResult());
		this.setBelongToCurrentTestCase(uitr.isBelongToCurrentTestCase());
		this.setElementCoreHtmlCode(uitr.getElementCoreHtmlCode());
		this.setElementCoreHtmlCodeWithoutGuidValue(uitr.getElementCoreHtmlCodeWithoutGuidValue());
		this.setTriggeredBy(uitr.getTriggeredBy());
		this.setScreenUitrProbability(uitr.getScreenUitrProbability());
		this.setXpath(uitr.getXpath());

	}

	/**
	 * Instantiates a new web element training record.
	 */
	public WebElementTrainingRecord() {
		super("WebElementTrainingRecord");
		inputLabelName = ""; // htmlLabelContent
		inputMLHtmlCode = "";
		trainedResult = ""; // "__ATE__Error___" or succeed with eventid
		userInputType = UserInputType.USERINPUT;
	}

	@Override
	public void assignedFrom(INonPointerAssignment value) {
		super.assignedFrom(value);
		if (value instanceof WebElementTrainingRecord) {
			WebElementTrainingRecord record = (WebElementTrainingRecord) value;
			this.setActionTrigger(record.isActionTrigger());
			this.setBelongToCurrentTestCase(record.isBelongToCurrentTestCase());
			this.setInputLabelName(record.getInputLabelName());
			this.setInputMLHtmlCode(record.getInputMLHtmlCode());
			this.setPioPredictConfidence(record.getPioPredictConfidence());
			this.setPioPredictLabelResult(record.getPioPredictLabelResult());
			this.setTestcases(record.getTestcases());
			this.setTrainedResult(record.getTrainedResult());
			this.setUserInputType(record.getUserInputType());
			this.setUserValues(record.getUserValues());
			this.setComputedCssSizes(record.getComputedCssSizes());
			this.setElementCoreHtmlCode(record.getElementCoreHtmlCode());
			this.setElementCoreHtmlCodeWithoutGuidValue(record.getElementCoreHtmlCodeWithoutGuidValue());
			this.setElementCoreHtmlNode(record.getElementCoreHtmlNode());
			this.setInputMLHtmlWords(record.getInputMLHtmlWords());
			this.setScreenUitrProbability(record.getScreenUitrProbability());
			this.setTriggeredBy(record.getTriggeredBy());
			this.setXpath(record.getXpath());
		}

	}

	/**
	 * Instantiates a new user input training record.
	 *
	 * @param labelName
	 *            the label name
	 * @param mlHtmlCode
	 *            the ml html code
	 */
	public WebElementTrainingRecord(String labelName, String mlHtmlCode, UserInputType userInputType, String nodeLabelName) {
		super(nodeLabelName);
		this.inputLabelName = labelName;
		this.inputMLHtmlCode = mlHtmlCode;
		trainedResult = ""; // "__ATE__Error___" or succeed with eventid

		this.userInputType = userInputType;
	}

	/**
	 * Instantiates a new web element training record.
	 *
	 * @param labelName the label name
	 * @param mlHtmlCode the ml html code
	 * @param userInputType the user input type
	 */
	public WebElementTrainingRecord(String labelName, String mlHtmlCode, UserInputType userInputType) {
		super("WebElementTrainingRecord");
		this.inputLabelName = labelName;
		this.inputMLHtmlCode = mlHtmlCode;
		trainedResult = ""; // "__ATE__Error___" or succeed with eventid

		this.userInputType = userInputType;
	}

	/**
	 * @return the inputLabelName
	 */

	public final String getInputLabelName() {
		return inputLabelName;
	}

	/**
	 * @param inputLabelName
	 *            the inputLabelName to set
	 */

	public final void setInputLabelName(String inputLabelName) {
		this.inputLabelName = inputLabelName;
	}

	/**
	 * @return the inputMLHtmlCode
	 */
	public final String getInputMLHtmlCode() {
		return inputMLHtmlCode;
	}

	/**
	 * @param inputMLHtmlCode
	 *            the inputMLHtmlCode to set
	 */
	public final void setInputMLHtmlCode(String inputMLHtmlCode) {
		this.inputMLHtmlCode = inputMLHtmlCode;
	}

	/**
	 * @return the trainedResult
	 */
	public String getTrainedResult() {
		return trainedResult;
	}

	/**
	 * @param trainedResult
	 *            the trainedResult to set
	 */
	public void setTrainedResult(String trainedResult) {
		this.trainedResult = trainedResult;
	}

	/**
	 * @return the pioPredictLabelResult
	 */
	public PredictedFieldName getPioPredictLabelResult() {
		return pioPredictLabelResult;
	}

	/**
	 * @param pioPredictLabelResult
	 *            the pioPredictLabelResult to set
	 */
	public void setPioPredictLabelResult(PredictedFieldName pioPredictLabelResult) {
		if (this.pioPredictLabelResult == null) this.pioPredictLabelResult = pioPredictLabelResult;
		else
			this.nonPointerAssign(pioPredictLabelResult, this.pioPredictLabelResult);
	}


	/**
	 * @return the userFinalizedLabelResult
	 */
	// public boolean isUserFinalizedLabelResult() {
	// return userFinalizedLabelResult;
	// }
	// /**
	// * @param userFinalizedLabelResult the userFinalizedLabelResult to set
	// */
	// public void setUserFinalizedLabelResult(boolean userFinalizedLabelResult)
	// {
	// this.userFinalizedLabelResult = userFinalizedLabelResult;
	// }

	/**
	 * @return the userInputType
	 */
	public UserInputType getUserInputType() {
		return userInputType;
	}

	/**
	 * @param userInputType the userInputType to set
	 */
	public void setUserInputType(UserInputType userInputType) {
		this.userInputType = userInputType;
	}

	/**
	 * @return the userValues
	 */
	public Set<UserInputValue> getUserValues() {
		return userValues;
	}

	/**
	 * @param userValues the userValues to set
	 */
	public void setUserValues(Set<UserInputValue> userValues) {
		this.nonPointerAssign(userValues, this.userValues, true);
	}

	/**
	 * @return the pioPredictConfidence
	 */
	public Double getPioPredictConfidence() {
		return pioPredictConfidence;
	}

	/**
	 * @param pioPredictConfidence the pioPredictConfidence to set
	 */
	public void setPioPredictConfidence(Double pioPredictConfidence) {
		this.pioPredictConfidence = pioPredictConfidence;
	}



	/**
	 * @return the actionTrigger
	 */
	public boolean isActionTrigger() {
		return actionTrigger;
	}

	/**
	 * @param actionTrigger the actionTrigger to set
	 */
	public void setActionTrigger(boolean actionTrigger) {
		this.actionTrigger = actionTrigger;
	}

	/**
	 * @return the belongToCurrentTestCase
	 */
	public boolean isBelongToCurrentTestCase() {
		return belongToCurrentTestCase;
	}

	/**
	 * @param belongToCurrentTestCase the belongToCurrentTestCase to set
	 */
	public void setBelongToCurrentTestCase(boolean belongToCurrentTestCase) {
		this.belongToCurrentTestCase = belongToCurrentTestCase;
	}


	@JsonIgnore
	public String getCoreGuid() {

		return RunnerGlobalUtils.parseFirstAteGuidByBoundary(this.getElementCoreHtmlCode());

	}
	/**
	 * @return the elementCoreHtmlCode
	 */
	public String getElementCoreHtmlCode() {
		return elementCoreHtmlCode;
	}

	public String parseDefaultLabelFromTextContent() {
		String label = "";

		if (this.getElementCoreHtmlNode().isPresent()) {
			Node cloned = this.getElementCoreHtmlNode().get().cloneNode(true);
			label = GlobalUtils.removeScriptStylesAndInvisiablesFromNode(cloned).getTextContent();
		} else {
			Optional<Element> coreElement = GlobalUtils.html2Element(elementCoreHtmlCode);
			if (coreElement.isPresent()) {
				Node cloned = coreElement.get().cloneNode(true);
				label = GlobalUtils.removeScriptStylesAndInvisiablesFromNode(cloned).getTextContent();
			} else {
				label = "";
			}
		}
		label = WordUtils.capitalize(label);
		label = label.replaceAll("[^a-zA-Z0-9]", "");

		//TODO need to put this 150 value into the property file
		if (label.length()>150) {
			//too long, this is useless to be used as default label;
			label = "";
		}
		return label;
	}

	private void collectWords(Node node, Set<String> collector) {
		if (node.getNodeType() == Node.TEXT_NODE) {
			collector.addAll(Arrays.asList(node.getNodeValue().replaceAll("[^A-Za-z0-9]", " ").split("\\s+")));
		}
		for (int index=0; index<node.getChildNodes().getLength(); index++) {
			collectWords(node.getChildNodes().item(index), collector);
		}
	}

	public Set<String> parseTextNodeWords() {
		Set<String> retVal = new HashSet<String>();
		collectWords(this.getElementCoreHtmlNode().orElse(GlobalUtils.html2Element(elementCoreHtmlCode).get()), retVal);
		return retVal;
	}

	private Map<String, String> lengthMostCloseLabel(Iterable<Map<String,String>> queryResults, String defaultLabel) {
		Map<String, String> retVal = queryResults.iterator().next();
		int distanceOfStringLenth = 1111110;
		for(java.util.Iterator<Map<String, String>> itr = queryResults.iterator(); itr.hasNext();) {
			Map<String, String> entry = itr.next();
			java.util.Iterator<Entry<String, String>> entryItr = entry.entrySet().iterator();
			Entry<String, String> labelEntry = entryItr.next();
			if ( Math.abs(labelEntry.getValue().length()
					- defaultLabel.length()) < distanceOfStringLenth ) {
				distanceOfStringLenth = Math.abs(labelEntry.getValue().length() - defaultLabel.length());
				retVal = entry;
			}
		}
		return retVal;
	}
	public Map<String, String> queryPossibleLabelFromNeo4j(PredictedFieldNameRepo neo4jRepo) {
		String label = parseDefaultLabelFromTextContent();
		String[] labels = label.split("(?=\\p{Upper})");

		for (int index=0; index<labels.length; index++) {
			//'(?i)(?=.*ti)(?=.*job).*ut.*'
			if (labels[index].length()>=3) {
				String queryKeyWordsRegex = "(?i)";

				for (int ind=index; ind<labels.length; ind++) {

					if (ind == labels.length-1){
						//last word
						queryKeyWordsRegex = queryKeyWordsRegex + ".*" + labels[ind] + ".*";
					} else {
						queryKeyWordsRegex = queryKeyWordsRegex + "(?=.*" + labels[ind] + ")";
					}
				}
				if (!queryKeyWordsRegex.equalsIgnoreCase("(?i)")) {
					Iterable<Map<String, String>> matchedLabelValues = neo4jRepo.getDistinctMatchingFieldNames(queryKeyWordsRegex);
					if (matchedLabelValues.iterator().hasNext()) {
						return lengthMostCloseLabel(matchedLabelValues,label);
					}
				}
			}
		}
		return new HashMap<String,String>();
	}
	/**
	 * @param elementCoreHtmlCode the elementCoreHtmlCode to set
	 */
	public void setElementCoreHtmlCode(String elementCoreHtmlCode) {
		this.elementCoreHtmlCode = elementCoreHtmlCode;
	}

	/**
	 * @return the triggeredBy
	 */
	//@Relationship(type=Relations.TRIGGERED_BY,  direction = Relationship.INCOMING)
	public ScreenElementChangeUITR getTriggeredBy() {
		return triggeredBy;
	}

	/**
	 * @param triggeredBy the triggeredBy to set
	 */
	//see here https://github.com/neo4j/neo4j-ogm/issues/56 for the reason setting relationship annotion here.
	@Relationship(type=Relations.TRIGGERED_BY,  direction = Relationship.INCOMING)
	public void setTriggeredBy(ScreenElementChangeUITR triggeredBy) {
		this.triggeredBy = triggeredBy;
	}

	/**
	 * @return the elementCoreHtmlCodeWithoutGuidValue
	 */
	public String getElementCoreHtmlCodeWithoutGuidValue() {
		return elementCoreHtmlCodeWithoutGuidValue;
	}

	/**
	 * @param elementCoreHtmlCodeWithoutGuidValue the elementCoreHtmlCodeWithoutGuidValue to set
	 */
	public void setElementCoreHtmlCodeWithoutGuidValue(
			String elementCoreHtmlCodeWithoutGuidValue) {
		this.elementCoreHtmlCodeWithoutGuidValue = elementCoreHtmlCodeWithoutGuidValue;
	}

	/**
	 * @return the screenUitrProbability
	 */
	public long getScreenUitrProbability() {
		return screenUitrProbability;
	}

	/**
	 * @param screenUitrProbability the screenUitrProbability to set
	 */
	public void setScreenUitrProbability(long screenUitrProbability) {
		this.screenUitrProbability = screenUitrProbability;
	}

	/**
	 * @return the alternativeUserInputTypes
	 */
	public Set<UserInputType> getAlternativeUserInputTypes() {
		return alternativeUserInputTypes;
	}

	/**
	 * @param alternativeUserInputTypes the alternativeUserInputTypes to set
	 */
	public void setAlternativeUserInputTypes(
			Set<UserInputType> alternativeUserInputTypes) {
		this.alternativeUserInputTypes = alternativeUserInputTypes;
	}

	/**
	 * @return the elementCoreHtmlNode
	 */
	public Optional<Node> getElementCoreHtmlNode() {
		return elementCoreHtmlNode;
	}

	/**
	 * @param elementCoreHtmlNode the elementCoreHtmlNode to set
	 */
	public void setElementCoreHtmlNode(Optional<Node> elementCoreHtmlNode) {
		this.elementCoreHtmlNode = elementCoreHtmlNode;
	}


	/**
	 * @return the computedCssSizes
	 */
	public Set<ComputedCssSize> getComputedCssSizes() {
		return computedCssSizes;
	}

	/**
	 * @param computedCssSizes the computedCssSizes to set
	 */
	public void setComputedCssSizes(Set<ComputedCssSize> computedCssSizes) {
		this.nonPointerAssign(computedCssSizes, this.computedCssSizes);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getIdentifierString() {

		return getElementCoreHtmlCodeWithoutGuidValue() ;
	}

	public List<String> parseDirectHtmlChildren(boolean includeInVisibles) {

			List<String> retVal = new ArrayList<String>();
			try {

				Document domMlHtmlSource = GlobalUtils.html2Dom(this.inputMLHtmlCode);
				NodeList directChildren = GlobalUtils.getBodyEquevelantElement(domMlHtmlSource).getChildNodes();
				if (directChildren.getLength()==1) {

					retVal.add(this.inputMLHtmlCode);
				} else {
					for (int index=0; index<directChildren.getLength(); index++) {
						if (directChildren.item(index).getNodeType()==Node.ELEMENT_NODE ) {
							String invisible = Optional
									.ofNullable(
											((Element) directChildren.item(index))
													.getAttribute(WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME))
									.orElse("");
							if (!invisible
									.equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE)) {
								retVal.add(GlobalUtils.domNodeToString(directChildren.item(index), true, true));
							}
						}
					}
				}
				return retVal;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				retVal.add(this.inputMLHtmlCode);
				return retVal;
			}
		}

	public List<WebElementTrainingRecord> parseChildUitrs() {
		List<String> childrenMlCode = this.parseDirectHtmlChildren(false);
		List<WebElementTrainingRecord> retVal = new ArrayList<WebElementTrainingRecord>();
		if (childrenMlCode.size()>1) {
			childrenMlCode.forEach(code->{
				WebElementTrainingRecord uitr = new WebElementTrainingRecord(this.getNodeLabelName(), this);

				uitr.setInputMLHtmlCode(code);

				uitr.setInputMLHtmlWords(GlobalUtils.removeTagAndAttributeNamesAndDigits(code));

				uitr.setElementCoreHtmlCode(code);
				uitr.setElementCoreHtmlCodeWithoutGuidValue(GlobalUtils.convertToComparableString(uitr.getElementCoreHtmlCode()));

				uitr.setElementCoreHtmlNode(GlobalUtils.html2Node(code));
				uitr.setGraphId(null);
				uitr.setId(null);

				ComputedCssSize size = GlobalUtils.parseComputedCssSize(uitr
						.getInputMLHtmlCode());
				int screenWidth = this.getComputedCssSizes().iterator().next().getScreenWidth();
				int topDocWidth = this.getComputedCssSizes().iterator().next().getTopDocumentWidth();
				int topDocHeight = this.getComputedCssSizes().iterator().next().getTopDocumentHeight();
				uitr.getComputedCssSizes().clear();
				//uitr.getComputedCssSizes().add(size);
				GlobalUtils.normalizeUitr(uitr, screenWidth, topDocWidth, topDocHeight);
				retVal.add(uitr);
			});
		} else {
			retVal.add(this);
		}
		//System.out.println("a");
		return retVal;
	}


	public long calculateAteCollectableTagsCount(List<String> userCollectableTags) {
		String[] aLinkArray = {"a"};
		List<String> allTags = new ArrayList<String>(Arrays.asList(aLinkArray));
		allTags.addAll(userCollectableTags);
		String mlHtmlSource = this.getInputMLHtmlCode();
		long inputCount=0l;
		for (int index =0; index<allTags.size();index ++) {
			try {
				Document domMlHtmlSource = GlobalUtils.html2Dom(mlHtmlSource);
				inputCount = inputCount
						+ $(domMlHtmlSource)
								.find(allTags
										.get(index)).not("["
										+ WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME
										+ "=\""
										+ WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE
										+ "\"]")
								.get()
								.size();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

			}
		}
		return inputCount;
	}

	public List<Element> parseAteCollectableTaggedElements(List<String> userCollectableTags) {
		String[] aLinkArray = {"a"};
		List<String> allTags = new ArrayList<String>(Arrays.asList(aLinkArray));
		allTags.addAll(userCollectableTags);
		String mlHtmlSource = this.getInputMLHtmlCode();
		List<Element> retVal = new ArrayList<Element>();
		for (int index =0; index<allTags.size();index ++) {
			try {
				Document domMlHtmlSource = GlobalUtils.html2Dom(mlHtmlSource);
				retVal.addAll($(domMlHtmlSource)
								.find(allTags
										.get(index)).not("["
										+ WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME
										+ "=\""
										+ WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE
										+ "\"]")
								.get());

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

			}
		}
		return retVal;
	}

	/**
	 * @return the inputMLHtmlCodeChangedByUI
	 */
	public boolean isInputMLHtmlCodeChangedByUI() {
		return inputMLHtmlCodeChangedByUI;
	}

	/**
	 * @param inputMLHtmlCodeChangedByUI the inputMLHtmlCodeChangedByUI to set
	 */
	public void setInputMLHtmlCodeChangedByUI(boolean inputMLHtmlCodeChangedByUI) {
		this.inputMLHtmlCodeChangedByUI = inputMLHtmlCodeChangedByUI;
	}

	/**
	 * @return the xpath
	 */
	public String getXpath() {
		return xpath;
	}

	/**
	 * @param xpath the xpath to set
	 */
	public void setXpath(String xpath) {
		this.xpath = xpath;
	}


}
