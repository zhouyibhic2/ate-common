/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2017, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.service.parallel;
import com.google.common.util.concurrent.Monitor;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;


// TODO: Auto-generated Javadoc
/**
 * This class PausableFutureThreadExecutor defines ....
 * @author Peidong Hu
 *
 */
public class PausableThreadExecutor extends ScheduledThreadPoolExecutor{


	    private boolean isPaused;

	    private final Monitor monitor = new Monitor();
	    private final Monitor.Guard paused = new Monitor.Guard(monitor) {
	        @Override
	        public boolean isSatisfied() {
	            return isPaused;
	        }
	    };

	    private final Monitor.Guard notPaused = new Monitor.Guard(monitor) {
	        @Override
	        public boolean isSatisfied() {
	            return !isPaused;
	        }
	    };

	    public PausableThreadExecutor(int corePoolSize, ThreadFactory threadFactory) {
	        super(corePoolSize, threadFactory);
	    }

	    protected void beforeExecute(Thread t, Runnable r) {
	        super.beforeExecute(t, r);
	        monitor.enterWhenUninterruptibly(notPaused);
	        try {
	            monitor.waitForUninterruptibly(notPaused);
	        } finally {
	            monitor.leave();
	        }
	    }

	    public void pause() {
	        monitor.enterIf(notPaused);
	        try {
	            isPaused = true;
	        } finally {
	            monitor.leave();
	        }
	    }

	    public void resume() {
	        monitor.enterIf(paused);
	        try {
	            isPaused = false;
	        } finally {
	            monitor.leave();
	        }
	    }
	
}
