/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model.relationship;

// TODO: Auto-generated Javadoc
/**
 * This class Relations defines ....
 * @author Peidong Hu
 *
 */
public final class Relations {

	/** The Constant STEP_INTO. */
	final public static String STEP_IN = "STEP_IN";

	/** The Constant STEP_OUT. */
	final public static String STEP_OUT = "STEP_OUT";

	/** The Constant CONTAINS_SUB_INDUSTRY_CATEGORY. */
	final public static String CONTAINS_SUB_INDUSTRY_CATEGORY = "CONTAINS_SUB_INDUSTRY_CATEGORY";


	/** The Constant PARENT_TEST_SUITE. */
	final public static String SUB_SUITE_OF= "SUB_SUITE_OF";

	/** The Constant HOSTING_TEST_SUITE. */
	final public static String HOSTING_TEST_SUITE= "HOSTING_TEST_SUITE";

	/** The Constant PARENT_INDUSTRY_CATEGORY. */
	final public static String PARENT_INDUSTRY_CATEGORY = "PARENT_INDUSTRY_CATEGORY";

	/** The Constant SOURCED_FROM. */
	final public static String SOURCING_DOMS = "SOURCING_DOMS";

	/** The Constant PREDICTED_INTO. */
	final public static String PREDICTED_USER_INPUT_RESULTS = "PREDICTED_USER_INPUT_RESULTS";

	/** The Constant PREDICTED_USER_ACTIONELEMENT_RESULTS. */
	final public static String PREDICTED_USER_SCREENJUMP_ELEMENT_RESULTS = "PREDICTED_USER_SCREENJUMP_ELEMENT_RESULTS";//NOPMD

	/** The Constant PREDICTED_USER_ACTIONELEMENT_RESULTS. */
	final public static String PREDICTED_USER_IN_SCREENJUMP_ELEMENT_RESULTS = "PREDICTED_USER_IN_SCREENJUMP_ELEMENT_RESULTS";//NOPMD

	/** Screen change could be in same screen, but also in other screen. will have another relationship to indicate which screennode has element changed by this result.*/
	final public static String PREDICTED_USER_SCREEN_ELEMENT_CHANGE_RESULTS = "PREDICTED_USER_SCREEN_ELEMENT_CHANGE_RESULTS";//NOPMD

	/** The Constant CHANGE_SCREEN_ELEMENT. this relationship only changes the screen elements, but not trigger screen jumping*/
	final public static String CHANGE_SCREEN_ELEMENT = "CHANGE_SCREEN_ELEMENT";

	/** The Constant USER_VALUES. */
	final public static String USER_VALUES = "USER_VALUES";

	/** The Constant COMPUTED_CSS_SIZE. */
	final public static String COMPUTED_CSS_SIZE = "COMPUTED_CSS_SIZE";

	/** The Constant PREDICTED_FIELD_NAME. */
	final public static String PREDICTED_FIELD_NAME = "PREDICTED_FIELD_NAME";

	final public static String CHILD_POSITION_UITR_IN_GROUP = "CHILD_POSITION_UITR_IN_GROUP";

	/** The Constant IN_TESTCASE. */
	final public static String IN_TESTCASE = "IN_TESTCASE";

	/** The Constant CONTAINS_SCREEN. */
	final public static String CONTAINS_SCREEN = "CONTAINS_SCREEN";

	final public static String TRIGGERED_BY = "TRIGGERED_BY";

	private Relations(){};


}
