/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model.domain;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.Relationship;

import com.bigtester.ate.tcg.model.relationship.InTestCase;
import com.bigtester.ate.tcg.model.relationship.Relations;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

// TODO: Auto-generated Javadoc
/**
 * This class BaseAteScreenEntityNode defines ....
 * @author Peidong Hu
 *
 */
//@JsonIdentityInfo(generator=JSOGGenerator.class )
//@JsonTypeInfo(
//	    use = JsonTypeInfo.Id.MINIMAL_CLASS,
//	    include = JsonTypeInfo.As.PROPERTY,
//	    property = "@class")
@JsonIdentityInfo(generator=JSOGGenerator.class )
abstract public class BaseAteTestEntityNode extends BaseAteNode {
	
	@Property
	private String inputMLHtmlWords;
	/** The testcases. */
	@Relationship(type = Relations.IN_TESTCASE, direction=Relationship.OUTGOING)
	@JsonManagedReference
	private Set<InTestCase> testcases = new HashSet<InTestCase>();
	
		

	/**
	 * Adds the into test case.
	 *
	 * @param node the node
	 * @param testcase the testcase
	 * @param contextScreen the context screen
	 * @return the in test case
	 */
	public InTestCase addIntoTestCase(WebElementTrainingRecord node, TestCase testcase, Neo4jScreenNode contextScreen) {
		InTestCase retVal;
		final InTestCase inTestCase = new InTestCase(node, testcase, contextScreen);
		retVal = inTestCase;
		if (!this.testcases.contains(inTestCase)){
			this.testcases.add(inTestCase);
			//testcase.getTestCaseNodeRelationships().add(inTestCase);
		}
		else {
			Iterator<InTestCase> itr = this.testcases.iterator();
			while(itr.hasNext()) {
				InTestCase temp = itr.next();
				if (temp.equals(inTestCase)) {
					if (!temp.getContextScreenGraphIds().contains(contextScreen.getGraphId())) {
						temp.getContextScreenGraphIds().add(contextScreen.getGraphId());
						retVal = temp; 
					}
				}
			}
		}
		return retVal;
	}
	/**
	 * @return the inputMLHtmlWords
	 */
	public String getInputMLHtmlWords() {
		return inputMLHtmlWords;
	}

	/**
	 * @param inputMLHtmlWords the inputMLHtmlWords to set
	 */
	public void setInputMLHtmlWords(String inputMLHtmlWords) {
		this.inputMLHtmlWords = inputMLHtmlWords;
	}
	/**
	 * Adds the into test case.
	 *
	 * @param node the node
	 * @param testcase the testcase
	 * @param contextScreen the context screen
	 * @return the in test case
	 */
	public InTestCase removeFromTestCase(WebElementTrainingRecord node, TestCase testcase, Neo4jScreenNode contextScreen) {
		InTestCase retVal;
		final InTestCase inTestCase = new InTestCase(node, testcase, contextScreen);
		retVal = inTestCase;
		if (this.testcases.contains(inTestCase)){
			this.testcases.remove(inTestCase);
		}
		return retVal;
	}
	
	/**
	 * Adds the into test case.
	 *
	 * @param node the node
	 * @param testcase the testcase
	 * @return the in test case
	 */
	public InTestCase addIntoTestCase(BaseAteTestEntityNode node, TestCase testcase) {
		InTestCase retVal;
		final InTestCase inTestCase = new InTestCase(node, testcase);
		retVal = inTestCase;
		if (!this.testcases.contains(inTestCase)){
			testcases.add(inTestCase);
			//testcase.getTestCaseNodeRelationships().add(inTestCase);
		}
		return retVal;
	}

	/**
	 * @param nodeLblName
	 */
	public BaseAteTestEntityNode(String nodeLblName) {
		super(nodeLblName);
		
	}
	/**
	 * @return the testcases
	 */
	@JsonManagedReference
	public Collection<InTestCase> getTestcases() {
		return testcases;
	}

	/**
	 * @param testcases the testcases to set
	 */
	@JsonManagedReference
	public void setTestcases(Collection<InTestCase> testcases) {
		this.nonPointerAssign(testcases, this.testcases);
		//this.testcases = testcases;
	}

}
