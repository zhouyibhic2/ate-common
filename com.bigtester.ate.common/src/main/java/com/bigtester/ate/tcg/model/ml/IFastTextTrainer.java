package com.bigtester.ate.tcg.model.ml;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.bigtester.ate.tcg.model.ml.ITrainingEntityPioRepo.MyPair;

///don't delete, this is fasttext trainer
public interface IFastTextTrainer extends ITebloudMachine {

	static final String CURRENTDIR = StringUtils.isEmpty(System.getProperty("catalina.base"))
			? System.getProperty("user.dir") + "/"
			: System.getProperty("catalina.base") + "/";

	static final String FAST_TEXT_TRAINING_DATA_FOLDER_ROOT = "src/test/resources/data/";
	static final String FAST_TEXT_TRAINING_DATA_FILE_NAME = "labeled_data.txt";
	static final String FAST_TEXT_TRAINING_MODELS_FOLDER_ROOT = "src/test/resources/models/";
	static final String FAST_TEXT_TRAINING_MODELS_FILE_PREFIX = "supervised.model";
	static final String FAST_TEXT_TRAINING_MODELS_VEC_FILE_NAME = FAST_TEXT_TRAINING_MODELS_FILE_PREFIX + ".vec";

	static final String FAST_TEXT_TRAINING_UNKNOWN_WORD = "==UNKNOWN==";
	public String trainFastTextModelForAppEngine(List<MyPair> data);

	public static String getVecFilePathName(String autName) {
		return CURRENTDIR + FAST_TEXT_TRAINING_MODELS_FOLDER_ROOT + autName + "/" + FAST_TEXT_TRAINING_MODELS_VEC_FILE_NAME;
	}

}
