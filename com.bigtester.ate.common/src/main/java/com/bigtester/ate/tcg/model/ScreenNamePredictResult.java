/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model;

// TODO: Auto-generated Javadoc
/**
 * This class ScreenNamePredictResult defines ....
 * @author Peidong Hu
 *
 */
public class ScreenNamePredictResult {

	final private String screenTrainingWords;
	final private Double confidence;
	final private String screenName;
	
	/**
	 * 
	 */
	public ScreenNamePredictResult(String screenTrainingWords, Double confidence, String screenName) {
		this.screenTrainingWords = screenTrainingWords;
		this.confidence = confidence;
		this.screenName = screenName;
	}
	public ScreenNamePredictResult() {
		this.screenTrainingWords = "";
		this.confidence = 0.0d;
		this.screenName = "";
	}

	/**
	 * @return the screenTrainingWords
	 */
	public String getScreenTrainingWords() {
		return screenTrainingWords;
	}

	/**
	 * @return the confidence
	 */
	public Double getConfidence() {
		return confidence;
	}

	/**
	 * @return the screenName
	 */
	public String getScreenName() {
		return screenName;
	}

	
	
}
