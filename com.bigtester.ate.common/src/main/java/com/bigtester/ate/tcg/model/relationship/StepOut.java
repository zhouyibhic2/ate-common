/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model.relationship;

import org.eclipse.jdt.annotation.Nullable;
import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

import com.bigtester.ate.tcg.model.ATENeo4jNodeComparision;
import com.bigtester.ate.tcg.model.domain.AbstractScreenNode;
import com.bigtester.ate.tcg.model.domain.BaseAteTestEntityNode;
import com.bigtester.ate.tcg.model.domain.Neo4jScreenNode;
import com.bigtester.ate.tcg.model.domain.ScreenJumperElementTrainingRecord;
import com.bigtester.ate.tcg.model.domain.UserInputTrainingRecord;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

// TODO: Auto-generated Javadoc
/**
 * This class StepInto defines ....
 * @author Peidong Hu
 *
 */
@RelationshipEntity (type=Relations.STEP_OUT)
@JsonIdentityInfo(generator=JSOGGenerator.class)
//@JsonIdentityInfo(generator=ObjectIdGenerators.UUIDGenerator.class, property="@uuid")
public class StepOut {
	
	public static enum StepOutTriggerType{
		SCREENJUMPERSTEPOUT, INSCREENJUMPERSTEPOUT, SCREENELEEMENTCHANGESTEPOUT
	}
	
	/** The node id. */
	@Nullable 
	@GraphId
	private Long nodeId;
	
	/** The start node. */
	@StartNode
	@Nullable
	//@JsonBackReference
	//TODO use back reference
	private UserInputTrainingRecord startNode;
	
	/** The end node. */
	@EndNode
	@Nullable
	private Neo4jScreenNode endNode;
	
	/** The trigger type. */
	@Property
	@Nullable
	private StepOutTriggerType triggerType;
	/**
	 * Instantiates a new step into.
	 *
	 * @param startNode the start node
	 * @param endNode the end node
	 * @param uitrId the uitr id
	 */
	public StepOut(UserInputTrainingRecord startNode, Neo4jScreenNode endNode, StepOutTriggerType triggerType) {
		this.startNode = startNode;
		this.endNode = endNode; 
		this.triggerType = triggerType;
	}
	
	/**
	 * Instantiates a new step into.
	 */
	public StepOut() {//NOPMD
		super();
	}
	/**
	 * @return the startNode
	 */
	@Nullable
	//@JsonBackReference
	public UserInputTrainingRecord getStartNode() {
		return startNode;
	}

//	/**
//	 * @param startNode the startNode to set
//	 */
//	@JsonBackReference
//	public void setStartNode(Neo4jScreenNode startNode) {
//		this.startNode = startNode;
//	}

	/**
	 * @return the endNode
	 */
	@Nullable
	
	public Neo4jScreenNode getEndNode() {
		return endNode;
	}

	/**
	 * @param endNode the endNode to set
	 */
	
	public void setEndNode(Neo4jScreenNode endNode) {
		this.endNode = endNode;
	}


	 
	/**
	 * @return the nodeId
	 */
	@Nullable
	public Long getNodeId() {
		return nodeId;
	}

	/**
	 * @param nodeId the nodeId to set
	 */
	public void setNodeId(Long nodeId) {
		this.nodeId = nodeId;
	}
	@Override
	public boolean equals(@Nullable Object obj) {
	    if (obj == null) {
	        return false;
	    }
	    if (!StepOut.class.isAssignableFrom(obj.getClass())) {
	        return false;
	    }
	    final StepOut other = (StepOut) obj;
	    if ((this.startNode == null) ? (other.startNode != null) : !this.startNode.equals(other.startNode)) {
	        return false;
	    }
	    if ((this.endNode == null) ? (other.endNode != null) : !this.endNode.equals(other.endNode)) {
	        return false;
	    }
	    return true;
	}

	@Override
	public int hashCode() {
	    int hash = 3;
	    hash = 53 * hash + (this.startNode != null ? this.startNode.hashCode() : 0);
	    hash = 53 * hash + (this.endNode != null ? this.endNode.hashCode() : 0);
	    return hash;
	}
 
	/**
	 * @param triggerType the triggerType to set
	 */
	public void setTriggerType(StepOutTriggerType triggerType) {
		this.triggerType = triggerType;
	}

	/**
	 * @return the triggerType
	 */
	public StepOutTriggerType getTriggerType() {
		return triggerType;
	}

	/**
	 * @param startNode the startNode to set
	 */
	//@JsonBackReference
	public void setStartNode(UserInputTrainingRecord startNode) {
		this.startNode = startNode;
	} 
}
