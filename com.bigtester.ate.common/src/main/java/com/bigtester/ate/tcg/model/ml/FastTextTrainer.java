package com.bigtester.ate.tcg.model.ml;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.bigtester.ate.tcg.model.ml.ITrainingEntityPioRepo.MyPair;
import com.bigtester.tebloud.machine.fasttext.config.TrainingParams;


///don't delete, this is fasttext trainer
public class FastTextTrainer extends TebloudMachine implements IFastTextTrainer{
	final private TrainingParams trainingParams;
	/**
	 * @param autName
	 */
	public FastTextTrainer(String autName, TrainingParams trainingParams) {
		super(autName, new AteFastText());
		this.trainingParams = trainingParams;

	}

	public static String __label__ = "__label__";

	static void storeTextLinesToFile(List<String> listOfTextLines, String fileName) {
        try {
            PrintWriter outputFile = new PrintWriter(new FileWriter(fileName, false));

            for (String line : listOfTextLines) {
                outputFile.println(line);
            }

            outputFile.close();

        } catch (IOException ioException) {
            System.out.println("\n\n Cannot write to file \"" + fileName + "\"");
        }
    }


  public void writeEmptyFile(String subFolderName){
	    String PATH = IFastTextTrainer.CURRENTDIR + IFastTextTrainer.FAST_TEXT_TRAINING_DATA_FOLDER_ROOT+subFolderName+"/";
	    String directoryName = PATH;
	    String fileName = IFastTextTrainer.FAST_TEXT_TRAINING_DATA_FILE_NAME;

	    File directory = new File(directoryName);
	    if (! directory.exists()){
	        directory.mkdir();
	        // If you require it to make the entire directory path including parents,
	        // use directory.mkdirs(); here instead.
	    }

	    File file = new File(directoryName + "/" + fileName);
	    try{
	        file.createNewFile();
	    }
	    catch (IOException e){
	        e.printStackTrace();
	        System.exit(-1);
	    }

	    String MODELPATH = IFastTextTrainer.CURRENTDIR + IFastTextTrainer.FAST_TEXT_TRAINING_MODELS_FOLDER_ROOT+subFolderName+"/";
	    String modelDirectoryName = MODELPATH;
	    File modelDirectory = new File(modelDirectoryName);
	    if (! modelDirectory.exists()){
	        modelDirectory.mkdir();
	        // If you require it to make the entire directory path including parents,
	        // use directory.mkdirs(); here instead.
	    }

	}

  public String trainFastTextModelForAppEngine(List<MyPair> data) {
	  final List<MyPair> newPairs = new ArrayList<MyPair>();
	  if (data.size()<500) {
		  Map<String, Integer> wordCounts = Arrays.stream(data.stream().map(pair->{
			  return pair.value();
		  }).collect(Collectors.joining(" ")).split("\\s+")).collect(Collectors.toMap(k -> k, v -> 1, (v1,v2) -> v1+v2));
		  if (wordCounts.entrySet().stream().filter(entry->entry.getValue()<2).count()>0)
			  wordCounts.entrySet().removeIf(entry->entry.getValue()>=2);
		  else if (wordCounts.entrySet().stream().filter(entry->entry.getValue()<3).count()>0)
			  wordCounts.entrySet().removeIf(entry->entry.getValue()>=3);
		  else if (wordCounts.entrySet().stream().filter(entry->entry.getValue()<4).count()>0)
			  wordCounts.entrySet().removeIf(entry->entry.getValue()>=4);
		  Set<String> lowFrenquencyWords = wordCounts.keySet();
		  lowFrenquencyWords.removeIf(str->StringUtils.isEmpty(str));
		  data.forEach(pair->{
			  String text=pair.value();
			  if (lowFrenquencyWords.size()>=2) {
					    Iterator<String> itr = lowFrenquencyWords.iterator();

						  for (int index=0; index<lowFrenquencyWords.size()/2; index++) {
							  String lowFWord = itr.next();
							  if (text.contains(lowFWord))
							  text = text.replaceAll(" " + lowFWord + " ", " " + FAST_TEXT_TRAINING_UNKNOWN_WORD + " ");
						  }

			  }
			  else if (lowFrenquencyWords.size()==1){
				  Iterator<String> itr = lowFrenquencyWords.iterator();
				      for (int index=0; index<lowFrenquencyWords.size(); index++) {
						  String lowFWord = itr.next();
						  if (text.contains(lowFWord))
						  text = text.replaceAll(" " + lowFWord + " ", " " + FAST_TEXT_TRAINING_UNKNOWN_WORD + " ");
					  }

			  }
			  newPairs.add(new MyPair(pair.key(), text));

		  });

	  } else {
		  newPairs.addAll(data);
	  }
	  data = newPairs;

	  writeEmptyFile(this.getAutName());
	  storeTextLinesToFile(data.stream().map(pair->{
		  return __label__ + pair.key() + " " + pair.value().replaceAll("\\(", "").replaceAll("\\)", "");
	  }).collect(Collectors.toList()), IFastTextTrainer.CURRENTDIR + IFastTextTrainer.FAST_TEXT_TRAINING_DATA_FOLDER_ROOT+getAutName()+"/" + IFastTextTrainer.FAST_TEXT_TRAINING_DATA_FILE_NAME);

	  IAteFastText jft = new AteFastText();
	// Train supervised model

	  jft.runCmd(new String[] {
	          "supervised",
	          "-input", IFastTextTrainer.CURRENTDIR + IFastTextTrainer.FAST_TEXT_TRAINING_DATA_FOLDER_ROOT+getAutName()+"/" +IFastTextTrainer.FAST_TEXT_TRAINING_DATA_FILE_NAME,
	          "-output", IFastTextTrainer.CURRENTDIR + IFastTextTrainer.FAST_TEXT_TRAINING_MODELS_FOLDER_ROOT+getAutName()+"/" + IFastTextTrainer.FAST_TEXT_TRAINING_MODELS_FILE_PREFIX,
	          "-dim",String.valueOf(this.trainingParams.getDim()),
	          "-lrUpdateRate", String.valueOf(this.trainingParams.getLrUpdateRate()),
	          "-ws", String.valueOf(this.trainingParams.getWs()),
	          "-neg", String.valueOf(this.trainingParams.getNeg()),
	          "-lr",String.valueOf(this.trainingParams.getLr()),
	          "-epoch",String.valueOf(this.trainingParams.getEpoch()),
	          		"-thread",String.valueOf(this.trainingParams.getThread()),
	          "-wordNgrams",String.valueOf(this.trainingParams.getWordNgrams()),
	          		"-loss",this.trainingParams.getLoss()
	  });
	  this.setAteFastText(jft);
	 return "done";
  }


/**
 * @return the trainingParams
 */
public TrainingParams getTrainingParams() {
	return trainingParams;
}

}
