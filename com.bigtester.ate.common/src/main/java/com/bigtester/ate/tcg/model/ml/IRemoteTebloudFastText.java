/**
 *
 */
package com.bigtester.ate.tcg.model.ml;

import java.io.IOException;

import java.util.concurrent.ExecutionException;

import com.bigtester.ate.tcg.model.ml.IAteFastText.ProbLabel;
import com.bigtester.tebloud.machine.fasttext.config.TrainingParams;
import com.bigtester.tebloud.tcg.config.RunningModeProperties;
import com.bigtester.tebloud.tcg.config.TebloudApplicationProperties;

/**
 * @author peidong
 *
 */
public interface IRemoteTebloudFastText extends IAteFastText{
	public void loadModel(String autName, RunningModeProperties runningModeProperties);
	public void loadModel(String autName, RunningModeProperties runningModeProperties, TebloudApplicationProperties appProperties);

	public String remoteTraining(String autName, TrainingParams trainingParams)
			throws ExecutionException, InterruptedException, IOException;
	public ProbLabel predictProba(String autName, String text);
}
