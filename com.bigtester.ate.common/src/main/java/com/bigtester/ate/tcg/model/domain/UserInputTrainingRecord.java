/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model.domain;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.xml.parsers.ParserConfigurationException;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.bigtester.RunnerGlobalUtils;
import com.bigtester.ate.tcg.model.ScreenNamePredictStrategy;
import com.bigtester.ate.tcg.model.relationship.Relations;
import com.bigtester.ate.tcg.model.relationship.StepOut;
import com.bigtester.ate.tcg.model.relationship.StepOut.StepOutTriggerType;
import com.bigtester.ate.tcg.model.ws.api.ScreenStepsAdvice;
import com.bigtester.ate.tcg.runner.model.ITestStepExecutionContext;
import com.bigtester.ate.tcg.runner.model.ITestStepRunner;
import com.bigtester.ate.tcg.runner.screenobject.handler.IWebElementHandler;
import com.bigtester.ate.tcg.runner.screenobject.handler.IWebElementHandler.ActionResult;
import com.bigtester.ate.tcg.utils.GlobalUtils;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

// TODO: Auto-generated Javadoc
/**
 * This class TrainingRecord defines ....
 * @author Peidong Hu
 *
 */

/**
 * @author ashraf
 *
 */
@NodeEntity
//@JsonIdentityInfo(generator=ObjectIdGenerators.UUIDGenerator.class, property="@uuid")
@JsonIdentityInfo(generator=JSOGGenerator.class )
public class UserInputTrainingRecord extends WebElementTrainingRecord implements ITestStepRunner,  INameLabelPredictableObjectNonPointerAssignment, INonPointerAssignment{
	
	/** The step outs. */
	//@JsonIgnore
	@Relationship(type = Relations.STEP_OUT, direction = Relationship.OUTGOING)
	//@JsonManagedReference
	private Set<StepOut> stepOuts = new HashSet<StepOut>();
	@Relationship(type=Relations.CHILD_POSITION_UITR_IN_GROUP, direction=Relationship.INCOMING)
	private UserInputTrainingRecord childPositionUitrInGroup;
	/**
	 * @return the stepOuts
	 */
	//@JsonManagedReference
	public Set<StepOut> getStepOuts() {
		return stepOuts;
	}

	/**
	 * @return the childPositionUitrInGroup
	 */
	//@Relationship(type=Relations.CHILD_POSITION_UITR_IN_GROUP, direction=Relationship.OUTGOING)
	public UserInputTrainingRecord getChildPositionUitrInGroup() {
		return childPositionUitrInGroup;
	}

	/**
	 * @param childPositionUitrInGroup the childPositionUitrInGroup to set
	 */
	@Relationship(type=Relations.CHILD_POSITION_UITR_IN_GROUP, direction=Relationship.INCOMING)
	public void setChildPositionUitrInGroup(
			UserInputTrainingRecord childPositionUitrInGroup) {
		this.childPositionUitrInGroup = childPositionUitrInGroup;
	}
	/**
	 * @param stepOuts the stepOuts to set
	 */
	//@JsonManagedReference
	public void setStepOuts(Set<StepOut> stepOuts) {
		this.nonPointerAssign(stepOuts, this.stepOuts);
	}
	


	
	
	/**
	 * Instantiates a new screen user input training record.
	 */
	public UserInputTrainingRecord() {
		super("UserInputTrainingRecord");
	}
	
	/**
	 * Instantiates a new user input training record.
	 *
	 * @param labelName the label name
	 */
	public UserInputTrainingRecord(String labelName) {
		super(labelName);
	}
	public UserInputTrainingRecord(WebElementTrainingRecord uitr) {
		super("UserInputTrainingRecord", uitr);
	}
	public UserInputTrainingRecord(String nodeLabelName, WebElementTrainingRecord uitr) {
		super(nodeLabelName, uitr);
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void assignedFrom(INonPointerAssignment value) {
		super.assignedFrom(value);
		if (value instanceof UserInputTrainingRecord) {
			UserInputTrainingRecord record = (UserInputTrainingRecord) value;
			
			this.setChildPositionUitrInGroup(record.getChildPositionUitrInGroup());
			
			this.setStepOuts(record.getStepOuts());
		}
		
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getComparableLabelName() {
		return this.getPioPredictLabelResult().getValue();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getComparableHtmlCode() {
	
		return this.getElementCoreHtmlCodeWithoutGuidValue();
	}

	/**
	 * Adds the step out.
	 *
	 * @param screenNode the screen node
	 * @param triggerType the trigger type
	 * @return the step out
	 */
	public StepOut addStepOut(Neo4jScreenNode screenNode, StepOutTriggerType triggerType) {
		StepOut retVal;
		final StepOut stepOut = new StepOut(this, screenNode, triggerType);
		retVal = stepOut;
		if (!this.getStepOuts().contains(stepOut)){
			this.getStepOuts().add(stepOut);
		}
		return retVal;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getStepPriority() {
		// TODO Auto-generated method stub
		return this.getUserInputType().getPriority();
	}

	private ActionResult executeOtherUitrs(UserInputTrainingRecord uitr, ITestStepExecutionContext executeEngine, ScreenStepsAdvice stepAdvice) {
		for (int index = 1; index<stepAdvice.getProbabilitySortedTestRunnerSameLabeledUitrTable().get(uitr.getCoreGuid()).size(); index++) {
			UserInputTrainingRecord uitrnew = new UserInputTrainingRecord(stepAdvice.getProbabilitySortedTestRunnerSameLabeledUitrTable().get(uitr.getCoreGuid()).get(index));
			if (uitrnew.executeUitrStep(executeEngine, stepAdvice) == ActionResult.SUCCESS)
				return ActionResult.SUCCESS;
			
		}
		return ActionResult.FAILED_FOR_UNKNOWN_REASON;
	}
	/**
	 * {@inheritDoc}
	 */
	public ActionResult executeUitrStep(ITestStepExecutionContext executeEngine, ScreenStepsAdvice stepAdvice) {
		return executeUitrStep(executeEngine, stepAdvice, this.getUserValues().iterator().next().getValue(), this.getUserInputType());
		
			
	}
	
	public ActionResult executeUitrStep(ITestStepExecutionContext executeEngine, ScreenStepsAdvice stepAdvice, String inputText, UserInputType uitrType) {
		
		final Map<IWebElementHandler, List<Element>> matchedElementInHtml = new HashMap<IWebElementHandler, List<Element>>();
		Optional<Element> node = GlobalUtils.html2Element(this.getElementCoreHtmlCode());
		System.out.println(this.getElementCoreHtmlCode());
		try {
			List<IWebElementHandler> allHandlers =executeEngine.getWebElementHandlers();
			List<IWebElementHandler> toMatchHandlers = null;
			if (uitrType != null) {
				toMatchHandlers = allHandlers.stream().filter(hdl->hdl.getApplicableUitrType().equals(uitrType)).collect(Collectors.toList());
			} else {
				toMatchHandlers = allHandlers;
			}
			toMatchHandlers.stream().parallel().forEach(handler->{
				
					List<Element> tmpMatched = handler.match(node.get());
					if (!tmpMatched.isEmpty()) {
						System.out.println("matched web element handler for: " + this.getComparableHtmlCode());
						matchedElementInHtml.put(handler, tmpMatched);
					}
				
			});
		} catch (Throwable thr) {
			//failed to get node matched (bad html code, it looks like that this is a bad prediction.
			//Do nothing
			thr.printStackTrace();
			return ActionResult.FAILED_FOR_UNKNOWN_REASON;
		}
		if (matchedElementInHtml.isEmpty())
			//Do nothing
			
			return ActionResult.NOT_TYPABLE;
		else {
			//remove the matched handler if it is same node handler and has child handler matched.
			matchedElementInHtml.entrySet().stream().forEach(entrySet->{
				final Map<IWebElementHandler, List<Element>> matchedElementInHtmlWithoutThisEntry = matchedElementInHtml
						.entrySet()
						.stream()
						.filter(iEntry -> !iEntry.getKey()
								.equals(entrySet.getKey()))
						.collect(
								Collectors.toMap(
										Map.Entry::getKey,
										Map.Entry::getValue));
				entrySet.getValue().removeIf(nodeA->{
					return matchedElementInHtmlWithoutThisEntry.entrySet().stream().filter(entrySetA->{
						if (entrySetA.getValue().contains(nodeA)) {
							if (entrySet.getKey().getClass().isAssignableFrom(entrySetA.getKey().getClass())) {
								return true;
							}
						}
						return false;
					}).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).size()>0;
				});
				
			});
			
			boolean result = matchedElementInHtml.entrySet().stream().anyMatch(entrySet->{
				return entrySet.getValue().stream().anyMatch(elem->{
					ActionResult result1 = entrySet.getKey().doAcceptableAction(executeEngine.getMyWebDriver()
							, inputText, elem);
					if (result1 == IWebElementHandler.ActionResult.SUCCESS) {
						return true;
					}
					return false;
				});
			});
			
			return result?ActionResult.SUCCESS:ActionResult.VALUE_NOT_TYPED_IN_CORRECTLY;
		}
			
	}
	 
	/**
	 * {@inheritDoc}
	 */
	public Map<ScreenNamePredictStrategy, ScreenStepsAdvice> executeStep(ITestStepExecutionContext executeEngine, ScreenStepsAdvice stepAdvice) throws Throwable {
	
		ActionResult result =  executeUitrStep(executeEngine, stepAdvice);
		System.out.println("user input typed: " + this.getInputLabelName());
		if (result != ActionResult.SUCCESS) {
			System.out.println("user input try other inputs: " + this.getInputLabelName());
			executeOtherUitrs(this, executeEngine, stepAdvice);
		} 
		
		return executeEngine.getCurrentExecutingScreenStepAdvices();

			
	}
	

	protected static boolean tryClickVisibleElementInMachineLearningHtmlCodeUntilSuccess(WebDriver webDriver, String guid, String mlHtmlCode) {
		
		//TODO need to change to go from top to bottom of the html tree 
		boolean retVal = true;
		try {
			WebElement webE = webDriver.findElement(By.cssSelector("[ate-guid='" + guid +"']"));
			webE.click();
		} catch (Throwable exe){
			if (exe instanceof ElementNotVisibleException) {
				String newGuid = RunnerGlobalUtils.parseNextAteGuid(mlHtmlCode, guid);
				tryClickVisibleElementInMachineLearningHtmlCodeUntilSuccess(webDriver, newGuid, mlHtmlCode);
			} else {
				throw exe;
			}
		}
		return retVal;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ExecutionStatus getStepStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setStepStatus(ExecutionStatus status) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@JsonIgnore
	public boolean isExecutable() {
		// TODO Auto-generated method stub
		return (this.getComputedCssSizes().iterator().next().getUitrWidth()/2 > 5 && this.getComputedCssSizes().iterator().next().getUitrHeight()/2>5);
		
	}

	
	
}
