/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.runner.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Generated;

import com.bigtester.ate.tcg.model.domain.HTMLSource;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({ "pages", "screenUrl", "domain", "domainProtocol",
		"domainPort" })
public class DevToolMessage {

	@JsonProperty("pages")
	private List<HTMLSource> pages = new ArrayList<HTMLSource>();
	@JsonProperty("screenUrl")
	private String screenUrl;

	@JsonProperty("screenWidth")
	private String screenWidth;

	@JsonProperty("topDocumentHeight")
	private long topDocumentHeight;
	@JsonProperty("topDocumentWidth")
	private long topDocumentWidth;
	@JsonProperty("domain")
	private String domain;
	@JsonProperty("domainProtocol")
	private String domainProtocol;
	@JsonProperty("domainPort")
	private Integer domainPort;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@Override
	public boolean equals(Object obj) {
	    if (obj == null) {
	        return false;
	    }
	    if (!DevToolMessage.class.isAssignableFrom(obj.getClass())) {
	        return false;
	    }
	    final DevToolMessage other = (DevToolMessage) obj;
	    if ((this.screenUrl == null) ? (other.screenUrl != null) : !this.screenUrl.equalsIgnoreCase(other.screenUrl)) {
	        return false;
	    }
	    if (!this.domain.equalsIgnoreCase(other.domain)) {
	        return false;
	    }
	    if (this.pages.size()!=other.getPages().size()) {
	    	return false;
	    }
	    List<Boolean> pageSameResultList = new ArrayList<Boolean>(Arrays.asList(true));
	    //need to be changed as the guid has been removed before any uitr action on page since we have changed the element locator to xpath
	    //new solution need to find away to compare two html pages are same, which is very difficult algo
	    this.pages.forEach(thisPage-> {
			List<HTMLSource> sameFrame = other
					.getPages()
					.stream()
					.filter(otherPage -> thisPage.getDocGuid()
							.equalsIgnoreCase(otherPage.getDocGuid())
							&& thisPage.getDomDoc().equalsIgnoreCase(
									otherPage.getDomDoc()))
					.collect(Collectors.toList());
			System.out.println(thisPage.getDocGuid());
	    	if (sameFrame.isEmpty()) {
	    		pageSameResultList.set(0, false);
	    	}
	    });
	    System.out.println("");
	    if (pageSameResultList.get(0) == false)
	    	return false;

	    return true;
	}

	@Override
	public int hashCode() {
	    int hash = 3;
	    hash = 53 * hash + (this.screenUrl != null ? this.screenUrl.hashCode() : 0);
	    hash = 53 * hash + (this.domain != null ? this.domain.hashCode() : 0);
	    hash = 53 * hash + this.pages.size();
	    for (int index=0; index<this.pages.size(); index++) {
	    	hash = 53 * hash + (this.pages.get(index).getDomDoc() != null ? this.pages.get(index).getDomDoc().hashCode() : 0);
	    }
	    return hash;
	}
	/**
	 *
	 * @return The pages
	 */
	@JsonProperty("pages")
	public List<HTMLSource> getPages() {
		return pages;
	}

	/**
	 *
	 * @param pages
	 *            The pages
	 */
	@JsonProperty("pages")
	public void setPages(List<HTMLSource> pages) {
		this.pages = pages;
	}

	/**
	 *
	 * @return The screenUrl
	 */
	@JsonProperty("screenUrl")
	public String getScreenUrl() {
		return screenUrl;
	}

	/**
	 *
	 * @param screenUrl
	 *            The screenUrl
	 */
	@JsonProperty("screenUrl")
	public void setScreenUrl(String screenUrl) {
		this.screenUrl = screenUrl;
	}

	/**
	 *
	 * @return The domain
	 */
	@JsonProperty("domain")
	public String getDomain() {
		return domain;
	}

	/**
	 *
	 * @param domain
	 *            The domain
	 */
	@JsonProperty("domain")
	public void setDomain(String domain) {
		this.domain = domain;
	}

	/**
	 *
	 * @return The domainProtocol
	 */
	@JsonProperty("domainProtocol")
	public String getDomainProtocol() {
		return domainProtocol;
	}

	/**
	 *
	 * @param domainProtocol
	 *            The domainProtocol
	 */
	@JsonProperty("domainProtocol")
	public void setDomainProtocol(String domainProtocol) {
		this.domainProtocol = domainProtocol;
	}

	/**
	 *
	 * @return The domainPort
	 */
	@JsonProperty("domainPort")
	public Integer getDomainPort() {
		return domainPort;
	}

	/**
	 *
	 * @param domainPort
	 *            The domainPort
	 */
	@JsonProperty("domainPort")
	public void setDomainPort(Integer domainPort) {
		this.domainPort = domainPort;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	/**
	 * Sets the additional property.
	 *
	 * @param name the name
	 * @param value the value
	 */
	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	/**
	 * @return the topDocumentHeight
	 */
	public long getTopDocumentHeight() {
		return topDocumentHeight;
	}

	/**
	 * @param topDocumentHeight the topDocumentHeight to set
	 */
	public void setTopDocumentHeight(long topDocumentHeight) {
		this.topDocumentHeight = topDocumentHeight;
	}

	/**
	 * @return the topDocumentWidth
	 */
	public long getTopDocumentWidth() {
		return topDocumentWidth;
	}

	/**
	 * @param topDocumentWidth the topDocumentWidth to set
	 */
	public void setTopDocumentWidth(long topDocumentWidth) {
		this.topDocumentWidth = topDocumentWidth;
	}

	/**
	 * @return the screenWidth
	 */
	public String getScreenWidth() {
		return screenWidth;
	}

	/**
	 * @param screenWidth the screenWidth to set
	 */
	public void setScreenWidth(String screenWidth) {
		this.screenWidth = screenWidth;
	}

}
