/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model.ws.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.commons.collections4.map.LinkedMap;

import com.bigtester.ate.tcg.runner.model.DevToolMessage;
import com.bigtester.ate.tcg.runner.model.ITestStepExecutionContext;
import com.bigtester.ate.tcg.runner.model.ITestStepRunner;
import com.bigtester.ate.tcg.runner.model.ITestStepRunner.ExecutionStatus;
import com.bigtester.ate.tcg.service.IScreenNodeCrud;
import com.bigtester.ate.tcg.model.ScreenNamePredictStrategy;
import com.bigtester.ate.tcg.model.domain.InScreenJumperTrainingRecord;
import com.bigtester.ate.tcg.model.domain.ScreenElementChangeUITR;
import com.bigtester.ate.tcg.model.domain.ScreenJumperElementTrainingRecord;
import com.bigtester.ate.tcg.model.domain.TestSuite;
import com.bigtester.ate.tcg.model.domain.UserInputTrainingRecord;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord.UserInputType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

// TODO: Auto-generated Javadoc
/**
 * This class StepAdvice defines ....
 * @author Peidong Hu
 *
 */
@JsonIgnoreProperties({"devToolMessage", "executedStepRunner", "executedJumperElementInThisScreen" })
public class ScreenStepsAdvice {



	public static class StepExecutionStatus {
		final private ITestStepRunner.ExecutionStatus executionStatus;
		public StepExecutionStatus(ITestStepRunner.ExecutionStatus status) {
			this.executionStatus = status;
		}
		/**
		 * @return the executionStatus
		 */
		public ITestStepRunner.ExecutionStatus getExecutionStatus() {
			return executionStatus;
		}
	}

	private Map<String, List<WebElementTrainingRecord>> probabilitySortedTestRunnerSameLabeledUitrTable;

	private final LinkedMap<ITestStepRunner, StepExecutionStatus> executedStepRunner = new LinkedMap<ITestStepRunner, StepExecutionStatus>();

	private Optional<DevToolMessage> devToolMessage = Optional.ofNullable(null);

	private List<ITestStepRunner> testStepRunners = new ArrayList<ITestStepRunner>();
	/** The judge threshold. */
	public static final Double judgeThreshold = 0.009D;

	/** The screen name. */
	private String screenName = "";

	/** The screen name confidence. */
	private Double screenNameConfidence = 0D;

	private ScreenNamePredictStrategy screenNamePredictStrategy = ScreenNamePredictStrategy.PREDICTED_BY_VISIBLE_ELEMENT_TEXT;

	//final private Map<ScreenNamePredictStrategy, Map<String, Double>> possibleScreenNames = new HashMap<ScreenNamePredictStrategy, Map<String, Double>>();

	private Set<InScreenJumperTrainingRecord> executedInScreenJumperUitrs = new HashSet<InScreenJumperTrainingRecord>();

	/** The screen jumper uitrs. */
	private Set<ScreenJumperElementTrainingRecord> executedScreenJumperUitrs = new HashSet<ScreenJumperElementTrainingRecord>();

	/** The screen element change uitrs. */
	private Set<ScreenElementChangeUITR> executedScreenElementChangeUitrs = new HashSet<ScreenElementChangeUITR>();

	/** The user input uitrs. */
	private List<UserInputTrainingRecord> executedUserInputUitrs = new ArrayList<UserInputTrainingRecord>();

	/** The in screen jumper uitrs. */
	private Set<InScreenJumperTrainingRecord> inScreenJumperUitrs = new HashSet<InScreenJumperTrainingRecord>();

	/** The screen jumper uitrs. */
	//private Set<ScreenJumperElementTrainingRecord> screenJumperUitrs = new HashSet<ScreenJumperElementTrainingRecord>();

	/** The screen element change uitrs. */
	//private Set<ScreenElementChangeUITR> screenElementChangeUitrs = new HashSet<ScreenElementChangeUITR>();

	/** The user input uitrs. */
	private List<UserInputTrainingRecord> userInputUitrs = new ArrayList<UserInputTrainingRecord>();

	/** The correction needed uitr graph ids. */
	private List<Long> correctionNeededUitrGraphIds = new ArrayList<Long>();

	/** The in screen jumper uitrs. */
	private Set<InScreenJumperTrainingRecord> archivedInScreenJumperUitrs = new HashSet<InScreenJumperTrainingRecord>();

	/** The screen jumper uitrs. */
	private Set<ITestStepRunner> archivedScreenJumperUitrs = new HashSet<ITestStepRunner>();

	/** The screen element change uitrs. */
	private Set<ITestStepRunner> archivedScreenElementChangeUitrs = new HashSet<ITestStepRunner>();

	/** The user input uitrs. */
	private List<UserInputTrainingRecord> archivedUserInputUitrs = new ArrayList<UserInputTrainingRecord>();

	/** The alternative user input types. */
	private Map<Long, Set<UserInputType>> alternativeUserInputTypes = new HashMap<Long, Set<UserInputType>>();

	/** The last screen in test case. */
	boolean lastScreenInTestCase = false;

	/** The executed element. */
	private transient Optional<UserInputTrainingRecord> executedJumperElementInThisScreen = Optional.empty();

	private final List<TestSuite> testSuiteMap = new ArrayList<TestSuite>();

	private String testCaseName = "";
	/**
	 * Instantiates a new step advice.
	 */
	public ScreenStepsAdvice() {//NOPMD
		//judgeThreshold = 0.009D;
	}
	/**
	 * @return the screenName
	 */
	public String getScreenName() {
		return screenName;
	}
	/**
	 * @param screenName the screenName to set
	 */
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	/**
	 * @return the inScreenJumperUitrs
	 */
	public Set<InScreenJumperTrainingRecord> getInScreenJumperUitrs() {
		return inScreenJumperUitrs;
	}
	/**
	 * @param inScreenJumperUitrs the inScreenJumperUitrs to set
	 */
	public void setInScreenJumperUitrs(
			Set<InScreenJumperTrainingRecord> inScreenJumperUitrs) {
		this.inScreenJumperUitrs = inScreenJumperUitrs;
	}
	/**
	 * @return the screenJumperUitrs
	 */
	@JsonIgnore
	public Set<ITestStepRunner> getScreenJumperUitrs() {
		Set<ITestStepRunner> screenJumpers = this.testStepRunners
				.stream()
				.filter(stepRunner -> stepRunner.getUserInputType().equals(
						UserInputType.SCREENJUMPER))
				.collect(Collectors.toSet());
		return screenJumpers;
	}
	/**
	 * @param screenJumperUitrs the screenJumperUitrs to set
	 */
//	public void setScreenJumperUitrs(
//			Set<ScreenJumperElementTrainingRecord> screenJumperUitrs) {
//		this.screenJumperUitrs = screenJumperUitrs;
//	}
	/**
	 * @return the screenElementChangeUitrs
	 */
	@JsonIgnore
	public Set<ITestStepRunner> getScreenElementChangeUitrs() {
		Set<ITestStepRunner> screenElementChangeUitrs = this.testStepRunners
				.stream()
				.filter(stepRunner -> stepRunner.getUserInputType().equals(
						UserInputType.SCREENCHANGEUITR))
				.collect(Collectors.toSet());
		return screenElementChangeUitrs;
	}
	/**
	 * @param screenElementChangeUitrs the screenElementChangeUitrs to set
	 */
//	public void setScreenElementChangeUitrs(
//			Set<ScreenElementChangeUITR> screenElementChangeUitrs) {
//		this.screenElementChangeUitrs = screenElementChangeUitrs;
//	}
	/**
	 * @return the userInputUitrs
	 */
	public List<UserInputTrainingRecord> getUserInputUitrs() {
		return userInputUitrs;
	}
	/**
	 * @param userInputUitrs the userInputUitrs to set
	 */
	public void setUserInputUitrs(List<UserInputTrainingRecord> userInputUitrs) {
		this.userInputUitrs = userInputUitrs;
	}
	/**
	 * @return the correctionNeededUitrGraphIds
	 */
	public List<Long> getCorrectionNeededUitrGraphIds() {
		return correctionNeededUitrGraphIds;
	}
	/**
	 * @param correctionNeededUitrGraphIds the correctionNeededUitrGraphIds to set
	 */
	public void setCorrectionNeededUitrGraphIds(
			List<Long> correctionNeededUitrGraphIds) {
		this.correctionNeededUitrGraphIds = correctionNeededUitrGraphIds;
	}
	/**
	 * @return the screenNameConfidence
	 */
	public Double getScreenNameConfidence() {
		return screenNameConfidence;
	}
	/**
	 * @param screenNameConfidence the screenNameConfidence to set
	 */
	public void setScreenNameConfidence(Double screenNameConfidence) {
		this.screenNameConfidence = screenNameConfidence;
	}
	/**
	 * @return the judgeThrethold
	 */
	public Double getJudgeThreshold() {
		return judgeThreshold;
	}
	/**
	 * @return the archivedInScreenJumperUitrs
	 */
	public Set<InScreenJumperTrainingRecord> getArchivedInScreenJumperUitrs() {
		return archivedInScreenJumperUitrs;
	}
	/**
	 * @param archivedInScreenJumperUitrs the archivedInScreenJumperUitrs to set
	 */
	public void setArchivedInScreenJumperUitrs(
			Set<InScreenJumperTrainingRecord> archivedInScreenJumperUitrs) {
		this.archivedInScreenJumperUitrs = archivedInScreenJumperUitrs;
	}
	/**
	 * @return the archivedScreenJumperUitrs
	 */
	public Set<ITestStepRunner> getArchivedScreenJumperUitrs() {
		return archivedScreenJumperUitrs;
	}
	/**
	 * @param archivedScreenJumperUitrs the archivedScreenJumperUitrs to set
	 */
	public void setArchivedScreenJumperUitrs(
			Set<ITestStepRunner> archivedScreenJumperUitrs) {
		this.archivedScreenJumperUitrs = archivedScreenJumperUitrs;
	}
	/**
	 * @return the archivedScreenElementChangeUitrs
	 */
	public Set<ITestStepRunner> getArchivedScreenElementChangeUitrs() {
		return archivedScreenElementChangeUitrs;
	}
	/**
	 * @param archivedScreenElementChangeUitrs the archivedScreenElementChangeUitrs to set
	 */
	public void setArchivedScreenElementChangeUitrs(
			Set<ITestStepRunner> archivedScreenElementChangeUitrs) {
		this.archivedScreenElementChangeUitrs = archivedScreenElementChangeUitrs;
	}
	/**
	 * @return the archivedUserInputUitrs
	 */
	public List<UserInputTrainingRecord> getArchivedUserInputUitrs() {
		return archivedUserInputUitrs;
	}
	/**
	 * @param archivedUserInputUitrs the archivedUserInputUitrs to set
	 */
	public void setArchivedUserInputUitrs(
			List<UserInputTrainingRecord> archivedUserInputUitrs) {
		this.archivedUserInputUitrs = archivedUserInputUitrs;
	}
	/**
	 * @return the alternativeUserInputTypes
	 */
	public Map<Long, Set<UserInputType>> getAlternativeUserInputTypes() {
		return alternativeUserInputTypes;
	}
	/**
	 * @param alternativeUserInputTypes the alternativeUserInputTypes to set
	 */
	public void setAlternativeUserInputTypes(
			Map<Long, Set<UserInputType>> alternativeUserInputTypes) {
		this.alternativeUserInputTypes = alternativeUserInputTypes;
	}
	/**
	 * @return the lastScreenInTestCase
	 */
	public boolean isLastScreenInTestCase() {
		return lastScreenInTestCase;
	}
	/**
	 * @param lastScreenInTestCase the lastScreenInTestCase to set
	 */
	public void setLastScreenInTestCase(boolean lastScreenInTestCase) {
		this.lastScreenInTestCase = lastScreenInTestCase;
	}

	/**
	 * @return the executedInScreenJumperUitrs
	 */
	public Set<InScreenJumperTrainingRecord> getExecutedInScreenJumperUitrs() {
		return executedInScreenJumperUitrs;
	}
	/**
	 * @param executedInScreenJumperUitrs the executedInScreenJumperUitrs to set
	 */
	public void setExecutedInScreenJumperUitrs(
			Set<InScreenJumperTrainingRecord> executedInScreenJumperUitrs) {
		this.executedInScreenJumperUitrs = executedInScreenJumperUitrs;
	}
	/**
	 * @return the executedScreenJumperUitrs
	 */
	public Set<ScreenJumperElementTrainingRecord> getExecutedScreenJumperUitrs() {
		return executedScreenJumperUitrs;
	}
	/**
	 * @param executedScreenJumperUitrs the executedScreenJumperUitrs to set
	 */
	//TODO change to map<ITestStepRunner, ExecutionStatus> to record each execution status.
	public void setExecutedScreenJumperUitrs(
			Set<ScreenJumperElementTrainingRecord> executedScreenJumperUitrs) {
		this.executedScreenJumperUitrs = executedScreenJumperUitrs;
	}
	/**
	 * @return the executedScreenElementChangeUitrs
	 */
	public Set<ScreenElementChangeUITR> getExecutedScreenElementChangeUitrs() {
		return executedScreenElementChangeUitrs;
	}
	/**
	 * @param executedScreenElementChangeUitrs the executedScreenElementChangeUitrs to set
	 */
	public void setExecutedScreenElementChangeUitrs(
			Set<ScreenElementChangeUITR> executedScreenElementChangeUitrs) {
		this.executedScreenElementChangeUitrs = executedScreenElementChangeUitrs;
	}
	/**
	 * @return the executedUserInputUitrs
	 */
	public List<UserInputTrainingRecord> getExecutedUserInputUitrs() {
		return executedUserInputUitrs;
	}
	/**
	 * @param executedUserInputUitrs the executedUserInputUitrs to set
	 */
	public void setExecutedUserInputUitrs(
			List<UserInputTrainingRecord> executedUserInputUitrs) {
		this.executedUserInputUitrs = executedUserInputUitrs;
	}
	/**
	 * @return the executedElementInThisStep
	 */
	public Optional<UserInputTrainingRecord> getExecutedJumperElementInThisScreen() {
		return executedJumperElementInThisScreen;
	}
	/**
	 * @param executedElementInThisStep the executedElementInThisStep to set
	 */
	public void setExecutedJumperElementInThisScreen(
			Optional<UserInputTrainingRecord> executedJumperElementInThisScreen) {
		this.executedJumperElementInThisScreen = executedJumperElementInThisScreen;
	}

	/**
	 * @return the testStepRunners
	 */
	public List<ITestStepRunner> getTestStepRunners() {
		return testStepRunners;
	}

	/**
	 * @return the screenNamePredictStrategy
	 */
	public ScreenNamePredictStrategy getScreenNamePredictStrategy() {
		return screenNamePredictStrategy;
	}
	/**
	 * @param screenNamePredictStrategy the screenNamePredictStrategy to set
	 */
	public void setScreenNamePredictStrategy(ScreenNamePredictStrategy screenNamePredictStrategy) {
		this.screenNamePredictStrategy = screenNamePredictStrategy;
	}
	/**
	 * @return the devToolMessage
	 */

	public DevToolMessage getDevToolMessage() {
		return devToolMessage.get();
	}
	/**
	 * @param devToolMessage the devToolMessage to set
	 */
	public void setDevToolMessage(DevToolMessage devToolMessage) {
		this.devToolMessage = Optional.of(devToolMessage);
	}
	/**
	 * @return the executedStepRunner
	 */
	public LinkedMap<ITestStepRunner, StepExecutionStatus> getExecutedStepRunner() {
		return executedStepRunner;
	}

	private void tuneExecutableScreenJumpers() {
		//archive lower confident jumpers since we only want one jumper on the screen for this specific test case.
		Set<ITestStepRunner> jumpers = this.getScreenJumperUitrs();
		Comparator<ITestStepRunner> confidentCom = Comparator
				.comparing(ITestStepRunner::getPioPredictConfidence);
		ITestStepRunner mostConfidentJumper = jumpers.stream()
				.max(confidentCom).get();
		Set<ITestStepRunner> lowConfJumpers = jumpers
				.stream()
				.filter(jumper -> jumper.getPioPredictConfidence() < mostConfidentJumper
						.getPioPredictConfidence()).collect(Collectors.toSet());

		this.getArchivedScreenJumperUitrs().addAll(lowConfJumpers);
		this.getTestStepRunners().removeAll(lowConfJumpers);

		Set<ITestStepRunner> equalMaxConfJumpers = jumpers
				.stream()
				.filter(jumper -> jumper.getPioPredictConfidence() == mostConfidentJumper
						.getPioPredictConfidence()).collect(Collectors.toSet());
		if (equalMaxConfJumpers.size()>1) {
			this.getArchivedScreenJumperUitrs().addAll(equalMaxConfJumpers);
			this.getArchivedScreenJumperUitrs().remove(mostConfidentJumper);
			this.getTestStepRunners().removeAll(equalMaxConfJumpers);
			this.getTestStepRunners().add(mostConfidentJumper);
		}
	}


	private void tuneScreenElementChanger() {
		Set<ITestStepRunner> elementChangers = this.getScreenElementChangeUitrs();
		Iterator<ITestStepRunner> changerItr = elementChangers.iterator();
		while(changerItr.hasNext()) {
			ITestStepRunner uitr = changerItr.next();
			if (uitr.getPioPredictConfidence() < this.getJudgeThreshold()) {
				this.getArchivedScreenElementChangeUitrs().add(uitr);
				changerItr.remove();
			} else {
				Set<ITestStepRunner> sameLabeledUitrs = elementChangers
						.stream()
						.parallel()
						.filter(uit -> uit.getInputLabelName().equalsIgnoreCase(
								uitr.getInputLabelName()))
						.collect(Collectors.toSet());

				if (sameLabeledUitrs.size() > 1) {
					Comparator<ITestStepRunner> confidentCom = Comparator
							.comparing(ITestStepRunner::getPioPredictConfidence);
					ITestStepRunner maxConfUitr = sameLabeledUitrs.stream().max(confidentCom).get();
					this.getArchivedScreenElementChangeUitrs().addAll(sameLabeledUitrs);
					this.getArchivedScreenElementChangeUitrs().remove(maxConfUitr);
					this.getTestStepRunners().removeAll(sameLabeledUitrs);
					this.getTestStepRunners().add(maxConfUitr);

				}
			}

		}
	}
	public void tuneStepRunners() {
		if (this.getScreenJumperUitrs().size()>0)
			this.tuneExecutableScreenJumpers();
		if (this.getScreenElementChangeUitrs().size()>0)
			this.tuneScreenElementChanger();
	}


	public boolean isAlreadyExecutedStepRunner(ITestStepExecutionContext engine, ITestStepRunner stepRunner) {
		//TODO need to add code to compare core node guid as well. if it is same, and inputLabelName is same need to return true as well.
		boolean thisAdviceExecuted = getExecutedStepRunner().containsKey(stepRunner);

		if (!thisAdviceExecuted && !engine.getExecutedStepAdvices().isEmpty()) {
			if (engine.getExecutedStepAdvices()
					.get(engine.getExecutedStepAdvices().size() - 1).get(0)
					.getScreenName().equalsIgnoreCase(this.screenName)) {
				thisAdviceExecuted = thisAdviceExecuted  || engine.getExecutedStepAdvices()
				.get(engine.getExecutedStepAdvices().size() - 1).stream().anyMatch(advice->{
					return advice.getExecutedStepRunner().keySet().stream().anyMatch(runner->{
														return runner
																.getCoreGuid()
																.equalsIgnoreCase(stepRunner
																		.getCoreGuid())
																&& runner
																		.getInputLabelName()
																		.equalsIgnoreCase(
																				stepRunner
																						.getInputLabelName());


					});
				});

			}
		}
		return thisAdviceExecuted;
	}

	public void beforeScreenTestActions(ITestStepExecutionContext engine) {

		this.tuneStepRunners();
		this.testStepRunners = prioritizeTestStepRunners();

		if (engine.getRunnerProperties().isEnableDemoCode()) {
			if (this.getScreenName().equals(engine.getRunnerProperties().getPauseScreenName())) {
				this.getTestStepRunners().removeIf(runner->runner.getPioPredictConfidence()<engine.getRunnerProperties().getDemoConfidenceThredthold());
				if (this.getTestStepRunners().size()<=engine.getRunnerProperties().getPauseUitrsCount()) {
					this.getTestStepRunners().removeIf(runner->runner.getUserInputType().equals(UserInputType.SCREENJUMPER));
				}
			}
		}
	}

	public void postScreenChangedActions(ITestStepExecutionContext executeEngine, ITestStepRunner stepRunner) {
		//stepRunner is a screenJumper
		if (!stepRunner.getUserInputType().equals(UserInputType.SCREENJUMPER)) {
			ScreenJumperElementTrainingRecord aJumper = new
			ScreenJumperElementTrainingRecord((UserInputTrainingRecord) stepRunner);
			replaceTestStepRunner(stepRunner, aJumper);
		}
		executeEngine.appendExecutedStepAdvice(this);
	}
	public boolean isSuccessAdvice(IScreenNodeCrud screenNodeService) {
		if (this.isEndScreen(screenNodeService)) {
			this.getUserInputUitrs().clear();
			this.setLastScreenInTestCase(true);
			return true;
		} else {
			return (((getExecutedInScreenJumperUitrs().size()>0 || this.getScreenElementChangeUitrs().size()>0 || getScreenJumperUitrs().size()>0)) && getJudgeThreshold()<=getScreenNameConfidence());
		}
	}

	public boolean isEndScreen(IScreenNodeCrud screenNodeService) {
		boolean retVal = false;
		Iterable<String> endScreenNames = screenNodeService.findDistinctEndScreenNamesInTestCase(null);
		long matchEndScreenNamesInTestCase = StreamSupport
				.stream(endScreenNames.spliterator(), true)
				.filter(endScreenName -> endScreenName.equalsIgnoreCase(this
						.getScreenName())).count();
		if (matchEndScreenNamesInTestCase>0)
			retVal = true;
		return retVal;
	}

	private void removeExecutedStepsAfterScreenElementChangerExecutedInNewAdvice(ScreenStepsAdvice newStepAdvice) {
		for(Iterator<ITestStepRunner> itr = newStepAdvice.getTestStepRunners().iterator(); itr.hasNext();) {
			ITestStepRunner newRunner = itr.next();

			if (!this.getExecutedStepRunner().keySet()
					.stream()
					.filter(runner -> runner.getCoreGuid().equalsIgnoreCase(
							newRunner.getCoreGuid()) || runner.getInputLabelName().equalsIgnoreCase(newRunner.getInputLabelName()))
					.collect(Collectors.toList()).isEmpty()) {
				itr.remove();
			}

		}
//		//TODO concurrent modification error in loop
//		for(Iterator<ITestStepRunner> itr2 = this.getTestStepRunners().iterator(); itr2.hasNext();) {
//			ITestStepRunner oldRunner = itr2.next();
//			if (newStepAdvice
//					.getTestStepRunners()
//					.stream()
//					.filter(runner -> runner.getCoreGuid().equalsIgnoreCase(
//							oldRunner.getCoreGuid()))
//					.collect(Collectors.toList()).isEmpty()) {
//				this.getTestStepRunners().remove(oldRunner);
//			}
//		}

	}

	public void postElementChangedActions(ITestStepExecutionContext executeEngine, Map<ScreenNamePredictStrategy, ScreenStepsAdvice> newAdvices, ITestStepRunner stepRunner) {
		//stepRunner is a screenElementChanger
		//TODO should we do this replacement?
		if (!stepRunner.getUserInputType().equals(UserInputType.SCREENCHANGEUITR)) {
			ScreenElementChangeUITR aElementChanger = new ScreenElementChangeUITR(
					(UserInputTrainingRecord) stepRunner);
			replaceTestStepRunner(stepRunner,aElementChanger);

		}
		for (Entry<ScreenNamePredictStrategy, ScreenStepsAdvice> entry: newAdvices.entrySet()) {
			removeExecutedStepsAfterScreenElementChangerExecutedInNewAdvice(
					entry.getValue());
		}
		//this.prioritizeTestStepRunners();
		executeEngine.appendExecutedStepAdvice(this);
	}
	private void replaceTestStepRunner(ITestStepRunner org, ITestStepRunner replacement) {
		int index = this.getTestStepRunners().indexOf(org);
		this.getTestStepRunners().remove(index);
		this.getTestStepRunners().add(index, replacement);
		StepExecutionStatus status = this.getExecutedStepRunner().remove(org);
		this.getExecutedStepRunner().put(replacement, status);
	}
	public void postUserInputActions(ITestStepExecutionContext executeEngine, ITestStepRunner stepRunner) {
		//stepRunner is a screenJumper
		if (!stepRunner.getUserInputType().equals(UserInputType.USERINPUT)) {
			UserInputTrainingRecord userInput = new UserInputTrainingRecord(
					(UserInputTrainingRecord) stepRunner);
			replaceTestStepRunner(stepRunner, userInput);
		}
	}
	public void beforeStepExecutionActions(ITestStepRunner step) {
		step.setStepStatus(ExecutionStatus.ONGOING);
	}
	public void afterStepExecutionActions(ITestStepExecutionContext engine, ITestStepRunner step) {
		step.setStepStatus(ExecutionStatus.EXECUTED);
		getExecutedStepRunner().put(step, new StepExecutionStatus(step.getStepStatus()));

		if (engine.getRunnerProperties().isEnableDemoCode()) {
			if (this.getScreenName().equals(engine.getRunnerProperties().getPauseScreenName())) {
				if (this.getTestStepRunners().size()<=engine.getRunnerProperties().getPauseUitrsCount()) {
					try {
						Thread.sleep(15000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}

	}
	public void afterStepSuccessActions(ITestStepRunner step) {
		step.setStepStatus(ExecutionStatus.PASS);
		getExecutedStepRunner().put(step, new StepExecutionStatus(step.getStepStatus()));
		this.cleanUpDuplicatedStepRunnerLabeledIncorrectly(step);
	}
	public void afterStepFailedActions(ITestStepRunner step) {
		step.setStepStatus(ExecutionStatus.FAIL);
		getExecutedStepRunner().put(step, new StepExecutionStatus(step.getStepStatus()));
		//this.cleanUpDuplicatedStepRunnerLabeledIncorrectly(step);
	}
	public void cleanUpDuplicatedStepRunnerLabeledIncorrectly(ITestStepRunner uitrExecutedSuccessfully) {
			for(Iterator<ITestStepRunner> itr = this.getTestStepRunners().iterator(); itr.hasNext();) {
				ITestStepRunner stepRunner = itr.next();
				if (stepRunner.getComparableHtmlCode().equalsIgnoreCase(uitrExecutedSuccessfully.getComparableHtmlCode()) &&
						stepRunner.getUserInputType()!=uitrExecutedSuccessfully.getUserInputType()) {
					itr.remove();
				}
			}
	}

	public List<ITestStepRunner> prioritizeTestStepRunners() {
		Comparator<ITestStepRunner> cmp = Comparator.comparing(runner->runner.getUserInputType().getPriority());
		return getTestStepRunners().stream().sorted(cmp).collect(Collectors.toList());
	}
	/**
	 * @return the probabilitySortedTestRunnerSameLabeledUitrTable
	 */
	public Map<String, List<WebElementTrainingRecord>> getProbabilitySortedTestRunnerSameLabeledUitrTable() {
		return probabilitySortedTestRunnerSameLabeledUitrTable;
	}
	/**
	 * @param probabilitySortedTestRunnerSameLabeledUitrTable the probabilitySortedTestRunnerSameLabeledUitrTable to set
	 */
	public void setProbabilitySortedTestRunnerSameLabeledUitrTable(
			Map<String, List<WebElementTrainingRecord>> probabilitySortedTestRunnerSameLabeledUitrTable) {
		this.probabilitySortedTestRunnerSameLabeledUitrTable = probabilitySortedTestRunnerSameLabeledUitrTable;
	}
	/**
	 * @return the testSuiteMap
	 */
	public List<TestSuite> getTestSuiteMap() {
		return testSuiteMap;
	}
	/**
	 * @return the testCaseName
	 */
	public String getTestCaseName() {
		return testCaseName;
	}
	/**
	 * @param testCaseName the testCaseName to set
	 */
	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}

}
