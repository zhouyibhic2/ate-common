/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model.domain;

import java.awt.geom.Point2D;

import org.eclipse.jdt.annotation.Nullable;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;

// TODO: Auto-generated Javadoc
/**
 * This class UserInputValue defines ....
 * @author Peidong Hu
 *
 */
@NodeEntity
public class ComputedCssSize extends BaseAteNode implements INameLabelPredictableObjectNonPointerAssignment, INonPointerAssignment{
	
	@Property
	private double leftTopCornerX=0;
	@Property
	private double leftTopCornerY=0;
	@Property
	private double centerX=this.topDocumentWidth/2;
	@Property
	private double centerY=this.topDocumentHeight/2;
	@Property
	private double distanceToCenter = 0;
	
	@Property
	private int uitrWidth=0;
	
	@Property
	private int uitrHeight=0;
	
	@Property
	private int uitrSize=0;
	
	@Property
	private int topDocumentWidth=0;
	
	@Property
	private int screenWidth=0;
	
	@Property
	private int topDocumentHeight=0;
	
	@Property
	private int topDocumentSize=0;
	
	@Property
	private float uitrDocumentSizeRatio=0.0f;
	/**
	 * Instantiates a new user input value.
	 */
	public ComputedCssSize() {
		super("ComputedCssSize");
	}
	
	/**
	 * Instantiates a new user input value.
	 *
	 * @param value the value
	 */
	public ComputedCssSize(int uitrWidth, int uitrHeight, int screenWidth, int topDocumentWidth, int topDocumentHeight, int leftTopCornerX, int leftTopCornerY) {
		super("ComputedCssSize");
		this.setUitrWidth(uitrWidth);
		this.setUitrHeight(uitrHeight);
		this.setScreenWidth(screenWidth);
		this.setTopDocumentWidth(topDocumentWidth);
		this.setTopDocumentHeight(topDocumentHeight);
		this.setLeftTopCornerX(leftTopCornerX);
		this.setLeftTopCornerY(leftTopCornerY);
	}
	
	 

 

	/**
	 * @return the valueId
	 */
	@Nullable
	public Long getValueId() {
		return getGraphId();
	}

	/**
	 * @param valueId the valueId to set
	 */
	public void setValueId(Long valueId) {
		this.setGraphId(valueId);
	}
 

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void assignedFrom(INonPointerAssignment value) {
		if (value instanceof ComputedCssSize) {
			ComputedCssSize record = (ComputedCssSize) value;
			this.setUitrWidth(record.getUitrWidth());
			this.setUitrHeight(record.getUitrHeight());
			this.setScreenWidth(record.getScreenWidth());
			this.setTopDocumentWidth(record.getTopDocumentWidth());
			this.setTopDocumentHeight(record.getTopDocumentHeight());
			this.setLeftTopCornerX(record.getLeftTopCornerX());
			this.setLeftTopCornerY(record.getLeftTopCornerY());
		}
	}

	

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getComparableLabelName() {
		return String.valueOf(this.getTopDocumentWidth()) + "," + String.valueOf(this.getTopDocumentHeight()) +"," +String.valueOf(this.getUitrWidth()) +"," + String.valueOf(this.getUitrHeight());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getComparableHtmlCode() {
		return "<value>" + this.getComparableLabelName() +"</value>";
	}

	private void reCaculateDimension() {
		this.uitrSize = this.getUitrWidth()*this.getUitrHeight();
		this.topDocumentSize = this.getTopDocumentWidth()*this.getTopDocumentHeight();
		if (this.getTopDocumentSize()!=0) {
			this.uitrDocumentSizeRatio = (float)this.getUitrSize()/this.getTopDocumentSize();
		}
		this.centerX=this.topDocumentWidth/2;
		this.centerY=this.topDocumentHeight/2;
		this.distanceToCenter = Point2D.distance(this.leftTopCornerX, this.leftTopCornerY, this.centerX, this.centerY);
	}
	/**
	 * @return the uitrWidth
	 */
	public int getUitrWidth() {
		return uitrWidth;
	}

	/**
	 * @param uitrWidth the uitrWidth to set
	 */
	public void setUitrWidth(int uitrWidth) {
		this.uitrWidth = uitrWidth;
		reCaculateDimension();
		
	}

	/**
	 * @return the uitrHeight
	 */
	public int getUitrHeight() {
		return uitrHeight;
	}

	/**
	 * @param uitrHeight the uitrHeight to set
	 */
	public void setUitrHeight(int uitrHeight) {
		this.uitrHeight = uitrHeight;
		reCaculateDimension();
	}

	/**
	 * @return the uitrSize
	 */
	public int getUitrSize() {
		return uitrSize;
	}

	

	/**
	 * @return the topDocumentWidth
	 */
	public int getTopDocumentWidth() {
		return topDocumentWidth;
	}

	/**
	 * @param topDocumentWidth the topDocumentWidth to set
	 */
	public void setTopDocumentWidth(int topDocumentWidth) {
		this.topDocumentWidth = topDocumentWidth;
		reCaculateDimension();
	}

	/**
	 * @return the topDocumentHeight
	 */
	public int getTopDocumentHeight() {
		return topDocumentHeight;
	}

	/**
	 * @param topDocumentHeight the topDocumentHeight to set
	 */
	public void setTopDocumentHeight(int topDocumentHeight) {
		this.topDocumentHeight = topDocumentHeight;
		reCaculateDimension();
	}

	/**
	 * @return the topDocumentSize
	 */
	public int getTopDocumentSize() {
		return topDocumentSize;
	}

	/**
	 * @return the uitrDocumentSizeRatio
	 */
	public float getUitrDocumentSizeRatio() {
		return uitrDocumentSizeRatio;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getIdentifierString() {
		 
		return this.getComparableHtmlCode();
	}

	/**
	 * @return the leftTopCornerX
	 */
	public double getLeftTopCornerX() {
		return leftTopCornerX;
	}

	/**
	 * @param leftTopCornerX the leftTopCornerX to set
	 */
	public void setLeftTopCornerX(double leftTopCornerX) {
		this.leftTopCornerX = leftTopCornerX;
		reCaculateDimension();
	}

	/**
	 * @return the leftTopCornerY
	 */
	public double getLeftTopCornerY() {
		return leftTopCornerY;
	}

	/**
	 * @param leftTopCornerY the leftTopCornerY to set
	 */
	public void setLeftTopCornerY(double leftTopCornerY) {
		this.leftTopCornerY = leftTopCornerY;
		reCaculateDimension();
	}

	/**
	 * @return the screenWidth
	 */
	public int getScreenWidth() {
		return screenWidth;
	}

	/**
	 * @param screenWidth the screenWidth to set
	 */
	public void setScreenWidth(int screenWidth) {
		this.screenWidth = screenWidth;
	}

   

	
}
