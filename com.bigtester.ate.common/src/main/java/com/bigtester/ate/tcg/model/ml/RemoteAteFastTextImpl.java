/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2017, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model.ml;

import com.bigtester.tebloud.machine.fasttext.FasttextEngineClient;
import com.bigtester.tebloud.machine.fasttext.config.TrainingParams;
import com.bigtester.tebloud.tcg.config.RunningModeProperties;
import com.bigtester.tebloud.tcg.config.TebloudApplicationProperties;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.DockerCmdExecFactory;
import com.github.dockerjava.api.command.ExecCreateCmdResponse;
import com.github.dockerjava.api.model.Frame;
import com.github.dockerjava.api.model.Info;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;
import com.github.dockerjava.core.command.ExecStartResultCallback;
import com.github.dockerjava.jaxrs.JerseyDockerCmdExecFactory;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class RemoteAteFastTextImpl implements IRemoteTebloudFastText {

	private final LocalDocker localDocker;

	public static class LocalDocker {
		public DockerClientConfig config = DefaultDockerClientConfig.createDefaultConfigBuilder()
				.withDockerHost(fastTextServerDockerDomainUrl)

				.build();

		// using jaxrs/jersey implementation here (netty impl is also available)
		public DockerCmdExecFactory dockerCmdExecFactory = new JerseyDockerCmdExecFactory().withReadTimeout(1000)
				.withConnectTimeout(1000).withMaxTotalConnections(100).withMaxPerRouteConnections(10);

		public DockerClient dockerClient = DockerClientBuilder.getInstance(config)
				.withDockerCmdExecFactory(dockerCmdExecFactory).build();
	}

	private static String fastTextDomainUrl = "http://172.17.0.1:3278";
	private static String fastTextServerDockerDomainUrl = "tcp://172.17.0.1:2375";
	private static String fastTextDockerContainerName = "tebloud_fasttext_server";
	/*
	 * private FastTextWrapper.FastTextApi fta;
	 *
	 * public AteFastText() { fta = new FastTextWrapper.FastTextApi(); }
	 *
	 * @PreDestroy public void clearModelCache() { this.unloadModel();
	 * FastTextApi.deallocateReferences(); //FastTextApi.free(fta); fta.close(); }
	 */
	public RemoteAteFastTextImpl(String fastTextServerHost, String fastTextDockerContainerName) {
		fastTextDomainUrl = "http://" + fastTextServerHost + ":3278";
		fastTextServerDockerDomainUrl = "tcp://" + fastTextServerHost + ":2375";
		localDocker = new LocalDocker();
		RemoteAteFastTextImpl.fastTextDockerContainerName = fastTextDockerContainerName;
	}

	public void runCmd(String[] args) {
		// Prepend "fasttext" to the argument list so that it is compatible with C++'s
		// main()
		String[] cArgs = new String[args.length + 1];
		cArgs[0] = "fasttext";
		System.arraycopy(args, 0, cArgs, 1, args.length);
		// fta.runCmd(cArgs.length, new PointerPointer(cArgs));
		Info info = localDocker.dockerClient.infoCmd().exec();
		System.out.print(info);
	}

	public void loadModel(String autName) {

		CountDownLatch startSignal = new CountDownLatch(1);
		try {
			ExecCreateCmdResponse res = localDocker.dockerClient.execCreateCmd(fastTextDockerContainerName)
					.withCmd("bash", "-c", "export autName=" + autName + " && /home/peidong/startFastText").withAttachStdout(true)
					.exec();
			ExecStartResultCallback callback = new ExecStartResultCallback() {

				@Override
				public void onNext(Frame frame) {
					if (frame.toString().contains("Servlet 'dispatcherServlet' configured successfully")) {
						onComplete();
					}
					System.out.println("abcd" + frame);
				}

				@Override
				public void onComplete() {
					try {
						System.out.println("fasttext server  started!");

						// LocalDocker.dockerClient.close();
						startSignal.countDown();
						// } catch (IOException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
					} finally {
						startSignal.countDown();
					}
				}
			};

			localDocker.dockerClient.execStartCmd(res.getId()).exec(callback);
			try {
				startSignal.await(90, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		} finally {
			startSignal.countDown();
		}
	}

	public void loadModel(String autName, RunningModeProperties runningModeProperties) {

		CountDownLatch startSignal = new CountDownLatch(1);
		try {
			ExecCreateCmdResponse res = localDocker.dockerClient.execCreateCmd(fastTextDockerContainerName)
					.withCmd("bash", "-c",
							"export runningMode='" + runningModeProperties.getRunningMode()
									+ "' && export usecaseAPPTrainingEventName='"
									+ runningModeProperties.getUsecaseAPPTrainingEventName()
									+ "' && export tebloud_autName='"
									+ autName
									+ "' && export tebloud_decisionTreeChannelName='"
									+ "peidong-3442-wp"
									+ "' && /home/peidong/startFastText")
					.withAttachStdout(true).exec();
			ExecStartResultCallback callback = new ExecStartResultCallback() {

				@Override
				public void onNext(Frame frame) {
					if (frame.toString().contains("Servlet 'dispatcherServlet' configured successfully")) {
						onComplete();
					}
					System.out.println("abcd" + frame);
				}

				@Override
				public void onComplete() {
					try {
						System.out.println("fasttext server  started!");

						// LocalDocker.dockerClient.close();
						startSignal.countDown();
						// } catch (IOException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
					} finally {
						startSignal.countDown();
					}
				}
			};

			localDocker.dockerClient.execStartCmd(res.getId()).exec(callback);
			try {
				startSignal.await(90, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		} finally {
			startSignal.countDown();
		}
	}

	public void loadModel(String autName,  RunningModeProperties runningModeProperties, TebloudApplicationProperties appProperties) {

		CountDownLatch startSignal = new CountDownLatch(1);
		try {
			ExecCreateCmdResponse res = localDocker.dockerClient.execCreateCmd(fastTextDockerContainerName)
					.withCmd("bash", "-c",
							"export runningMode='" + runningModeProperties.getRunningMode()
									+ "' && export usecaseAPPTrainingEventName='"
									+ runningModeProperties.getUsecaseAPPTrainingEventName()
									+ "' && export tebloud_autName='"
									+ autName
									+ "' && export pio_decisionTreeChannelName='"
									+ appProperties.getPio().getDecisionTreeChannelName()
									+ "' && export fasttext_trainingParams_dim='"
									+ appProperties.getFasttext().getTrainingParams().getDim()
									+ "' && export fasttext_trainingParams_lrUpdateRate='"
									+ appProperties.getFasttext().getTrainingParams().getLrUpdateRate()
									+ "' && export fasttext_trainingParams_ws='"
									+ appProperties.getFasttext().getTrainingParams().getWs()
									+ "' && export fasttext_trainingParams_neg='"
									+ appProperties.getFasttext().getTrainingParams().getNeg()
									+ "' && export fasttext_trainingParams_lr='"
									+ appProperties.getFasttext().getTrainingParams().getLr()
									+ "' && export fasttext_trainingParams_epoch='"
									+ appProperties.getFasttext().getTrainingParams().getEpoch()
									+ "' && export fasttext_trainingParams_thread='"
									+ appProperties.getFasttext().getTrainingParams().getThread()
									+ "' && export fasttext_trainingParams_wordNgrams='"
									+ appProperties.getFasttext().getTrainingParams().getWordNgrams()
									+ "' && export fasttext_trainingParams_loss='"
									+ appProperties.getFasttext().getTrainingParams().getLoss()
									+ "' && /home/peidong/startFastText")

//					fasttext.trainingParams.dim=20
//					fasttext.trainingParams.=5
//					fasttext.trainingParams.ws=5
//					fasttext.trainingParams.neg=5
//					fasttext.trainingParams.lr=1.43
//					fasttext.trainingParams.epoch=65
//					fasttext.trainingParams.thread=8
//					fasttext.trainingParams.wordNgrams=1
//					fasttext.trainingParams.loss=hs


					.withAttachStdout(true).exec();
			ExecStartResultCallback callback = new ExecStartResultCallback() {

				@Override
				public void onNext(Frame frame) {
					if (frame.toString().contains("Servlet 'dispatcherServlet' configured successfully")) {
						onComplete();
					}
					System.out.println("abcd" + frame);
				}

				@Override
				public void onComplete() {
					try {
						System.out.println("fasttext server  started!");

						// LocalDocker.dockerClient.close();
						startSignal.countDown();
						// } catch (IOException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
					} finally {
						startSignal.countDown();
					}
				}
			};

			localDocker.dockerClient.execStartCmd(res.getId()).exec(callback);
			try {
				startSignal.await(90, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		} finally {
			startSignal.countDown();
		}
	}

	public void unloadModel() {
		CountDownLatch startSignal = new CountDownLatch(1);
		try {
			ExecCreateCmdResponse res = localDocker.dockerClient.execCreateCmd(fastTextDockerContainerName)
					.withCmd("bash", "-c", "/home/peidong/stopFastText").withAttachStdout(true).exec();
			ExecStartResultCallback callback = new ExecStartResultCallback() {

				@Override
				public void onComplete() {
					try {
						System.out.println("fasttext server  stopped!");

						// LocalDocker.dockerClient.close();
						startSignal.countDown();
						// } catch (IOException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
					} finally {
						startSignal.countDown();
					}
				}
			};
			localDocker.dockerClient.execStartCmd(res.getId()).exec(callback);
			try {
				startSignal.await(90, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;

		} finally {
			startSignal.countDown();
		}
	}

	/*
	 * public void test(String testFile) { test(testFile, 1); }
	 *
	 * public void test(String testFile, int k) { if (k <= 0) { throw new
	 * IllegalArgumentException("k must be positive"); } fta.test(testFile, k); }
	 *
	 * public String predict(String text){ return predict(text, 1).get(0); // NULL
	 * pointer exception? }
	 *
	 * public List<String> predict(String text, int k) { if (k <= 0) { throw new
	 * IllegalArgumentException("k must be positive"); }
	 * FastTextWrapper.StringVector sv = fta.predict(text, k); List<String>
	 * predictions = new ArrayList<>(); for (int i = 0; i < sv.size(); i++) {
	 * predictions.add(sv.get(i).getString()); } return predictions; }
	 */
	public IAteFastText.ProbLabel predictProba(String text) {
		try {
			return this.remotePredictProba(text);
		} catch (ExecutionException | InterruptedException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new IAteFastText.ProbLabel(-1, "");
		}
	}

	public IAteFastText.ProbLabel predictProba(String autName, String text) {
		try {
			return this.remotePredictProba(autName, text);
		} catch (ExecutionException | InterruptedException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new IAteFastText.ProbLabel(-1, "");
		}
	}
	private IAteFastText.ProbLabel remotePredictProba(String autName, String text)
			throws ExecutionException, InterruptedException, IOException {
		FasttextEngineClient client = new FasttextEngineClient(fastTextDomainUrl);
		StringBuilder tmp = new StringBuilder("");

		JsonObject jObj = client.sendQueryForAut(autName, ImmutableMap.<String, Object>of("text", text));
		client.close();
		Gson gson = new GsonBuilder().create();
		return gson.fromJson(jObj.toString(), IAteFastText.ProbLabel.class);
	}

	private IAteFastText.ProbLabel remotePredictProba(String text)
			throws ExecutionException, InterruptedException, IOException {
		FasttextEngineClient client = new FasttextEngineClient(fastTextDomainUrl);
		StringBuilder tmp = new StringBuilder("");

		JsonObject jObj = client.sendQuery(ImmutableMap.<String, Object>of("text", text));
		client.close();
		Gson gson = new GsonBuilder().create();
		return gson.fromJson(jObj.toString(), IAteFastText.ProbLabel.class);
	}
	public String remoteTraining(String autName, TrainingParams trainingParams)
			throws ExecutionException, InterruptedException, IOException {
		FasttextEngineClient client = new FasttextEngineClient(fastTextDomainUrl);
		StringBuilder tmp = new StringBuilder("");
		Gson gson = new GsonBuilder().create();
		JsonObject jObj = client.sendTrainingQuery(autName, gson.fromJson(gson.toJson(trainingParams), new TypeToken<Map<String, Object>>(){}.getType()));
		client.close();

		String result = jObj.get("result").getAsString();
		return result; //gson.fromJson(jObj.toString(), String.class);
	}


}
