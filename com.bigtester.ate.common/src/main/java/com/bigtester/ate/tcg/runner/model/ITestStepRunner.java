/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.runner.model;

import java.util.Map;

import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.Relationship;

import com.bigtester.ate.tcg.model.ScreenNamePredictStrategy;
import com.bigtester.ate.tcg.model.domain.PredictedFieldName;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord.UserInputType;
import com.bigtester.ate.tcg.model.relationship.Relations;
import com.bigtester.ate.tcg.model.ws.api.ScreenStepsAdvice;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

// TODO: Auto-generated Javadoc
/**
 * This class ITestStepRunner defines ....
 * @author Peidong Hu
 *
 */
@JsonTypeInfo(
	    use = JsonTypeInfo.Id.MINIMAL_CLASS,
	    include = JsonTypeInfo.As.PROPERTY,
	    property = "@class")
public interface ITestStepRunner {
	public static enum ExecutionStatus {
		SKIPPED(0), ONGOING(1), EXECUTED(2), FAIL(3), PASS(4);
		private int value;
		private ExecutionStatus(int value) {
			this.value = value;
		}
		public int getValue() {
			return value;
		}
	}
	String getXpath();
	ExecutionStatus getStepStatus();
	void setStepStatus(ExecutionStatus status);
	int getStepPriority();
	UserInputType getUserInputType();
	/** The input label name. */
	String getInputLabelName(); // htmlLabelContent

	Double getPioPredictConfidence();
	Map<ScreenNamePredictStrategy, ScreenStepsAdvice> executeStep(ITestStepExecutionContext executeEngine, ScreenStepsAdvice stepAdvice) throws Throwable;

	String getComparableHtmlCode();
	long getScreenUitrProbability();
	String getCoreGuid();

	String getElementCoreHtmlCode();
	@JsonIgnore
	boolean isExecutable();

}
