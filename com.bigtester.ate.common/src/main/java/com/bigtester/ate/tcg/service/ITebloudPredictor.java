/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2017, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.bigtester.ate.tcg.model.ScreenNamePredictResult;
import com.bigtester.ate.tcg.model.ScreenNamePredictStrategy;
import com.bigtester.ate.tcg.model.domain.HTMLSource;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord;
import com.bigtester.ate.tcg.model.ml.IMLPredictor;

// TODO: Auto-generated Javadoc
/**
 * This class IPioPredictor defines ....
 * @author Peidong Hu
 *
 */
public interface ITebloudPredictor {
	List<WebElementTrainingRecord> uitrPredict(String domainName, String testCaseName, String screenName, List<WebElementTrainingRecord> uitrs, boolean forRunner, String runningMode);
	double probabilityPredict(WebElementTrainingRecord uitr, String screenName, String testSuiteName, String subSuiteName, String testCaseName);
	public Map<ScreenNamePredictStrategy, Map<String, ScreenNamePredictResult>> pagePredict(String testCaseName, String domainName,
			Map<String, List<HTMLSource>> doms, boolean forRunner)
			throws IOException, ClassNotFoundException, ExecutionException,
			InterruptedException;
	public IMLPredictor getTebloudPredictorMachine();
}
