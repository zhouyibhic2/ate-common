/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.utils;
import static org.joox.JOOX.$;
import static org.joox.JOOX.attr;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.eclipse.jdt.annotation.Nullable;
import org.glassfish.jersey.internal.util.ExceptionUtils;
import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.DomSerializer;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.joox.Context;
import org.joox.Filter;
import org.jsoup.Jsoup;
import org.jsoup.helper.W3CDom;
import org.jsoup.parser.Parser;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.diff.Diff;

import com.bigtester.RunnerGlobalUtils;
import com.bigtester.ate.tcg.controller.WebFormUserInputsCollectorHtmlTerms;
import com.bigtester.ate.tcg.model.domain.ComputedCssSize;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord;
import com.bigtester.ate.tcg.utils.exception.Html2DomException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


// TODO: Auto-generated Javadoc
/**
 * The Class GlobalUtils.
 */
public final class GlobalUtils {

	public static final String GUID_REGEX = "[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}";
	private GlobalUtils() {

	}

	public static Document removeScriptStylesAndInvisiables(Document doc) {
		return (Document) removeScriptStylesAndInvisiablesFromNode(doc);
	}

	public static Element getBodyEquevelantElement(Document doc) {
		if (doc.getElementsByTagName("frameset").getLength()>0) {
	        return (Element) doc.getElementsByTagName("frameset").item(0);
	    } else {
	    	return (Element) doc.getElementsByTagName("body").item(0);
	    }
	}

	public static String parseStackTrace(Throwable e) {
		return ExceptionUtils.exceptionStackTraceAsString(e);
	}
	public static Document addSpacesIntoTextNodes(Document doc) {
		//Filter textNodefilter = new Filter
		//List<Node> textNodes = new ArrayList<Node>();
		List<Element> nodes = $(doc).find("*").get();
//		nodes.forEach(action);(new Filter() {
//	        @Override
//	        public boolean filter(Context context) {
//	        	System.out.println(" ");
//	        	NodeList children = context.element().getChildNodes();
//	        	for (int ind = 0; ind<children.getLength(); ind++) {
//
//	        	}
//	        	return context.element().getNodeType()==Node.TEXT_NODE;
//	        }
//	    })
		for (int index = 0; index < nodes.size(); index++) {
			Element cNode = nodes.get(index);
			NodeList children = cNode.getChildNodes();
        	for (int ind = 0; ind<children.getLength(); ind++) {
        		if (children.item(ind).getNodeType()==Node.TEXT_NODE) {
        			children.item(ind).setNodeValue(" " + children.item(ind).getNodeValue()+" ");
        			//children.item(ind).setNodeValue("ate_ate_ate_ate_ate_ate" + children.item(ind).getNodeValue()+"ate_ate_ate_ate_ate_ate");;
        		}
        	}


		}
		return doc;
	}

	public static Node removeScriptStylesAndInvisiablesFromNode(Node doc) {
		List<Element> nodes = $(doc).find("script").get();
		List<Element> styleNodes = $(doc).find("style").get();
		nodes.addAll(styleNodes);
		for (int index = 0; index < nodes.size(); index++) {
			nodes.get(index).getParentNode().removeChild(nodes.get(index));

		}
		List<Element> nodes2 = $(doc).find(
				attr(WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME,
						WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE))
				.get();
		for (int index = 0; index < nodes2.size(); index++) {
			nodes2.get(index).getParentNode()
					.removeChild(nodes2.get(index));
		}
		doc.normalize();
		return doc;
	}

	public static String replaceGuidId(String org) {


		      Pattern pat = Pattern.compile(GUID_REGEX);

		      Matcher m = pat.matcher(org);


		      StringBuffer sb = new StringBuffer();
		      while(m.find())  {
		         String rplcWith = UUID.randomUUID().toString();
		         m.appendReplacement(sb, rplcWith);
		      }
		      m.appendTail(sb);

		      return sb.toString();

	}

	public static String removeGuidId(String org) {


	      Pattern pat = Pattern.compile(GUID_REGEX);

	      Matcher m = pat.matcher(org);


	      StringBuffer sb = new StringBuffer();
	      while(m.find())  {
	         //String rplcWith = UUID.randomUUID().toString();
	         m.appendReplacement(sb, "");
	      }
	      m.appendTail(sb);

	      return sb.toString();

}

	public static String removeAllDigits(String org){

		Pattern p = Pattern.compile("-?\\d+");
		Matcher m = p.matcher(org);
		StringBuffer sb = new StringBuffer();
	      while(m.find())  {
	         m.appendReplacement(sb, "");
	      }
	      m.appendTail(sb);

	      return sb.toString();

	}

	public static boolean isSameHtmlCodeWithoutAteGuid(String source1, String source2) {
		return source1.equalsIgnoreCase(source2);
		/*
		HtmlCleaner cleaner = new HtmlCleaner();

		// take default cleaner properties
		CleanerProperties props = cleaner.getProperties();



		boolean retVal = false;
		Diff  myDiffSimilar = null;
		if (source1 == null && source2 == null) retVal = true;
		else if (source1!=null && source2!=null) {
			try {
				TagNode node1 = cleaner.clean(source1);
				Document dSource1 = new DomSerializer(props, true).createDOM(node1);
				TagNode node2 = cleaner.clean(source2);
				Document dSource2 = new DomSerializer(props, true).createDOM(node2);
				myDiffSimilar = DiffBuilder.compare(dSource1).withTest(dSource2)
					     .withNodeMatcher(new DefaultNodeMatcher(ElementSelectors.byNameAndText))
					     .checkForIdentical()
					     .build();
			} catch (ParserConfigurationException e) {
				myDiffSimilar = DiffBuilder.compare(source1).withTest(source2)
					     .withNodeMatcher(new DefaultNodeMatcher(ElementSelectors.byNameAndText))
					     .checkForIdentical()
					     .build();
			}

			if (myDiffSimilar != null && myDiffSimilar.hasDifferences()) {
				List<Difference> diffs = StreamSupport.stream(myDiffSimilar.getDifferences().spliterator(), true).filter(diff->
					diff.getComparison().getControlDetails().getTarget() == null || !diff.getComparison().getControlDetails().getTarget().getNodeName().equalsIgnoreCase("ate-guid")
				).collect(Collectors.toList());
				if (diffs.isEmpty()) {
					retVal = true;
				}
			} else {
				retVal = true;
			}
		}
		return retVal;*/
	}


//    String attributesToRemove = "ate-computed-height|ate-computed-lefttopcornerx|ate-computed-lefttopcornery|ate-computed-width";
public static String cleanHtmlFragment(String htmlFragment, String attributesToRemove) {
return htmlFragment.replaceAll("\\s+(?:" + attributesToRemove + ")\\s*=\\s*\"[^\"]*\"","");
}


	public static boolean isSimilarHtmlStructure(String source1, String source2) {


		HtmlCleaner cleaner = new HtmlCleaner();

		// take default cleaner properties
		CleanerProperties props = cleaner.getProperties();



		boolean retVal = false;
		Diff  myDiffSimilar = null;
		if (source1 == null && source2 == null) retVal = true;
		else if (source1!=null && source2!=null) {
			try {
				TagNode node1 = cleaner.clean(source1);
				Document dSource1 = new DomSerializer(props, true).createDOM(node1);
				TagNode node2 = cleaner.clean(source2);
				Document dSource2 = new DomSerializer(props, true).createDOM(node2);

				myDiffSimilar = DiffBuilder.compare(dSource1).withTest(dSource2)
						.ignoreComments().ignoreWhitespace()
					     .checkForSimilar()
					     .build();
			} catch (ParserConfigurationException e) {
				myDiffSimilar = DiffBuilder.compare(source1).withTest(source2)
						.ignoreComments().ignoreWhitespace()
					     .checkForSimilar()
					     .build();
			}

			retVal = !myDiffSimilar.hasDifferences();
		}
		retVal = retVal && !areInclusiveHtmlNodes(source1, source2);
		return retVal;
	}

public static boolean isSimilarHtmlStructure(Node node1, Node node2, String source1, String source2 ) {
		boolean retVal = false;
		Diff  myDiffSimilar = null;
		if (node1 == null && node2 == null) retVal = true;
		else if (node1!=null && node2!=null) {
				myDiffSimilar = DiffBuilder.compare(node1).withTest(node2)
						.ignoreComments().ignoreWhitespace()
					     .checkForSimilar()
					     .build();

			retVal = !myDiffSimilar.hasDifferences();
		}
		retVal = retVal && !areInclusiveHtmlNodes(source1, source2);
		return retVal;
	}

	public static boolean areInclusiveHtmlNodes(String source1, String source2) {

		boolean retVal = (source1
				.indexOf(RunnerGlobalUtils.parseFirstAteGuidByBoundary(source2)) !=-1 || source2
						.indexOf(RunnerGlobalUtils.parseFirstAteGuidByBoundary(source1)) !=-1 );
//		&& (source1.indexOf(RunnerGlobalUtils
//				.parseFirstAteGuidByBoundary(source1)) <= source1
//				.indexOf(RunnerGlobalUtils.parseFirstAteGuidByBoundary(source2))
//				|| source2.indexOf(RunnerGlobalUtils
//						.parseFirstAteGuidByBoundary(source2)) <= source2
//						.indexOf(RunnerGlobalUtils
//								.parseFirstAteGuidByBoundary(source1)));
		return retVal;
	}



	/**
	 * Html2 dom.
	 *
	 * @param source
	 *            the source
	 * @return the document
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws ParserConfigurationException
	 *             the parser configuration exception
	 * @throws Html2DomException
	 */
	public static Document html2Dom(String source) throws IOException,
			ParserConfigurationException, Html2DomException {
		org.jsoup.nodes.Document jsoupDoc = Jsoup.parse(source);
		W3CDom w3cd = new W3CDom();
		Document doc = null;
		try {
			doc = w3cd.fromJsoup(jsoupDoc);
		} catch (Exception exp) {

		DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance()
				.newDocumentBuilder();
		InputSource inputS = new InputSource();
		inputS.setCharacterStream(new StringReader(jsoupDoc.html()));


		try {

			doc = docBuilder.parse(inputS);// NOPMD

			// printDocument(doc.getDocumentElement(), System.out);
			// col = new WebFormUserInputsCollector(webD, doc, xpathOfFrame);
		} catch (Exception e) {
			// create an instance of HtmlCleaner
			HtmlCleaner cleaner = new HtmlCleaner();

			// take default cleaner properties
			CleanerProperties props = cleaner.getProperties();

			TagNode node = cleaner.clean(source);
			doc = new DomSerializer(props, true).createDOM(node);
		}
		}

		if (null == doc)
			throw new Html2DomException("html can't be converted to dom");
		return doc;
	}

	/**
	 * Html2 element.
	 *
	 * @param source the source
	 * @return the optional
	 */
	public static Optional<Element> html2Element(String source) {
//		ClassLoader classLoader = FileUtils.class.getClassLoader();
//
//		URL resource = classLoader.getResource("org/jsoup/nodes/Document.class");
//		System.out.println(resource);
		org.jsoup.nodes.Document jsoupDoc = Jsoup.parse("<html><body>" + source + " </body></html>", "", Parser.xmlParser());
		W3CDom w3cd = new W3CDom();
		Document doc = w3cd.fromJsoup(jsoupDoc);

		if (null == doc)
			return Optional.empty();
		return Optional.of((Element) doc.getFirstChild().getFirstChild().getFirstChild());
		/*try {
			return Optional.of((Element)DocumentBuilderFactory
				    .newInstance()
				    .newDocumentBuilder()
				    .parse(new ByteArrayInputStream(source.getBytes()))
				    .getDocumentElement());
		} catch (SAXException | IOException | ParserConfigurationException e) {
			// create an instance of HtmlCleaner
						HtmlCleaner cleaner = new HtmlCleaner();

						// take default cleaner properties
						CleanerProperties props = cleaner.getProperties();

						TagNode node = cleaner.clean(source);
						try {
							Document doc = new DomSerializer(props, true).createDOM(node);
							return Optional.of((Element) doc.getDocumentElement());
						} catch (ParserConfigurationException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							return Optional.empty();
						}
		}*/
	}

public static Optional<Node> html2Node(String source) {

		org.jsoup.nodes.Document jsoupDoc = Jsoup.parse("<html><body>" + source + " </body></html>", "", Parser.xmlParser());
		W3CDom w3cd = new W3CDom();
		Document doc = w3cd.fromJsoup(jsoupDoc);

		if (null == doc)
			return Optional.empty();
		return Optional.of(doc.getFirstChild().getFirstChild().getFirstChild());
}

	  static public String removeTagAndAttributeNamesAndDigits(String htmlSource) {

		  //HtmlCleaner cleaner = new HtmlCleaner();

			// take default cleaner properties
			//CleanerProperties props = cleaner.getProperties();

		//TagNode node = cleaner.clean(htmlSource);

		  String pageContent = GlobalUtils.removeAllDigits(GlobalUtils.removeAteAttributesValues(htmlSource));//cleaner. .getInnerHtml(node);
		  pageContent = pageContent.replaceAll("\\=\\=\\\"", "\"");
		  Pattern pattern = Pattern.compile("<(?!!)(?!/)\\s*([a-zA-Z0-9]+)(.*?)>");
		  Matcher matcher = pattern.matcher(pageContent);
		  StringBuffer sb = new StringBuffer(pageContent);
		  String newStr = sb.toString();
		  newStr = newStr.replaceAll("<\\s*/\\s*", "</");
		  newStr = newStr.replaceAll("<\\s*", "<");


		  while (matcher.find()) {
		      String tagName = matcher.group(1);
		      String attributes = matcher.group(2);
		      //newStr = newStr.replaceAll("</"+tagName, "");
		      //System.out.println("newStr1: " + newStr);
		      //System.out.println("tag name: " + tagName);
		      //System.out.println("     rest of the tag: " + attributes);
		      //Pattern attributePattern = Pattern.compile("(\\S+)=['\"]{1}([^>]*?)['\"]{1}");
		      Pattern attributePattern = Pattern.compile("(\\S+\\s*=\\s*['\"]){1}([^>]*?)(['\"]\\s*){1}");
		      Matcher attributeMatcher = attributePattern.matcher(attributes);
		     // System.out.println("attr list \n");
		      StringBuffer sbAttrValues = new StringBuffer();
		      while(attributeMatcher.find()) {

		          //String attributeName = attributeMatcher.group(1);
		          //String attributeValue = attributeMatcher.group(2);
		          //attributeMatcher.replaceAll("$2");

		          attributeMatcher.appendReplacement(sbAttrValues, " $2 ");
		    	  //newStr.replaceAll(regex, replacement)



		    	 // System.out.println(sb.toString());
		         // System.out.println("         attribute name: " + attributeName + "    value: " + attributeValue + "\n");
		      }
		      newStr = newStr.replace(attributes, sbAttrValues.toString()).replace("<"+tagName,"").replace("</"+tagName,"");
		      //System.out.println("newStr2: " + newStr);
		  }
	  	  //return sb.toString().replaceAll("[<>]", "").replaceAll(">", "").replaceAll("\\s+", " ").replaceAll(",", " ");
		  String retVal = newStr.toString().replaceAll("[<>\\_'\",!;\\.\\-:\\)\\(]", " ").replaceAll("/", " ").replaceAll("\\s+", " ");
		  //System.out.println("machine train html: " + retVal);
		  return retVal;
	  }

	  public static String removeAteAttributesValues(String inStr) {
		  String attributesToRemove = WebFormUserInputsCollectorHtmlTerms.PREVIOUS_PROCESSED_ATTR_NAME
					+ "|"
					+ WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME
					+ "|ate-guid|ate-computed-height|ate-computed-lefttopcornerx|ate-computed-lefttopcornery|ate-computed-width|ate-computed-xpath|xmlns";
			inStr = GlobalUtils.cleanHtmlFragment(inStr, attributesToRemove);
			return inStr;
	  }
	  public static String convertToComparableString(String inStr) {
			inStr = GlobalUtils.removeAteAttributesValues(inStr);
			inStr = inStr.replaceAll("\\s*[\\r\\n]+\\s*", "").trim();
			inStr = inStr.replaceAll("\\s+", "");
			inStr = inStr.replaceAll("\\t", "");

			return inStr;
		}

	  public static void normalizeUitr(WebElementTrainingRecord uitr, int screenWidth,
				int topDocWidth, int topDocHeight) {
//			uitr.setInputMLHtmlCode(uitr.getInputMLHtmlCode().replaceAll("\r", "")
//					.replaceAll("\n", ""));
//
//			uitr.setInputMLHtmlWords(GlobalUtils.removeTagAndAttributeNamesAndDigits(uitr
//					.getInputMLHtmlCode()));
//			if (uitr.getElementCoreHtmlCode().isEmpty()) {
//				uitr.setElementCoreHtmlCode(uitr.getInputMLHtmlCode());
//			}
//
//			uitr.setElementCoreHtmlCode(uitr.getElementCoreHtmlCode()
//					.replaceAll("\r", "").replaceAll("\n", ""));
//			uitr.setElementCoreHtmlCodeWithoutGuidValue(convertToComparableString(uitr.getElementCoreHtmlCode()));
		  	nomalizeUitr(uitr);
			ComputedCssSize size = GlobalUtils.parseComputedCssSize(uitr
					.getInputMLHtmlCode());

			if (uitr.getElementCoreHtmlCode() != null) {
				ComputedCssSize coreSize = GlobalUtils.parseComputedCssSize(uitr
						.getElementCoreHtmlCode());
				if (coreSize.getUitrSize() > size.getUitrSize())
					size = coreSize;
			}
			size.setScreenWidth(screenWidth);
			size.setTopDocumentWidth(topDocWidth);
			size.setTopDocumentHeight(topDocHeight);
			HashSet<ComputedCssSize> sizes = new HashSet<ComputedCssSize>();
			sizes.add(size);
			uitr.setComputedCssSizes(sizes);
		}

	  public static void nomalizeUitr(WebElementTrainingRecord uitr) {
			uitr.setInputMLHtmlCode(GlobalUtils.normalizeHtmlString(uitr.getInputMLHtmlCode().replaceAll("\r", "")
					.replaceAll("\n", "")));

			uitr.setInputMLHtmlWords(GlobalUtils.removeTagAndAttributeNamesAndDigits(uitr
					.getInputMLHtmlCode()));
			if (uitr.getElementCoreHtmlCode().isEmpty()) {
				uitr.setElementCoreHtmlCode(uitr.getInputMLHtmlCode());
			}

			uitr.setElementCoreHtmlCode(normalizeHtmlString(uitr.getElementCoreHtmlCode()
					.replaceAll("\r", "").replaceAll("\n", "")));
			uitr.setElementCoreHtmlCodeWithoutGuidValue(convertToComparableString(uitr.getElementCoreHtmlCode()));
			uitr.setXpath(parseComputedXpath(uitr.getInputMLHtmlCode()));

		}

static public String parseComputedXpath(String htmlSource) {


		  String pageContent = htmlSource;//cleaner. .getInnerHtml(node);
		  pageContent = pageContent.replaceAll("\\=\\=\\\"", "\"");
		  Pattern pattern = Pattern.compile("<(?!!)(?!/)\\s*([a-zA-Z0-9]+)(.*?)>");
		  Matcher matcher = pattern.matcher(pageContent);
		  StringBuffer sb = new StringBuffer(pageContent);
		  String newStr = sb.toString();
		  newStr = newStr.replaceAll("<\\s*/\\s*", "</");
		  newStr = newStr.replaceAll("<\\s*", "<");

		  String xpath = "";
		  while (matcher.find()) {

		      String attributes = matcher.group(2);
		      Pattern attributePattern = Pattern.compile("(\\S+\\s*=\\s*['\"]){1}([^>]*?)(['\"]\\s*){1}");
		      Matcher attributeMatcher = attributePattern.matcher(attributes);
		     // System.out.println("attr list \n");
		      while(attributeMatcher.find()) {

		          String attributeName = attributeMatcher.group(1);
		          String attributeValue = attributeMatcher.group(2);

		          if (attributeName.equalsIgnoreCase("ate-computed-xpath=\"")) {
		        	  xpath = attributeValue;
		        	  break;
		          }
		      }
		      break;
		  }

		  return xpath;
	  }

	  static public ComputedCssSize parseComputedCssSize(String htmlSource) {


		  String pageContent = htmlSource;//cleaner. .getInnerHtml(node);
		  pageContent = pageContent.replaceAll("\\=\\=\\\"", "\"");
		  Pattern pattern = Pattern.compile("<(?!!)(?!/)\\s*([a-zA-Z0-9]+)(.*?)>");
		  Matcher matcher = pattern.matcher(pageContent);
		  StringBuffer sb = new StringBuffer(pageContent);
		  String newStr = sb.toString();
		  newStr = newStr.replaceAll("<\\s*/\\s*", "</");
		  newStr = newStr.replaceAll("<\\s*", "<");

		  ComputedCssSize retVal = new ComputedCssSize();
		  while (matcher.find()) {

		      String attributes = matcher.group(2);
		      Pattern attributePattern = Pattern.compile("(\\S+\\s*=\\s*['\"]){1}([^>]*?)(['\"]\\s*){1}");
		      Matcher attributeMatcher = attributePattern.matcher(attributes);
		     // System.out.println("attr list \n");
		      while(attributeMatcher.find()) {

		          String attributeName = attributeMatcher.group(1);
		          String attributeValue = attributeMatcher.group(2);

		          if (attributeName.equalsIgnoreCase("ate-computed-width=\"")) {
		        	  retVal.setUitrWidth(Integer.valueOf(attributeValue));
		          }
		          if (attributeName.equalsIgnoreCase("ate-computed-height=\"")) {
		        	  retVal.setUitrHeight(Integer.valueOf(attributeValue));
		          }
		          if (attributeName.equalsIgnoreCase("ate-computed-lefttopcornerx=\"")) {
		        	  retVal.setLeftTopCornerX(Double.valueOf(attributeValue));
		          }
		          if (attributeName.equalsIgnoreCase("ate-computed-lefttopcornery=\"")) {
		        	  retVal.setLeftTopCornerY(Double.valueOf(attributeValue));
		          }
		          if (retVal.getUitrWidth()!=0 && retVal.getUitrHeight()!=0 && retVal.getLeftTopCornerX()!=0 && retVal.getLeftTopCornerY()!=0)
		        	  break;
		      }
		      break;
		  }

		  return retVal;
	  }
	/**
	 * Prints the document.
	 *
	 * @param doc
	 *            the doc
	 * @param out
	 *            the out
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws TransformerException
	 *             the transformer exception
	 */
	public static void printDocument(Node doc, OutputStream out)// NOPMD
			throws IOException, TransformerException {
		TransformerFactory transF = TransformerFactory.newInstance();
		Transformer transformer = transF.newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		// transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		// transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		// transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		// transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount",
		// "4");

		transformer.transform(new DOMSource(doc), new StreamResult(
				new OutputStreamWriter(out, "UTF-8")));
	}

	public static String printObject(Object obj) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "failed print";
		}
	}
	  public static String removeAllXmlNamespace(String xmlData)
	  {
	      String xmlnsPattern = "\\s+xmlns\\s*(:\\w)?\\s*=\\s*\\\"(?<url>[^\\\"]*)\\\"";
	      return xmlData.replaceAll(xmlnsPattern, "");


	  }

	  public static String normalizeHtmlString (String inHtmlString) {
		  Optional<Node> node = GlobalUtils.html2Node(inHtmlString);
		  return node.isPresent()? GlobalUtils.domNodeToString(node.get(), true, false) : "";
	  }
	public static String domNodeToString(Node node, boolean omitXmlDeclaration, boolean prettyPrint) {
	    if (node == null) {
	        throw new IllegalArgumentException("node is null.");
	    }

	    try {
	        // Remove unwanted whitespaces
//	        XPath xpath = XPathFactory.newInstance().newXPath();
//	        XPathExpression expr = xpath.compile("//text()[normalize-space()='']");
//	        NodeList nodeList = (NodeList)expr.evaluate(node, XPathConstants.NODESET);
//
//	        for (int i = 0; i < nodeList.getLength(); ++i) {
//	            Node nd = nodeList.item(i);
//	            if (nd!=null && nd.getParentNode()!=null)
//	            nd.getParentNode().removeChild(nd);
//	        }

	        // Create and setup transformer
	        Transformer transformer =  TransformerFactory.newInstance().newTransformer();
	        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

	        if (omitXmlDeclaration == true) {
	           transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
	        }

	        if (prettyPrint == true) {
	           transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	           transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
	        }

	        // Turn the node into a string
	        StringWriter writer = new StringWriter();
	        transformer.transform(new DOMSource(node), new StreamResult(writer));
	        return removeAllXmlNamespace(writer.toString());
	    } catch (TransformerException e) {
	        throw new RuntimeException(e);
	    }
//	    catch (XPathExpressionException e) {
//	        throw new RuntimeException(e);
//	    }
	}
	/**
	 * Instantiates a new web form elements collector.
	 *
	 * @param domDoc
	 *            the dom doc
	 * @param parentFrame
	 *            the parent frame, Nullable
	 * @throws ParserConfigurationException
	 *             the parser configuration exception
	 * @throws Html2DomException
	 */
	public static Document cleanUpDoc(Document domDoc,
			String xpathOfParentFrame) throws ParserConfigurationException,
			Html2DomException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		DocumentBuilder builder = dbf.newDocumentBuilder();
		Document tempDoc = builder.newDocument();
		if (null == tempDoc)
			throw new Html2DomException("dom builder");
		Document cleanedDoc = tempDoc;

		Node root = cleanedDoc.importNode(domDoc.getDocumentElement(), true);
		cleanedDoc.appendChild(root);
		Element tempElement = cleanedDoc.getDocumentElement();
		if (tempElement == null)
			throw new Html2DomException("dom element");
		fnCleanNode(tempElement);
		return cleanedDoc;
	}
	/**
	 * Fn clean node.
	 *
	 * @param node
	 *            the node
	 */
	private static void fnCleanNode(Node node) {
		int index = 0;
		NodeList cNodes = node.getChildNodes();
		Node tmpNode;
		while ((tmpNode = cNodes.item(index++)) != null)//NOPMD
			// NOPMD
			switch (tmpNode.getNodeType()) { // NOPMD
			case Node.ELEMENT_NODE: // Element Node
				if (tmpNode.getNodeName().equalsIgnoreCase("br")) {
					node.removeChild(tmpNode);
					index--;// NOPMD
					break;
				} else {
					fnCleanNode(tmpNode);
					break;
				}
			case Node.TEXT_NODE: // Text Node
				if (tmpNode.getNodeValue().trim().equals("")) {
					node.removeChild(tmpNode);
					index--; //NOPMD
					break;
				} else {
					break;
				}
			case Node.COMMENT_NODE: // Comment Node (and Text Node without
									// non-whitespace
				// content)
				node.removeChild(tmpNode);
				index--;
			}
	}

	/**
	 * Gets the string from input stream.
	 *
	 * @param inputS
	 *            the is
	 * @return the string from input stream
	 * @throws IOException
	 */
	public static String getStringFromInputStream(InputStream inputS)
			throws IOException {

		BufferedReader reader;
		StringBuilder stringBuilder = new StringBuilder();
		String line;

		reader = new BufferedReader(new InputStreamReader(inputS));
		while ((line = reader.readLine()) != null) {// NOPMD
			stringBuilder.append(line);
		}
		if (reader != null) {
			reader.close();
		}
		String retVal = stringBuilder.toString();
		if (null == retVal)
			retVal = "";
		return retVal;

	}

	/**
	 * Null to non null handling.
	 *
	 * @param obj the obj
	 * @param exceptionMsg the exception msg
	 * @return the object
	 */
	public static Object nullToNonNullHandling(@Nullable Object obj, String exceptionMsg) {
		if (null == obj) throw new IllegalStateException(exceptionMsg);
		return obj;
	}
	/**
	 * Gets the target object.
	 *
	 * @param <T>
	 *            the generic type
	 * @param proxy
	 *            the proxy
	 * @return the target object
	 * @throws Exception
	 */

	@SuppressWarnings("unchecked")
	public static <T> T getTargetObject(@Nullable Object proxy) { //NOPMD
		if (proxy == null ) throw GlobalUtils.createNotInitializedException("proxyed object");
		try {
		if (AopUtils.isJdkDynamicProxy(proxy)) {

				return (T) getTargetObject(((Advised) proxy).getTargetSource()//NOPMD
						.getTarget());

		} else if(AopUtils.isAopProxy(proxy) && proxy instanceof Advised) {
		    return (T) ((Advised)proxy).getTargetSource().getTarget();

		}
		} catch (Exception e) {//NOPMD
			throw GlobalUtils.createInternalError("proxied object error", e);
		}
		return (T) proxy; // expected to be cglib proxy then, which is simply a
							// specialized class
	}

	/**
	 * Throw not initialized exception.
	 *
	 * @param variableName
	 *            the variable name
	 */
	public static IllegalStateException createNotInitializedException(
			String variableName) {
		return new IllegalStateException(variableName
				+ " not correctly populated.");
	}

	/**
	 * Throw not initialized exception.
	 *
	 * @param variableName
	 *            the variable name
	 */
	public static IllegalStateException createNotInitializedException(
			String variableName, Throwable cause) {
		return new IllegalStateException(variableName
				+ " not correctly populated.", cause);
	}

	/**
	 * Creates the internal error.
	 *
	 * @param errorPlace
	 *            the error place
	 * @return the illegal state exception
	 */
	public static IllegalStateException createInternalError(String errorPlace) {
		return new IllegalStateException("internal error at: " + errorPlace);
	}

	/**
	 * Creates the internal error.
	 *
	 * @param errorPlace
	 *            the error place
	 * @return the illegal state exception
	 */
	public static IllegalStateException createInternalError(String errorPlace,
			Throwable cause) {
		return new IllegalStateException("internal error at: " + errorPlace,
				cause);
	}

}
