/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.service.repository;

import java.util.Map;
import java.util.stream.Stream;

import org.neo4j.ogm.model.Result;
import org.springframework.data.neo4j.annotation.Depth;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bigtester.ate.tcg.model.domain.UserInputTrainingRecord;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord;


// TODO: Auto-generated Javadoc
/**
 * This class ScreenNodeRepo defines ....
 * @author Peidong Hu
 *
 */
@Repository
public interface UserInputTrainingRecordRepo extends Neo4jRepository<UserInputTrainingRecord, Long> {

	/**
	 * Find by pio predict label result value.
	 *
	 * @param value the value
	 * @return the iterable
	 */
	Iterable<UserInputTrainingRecord> findByPioPredictLabelResultValue(String value);

	/**
	 * Find by input label name.
	 *
	 * @param value the value
	 * @return the iterable
	 */
	Iterable<UserInputTrainingRecord> findByInputLabelName(String value);

	/**
	 * Find by input ml html code.
	 *
	 * @param htmlCode the html code
	 * @return the iterable
	 */
	Iterable<UserInputTrainingRecord> findByInputMLHtmlCode(String htmlCode);

	/**
	 * Find by element core html code without ate guid.
	 *
	 * @param elementCoreHtmlCodeWithoutGuidValue the element core html code without guid value
	 * @return the iterable
	 */
	//@Query("match (uitr:UserInputTrainingRecord)-[r where uitr.elementCoreHtmlCodeWithoutGuidValue={elementCoreHtmlCodeWithoutGuidValue} return uitr")
	//@Depth(2)
	Iterable<UserInputTrainingRecord> findByElementCoreHtmlCodeWithoutGuidValue(String elementCoreHtmlCodeWithoutGuidValue);
	//'match (n)-[l]-(p) match (n:Neo4jScreenNode)-[r:IN_TESTCASE]-(m:TestCase{name:'RegisterLogin'})  return n,l'
	@Query("match (uitr:UserInputTrainingRecord{elementCoreHtmlCodeWithoutGuidValue:{elementCoreHtmlCodeWithoutGuidValue}})-[l]-(q) match (uitr)-[r:IN_TESTCASE]-(m:TestCase{name:{testCaseName}}) return uitr, l,q")
	//@Depth(2)
	Iterable<UserInputTrainingRecord> findByElementCoreHtmlCodeWithoutGuidValueaInTestCase(@Param("elementCoreHtmlCodeWithoutGuidValue") String elementCoreHtmlCodeWithoutGuidValue, @Param("testCaseName") String testCaseName);

	@Query("match (n:WebElementTrainingRecord)  where n.inputLabelName is not null and n.inputMLHtmlWords is not null return n.inputMLHtmlWords as trainingWords, n.inputLabelName as label")
	Iterable<Map<String, String>> findAllUitrLabelAndTrainingWords();

	@Query("match (n) where id(n)={graphId} OPTIONAL MATCH (n)-[r]-()  delete n,r")
	Result deleteUitr(@Param("graphId") Long graphId);

	@Query("MATCH (m:TebloudUserZone)-[d:CONTAINS_SCREEN]->(screen:Neo4jScreenNode)-[userinput_r]-(userinput)-[inTest:IN_TESTCASE]-(testcase:TestCase), (userinput)-[:PREDICTED_FIELD_NAME]-(label), (userinput)-[v:USER_VALUES]-(value1) where m.zoneDomainName=~{domainName} and screen.name=~{screenName} and testcase.name=~{testCaseName} and label.value={labelName} RETURN userinput,v, value1")

	Iterable<UserInputTrainingRecord> findAllUitrsByPioPredictLabelResultValue(
			@Param("domainName") String domainName,
			@Param("testCaseName") String testCaseName,
			@Param("screenName") String screenName, @Param("labelName") String labelName);

	@Query("MATCH (m:TebloudUserZone)-[d:CONTAINS_SCREEN]->(screen:Neo4jScreenNode)-[userinput_r]-(userinput)-[inTest:IN_TESTCASE]-(testcase:TestCase), (userinput)-[v:USER_VALUES]-(value1) where m.zoneDomainName=~{domainName} and screen.name=~{screenName} and testcase.name=~{testCaseName} and userinput.inputLabelName={labelName} RETURN userinput,v, value1")
	//@Depth(2) to support depth in custom query, the return statement needs to have all node and relationship returned to wrap it up
	Iterable<UserInputTrainingRecord> findAllUitrsByUserInputLabelNameProperty(
			@Param("domainName") String domainName,
			@Param("testCaseName") String testCaseName,
			@Param("screenName") String screenName, @Param("labelName") String labelName);

	@Query("match (z:TebloudUserZone)-[r:CONTAINS_SCREEN]-()-[]-(we:WebElementTrainingRecord) where z.zoneDomainName={zoneDomainName} return we")
	Iterable<WebElementTrainingRecord> findAllUitrsInZone(@Param("zoneDomainName") String zoneDomainName) ;


}
