/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.controller;

import org.apache.commons.lang3.ArrayUtils;

// TODO: Auto-generated Javadoc
/**
 * This class WebFormUserInputsCollectorHtmlTerms defines ....
 * @author Peidong Hu
 *
 */
public class WebFormUserInputsCollectorHtmlTerms {
	public enum HtmlInputType {
		RADIO ("radio"),HIDDEN ("hidden"),SUBMIT ("submit"),TEXT("text"),DATE("date"),BUTTON("button"),DATETIME("datetime"),
		DATETIMELOCAL("datetime-local"),EMAIL("email"), FILE("file"), IMAGE("image"),MONTH("month"), NUMBER("number"),
		PASSWORD("password"), RANGE("range"), RESET("reset"), SEARCH("search"),TEL("tel"), TIME("time"),
		URL("url"), WEEK("week"),CHECKBOX("checkbox");

	    private final String name;       

	    private HtmlInputType(String str) {
	        name = str;
	    }

	    public boolean equalsName(String otherName) {
	        // (otherName == null) check is not needed because name.equals(null) returns false 
	        return name.equals(otherName);
	    }

	    public String toString() {
	       return this.name;
	    }
	}
	public enum UserCollectableHtmlTag {
		SELECT("select"),
		INPUT("input"), TEXTAREA("textarea"), SUBMIT("submit"),BUTTON("button"); 

	    private final String name;       

	    private UserCollectableHtmlTag(String str) {
	        name = str;
	    }

	    public boolean equalsName(String otherName) {
	        // (otherName == null) check is not needed because name.equals(null) returns false 
	        return name.equals(otherName);
	    }

	    public String toString() {
	       return this.name;
	    }
	}
	/** The Constant PREVIOUS_PROCESSED_ATTR_NAME. */
	public final static String PREVIOUS_PROCESSED_ATTR_NAME = "ate-processed";
	
	/** The Constant ATE_INVISIBLE_ATTR_NAME. */
	public final static String ATE_INVISIBLE_ATTR_NAME = "ate-invisible";
	
	/** The Constant ATE_YES_ATTR_VALUE. */
	public final static String ATE_YES_ATTR_VALUE = "yes";
	
	/** The Constant USER_NOT_CHANGABLE_INPUT_TYPES. */
	final static public String[] USER_NOT_CHANGABLE_INPUT_TYPES = { HtmlInputType.HIDDEN.toString()};
	
	/** The Constant USER_FORM_SUBMIT_INPUT_TYPES. */
	final static public String[] USER_FORM_SUBMIT_INPUT_TYPES = { HtmlInputType.SUBMIT.toString() };
	
	/** The Constant LEFT_LABELED_INPUT_TYPES. */
	final static public String[] LEFT_LABELED_INPUT_TYPES = { HtmlInputType.TEXT.toString(), HtmlInputType.DATE.toString(),
		//HtmlInputType.BUTTON.toString(),
		HtmlInputType.DATETIME.toString(), HtmlInputType.DATETIMELOCAL.toString(), HtmlInputType.EMAIL.toString(), HtmlInputType.FILE.toString(), HtmlInputType.IMAGE.toString(),
		HtmlInputType.MONTH.toString(), HtmlInputType.NUMBER.toString(), HtmlInputType.PASSWORD.toString(), HtmlInputType.RANGE.toString(), HtmlInputType.RESET.toString(), HtmlInputType.SEARCH.toString(),
		HtmlInputType.TEL.toString(), HtmlInputType.TIME.toString(), HtmlInputType.URL.toString(), HtmlInputType.WEEK.toString() };

	/** The Constant RIGHT_LABELED_INPUT_TYPES. */
	final static public String[] RIGHT_LABELED_INPUT_TYPES = { HtmlInputType.RADIO.toString(),
		HtmlInputType.CHECKBOX.toString() };

	/** The Constant USER_CHANGABLE_INPUT_TAGS. */
	final static public String[] USER_CHANGABLE_INPUT_TAGS = { UserCollectableHtmlTag.SELECT.toString(),
		UserCollectableHtmlTag.INPUT.toString(), UserCollectableHtmlTag.TEXTAREA.toString(), UserCollectableHtmlTag.SUBMIT.toString() };

//	/** The Constant USER_CLICKABLE_TAGS. */
//	final static public String[] USER_CLICKABLE_TAGS = { "button", "a"};
//	
	/** The Constant USER_CLICKABLE_TAGS. */
	final static public String[] USER_CLICKABLE_TAGS_EXCEPT_ALINK = { UserCollectableHtmlTag.BUTTON.toString()}; //only apply to input types. <a> will be treated separately, Ctrl+Alt+H to see the reference of this field usage.
	
	
	/** The Constant USER_COLLECTABLE_TAGS. */
	final static public String[] USER_COLLECTABLE_TAGS_EXCEPT_ALINK = ArrayUtils.addAll(USER_CLICKABLE_TAGS_EXCEPT_ALINK, USER_CHANGABLE_INPUT_TAGS);


	/**
	 * 
	 */
	private WebFormUserInputsCollectorHtmlTerms() {
		// TODO Auto-generated constructor stub
	}

}
