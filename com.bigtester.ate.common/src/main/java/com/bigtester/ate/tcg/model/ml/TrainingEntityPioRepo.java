/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model.ml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bigtester.ate.tcg.controller.WebFormUserInputsCollectorHtmlTerms;
import com.bigtester.ate.tcg.model.ScreenNamePredictStrategy;
import com.bigtester.ate.tcg.model.domain.ComputedCssSize;
import com.bigtester.ate.tcg.model.domain.HTMLSource;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord;
import com.bigtester.ate.tcg.utils.GlobalUtils;
import com.bigtester.ate.tcg.utils.exception.Html2DomException;
import com.bigtester.tebloud.machine.fasttext.config.TrainingParams;
import com.bigtester.tebloud.tcg.config.RunningModeProperties.RunningMode;
import com.google.common.collect.ImmutableMap;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import io.prediction.ATEEventClient;
import io.prediction.AteEvent;
import io.prediction.Event;
import io.prediction.EventClient;

import org.w3c.dom.Attr;

// TODO: Auto-generated Javadoc
/**
 * This class PredictionIOTrainer defines ....
 *
 * @author Peidong Hu
 *
 */
//@Component
@PropertySource("classpath:application.properties")

public class TrainingEntityPioRepo extends TebloudMachine implements ITrainingEntityPioRepo{
	protected static Log _log = LogFactoryUtil.getLog(TrainingEntityPioRepo.class);

	private ATEEventClient pioDecisionTreeEventClient;
	private EventClient pioScreenNameEventClient;
	private ATEEventClient pioUitrStoreEventClient;


	private static String PIODECISIONTREEAPPCHANNEL;
	//for home desktop development

	public static String EVENT_SERVER_DOMAIN;// = "pio-ate-v-ate-tcg-prediction-data";
	//for home desktop development
	public static final String EVENT_SERVER_PORT = "7070";
	//for home desktop development
	//public static final String LABEL_ENGINE_SERVER_PORT = "8000";
	//for home desktop dev
	//public static final String DECISION_ENGINE_SERVER_PORT = "8010";

	//public static final String SCREENNAME_ENGINE_SERVER_PORT = "8110";


	/** The Constant SAMPLETEXTCLASSIFIERACCESSKEY. */
	// public static final String SAMPLETEXTCLASSIFIERACCESSKEY =
	// "bPkjUBBUYM7yrGHDzuBLvw6F7go2vPqbqferF6MzGx7PHygsQn1elM8hRIhF3DUY";
	public static final String SAMPLETEXTCLASSIFIERACCESSKEY = "JPEaFI2xyLqpz0TGmMc156wqDl2tJ21PK6yHtMvCyev4aI5ATVnUoKTqmg3kCUBs";

	/** The Constant DECISION_TREE_CLASSIFIER_ACCESSKEY. */
	//public static final String DECISION_TREE_CLASSIFIER_ACCESSKEY = "o82oGn6EqKSfo6K5gjzIf6RTq54uLwwwGdsDRsUrOseeeJJ27HZoCzCHXWV2m7Yb";
	//for home desktop development, TestApp
	public static final String DECISION_TREE_CLASSIFIER_ACCESSKEY = "KfF59ZIqk8g32U2hxAPWqvuyvxlfn9d2puBaScKvY9KcNXsFWwuoHBfQiCzlpKrW";

	public static final String SCREENNAME_CLASSIFIER_ACCESSKEY = "P7qijFYit5SXg5yWRNTQzN3U1ACYTghuqExYZNsK8B7P0ha7S1KigDDP9PoYHKxu";

	/** The Constant EVENTSERVERURL, use the docker0 virtual ip */
	// public static final String EVENTSERVERURL = "http://172.16.173.50:7070";
	public static  String EVENTSERVERURL;// = "http://"+EVENT_SERVER_DOMAIN+":" + EVENT_SERVER_PORT;

	/** The Constant ENGINESERVERURL. */
	// public static final String ENGINESERVERURL = "http://172.16.173.50:8000";
//	public static  String LABELENGINESERVERURL;// = "http://"+EVENT_SERVER_DOMAIN+":" + LABEL_ENGINE_SERVER_PORT;
//
//	public static  String DECISION_ENGINE_SERVERURL;// = "http://"+EVENT_SERVER_DOMAIN+":" + DECISION_ENGINE_SERVER_PORT;
//
//	public static  String SCREENNAME_ENGINE_SERVERURL;// = "http://"+EVENT_SERVER_DOMAIN+":" + DECISION_ENGINE_SERVER_PORT;

	public static boolean FAST_TEXT_TRAINER_ENABLED;


	/** The categories. */
	public static List<String> categories = new ArrayList<String>();

	/**
	 * Instantiates a new prediction io trainer.
	 * https://stackoverflow.com/questions/35108778/spring-bean-with-runtime-constructor-arguments
	 */
	public  TrainingEntityPioRepo(String autName, IAteFastText fastText) {
		super(autName, fastText);
	};


	@PostConstruct
    public void init(){
		EVENTSERVERURL = "http://"+EVENT_SERVER_DOMAIN+":" + EVENT_SERVER_PORT;
		pioDecisionTreeEventClient = new ATEEventClient(DECISION_TREE_CLASSIFIER_ACCESSKEY,
					EVENTSERVERURL,PIODECISIONTREEAPPCHANNEL, 50);
		pioScreenNameEventClient = new EventClient(TrainingEntityPioRepo.SCREENNAME_CLASSIFIER_ACCESSKEY,null,
				EVENTSERVERURL, 2);
		pioUitrStoreEventClient = new ATEEventClient(TrainingEntityPioRepo.SAMPLETEXTCLASSIFIERACCESSKEY,
				EVENTSERVERURL, 50);
		/** The Constant ENGINESERVERURL. */
		// public static final String ENGINESERVERURL = "http://172.16.173.50:8000";
		//LABELENGINESERVERURL = "http://"+EVENT_SERVER_DOMAIN+":" + LABEL_ENGINE_SERVER_PORT;

		//DECISION_ENGINE_SERVERURL = "http://"+EVENT_SERVER_DOMAIN+":" + DECISION_ENGINE_SERVER_PORT;

		//SCREENNAME_ENGINE_SERVERURL = "http://"+EVENT_SERVER_DOMAIN+":" + SCREENNAME_ENGINE_SERVER_PORT;

    }
	@PreDestroy
	public void tearDown() throws IOException {
		//_log.info("pio client connections closed");
		pioScreenNameEventClient.close();
		pioDecisionTreeEventClient.close();
		this.pioUitrStoreEventClient.close();
	}
	@Value("${event_server_domain}")
	public void setEvent_server_domain(String event_server_domain) {
		EVENT_SERVER_DOMAIN = event_server_domain;
	}

	@Value("${fast_text_trainer_enabled}")
	public void setFast_text_trainer_enabled(boolean fast_text_trainer_enabled) {
		FAST_TEXT_TRAINER_ENABLED = fast_text_trainer_enabled;
	}
	/**
	 * @param pioDecisionTreeAppChannel the pioDecisionTreeAppChannel to set
	 */
	@Value("${tebloud.pio.decisionTreeChannelName}")
	public void setPioDecisionTreeAppChannel(String pioDecisionTreeAppChannel) {
		PIODECISIONTREEAPPCHANNEL = pioDecisionTreeAppChannel;
	}
	/**
	 * Sent training entity.
	 *
	 * @param record
	 *            the record
	 * @return the string
	 * @throws ExecutionException
	 *             the execution exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Deprecated
	public String sentTrainingEntity(WebElementTrainingRecord record)
			throws ExecutionException, InterruptedException, IOException {

		String retVal = "";
		String label = !StringUtils.isEmpty(record.getInputLabelName()) ? record.getInputLabelName() : record.getPioPredictLabelResult().getValue();
		//train three times with different guid will increase the prediction rate
//		for (int index=0; index<3; index++) {
//			if (index == 0) {
//				String inputMLHtmlCode = record.getInputMLHtmlWords();
//				retVal = sentTrainingEntity(inputMLHtmlCode, label);
//			} else {
				String inputMLHtmlCode = GlobalUtils.removeTagAndAttributeNamesAndDigits(GlobalUtils.replaceGuidId(record.getInputMLHtmlCode()));
				retVal = sentTrainingEntity(inputMLHtmlCode, label, "$set");
//			}
//		}
		return retVal;
	}

	public String sentUitrTrainingEntity(WebElementTrainingRecord record, String runningMode)
			throws ExecutionException, InterruptedException, IOException {

		String retVal = "";
		String label = !StringUtils.isEmpty(record.getInputLabelName()) ? record.getInputLabelName() : record.getPioPredictLabelResult().getValue();
		String inputMLHtmlCode = this.parseMLText(record, runningMode);
//		if (runningMode.equals(RunningMode.USECASE_APP.toString())) {
//			inputMLHtmlCode = GlobalUtils.removeTagAndAttributeNamesAndDigits(GlobalUtils.replaceGuidId(record.getInputMLHtmlCode()));
//		} else {
//			inputMLHtmlCode = GlobalUtils.removeAteAttributesValues(GlobalUtils.replaceGuidId(record.getInputMLHtmlCode()));
//		}
		retVal = sentTrainingEntity(inputMLHtmlCode, label, getAutName());
		return retVal;
	}

	private String sentTrainingEntity(String text, String label, String eventName)
			throws ExecutionException, InterruptedException, IOException {
//		EventClient client = new EventClient(SAMPLETEXTCLASSIFIERACCESSKEY,
//				EVENTSERVERURL);
		String iId = UUID.randomUUID().toString();
		Event event = new Event()
				.event(eventName)
				.entityType("phrase")
				// NOPMD
				.entityId(iId)
				.properties(
						ImmutableMap.<String, Object> of("phrase",
								text, "Interest",
								//record.getInputLabelName()));
								label));

		String retVal = pioUitrStoreEventClient.createEvent(event);
		//client.close();
		if (null == retVal) {
			iId = "";
		}
		//train three times with different guid will increase the prediction rate

		return iId;
	}
//	public static class MyPair
//	{
//	    private final String key;
//	    private final String value;
//
//	    public MyPair(String aKey, String aValue)
//	    {
//	        key   = aKey;
//	        value = aValue;
//	    }
//
//	    public String key()   { return key; }
//	    public String value() { return value; }
//	}

	@Deprecated
	public List<MyPair> queryAllTrainingEntity(String eventServerurl)
			throws ExecutionException, InterruptedException, IOException {
//		ATEEventClient client = new ATEEventClient(SAMPLETEXTCLASSIFIERACCESSKEY,
//				eventServerurl);
		List<Event> allEvents = this.pioUitrStoreEventClient.getEvents();
//		client.close();
		List<MyPair> retVal = new ArrayList<MyPair>();
		allEvents.forEach(event->{
			Map<String, Object> properties = event.getProperties();
			String label = (String) properties.get("Interest");
			String text = (String) properties.get("phrase");
			retVal.add(new MyPair(label, text));
		});
		return retVal;
	}

	public List<MyPair> queryAllUitrTrainingEntitiesForAut()
			throws ExecutionException, InterruptedException, IOException {
//		ATEEventClient client = new ATEEventClient(SAMPLETEXTCLASSIFIERACCESSKEY,
//				EVENTSERVERURL);
		List<AteEvent> allEvents = pioUitrStoreEventClient.getAteEventsForAut(this.getAutName());
		//client.close();
		List<MyPair> retVal = new ArrayList<MyPair>();
		allEvents.forEach(event->{
			Map<String, Object> properties = event.getProperties();
			String label = (String) properties.get("Interest");
			String text = (String) properties.get("phrase");
			retVal.add(new MyPair(label, text));
		});
		return retVal;
	}

	public List<AteEvent> queryAllEventsForAut()
			throws ExecutionException, InterruptedException, IOException {
//		ATEEventClient client = new ATEEventClient(SAMPLETEXTCLASSIFIERACCESSKEY,
//				EVENTSERVERURL);
		return pioUitrStoreEventClient.getAteEventsForAut(this.getAutName());
	}

	public List<AteEvent> queryAllDecisionTreeEventsForAut()
			throws ExecutionException, InterruptedException, IOException {
//		ATEEventClient client = new ATEEventClient(SAMPLETEXTCLASSIFIERACCESSKEY,
//				EVENTSERVERURL);
		return pioDecisionTreeEventClient.getAteEventsForAut("");
	}


	public String deleteUitrDecisionTrainingEntity(String entityId)
			throws ExecutionException, InterruptedException, IOException {
//		EventClient client = new EventClient(DECISION_TREE_CLASSIFIER_ACCESSKEY,
//				EVENTSERVERURL);
		String eId = pioDecisionTreeEventClient.deleteItem(entityId);
//		client.close();
		return eId;

	}

	public boolean deleteUitrDecisionTrainingEvent(String eId)
			throws ExecutionException, InterruptedException, IOException {
//		EventClient client = new EventClient(SAMPLETEXTCLASSIFIERACCESSKEY,
//				EVENTSERVERURL);
		boolean rId = this.pioDecisionTreeEventClient.deleteEvent(eId);
//		client.close();
		return rId;

	}


	public String deleteUitrTrainingEntity(String entityId)
			throws ExecutionException, InterruptedException, IOException {
//		EventClient client = new EventClient(SAMPLETEXTCLASSIFIERACCESSKEY,
//				EVENTSERVERURL);
		String eId = this.pioUitrStoreEventClient.deleteItem(entityId);
//		client.close();
		return eId;

	}

	public boolean deleteUitrTrainingEvent(String eId)
			throws ExecutionException, InterruptedException, IOException {
//		EventClient client = new EventClient(SAMPLETEXTCLASSIFIERACCESSKEY,
//				EVENTSERVERURL);
		boolean rId = this.pioUitrStoreEventClient.deleteEvent(eId);
//		client.close();
		return rId;

	}

	public String deleteScreenNameTrainingEntity(String entityId)
			throws ExecutionException, InterruptedException, IOException {
//		EventClient client = new EventClient(TrainingEntityPioRepo.SCREENNAME_CLASSIFIER_ACCESSKEY,
//				EVENTSERVERURL);
		String eId = pioScreenNameEventClient.deleteItem(entityId);
		//client.close();
		return eId;

	}

	@Deprecated
	public String sentUitrDecisionTrainingEntity(WebElementTrainingRecord uitr, String screenName, boolean userTrainedRecord)
			throws ExecutionException, InterruptedException, IOException {
//		EventClient client = new EventClient(DECISION_TREE_CLASSIFIER_ACCESSKEY,
//				EVENTSERVERURL);


		Map<String, Object> properties = new ConcurrentHashMap<String, Object>();
		Double runDecision = uitr.isBelongToCurrentTestCase() ? 1d : 0d;
		//runDecision ==0 means this is not a valid user inputable web element. Only for diaplying purpose. This applies to 99% ui design.
		//only possible failed case is programmer is not using any inputable html tag, instead of using div or span to make clickables
		//we need to encourage user to manually add those elements to increase the prediction rate.
		if (userTrainedRecord && !uitr.isBelongToCurrentTestCase()) {
			runDecision = 0.5d;//for other test cases.
		}

		properties.put(
						"runDecision", runDecision
						);
//		properties.put(
//						"uitrSize",(double)
//						uitr.getComputedCssSizes().iterator().next()
//								.getUitrSize());
		ComputedCssSize uitrDimension = uitr.getComputedCssSizes().iterator().next();
		properties.put(
				"uitrWidth",(double)
				uitrDimension
						.getUitrWidth());
		properties.put(
				"uitrHeight",(double)
				uitrDimension
						.getUitrHeight());
		double xRatio = (double) (Math.round(1000*
				((uitrDimension
				.getLeftTopCornerX()-uitrDimension
				.getTopDocumentWidth()/2)
				/ uitrDimension
						.getTopDocumentWidth())));
		if(xRatio<-1000) xRatio = -1000;
		if(xRatio>1000) xRatio = 1000;
		properties.put(
						"locationXRatio", xRatio);

		double yRatio = (double) (Math.round (1000 *
				(uitrDimension
				.getLeftTopCornerY()-uitrDimension
				.getTopDocumentHeight()/2)
				/ uitrDimension
						.getTopDocumentHeight()));
		if (yRatio<-1000) xRatio = -1000;
		if (yRatio>1000) xRatio = 1000;
		properties.put(
						"locationYRatio", yRatio);
		properties.put(

						"numberOfAteCollectableTags", (double)
						uitr.calculateAteCollectableTagsCount(Arrays
								.asList(WebFormUserInputsCollectorHtmlTerms.USER_COLLECTABLE_TAGS_EXCEPT_ALINK)));


		properties.put("screenWidth", Integer.valueOf(uitr.getComputedCssSizes().iterator().next().getScreenWidth()).toString());
//		properties.put("testCaseName", uitr
//				.getTestcases().iterator().next().getEndNode()
//				.getName());
		properties.put("screenName",screenName);
		properties.put("uitrLabelName", uitr.getInputLabelName());
		String entityId = StringUtils.isEmpty(uitr.getTrainedResult())?UUID.randomUUID().toString():uitr.getTrainedResult();
		Event event = new Event()
				.event("$set")
				.entityType("uitr")
				// NOPMD
				.entityId(entityId)
				.properties(properties);

		String retVal = pioDecisionTreeEventClient.createEvent(event);
		//client.close();
		if (null == retVal) {
			entityId = "";
		}
		//train three times with different guid will increase the prediction rate

		return entityId;
	}

	public String sentUitrDecisionTrainingEntityToAppEngine(WebElementTrainingRecord uitr, String screenName,  boolean userTrainedRecord, String testSuiteName, String subSuiteName, String testCaseName)
			throws ExecutionException, InterruptedException, IOException {
//		EventClient client = new EventClient(DECISION_TREE_CLASSIFIER_ACCESSKEY,
//				EVENTSERVERURL);


		Map<String, Object> properties = new ConcurrentHashMap<String, Object>();
		Double runDecision = uitr.isBelongToCurrentTestCase() ? 1d : 0d;
		//runDecision ==0 means this is not a valid user inputable web element. Only for diaplying purpose. This applies to 99% ui design.
		//only possible failed case is programmer is not using any inputable html tag, instead of using div or span to make clickables
		//we need to encourage user to manually add those elements to increase the prediction rate.
		if (userTrainedRecord && !uitr.isBelongToCurrentTestCase()) {
			runDecision = 0.5d;//for other test cases.
		}

		properties.put(
						"runDecision", runDecision
						);
//		properties.put(
//						"uitrSize",(double)
//						uitr.getComputedCssSizes().iterator().next()
//								.getUitrSize());
		ComputedCssSize uitrDimension = uitr.getComputedCssSizes().iterator().next();
		properties.put(
				"uitrWidth",(double)
				uitrDimension
						.getUitrWidth());
		properties.put(
				"uitrHeight",(double)
				uitrDimension
						.getUitrHeight());
		double xRatio = (double) (Math.round(1000*
				((uitrDimension
				.getLeftTopCornerX()-uitrDimension
				.getTopDocumentWidth()/2)
				/ uitrDimension
						.getTopDocumentWidth())));
		if(xRatio<-1000) xRatio = -1000;
		if(xRatio>1000) xRatio = 1000;
		properties.put(
						"locationXRatio", xRatio);

		double yRatio = (double) (Math.round (1000 *
				(uitrDimension
				.getLeftTopCornerY()-uitrDimension
				.getTopDocumentHeight()/2)
				/ uitrDimension
						.getTopDocumentHeight()));
		if (yRatio<-1000) xRatio = -1000;
		if (yRatio>1000) xRatio = 1000;
		properties.put(
						"locationYRatio", yRatio);
		properties.put(

						"numberOfAteCollectableTags", (double)
						uitr.calculateAteCollectableTagsCount(Arrays
								.asList(WebFormUserInputsCollectorHtmlTerms.USER_COLLECTABLE_TAGS_EXCEPT_ALINK)));


		properties.put("screenWidth", Integer.valueOf(uitr.getComputedCssSizes().iterator().next().getScreenWidth()).toString());
//		properties.put("testCaseName", uitr
//				.getTestcases().iterator().next().getEndNode()
//				.getName());
		properties.put("screenName",screenName);
		properties.put("uitrLabelName", uitr.getInputLabelName());
		properties.put("testSuiteName", testSuiteName);
		properties.put("subSuiteName", subSuiteName);
		properties.put("testCaseName", testCaseName);
		String entityId = StringUtils.isEmpty(uitr.getTrainedResult())?UUID.randomUUID().toString():uitr.getTrainedResult();
		Event event = new Event()
				.event("$set")
				.entityType("uitr")
				// NOPMD
				.entityId(entityId)
				.properties(properties);

		try {
			String retVal = pioDecisionTreeEventClient.createEvent(event);
			//client.close();
			if (null == retVal) {
				retVal = "";
			}
			//train three times with different guid will increase the prediction rate

			return retVal;
		} catch (ExecutionException |
				 InterruptedException | IOException e) {
			_log.info("failed get event id" + e.getLocalizedMessage());
			return "";
		}

	}

	private static boolean getNodeProcessedOrNot(Node node) {
		NamedNodeMap attrs = node.getAttributes();
		boolean processed = false;
		for (int index2 = 0; index2<attrs.getLength(); index2++) {
			Attr attr = (Attr) attrs.item(index2);

			if (attr.getName().equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.PREVIOUS_PROCESSED_ATTR_NAME)
					&& attr.getValue().equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE)) {
				processed = true;
				break;
			}
		}
		return processed;
	}
	private static String getUnprocessedTextContent(Node element) {

		if (!getNodeProcessedOrNot(element)){
			return element.getTextContent();
		} else {
			NodeList childrenNodes = element.getChildNodes();
			String childText = " ";
			for (int index=0; index<childrenNodes.getLength(); index++) {
				NamedNodeMap attrs;
				if (childrenNodes.item(index) instanceof Element) {
					attrs = childrenNodes.item(index).getAttributes();
				} else {
					attrs = element.getAttributes();
				}
				boolean processed = false;
				for (int index2 = 0; index2<attrs.getLength(); index2++) {
					Attr attr = (Attr) attrs.item(index2);

					if (attr.getName().equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.PREVIOUS_PROCESSED_ATTR_NAME)
							&& attr.getValue().equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE)) {
						processed = true;
						break;
					}
				}
				if (!processed)
					childText = childText + " " + childrenNodes.item(index).getTextContent();
				else if (childrenNodes.item(index) instanceof Element){
					childText = childText + " " + getUnprocessedTextContent(childrenNodes.item(index));
				}

			}
			return childText;
		}

	}

private static String getTextContent(Node element, boolean removeProcessedElementText) {

		if ((!removeProcessedElementText || !getNodeProcessedOrNot(element))
//				&& !(element.getNodeName().equalsIgnoreCase("script")
//				|| element.getNodeName().equalsIgnoreCase("style"))
				){
			return element.getTextContent();
		} else {
			NodeList childrenNodes = element.getChildNodes();
			String childText = " ";
			for (int index=0; index<childrenNodes.getLength(); index++) {
				NamedNodeMap attrs;
				if (childrenNodes.item(index) instanceof Element) {
					attrs = childrenNodes.item(index).getAttributes();
				} else {
					attrs = element.getAttributes();
				}
				boolean processed = false;
				for (int index2 = 0; index2<attrs.getLength(); index2++) {
					Attr attr = (Attr) attrs.item(index2);

					if (attr.getName().equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.PREVIOUS_PROCESSED_ATTR_NAME)
							&& attr.getValue().equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE)) {
						processed = true;
						break;
					}
				}
				if (!processed)
					childText = childText + " " + childrenNodes.item(index).getTextContent();
				else if (childrenNodes.item(index) instanceof Element){
					childText = childText + " " + getUnprocessedTextContent(childrenNodes.item(index));
				}

			}
			return childText;
		}

	}

	private static String removeInvisibleElementsAndGetText(String html, ScreenNamePredictStrategy predictingStrategy) {
		Document doc;
		try {
//			System.out.println("before removal: " + html);
			doc = GlobalUtils.html2Dom(html);

//			List<Element> nodes = $(doc).find("script").get();
//			List<Element> styleNodes = $(doc).find("style").get();
//			nodes.addAll(styleNodes);
//			for (int index = 0; index < nodes.size(); index++) {
//				nodes.get(index).getParentNode().removeChild(nodes.get(index));
//
//			}
//			List<Element> nodes2 = $(doc).find(
//					attr(WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME,
//							WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE))
//					.get();
//			for (int index = 0; index < nodes2.size(); index++) {
//				nodes2.get(index).getParentNode()
//						.removeChild(nodes2.get(index));
//			}
//			doc.normalize();
			doc = GlobalUtils.removeScriptStylesAndInvisiables(doc);
			doc = GlobalUtils.addSpacesIntoTextNodes(doc);
//			ByteArrayOutputStream out = new ByteArrayOutputStream();
//			GlobalUtils.printDocument(doc, out);
//			System.out.println("after removal: " + out.toString());
			String ret="";
			if (predictingStrategy==ScreenNamePredictStrategy.PREDICTED_BY_VISIBLE_AND_UNPROCESSED_ELEMENT_TEXT)
				ret = getTextContent(GlobalUtils.getBodyEquevelantElement(doc), true);//.getTextContent();
			else
				ret = getTextContent(GlobalUtils.getBodyEquevelantElement(doc), false);//.getTextContent();
			ret = ret.trim().replaceAll("[\\t\\n\\r]", " ")
					.replaceAll(" +", " ");
			// DOMSource domSource = new DOMSource(doc);
			// StringWriter writer = new StringWriter();
			// StreamResult result = new StreamResult(writer);
			// TransformerFactory tf = TransformerFactory.newInstance();
			// Transformer transformer;
			// transformer = tf.newTransformer();
			// transformer.transform(domSource, result);
			// String ret = writer.toString();
			if (ret == null)
				ret = "";
			return ret;
		} catch (IOException e1) {
			throw new IllegalStateException(
					"failed  to convert to html source from dom.");
		} catch (ParserConfigurationException e1) {
			throw new IllegalStateException(
					"failed to convert to html source from dom.");
		} catch (Html2DomException e1) {
			throw new IllegalStateException(
					"failed to convert to html source from dom.");
		}
		// } catch (TransformerConfigurationException e) {
		// throw new IllegalStateException("failed to create transformer.");
		// }
		// catch (TransformerException e) {
		// throw new
		// IllegalStateException("failed to convert to html source from dom.");
		// } finally {
		// return "";
		// }
// catch (TransformerException e) {
//	 throw new IllegalStateException(
//				"failed to convert to html source from dom.");
//		}

	}


	/**
	 * Train screen name.
	 *
	 * @param pageFrames the page frames
	 * @param screenName the screen name
	 * @param predictingStrategy the predicting strategy
	 * @return the string
	 * @throws ExecutionException the execution exception
	 * @throws InterruptedException the interrupted exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Deprecated
	public String trainScreenName(Set<HTMLSource> pageFrames,
			String screenName, ScreenNamePredictStrategy predictingStrategy) throws ExecutionException, InterruptedException,
			IOException {
//		EventClient client = new EventClient(SAMPLETEXTCLASSIFIERACCESSKEY,
//				EVENTSERVERURL);

		StringBuilder tmp = new StringBuilder("");
		boolean noTextInHtmlSource = true;
		for (java.util.Iterator<HTMLSource> itr = pageFrames.iterator(); itr
				.hasNext();) {
			HTMLSource source = itr.next();
			if (source.isVisible()) {
				tmp.append(" ");// NOPMD
				// remove previous screen elements
				String visibleDocText = removeInvisibleElementsAndGetText(source
						.getDomDoc(), predictingStrategy);
				if (visibleDocText.length() > 0) {
					tmp.append(visibleDocText);
					noTextInHtmlSource = false;
				}
			}
		}
		if (noTextInHtmlSource)
			return "";

		String iId = UUID.randomUUID().toString();
		Event event = new Event()
				.event("$set")
				.entityType("phrase")
				.entityId(iId)
				.properties(
						ImmutableMap.<String, Object> of("phrase",
								tmp.toString(), "Interest", screenName));

		// Event event = new Event()
		// .event("userfield")
		// .entityType("userfieldentity")
		// .entityId(UUID.randomUUID().toString())
		// .properties(
		// ImmutableMap.<String, Object> of("label",
		// toDouble(screenName), "text",
		// tmp.toString(), "category",
		// screenName));
		String retVal = this.pioUitrStoreEventClient.createEvent(event);
//		client.close();
		if (null == retVal) {
			iId = "";
		}
		return iId;
	}

	@Deprecated
	public String trainScreenName(String screenNameTrainingWords,
			String screenName) throws ExecutionException, InterruptedException,
			IOException {

		if (StringUtils.isEmpty(screenNameTrainingWords))
			return "";
//		EventClient client = new EventClient(TrainingEntityPioRepo.SCREENNAME_CLASSIFIER_ACCESSKEY,
//				EVENTSERVERURL);
		String iId = UUID.randomUUID().toString();
		Event event = new Event()
				.event("$set")
				.entityType("screenname")
				.entityId(iId)
				.properties(
						ImmutableMap.<String, Object> of("phrase",
								screenNameTrainingWords, "Interest", screenName));

		String retVal = pioScreenNameEventClient.createEvent(event);
		//client.close();
		if (null == retVal) {
			iId = "";
		}
		return iId;
	}

	public String trainScreenNameForAppEngine(String screenNameTrainingWords,
			String screenName) throws ExecutionException, InterruptedException,
			IOException {

		if (StringUtils.isEmpty(screenNameTrainingWords))
			return "";
//		EventClient client = new EventClient(TrainingEntityPioRepo.SCREENNAME_CLASSIFIER_ACCESSKEY,
//				EVENTSERVERURL);
		String iId = UUID.randomUUID().toString();
		Event event = new Event()
				.event(getAutName())
				.entityType("screenname")
				.entityId(iId)
				.properties(
						ImmutableMap.<String, Object> of("phrase",
								screenNameTrainingWords, "Interest", screenName));

		String retVal = pioScreenNameEventClient.createEvent(event);
		//client.close();
		if (null == retVal) {
			iId = "";
		}
		return iId;
	}

	@Deprecated
	public String trainLabelWords(String trainingWords,
			String label) throws ExecutionException, InterruptedException,
			IOException {

		if (StringUtils.isEmpty(trainingWords))
			return "";
//		EventClient client = new EventClient(SAMPLETEXTCLASSIFIERACCESSKEY,
//				EVENTSERVERURL);
		String iId = UUID.randomUUID().toString();
		Event event = new Event()
				.event("$set")
				.entityType("phrase")
				.entityId(iId)
				.properties(
						ImmutableMap.<String, Object> of("phrase",
								trainingWords, "Interest", label));

		String retVal = this.pioUitrStoreEventClient.createEvent(event);
//		client.close();
		if (null == retVal) {
			iId = "";
		}
		return iId;
	}

	public String trainLabelWordsForAppEngine(String trainingWords,
			String label) throws ExecutionException, InterruptedException,
			IOException {

		if (StringUtils.isEmpty(trainingWords))
			return "";
//		EventClient client = new EventClient(SAMPLETEXTCLASSIFIERACCESSKEY,
//				EVENTSERVERURL);
		String iId = UUID.randomUUID().toString();
		Event event = new Event()
				.event(getAutName())
				.entityType("phrase")
				.entityId(iId)
				.properties(
						ImmutableMap.<String, Object> of("phrase",
								trainingWords, "Interest", label));

		String retVal = this.pioUitrStoreEventClient.createEvent(event);
//		client.close();
		if (null == retVal) {
			iId = "";
		}
		return iId;
	}


	/**
	 * @return the pioDecisionTreeAppChannel
	 */
	public String getPioDecisionTreeAppChannel() {
		return PIODECISIONTREEAPPCHANNEL;
	}





	/**
	 * @return the pioDecisionTreeEventClient
	 */
	public EventClient getPioDecisionTreeEventClient() {
		return pioDecisionTreeEventClient;
	}


	/**
	 * @param pioDecisionTreeEventClient the pioDecisionTreeEventClient to set
	 */
	public void setPioDecisionTreeEventClient(ATEEventClient pioDecisionTreeEventClient) {
		this.pioDecisionTreeEventClient = pioDecisionTreeEventClient;
	}


	/**
	 * @return the pioScreenNameEventClient
	 */
	public EventClient getPioScreenNameEventClient() {
		return pioScreenNameEventClient;
	}


	/**
	 * @param pioScreenNameEventClient the pioScreenNameEventClient to set
	 */
	public void setPioScreenNameEventClient(EventClient pioScreenNameEventClient) {
		this.pioScreenNameEventClient = pioScreenNameEventClient;
	}


	/**
	 * @return the pioUitrStoreEventClient
	 */
	public ATEEventClient getPioUitrStoreEventClient() {
		return pioUitrStoreEventClient;
	}


	/**
	 * @param pioUitrStoreEventClient the pioUitrStoreEventClient to set
	 */
	public void setPioUitrStoreEventClient(ATEEventClient pioUitrStoreEventClient) {
		this.pioUitrStoreEventClient = pioUitrStoreEventClient;
	}


	/* (non-Javadoc)
	 * @see com.bigtester.ate.tcg.model.ml.ITrainingEntityPioRepo#trainRemoteFastTextModel(java.lang.String)
	 */
	@Override
	public String trainRemoteFastTextModel(String autName, TrainingParams trainingParams) {
		if (this.getAteFastText() instanceof IRemoteTebloudFastText) {
			try {
				return ((IRemoteTebloudFastText) this.getAteFastText()).remoteTraining(autName, trainingParams);
			} catch (ExecutionException | InterruptedException | IOException e) {
				return "Failed";
			}
		}
		return "Failed";
	}




}
