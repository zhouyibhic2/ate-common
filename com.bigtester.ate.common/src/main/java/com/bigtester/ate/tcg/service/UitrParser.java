/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2017, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.service;

import static org.hamcrest.Matchers.contains;
import static org.joox.JOOX.$;
import static org.joox.JOOX.attr;
import static org.w3c.dom.Node.TEXT_NODE;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.eclipse.jdt.annotation.Nullable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bigtester.ate.tcg.controller.WebFormUserInputsCollectorHtmlTerms;
import com.bigtester.ate.tcg.model.domain.HTMLSource;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord.UserInputType;
import com.bigtester.ate.tcg.model.ml.TebloudMachine;
import com.bigtester.ate.tcg.utils.GlobalUtils;
import com.bigtester.ate.tcg.utils.exception.Html2DomException;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

// TODO: Auto-generated Javadoc
/**
 * This class UitrParser defines ....
 * @author Peidong Hu
 *
 */
public class UitrParser {
	protected static Log _log = LogFactoryUtil.getLog(UitrParser.class);
//	public static String MONITORED_NODE_HTML_CODE = "permalink";
//	public static String MONITORED_LABEL = "";
//	public static int MONITORED_NODE_LENGTH = 1600;


	public static double minimumHeightOfUitr = 10;
	public static double minimumWidthOfUitr = 10;
	public static String NEW_HTML_SOURCE_MAP_KEY = "newDoms";
	public static String PREVIOUS_HTML_SOURCE_MAP_KEY = "previousDoms";
	private Optional<List<HTMLSource>> previousHtmlSource = Optional.empty();
	final private List<HTMLSource> newHtmlSource;
	final private ITebloudPredictor predictor;

	final private String testCaseName;
	final private String domainName;
	final private String screenName;

	/**
	 *
	 */
	public UitrParser(String testCaseName, String domainName, String screenName, Map<String, List<HTMLSource>> doms, ITebloudPredictor predictor) {
		// TODO Auto-generated constructor stub
		this.newHtmlSource = doms.get(NEW_HTML_SOURCE_MAP_KEY);
		this.previousHtmlSource = Optional.ofNullable(doms.get(PREVIOUS_HTML_SOURCE_MAP_KEY));
		this.predictor = predictor;
		this.testCaseName = testCaseName;
		this.domainName = domainName;
		this.screenName = screenName;
		initUitrParser();
	}

	public UitrParser(String testCaseName, String domainName, String screenName, List<HTMLSource> previousDom, List<HTMLSource> newDom, ITebloudPredictor predictor) {
		// TODO Auto-generated constructor stub
		this.newHtmlSource = newDom;
		this.previousHtmlSource = Optional.ofNullable(previousDom);
		this.predictor = predictor;
		this.testCaseName = testCaseName;
		this.domainName = domainName;
		this.screenName = screenName;
		initUitrParser();
	}
	private void initUitrParser() {
		generateDocumentNode();
		if (this.previousHtmlSource.isPresent()) {
			//note: this operation will override the value of newHtmlSource domDoc field with the newly added ate_processed attribute in html source
			//but unfortunately, this new domDoc can be successfully converted to Document again. In some web page, all body html will be converted to a text node.
			//the example is on netapp job application web site, in work experience tab when apply job.
			this.markProcessedAttributes();
		}
	}
	public UitrParser(String testCaseName, String domainName, String screenName, Set<HTMLSource> previousDom, Set<HTMLSource> newDom, ITebloudPredictor predictor) {
		this.newHtmlSource = new ArrayList<HTMLSource>(newDom);
		this.previousHtmlSource = Optional.ofNullable(new ArrayList<HTMLSource>(previousDom));
		this.predictor = predictor;
		this.testCaseName = testCaseName;
		this.domainName = domainName;
		this.screenName = screenName;
		initUitrParser();
	}
	public UitrParser(String testCaseName, String domainName, String screenName, Set<HTMLSource> newDom, ITebloudPredictor predictor) {
		this.newHtmlSource = new ArrayList<HTMLSource>(newDom);
		//this.previousHtmlSource = Optional.ofNullable(new ArrayList<HTMLSource>(previousDom));
		this.predictor = predictor;
		this.testCaseName = testCaseName;
		this.domainName = domainName;
		this.screenName = screenName;
		initUitrParser();
	}
	public UitrParser(String testCaseName, String domainName,String screenName, List<HTMLSource> newDom, ITebloudPredictor predictor) {
		// TODO Auto-generated constructor stub
		this.newHtmlSource = newDom;

		this.predictor = predictor;
		this.testCaseName = testCaseName;
		this.domainName = domainName;
		this.screenName = screenName;
		initUitrParser();
	}
	@Nullable
	private HTMLSource getPreviousProcessingDom(List<HTMLSource> previousDoms,
			HTMLSource newDom) {
		for (HTMLSource previous : previousDoms) {
			if (previous.getDocGuid().equalsIgnoreCase(newDom.getDocGuid())) {
				return previous;
			}
		}
		return null;
	}

	private List<Node> getTextNodes(Node node, boolean includeWhitespaceNodes) {
	    List<Node> textNodes = new ArrayList<Node>();
	    //String nonWhitespaceMatcher = "/\\S/";

	        if (node.getNodeType() == TEXT_NODE && !node.getParentNode().getNodeName().equalsIgnoreCase("script") && !node.getParentNode().getNodeName().equalsIgnoreCase("style")) {
	            if (includeWhitespaceNodes || !node.getNodeValue().trim().isEmpty()) {
	                textNodes.add(node);
	            }
	        } else {
	            for (int i = 0; i< node.getChildNodes().getLength(); i++) {
	                textNodes.addAll(getTextNodes(node.getChildNodes().item(i), includeWhitespaceNodes));
	            }
	        }
	    return textNodes;
	}

	public Map<Node, Double[]> parseAllWidthSamplingNodes() {
		Map<Node, Double[]> retVal = new HashMap<Node, Double[]>();
		//System.out.println("");
		this.newHtmlSource
				.parallelStream()
				.forEach(
						htmlSource -> {
							$(GlobalUtils.getBodyEquevelantElement(htmlSource.getDomDocument()))
									.find("a")
									.get()
									.parallelStream()
									.forEach(
											element -> {
												if (!element
														.getAttribute(
																WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME)
														.equalsIgnoreCase(
																WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE) &&
																!element
																.getAttribute("ate-computed-lefttopcornerx").equals("") &&
																element
																.getAttribute("ate-computed-lefttopcornerx")!=null) {
													Double[] locationXOfLeftRightCorner = {Double.parseDouble(element
															.getAttribute("ate-computed-lefttopcornerx")),
															Double.parseDouble(element
																	.getAttribute("ate-computed-lefttopcornerx")) +
																	Double.parseDouble(element
																			.getAttribute("ate-computed-width"))};
													retVal.put(
															element,locationXOfLeftRightCorner);
												}
											});
							$(GlobalUtils.getBodyEquevelantElement(htmlSource.getDomDocument()))
									.find("img")
									.get()
									.parallelStream()
									.forEach(
											element -> {
												if (!element
														.getAttribute(
																WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME)
														.equalsIgnoreCase(
																WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE) &&
																!element
																.getAttribute("ate-computed-lefttopcornerx").equals("") &&
																element
																.getAttribute("ate-computed-lefttopcornerx")!=null) {
													Double[] locationXOfLeftRightCorner = {Double.parseDouble(element
															.getAttribute("ate-computed-lefttopcornerx")),
															Double.parseDouble(element
																	.getAttribute("ate-computed-lefttopcornerx")) +
																	Double.parseDouble(element
																			.getAttribute("ate-computed-width"))};
													retVal.put(
															element,
															locationXOfLeftRightCorner);
												}
											});
							getTextNodes(GlobalUtils.getBodyEquevelantElement(htmlSource.getDomDocument()), false)
									.parallelStream()
									.forEach(
											tNode -> {
												if (!((Element) tNode
														.getParentNode())
														.getAttribute(
																WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME)
														.equalsIgnoreCase(
																WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE) &&
																!((Element) tNode
																		.getParentNode())
																.getAttribute("ate-computed-lefttopcornerx").equals("") &&
																((Element) tNode
																		.getParentNode())
																.getAttribute("ate-computed-lefttopcornerx")!=null) {
													Double[] locationXOfLeftRightCorner = {Double.parseDouble(((Element) tNode
															.getParentNode())
															.getAttribute("ate-computed-lefttopcornerx")),
															Double.parseDouble(((Element) tNode
																	.getParentNode())
																	.getAttribute("ate-computed-lefttopcornerx")) +
																	Double.parseDouble(((Element) tNode
																			.getParentNode())
																			.getAttribute("ate-computed-width"))};

													retVal.put(
															tNode,
															locationXOfLeftRightCorner);
												}
											});
						});
		return retVal;
	}
	private void markProcessedElementsInNewNode(Node previousNode,
			String attributeName, String attributeValue, Node newNode) {

		if (previousNode.getNodeType() == Node.ELEMENT_NODE) {

			// TODO Add falseInvisible case
			if (!(((Element) previousNode)
					.getAttribute(
							WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME)
					.equalsIgnoreCase(
							WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE) && !((Element) newNode)
					.getAttribute(
							WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME)
					.equalsIgnoreCase(
							WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE))) {
				((Element) newNode).setAttribute(attributeName, attributeValue);
			}
		}

		NodeList previousChildrenNodes = previousNode.getChildNodes();
		for (int index = 0; index < previousChildrenNodes.getLength(); index++) {
			if (previousChildrenNodes.item(index).getNodeType() == Node.ELEMENT_NODE) {
				String guid = ((Element) previousChildrenNodes.item(index))
						.getAttribute("ate-guid");
				List<Element> sameGuidNodesInNewDoc = $(newNode).find(
						attr("ate-guid", guid)).get();
				if (sameGuidNodesInNewDoc.size() >= 1) {
					// TODO Add falseInvisible case
					if (!(((Element) previousChildrenNodes.item(index))
							.getAttribute(
									WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME)
							.equalsIgnoreCase(
									WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE) && !sameGuidNodesInNewDoc
							.get(0)
							.getAttribute(
									WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME)
							.equalsIgnoreCase(
									WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE))) {
						for (int i = 0; i < sameGuidNodesInNewDoc.size(); i++) {
							sameGuidNodesInNewDoc.get(i).setAttribute(
									attributeName, attributeValue);
						}
					}
				}
				// } else if (sameGuidNodesInNewDoc.size() > 1) {
				// throw new
				// IllegalStateException("guid is not unique caused by wrong html source transformation and cleanup.");
				// }

				markProcessedElementsInNewNode(
						previousChildrenNodes.item(index), attributeName,
						attributeValue, newNode);
			}
		}

	}

	private String markProcessedElementsFromPreviousProcessingSession(
			Document newDoc, Document previousDoc) {

		String ret = "";
		try {
//			newDoc = GlobalUtils.html2Dom(newHtml);
//			previousDoc = GlobalUtils.html2Dom(previousHtml);

			Node newBody = GlobalUtils.getBodyEquevelantElement(newDoc);
			Node previousBody = GlobalUtils.getBodyEquevelantElement(previousDoc);
			markProcessedElementsInNewNode(
					previousBody,
					WebFormUserInputsCollectorHtmlTerms.PREVIOUS_PROCESSED_ATTR_NAME,
					WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE,
					newBody);

			//newDoc.normalize();

			DOMSource domSource = new DOMSource(newDoc);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer;
			transformer = tf.newTransformer();
			transformer.setOutputProperty("omit-xml-declaration", "yes");
			transformer.transform(domSource, result);
			ret = writer.toString();
			if (ret == null)
				ret = "";
			// return ret;

		} catch (TransformerConfigurationException e) {
			throw new IllegalStateException("failed to create transformer.");
		} catch (TransformerException e) {
			throw new IllegalStateException(
					"failed to convert to html source from dom.");
		}
		return ret;

	}

	public void markPreviousScreenDomProcessedElements() {
		if (null == this.newHtmlSource)
			return;
		for (int i = 0; i < this.newHtmlSource.size(); i++) {
			HTMLSource tempNewDom = this.newHtmlSource.get(i);

			if (!tempNewDom.isVisible())
				continue;
			if (this.previousHtmlSource.isPresent()) {
				HTMLSource previous = getPreviousProcessingDom(this.previousHtmlSource.get(),
						tempNewDom);
				if (previous != null) {
					if (tempNewDom.getDomDocument()==null) {
						try {
							tempNewDom.setDomDocument(GlobalUtils.html2Dom(tempNewDom.getDomDoc()));
							previous.setDomDocument(GlobalUtils.html2Dom(previous.getDomDoc()));
						} catch (IOException | ParserConfigurationException
								| Html2DomException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							continue;
						}
					}
					tempNewDom
							.setDomDoc(markProcessedElementsFromPreviousProcessingSession(
									tempNewDom.getDomDocument(),
									previous.getDomDocument()));
					// Need to reset DomText in training processing.
				}
			}
		}
	}
	private void generateDocumentNode() {
		this.newHtmlSource.stream().parallel().forEach(htmlSource->{
			try {
				htmlSource.setDomDocument(GlobalUtils.cleanUpDoc(GlobalUtils.html2Dom(htmlSource
						.getDomDoc()), htmlSource.getXpathOfFrame()));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		this.previousHtmlSource.ifPresent(previousDoms->{
			previousDoms.forEach(htmlS->{
				try {
					htmlS.setDomDocument(GlobalUtils.cleanUpDoc(GlobalUtils.html2Dom(htmlS
							.getDomDoc()), htmlS.getXpathOfFrame()));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
		});
	}
	private void markProcessedAttributes() {
		//pre-process doms

		if (this.previousHtmlSource.isPresent())
			markPreviousScreenDomProcessedElements();

	}
	public List<WebElementTrainingRecord> parseAllNewCreatedUitrsRawPredicted(String runningMode) {
		List<WebElementTrainingRecord> allPredictedUitrs = new ArrayList<WebElementTrainingRecord>();

		allPredictedUitrs = this.parseAllUitrsRawPredicted(true, false, runningMode);


		return allPredictedUitrs;

	}
	public List<WebElementTrainingRecord> parseAllUitrsRawPredicted(boolean removeProcessedElement, boolean forRunner, String runningMode){
		List<WebElementTrainingRecord> allNodes = getAllUitrNodes(removeProcessedElement, !forRunner);
		//System.out.println("s21");
		allNodes = this.predictor.uitrPredict(this.domainName, this.testCaseName, this.screenName, allNodes, forRunner, runningMode);
		allNodes.forEach(uitr->{
			if (uitr.getElementCoreHtmlCode().contains(TebloudMachine.MONITORED_NODE_HTML_CODE) && uitr.getElementCoreHtmlCode().length()<TebloudMachine.MONITORED_NODE_LENGTH)
				_log.info(TebloudMachine.MONITORED_NODE_HTML_CODE + " node after prediction1 " + uitr.getInputLabelName() + ":" + uitr.getPioPredictConfidence() + ":" + uitr.getElementCoreHtmlCode());

		});
		return allNodes;
	}


	public List<WebElementTrainingRecord> getAllUitrNodes(
			boolean removeProcessedElement, boolean removeLink) {
		final List<WebElementTrainingRecord> ret = new ArrayList<WebElementTrainingRecord>();

		//List<HTMLSource> previousDoms = sources.get("previousDoms");
		List<HTMLSource> newDoms = this.newHtmlSource;

		List<HTMLSource> topDocDom = newDoms.stream().filter(newDom->newDom.getIndex()==0).collect(Collectors.toList());
		int topDocWidth = topDocDom.iterator().next().getDocWidth();
		int topDocHeight = topDocDom.iterator().next().getDocHeight();
		int screenWidth = topDocDom.iterator().next().getScreenWidth();
		// TODO performance improvement by reduce the html2dom conversion

		newDoms.stream()
				.parallel()
				.forEach(
						source -> {
							try {
								Document newDoc;
								if (source.getDomDocument() == null)
									newDoc = GlobalUtils.html2Dom(source
										.getDomDoc());
								else
									newDoc = source.getDomDocument();

								Element newBody = GlobalUtils.getBodyEquevelantElement(newDoc);
								NodeList allNodes = newBody
										.getElementsByTagName("*");
								Stream<Node> nodeStream = IntStream.range(0, allNodes.getLength())
										   .mapToObj(allNodes::item);
								List<Node> filteredNodes = nodeStream.parallel().filter(node->node.getNodeType()==Node.ELEMENT_NODE && !node.getNodeName().equalsIgnoreCase("script")
										&& !(node.getNodeName().equalsIgnoreCase("link") && removeLink)).collect(Collectors.toList());
								_log.info("number of total filtered nodes: " + filteredNodes.size());
								filteredNodes.forEach(node->{


//								for (int index = 0; index < allNodes
//										.getLength(); index++) {//Slow 3
//
									//Node node = allNodes.item(index);
//									if(node.getNodeType()!=Node.ELEMENT_NODE || node.getNodeName().equalsIgnoreCase("script")
//											|| node.getNodeName().equalsIgnoreCase("link")) {
//										continue;
//									}
									String invisible = Optional
											.ofNullable(
													$(node).attr(
															WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME))
											.orElse("");
									 String processed =
									 Optional.ofNullable($(node).attr(WebFormUserInputsCollectorHtmlTerms.PREVIOUS_PROCESSED_ATTR_NAME)).orElse("");
									if (!invisible
											.equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE)
											&& !(removeProcessedElement && processed
													.equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE))) {

										StringWriter nodeStr = new StringWriter();
										try {
											$(node).write(nodeStr);
										} catch (Exception e) {
											// TODO Auto-generated catch block
											_log.info("failed  to convert to html source from dom." + e.getMessage());
											throw new IllegalStateException(
													"failed  to convert to html source from dom node.");
										}
										WebElementTrainingRecord newRecord = new WebElementTrainingRecord();
										newRecord
												.setUserInputType(UserInputType.USERINPUT);
										newRecord
												.setElementCoreHtmlCode(nodeStr
														.toString()
														.replaceAll("\r", "")
														.replaceAll("\n", ""));
										newRecord.setInputMLHtmlCode(newRecord
												.getElementCoreHtmlCode()
												.replaceAll("\r", "")
												.replaceAll("\n", ""));
										newRecord
												.setElementCoreHtmlCodeWithoutGuidValue(GlobalUtils
														.convertToComparableString(newRecord
																.getElementCoreHtmlCode()));
										newRecord
												.setElementCoreHtmlNode(Optional
														.of(node));
										GlobalUtils.normalizeUitr(newRecord, screenWidth, topDocWidth, topDocHeight);
										if (newRecord.getComputedCssSizes()
												.iterator().next()
												.getUitrSize() > 0 && newRecord.getComputedCssSizes().iterator().next().getUitrHeight()>minimumHeightOfUitr
												&& newRecord.getComputedCssSizes().iterator().next().getUitrWidth()>minimumWidthOfUitr) {
											//if ($(node).toString().contains("my first wp post"))
											//	_log.info("my first wp post title link node: " + $(node).toString());

											ret.add(newRecord);
										}
									}
								//}
								});

							} catch (IOException e1) {
								_log.info("failed  to convert to html source from dom." + e1.getMessage());
								throw new IllegalStateException(
										"failed  to convert to html source from dom.");
							} catch (ParserConfigurationException e1) {
								_log.info("failed  to convert to html source from dom." + e1.getMessage());
								throw new IllegalStateException(
										"failed to convert to html source from dom.");
							} catch (Html2DomException e1) {
								_log.info("failed  to convert to html source from dom." + e1.getMessage());
								throw new IllegalStateException(
										"failed to convert to html source from dom.");

							}
						});
		_log.info("returning from get all uitrs");

		return ret;

	}
	public List<Element> getChildElements(Node node)
    {
		List<Element> retVal = new ArrayList<Element>();
        node = node.getFirstChild();
        while (node != null )
        {
        	if (node.getNodeType() == Node.ELEMENT_NODE) {
        		retVal.add((Element) node);
        	}
        	node = node.getNextSibling();
        }
        return retVal;
    }
	public List<WebElementTrainingRecord> parseNonUserTrainedHtmlNodeUitrsForTrainer(
			List<WebElementTrainingRecord> allNewCreatedRawPredictedUitrs,
			//Map<String, List<HTMLSource>> doms,
			List<WebElementTrainingRecord> userTrainedUitrsOnThisScreen) {

		List<WebElementTrainingRecord> allNewCreatedRawPredictedUitrsCopy = new ArrayList<WebElementTrainingRecord>(allNewCreatedRawPredictedUitrs);
//		allNewCreatedRawPredictedUitrsCopy.stream().filter(uitr->uitr.getCoreGuid().equalsIgnoreCase("149e4653-9ae3-4ab9-b942-cc89de0a8b1a")
//				|| uitr.getCoreGuid().equalsIgnoreCase("a6f2d6e4-76e8-4bfe-910c-602248808ea9")).collect(Collectors.toList()).forEach(fName->{
//			System.out.println(fName.getInputLabelName() + ":" + fName.getElementCoreHtmlCode());
//		});

			userTrainedUitrsOnThisScreen.forEach(userTUitr -> {
				if (!userTUitr.getElementCoreHtmlNode().isPresent()) {
					userTUitr.setElementCoreHtmlNode(GlobalUtils
							.html2Node(userTUitr.getElementCoreHtmlCode()));
				}
			});
			Map<Node, WebElementTrainingRecord> allUserTrainedNodeIncludingChildNodeUitrMap = userTrainedUitrsOnThisScreen
					.stream()
					.map(userTrainedUitr -> userTrainedUitr.parseChildUitrs())
					.flatMap(childUitrList -> childUitrList.stream())
					.filter(childUitr -> childUitr.getElementCoreHtmlNode() != null
							&& childUitr.getElementCoreHtmlNode().isPresent())
					.collect(
							Collectors
									.toMap(WebElementTrainingRecord::getElementCoreHtmlNode,
											Function.identity()))
					.entrySet()
					.stream()
					.collect(
							Collectors.toMap(entry -> entry.getKey().get(),
									entry -> entry.getValue()));
			List<Node> allUserTrainedChildNodes = allUserTrainedNodeIncludingChildNodeUitrMap.keySet()
					.stream().collect(Collectors.toList());
			String allUserTrainedChildUitrsHtmlCode = allUserTrainedNodeIncludingChildNodeUitrMap.values().stream()
					.map(userUitr -> userUitr.getCoreGuid())
					.collect(Collectors.joining(" "));
			allNewCreatedRawPredictedUitrsCopy.forEach(uitr -> {
				if (allUserTrainedChildUitrsHtmlCode.contains(uitr.getCoreGuid())) {
					uitr.setBelongToCurrentTestCase(true);
					uitr.setGraphId(null);
					uitr.setId(null);
				} else {
					uitr.getTestcases().clear();
					uitr.setBelongToCurrentTestCase(false);
					uitr.setActionTrigger(false);
					uitr.setGraphId(null);
					uitr.setId(null);
				}

			});


			Map<Node, WebElementTrainingRecord> noSiblingParentNodeMappedToUserTrainedChildUitrs = new HashMap<Node,WebElementTrainingRecord>();
			List<WebElementTrainingRecord> noSiblingParentNodeUitrsCompareToUserTrainedUitrs = allNewCreatedRawPredictedUitrsCopy
					.stream()
					.parallel()
					.filter(rawUitr -> {
						boolean retVal = false;
						List<Node> parentOfUserTrainedUitrNodeWithNoOtherChild = allUserTrainedChildNodes
								.stream()
								.parallel()
								.filter(userTraiinngNode -> {
									boolean meetParentConditions = false;
									if (rawUitr.getElementCoreHtmlNode()
											.isPresent()) {
										Node rawUitrNode = rawUitr
												.getElementCoreHtmlNode().get();
										List<Element> childElements = getChildElements(rawUitrNode);
										while (childElements
												.size() == 1
												&& !(childElements
														.get(0))
														.getAttribute(
																"ate-guid")
														.equals(((Element) userTraiinngNode)
																.getAttribute("ate-guid"))) {
											rawUitrNode = childElements.get(0);
											childElements = getChildElements(rawUitrNode);
										}
										if (childElements
												.size() == 1
												&& (childElements
														.get(0))
														.getAttribute(
																"ate-guid")
														.equals(((Element) userTraiinngNode)
																.getAttribute("ate-guid"))) {
											if (rawUitr
													.getInputLabelName()
													.equalsIgnoreCase(
															allUserTrainedNodeIncludingChildNodeUitrMap
																	.get(userTraiinngNode)
																	.getInputLabelName())) {
												meetParentConditions = true;
											}
											else {
												rawUitr.setInputLabelName(allUserTrainedNodeIncludingChildNodeUitrMap
																	.get(userTraiinngNode)
																	.getInputLabelName());
												meetParentConditions = true;
											}
										}
									}
									return meetParentConditions;
								}).collect(Collectors.toList());
						if (!parentOfUserTrainedUitrNodeWithNoOtherChild
								.isEmpty()) {
							rawUitr.setBelongToCurrentTestCase(allUserTrainedNodeIncludingChildNodeUitrMap.get(parentOfUserTrainedUitrNodeWithNoOtherChild.get(0)).isBelongToCurrentTestCase());
							noSiblingParentNodeMappedToUserTrainedChildUitrs.put(rawUitr.getElementCoreHtmlNode().get(), allUserTrainedNodeIncludingChildNodeUitrMap.get(parentOfUserTrainedUitrNodeWithNoOtherChild.get(0)));
							retVal = true;
						}
						return retVal;
					}).collect(Collectors.toList());
			noSiblingParentNodeUitrsCompareToUserTrainedUitrs
					.forEach(noSiblingParentUitr -> {
						// System.out.println(GlobalUtils.domNodeToString(noSiblingParentUitr.getElementCoreHtmlNode().get(),
						// true, false));
						//noSiblingParentUitr.setBelongToCurrentTestCase(true);
						noSiblingParentUitr.setGraphId(null);
						noSiblingParentUitr.setId(null);
					});
			noSiblingParentNodeUitrsCompareToUserTrainedUitrs.stream().filter(uitr->uitr.getInputLabelName().equalsIgnoreCase("fullName")).collect(Collectors.toList()).forEach(fName->{
				System.out.println(fName.getInputLabelName() + " parent: " + fName.getElementCoreHtmlCode());
			});

			//TODO
			//recheck the all nodes and find the parent who has only one childElement or has childElements equal to
			//userTrainedElements or noSiblingParentNodeUitrsCompareToUserTrainedUitrs
			Map<Node, WebElementTrainingRecord> allUserTrainedNodeIncludingChildNodeAndDirectParentNodeUitrMap = new HashMap<Node, WebElementTrainingRecord>();
			allUserTrainedNodeIncludingChildNodeAndDirectParentNodeUitrMap.putAll(noSiblingParentNodeMappedToUserTrainedChildUitrs);
			allUserTrainedNodeIncludingChildNodeAndDirectParentNodeUitrMap.putAll(allUserTrainedNodeIncludingChildNodeUitrMap);

//			allUserTrainedNodeIncludingChildNodeUitrMap.entrySet().forEach(mappEntry->{
//				if (((Element)mappEntry.getKey()).getAttribute("ate-guid").equals("b8d5345c-ec56-4064-91f2-917365a799f8")
//						||
//						((Element)mappEntry.getKey()).getAttribute("ate-guid").equals("602a9e21-72da-4e5b-95bc-60db8e59ba11")) {
//					System.out.println(mappEntry.getValue().getInputLabelName() + " childLabeledMatch: " + mappEntry.getValue().getElementCoreHtmlCode());
//				}
//			});
//			allUserTrainedNodeIncludingChildNodeAndDirectParentNodeUitrMap.entrySet().forEach(mappEntry->{
//				if (((Element)mappEntry.getKey()).getAttribute("ate-guid").equals("b8d5345c-ec56-4064-91f2-917365a799f8")
//						||
//						((Element)mappEntry.getKey()).getAttribute("ate-guid").equals("602a9e21-72da-4e5b-95bc-60db8e59ba11")) {
//					System.out.println(mappEntry.getValue().getInputLabelName() + " noOtherLabeledMatch: " + mappEntry.getValue().getElementCoreHtmlCode());
//				}
//			});
		List<WebElementTrainingRecord> noOtherLabeledSiblingsParentNodeUitrsCompareToUserTrainedUitrs = allNewCreatedRawPredictedUitrsCopy
				.stream()
				.parallel()
				.filter(rawUitr -> {
					boolean meetConditions = false;
					if (rawUitr.getElementCoreHtmlNode().isPresent()) {
						Node rawUitrNode = rawUitr.getElementCoreHtmlNode()
								.get();
						List<Element> childElements = getChildElements(rawUitrNode);
						while (childElements.size() == 1) {
							rawUitrNode = childElements.get(0);
							childElements = getChildElements(rawUitrNode);
						}
						if (childElements.size() > 1) {
							// && (childElements
							// .get(0))
							// .getAttribute(
							// "ate-guid")
							// .equals(((Element) userTraiinngNode)
							// .getAttribute("ate-guid"))) {
							String childElementsGuids = childElements.stream()
									.map(ele -> ele.getAttribute("ate-guid"))
									.collect(Collectors.joining(" "));
							Map<Node, WebElementTrainingRecord> mappedUitrs = allUserTrainedNodeIncludingChildNodeAndDirectParentNodeUitrMap
									.entrySet()
									.stream()
									.filter(entry -> {
										return childElementsGuids.contains((((Element) entry
												.getKey())
												.getAttribute("ate-guid")));
									})
									.collect(
											Collectors.toMap(
													entry -> entry.getKey(),
													entry -> entry.getValue()));
							List<String> uniqueLabels = mappedUitrs.values()
									.stream()
									.map(uitr -> uitr.getInputLabelName())
									.collect(Collectors.toList()).stream()
									.distinct().collect(Collectors.toList());
//							if (((Element) rawUitrNode)
//									.getAttribute("ate-guid")
//									.equals("d0f4dffb-f7ed-4243-a84a-42b13acea6b9")) {
//								System.out.println("test point: "
//										+ uniqueLabels.size() + " "
//										+ childElements.size() + " "
//										+ mappedUitrs.size());
//							}
							if (uniqueLabels.size() == 1
									&& childElements.size() == mappedUitrs
											.size()) {
								meetConditions = true;
								rawUitr.setBelongToCurrentTestCase(mappedUitrs.values().iterator().next().isBelongToCurrentTestCase());
								if (!rawUitr.getInputLabelName()
										.equalsIgnoreCase(uniqueLabels.get(0))) {
									rawUitr.setInputLabelName(uniqueLabels
											.get(0));
								}
							}
						}
					}

					return meetConditions;
				}).collect(Collectors.toList());

			noOtherLabeledSiblingsParentNodeUitrsCompareToUserTrainedUitrs
			.forEach(noOtherLabeledSiblingsParentNodeUitr -> {
//				System.out.println(noOtherLabeledSiblingsParentNodeUitr.getInputLabelName() + " :" + GlobalUtils.domNodeToString(noOtherLabeledSiblingsParentNodeUitr.getElementCoreHtmlNode().get(),
//				 true, false));
				//noOtherLabeledSiblingsParentNodeUitr.setBelongToCurrentTestCase(true);
				noOtherLabeledSiblingsParentNodeUitr.setGraphId(null);
				noOtherLabeledSiblingsParentNodeUitr.setId(null);
			});


			List<WebElementTrainingRecord> decendentNodeUitrsCompareToUserTrainedUitrs = allNewCreatedRawPredictedUitrsCopy
					.stream()
					.parallel()
					.filter(rawUitr -> {
						boolean retVal = false;
						List<Node> singleDecendentOfUserTrainedUitrNodeWithNoOtherSiblings = allUserTrainedChildNodes
								.stream()
								.parallel()
								.filter(userTraiinngNode -> {
									boolean meetChildConditions = false;
									// if
									// (rawUitr.getElementCoreHtmlNode().isPresent()
									// &&
									// userTraiinngNode.getChildNodes().getLength()==1){
									if (rawUitr.getElementCoreHtmlNode()
											.isPresent()) {
										if (GlobalUtils
												.domNodeToString(
														userTraiinngNode, true,
														false)
												.contains(rawUitr.getCoreGuid())
												&& !allUserTrainedNodeIncludingChildNodeUitrMap
														.get(userTraiinngNode)
														.getCoreGuid()
														.equals(rawUitr
																.getCoreGuid())) {
											System.out.println(rawUitr
													.getInputLabelName()
													+ " child: "
													+ allUserTrainedNodeIncludingChildNodeUitrMap
															.get(userTraiinngNode)
															.getInputLabelName()
													+ " child: "
													+ rawUitr
															.getElementCoreHtmlCode());
											if (rawUitr
													.getInputLabelName()
													.equalsIgnoreCase(
															allUserTrainedNodeIncludingChildNodeUitrMap
																	.get(userTraiinngNode)
																	.getInputLabelName()))
												rawUitr.setBelongToCurrentTestCase(allUserTrainedNodeIncludingChildNodeUitrMap
														.get(userTraiinngNode).isBelongToCurrentTestCase());
												meetChildConditions = true;
										}
										// above code need further test. need to
										// manually add a uitr with children to
										// test it.
										// Node rawUitrNode =
										// rawUitr.getElementCoreHtmlNode().get();

										// Node originalUserTrainingNode =
										// userTraiinngNode;
										// while
										// (userTraiinngNode.getChildNodes()
										// .getLength() == 1
										// && (rawUitrNode instanceof Element &&
										// userTraiinngNode
										// .getChildNodes()
										// .item(0) instanceof Element)
										// && !((Element) userTraiinngNode
										// .getChildNodes()
										// .item(0))
										// .getAttribute(
										// "ate-guid")
										// .equals(((Element) rawUitrNode)
										// .getAttribute("ate-guid"))) {
										// userTraiinngNode = userTraiinngNode
										// .getChildNodes().item(0);
										// }
										// if (userTraiinngNode.getChildNodes()
										// .getLength() == 1
										// && (rawUitrNode instanceof Element &&
										// userTraiinngNode
										// .getChildNodes()
										// .item(0) instanceof Element)
										// && ((Element) userTraiinngNode
										// .getChildNodes()
										// .item(0))
										// .getAttribute(
										// "ate-guid")
										// .equals(((Element) rawUitrNode)
										// .getAttribute("ate-guid"))) {
										// System.out.println(rawUitr.getInputLabelName()
										// + ":"
										// +allUserTrainedNodeUitrMap.get(originalUserTrainingNode).getInputLabelName());
										// if
										// (rawUitr.getInputLabelName().equalsIgnoreCase(allUserTrainedNodeUitrMap.get(originalUserTrainingNode).getInputLabelName()))
										// meetChildConditions = true;
										// }
									}
									return meetChildConditions;
								}).collect(Collectors.toList());
						if (!singleDecendentOfUserTrainedUitrNodeWithNoOtherSiblings
								.isEmpty()) {
							retVal = true;
						}
						return retVal;
					}).collect(Collectors.toList());

			decendentNodeUitrsCompareToUserTrainedUitrs
					.forEach(noSiblingParentUitr -> {
//						System.out.println(GlobalUtils.domNodeToString(
//								noSiblingParentUitr.getElementCoreHtmlNode()
//										.get(), true, false));
						//noSiblingParentUitr.setBelongToCurrentTestCase(true);
						noSiblingParentUitr.setGraphId(null);
						noSiblingParentUitr.setId(null);
					});

			List<WebElementTrainingRecord> allUserTrainedNodeIncludingChildNodeUitrs = new ArrayList<WebElementTrainingRecord>(allUserTrainedNodeIncludingChildNodeUitrMap.values());
			allUserTrainedNodeIncludingChildNodeUitrs.addAll(noSiblingParentNodeUitrsCompareToUserTrainedUitrs);
			allUserTrainedNodeIncludingChildNodeUitrs.addAll(noOtherLabeledSiblingsParentNodeUitrsCompareToUserTrainedUitrs);
			allUserTrainedNodeIncludingChildNodeUitrs.addAll(decendentNodeUitrsCompareToUserTrainedUitrs);
			String allUserTrainedUitrsHtmlCode = allUserTrainedNodeIncludingChildNodeUitrs.stream()
					.map(userUitr -> userUitr.getCoreGuid())
					.collect(Collectors.joining(" "));
			List<WebElementTrainingRecord> userTrainedEquavelantNodes = allNewCreatedRawPredictedUitrsCopy
					.stream()
					.filter(uitr -> allUserTrainedUitrsHtmlCode.contains(uitr
							.getCoreGuid())).collect(Collectors.toList());
			allNewCreatedRawPredictedUitrsCopy.stream().filter(uitr->uitr.getInputLabelName().equalsIgnoreCase("fullName")).collect(Collectors.toList()).forEach(fName->{
				System.out.println(fName.getInputLabelName() + " a: " +fName.isBelongToCurrentTestCase() +" " + fName.getElementCoreHtmlCode());
			});
			allNewCreatedRawPredictedUitrsCopy.removeAll(userTrainedEquavelantNodes);
			allNewCreatedRawPredictedUitrsCopy.stream().filter(uitr->uitr.getInputLabelName().equalsIgnoreCase("fullName")).collect(Collectors.toList()).forEach(fName->{
				System.out.println(fName.getInputLabelName() + " b:" + fName.getElementCoreHtmlCode());
			});
//			allNewCreatedRawPredictedUitrsCopy.forEach(uitr -> {
//				if (!noSiblingParentNodeUitrsCompareToUserTrainedUitrs
//						.contains(uitr)) {
//					uitr.getTestcases().clear();
//					uitr.setBelongToCurrentTestCase(false);
//					uitr.setActionTrigger(false);
//					uitr.setGraphId(null);
//					uitr.setId(null);
//				}
//			});

			allNewCreatedRawPredictedUitrsCopy.stream().filter(uitr->uitr.getInputLabelName().equalsIgnoreCase("fullName")).collect(Collectors.toList()).forEach(fName->{
				System.out.println(fName.getInputLabelName() + " c: " +fName.isBelongToCurrentTestCase() +" "+ fName.getElementCoreHtmlCode());
			});

			userTrainedUitrsOnThisScreen.clear();
			userTrainedUitrsOnThisScreen.addAll(allUserTrainedNodeIncludingChildNodeUitrs);
//			userTrainedUitrsOnThisScreen.addAll(noSiblingParentNodeUitrsCompareToUserTrainedUitrs);
//			userTrainedUitrsOnThisScreen.addAll(noOtherLabeledSiblingsParentNodeUitrsCompareToUserTrainedUitrs);
//			userTrainedUitrsOnThisScreen.addAll(decendentNodeUitrsCompareToUserTrainedUitrs);
		return allNewCreatedRawPredictedUitrsCopy;

	}


	/**
	 * @return the previousHtmlSource
	 */
	public Optional<List<HTMLSource>> getPreviousHtmlSource() {
		return previousHtmlSource;
	}

	/**
	 * @return the newHtmlSource
	 */
	public List<HTMLSource> getNewHtmlSource() {
		return newHtmlSource;
	}

	public List<Document> getNewDomDocuments() {
		List<Document> docs = newHtmlSource.stream().map(mapper->mapper.getDomDocument()).collect(Collectors.toList());
		docs.removeIf(doc->doc==null);
		return docs;
	}

}
