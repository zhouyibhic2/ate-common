/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model.domain;


import org.eclipse.jdt.annotation.Nullable;
import org.neo4j.ogm.annotation.GraphId;
//import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Transient;
import org.w3c.dom.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

// TODO: Auto-generated Javadoc
/**
 * The Class Greeting.
 */
@NodeEntity
public class HTMLSource extends BaseAteNode implements INonPointerAssignment{


	///** The Constant serialVersionUID. */
	//private static final long serialVersionUID = 5806929726322626369L;

	/** The timestamp. */
	@Nullable
	private Long timestamp=System.currentTimeMillis();

	/** The xpath of frame. */
	//@Index
	private String xpathOfFrame;
	/** The content. */
	@Transient
	private String domDoc;

	private int index;
	/** The parent index. */
	private int parentIndex;

	/** The doc text. */
	private String docText;

	private String frameSrc;
	/** The visible. */
	private boolean visible;

	/** The doc guid. */
	private String docGuid;

	private int locationLeft;

	private int locationTop;

	private int screenWidth;

	private int docWidth;
	private int docHeight;

	@JsonIgnore
	@Transient
	private transient Document domDocument=null;

	/**
	 * Instantiates a new HTML source.
	 * spring won't be able to match the class to json if
	 * there is no empty constructor
	 */
	public HTMLSource() {
		super("HTMLSource");
		xpathOfFrame = "";
		domDoc = "";
		docText = "";
		visible = true;
		docGuid= "";
		frameSrc = "";
		index = 0;
		parentIndex = 0;
		locationLeft = 0;
		locationTop = 0;
		setDocWidth(0);
		setDocHeight(0);
	}



	/**
	 * @return the xpathOfFrame
	 */
	public String getXpathOfFrame() {
		return xpathOfFrame;
	}

	/**
	 * @param xpathOfFrame
	 *            the xpathOfFrame to set
	 */
	public void setXpathOfFrame(String xpathOfFrame) {
		this.xpathOfFrame = xpathOfFrame;
	}

	/**
	 * @return the domDoc
	 */
	public String getDomDoc() {
		return domDoc;
	}

	/**
	 * @param domDoc the domDoc to set
	 */
	public void setDomDoc(String domDoc) {
		this.domDoc = domDoc;
	}



	/**
	 * @return the parentIndex
	 */
	public int getParentIndex() {
		return parentIndex;
	}



	/**
	 * @param parentIndex the parentIndex to set
	 */
	public void setParentIndex(int parentIndex) {
		this.parentIndex = parentIndex;
	}



	/**
	 * @return the id
	 */
	@Nullable
	public Long getId() {
		return getGraphId();
	}



	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {//NOPMD
		this.setGraphId(id);
	}



	/**
	 * @return the docText
	 */
	public String getDocText() {
		return docText;
	}



	/**
	 * @param docText the docText to set
	 */
	public void setDocText(String docText) {
		this.docText = docText;
	}



	/**
	 * @return the timestamp
	 */
	@Nullable
	public Long getTimestamp() {
		return timestamp;
	}



	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}



	/**
	 * @return the visible
	 */
	public boolean isVisible() {
		return visible;
	}



	/**
	 * @param visible the visible to set
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public void assignedFrom(INonPointerAssignment value) {
		if (value instanceof HTMLSource) {
			HTMLSource record = (HTMLSource) value;
			this.setDocGuid(record.getDocGuid());
			this.setDocText(record.getDocText());
			this.setDomDoc(record.getDomDoc());
			this.setFrameSrc(record.getFrameSrc());
			this.setIndex(record.getIndex());
			this.setParentIndex(record.getParentIndex());
			Long temp = record.getTimestamp();
			if (null != temp)
				this.setTimestamp(temp);
			this.setVisible(record.isVisible());
			this.setXpathOfFrame(record.getXpathOfFrame());
			this.setLocationLeft(record.getLocationLeft());
			this.setLocationTop(record.getLocationTop());
			this.setDocWidth(record.getDocWidth());
			this.setDocHeight(record.getDocHeight());


		}

	}







	/**
	 * @return the docGuid
	 */
	public String getDocGuid() {
		return docGuid;
	}



	/**
	 * @param docGuid the docGuid to set
	 */
	public void setDocGuid(String docGuid) {
		this.docGuid = docGuid;
	}



	/**
	 * @return the frameSrc
	 */
	public String getFrameSrc() {
		return frameSrc;
	}



	/**
	 * @param frameSrc the frameSrc to set
	 */
	public void setFrameSrc(String frameSrc) {
		this.frameSrc = frameSrc;
	}



	/**
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}



	/**
	 * @param index the index to set
	 */
	public void setIndex(int index) {
		this.index = index;
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getIdentifierString() {

		return this.getDocGuid();
	}



	/**
	 * @return the locationLeft
	 */
	public int getLocationLeft() {
		return locationLeft;
	}



	/**
	 * @param locationLeft the locationLeft to set
	 */
	public void setLocationLeft(int locationLeft) {
		this.locationLeft = locationLeft;
	}



	/**
	 * @return the locationTop
	 */
	public int getLocationTop() {
		return locationTop;
	}



	/**
	 * @param locationTop the locationTop to set
	 */
	public void setLocationTop(int locationTop) {
		this.locationTop = locationTop;
	}



	/**
	 * @return the docWidth
	 */
	public int getDocWidth() {
		return docWidth;
	}



	/**
	 * @param docWidth the docWidth to set
	 */
	public void setDocWidth(int docWidth) {
		this.docWidth = docWidth;
	}



	/**
	 * @return the docHeight
	 */
	public int getDocHeight() {
		return docHeight;
	}



	/**
	 * @param docHeight the docHeight to set
	 */
	public void setDocHeight(int docHeight) {
		this.docHeight = docHeight;
	}



	/**
	 * @return the domDocument
	 */
	public Document getDomDocument() {
		return domDocument;
	}



	/**
	 * @param domDocument the domDocument to set
	 */
	public void setDomDocument(Document domDocument) {
		this.domDocument = domDocument;
	}



	/**
	 * @return the screenWidth
	 */
	public int getScreenWidth() {
		return screenWidth;
	}



	/**
	 * @param screenWidth the screenWidth to set
	 */
	public void setScreenWidth(int screenWidth) {
		this.screenWidth = screenWidth;
	}




}
