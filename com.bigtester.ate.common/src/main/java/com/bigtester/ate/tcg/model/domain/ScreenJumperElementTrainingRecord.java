/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.model.domain;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.bigtester.RunnerGlobalUtils;
import com.voodoodyne.jackson.jsog.JSOGGenerator;

import org.apache.commons.lang3.StringUtils;
import org.bigtester.ate.model.page.atewebdriver.exception.BrowserUnexpectedException;
import org.bigtester.ate.GlobalUtils;
import org.bigtester.ate.model.page.atewebdriver.MultiWindowsHandler;
import org.bigtester.ate.systemlogger.LogbackWriter;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
//import org.sikuli.script.Match;
import org.w3c.dom.Element;

import com.bigtester.ate.tcg.controller.WebFormUserInputsCollectorHtmlTerms;
import com.bigtester.ate.tcg.model.ScreenNamePredictStrategy;
import com.bigtester.ate.tcg.model.relationship.Relations;
import com.bigtester.ate.tcg.model.relationship.StepOut;
import com.bigtester.ate.tcg.model.relationship.StepOut.StepOutTriggerType;
import com.bigtester.ate.tcg.model.ws.api.ScreenStepsAdvice;
import com.bigtester.ate.tcg.model.ws.api.ScreenStepsAdvice.StepExecutionStatus;
import com.bigtester.ate.tcg.runner.model.DevToolMessage;
import com.bigtester.ate.tcg.runner.model.ITestStepExecutionContext;
import com.bigtester.ate.tcg.runner.model.ITestStepRunner;
import com.bigtester.ate.tcg.runner.screenobject.handler.IFilePickerDialogHandler;
import com.bigtester.ate.tcg.runner.screenobject.handler.IWebElementHandler;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

// TODO: Auto-generated Javadoc
/**
 * This class TrainingRecord defines ....
 * @author Peidong Hu
 *
 */

/**
 * @author ashraf
 *
 */
@NodeEntity
@JsonIdentityInfo(generator=JSOGGenerator.class )
//@JsonIdentityInfo(generator=ObjectIdGenerators.UUIDGenerator.class, property="@uuid")
public class ScreenJumperElementTrainingRecord extends UserInputTrainingRecord
		implements ITestStepRunner,
		INameLabelPredictableObjectNonPointerAssignment, INonPointerAssignment {

//	/** The step outs. */
//	@JsonIgnore
//	@Relationship(type = Relations.STEP_OUT, direction = Relationship.OUTGOING)
//	private Set<Neo4jScreenNode> stepOuts = new HashSet<Neo4jScreenNode>();
		/**
	 * Instantiates a new screen action element training record.
	 */
	public ScreenJumperElementTrainingRecord() {
		super("ScreenActionElementTrainingRecord");
	}

	public ScreenJumperElementTrainingRecord(WebElementTrainingRecord uitr) {
		super("ScreenActionElementTrainingRecord", uitr);
		this.setUserInputType(UserInputType.SCREENJUMPER);
	}

	public ScreenJumperElementTrainingRecord(UserInputTrainingRecord uitr) {
		super("ScreenActionElementTrainingRecord", uitr);
		this.setUserInputType(UserInputType.SCREENJUMPER);
	}

	/**
	 * Adds the step out.
	 *
	 * @param screenNode the screen node
	 * @return the step out
	 */
	public StepOut addStepOut(Neo4jScreenNode screenNode) {
		return addStepOut(screenNode, StepOutTriggerType.SCREENJUMPERSTEPOUT);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void assignedFrom(INonPointerAssignment value) {
		super.assignedFrom(value);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getComparableLabelName() {
		return this.getPioPredictLabelResult().getValue();
	}

	protected Map<ScreenNamePredictStrategy, ScreenStepsAdvice> tryClickGuidElementInMachineLearningHtmlCodeUntilSuccess(
			String guid, String mlHtmlCode,
			ITestStepExecutionContext executeEngine,
			ScreenStepsAdvice stepAdvice,DevToolMessage message, String excludedGuid) {
		Map<ScreenNamePredictStrategy, ScreenStepsAdvice> newStepAdvices = new HashMap<ScreenNamePredictStrategy, ScreenStepsAdvice>();
		if (!StringUtils.isEmpty(excludedGuid) && guid.equals(excludedGuid)) return newStepAdvices;
		try{

			WebElement webE = executeEngine.findElement(guid);
			//WebElement webE = executeEngine.getMyWebDriver().getWebDriverInstance().findElement(By.cssSelector("[ate-guid='" + guid +"']"));
			DevToolMessage newScreenMessage=null;
			if (webE.getSize().getWidth()/2 > 10 && webE.getSize().getHeight()/2>5) {
				Actions builder = new Actions(executeEngine.getMyWebDriver().getWebDriverInstance());
				builder.moveToElement(webE, webE.getSize().getWidth()/2, webE.getSize().getHeight()/2).click().build().perform();
				//action build seems not triggering the afterClickOn event, this is a hack
				//TODO better way to trigger this event.
				(( MultiWindowsHandler ) GlobalUtils.getTargetObject(executeEngine.getMyWebDriver().getMultiWindowsHandler())).afterClickOn(webE, executeEngine.getMyWebDriver().getWebDriverInstance());
				executeEngine.getMyWebDriver().getMultiWindowsHandler().getAlerts().forEach(alert->{
					alert.accept();
				});
				System.out.println("screen jumper location based click: " + this.getInputLabelName());
				//webE.click();
				//Thread.sleep(5000);

				stepAdvice.setExecutedJumperElementInThisScreen(Optional.of(this));
				executeEngine.getMyWebDriver().getMultiWindowsHandler().focusOnLatestWindow();
				newScreenMessage = executeEngine.downloadScreenMessage();
				newStepAdvices = executeEngine
						.getStepAdvice(executeEngine.getTestCaseName(),newScreenMessage, stepAdvice);
			}

			if (newStepAdvices.isEmpty()) {
				if (newScreenMessage!=null && !newScreenMessage.equals(message)) {
					executeEngine.getMyWebDriver().getWebDriverInstance().navigate().back();
				}
				webE = executeEngine.findElement(guid);
				webE.click();
				//(( MultiWindowsHandler ) executeEngine.getMyWebDriver().getMultiWindowsHandler()).afterClickOn(webE, executeEngine.getMyWebDriver().getWebDriverInstance());
				executeEngine.getMyWebDriver().getMultiWindowsHandler().getAlerts().forEach(alert->{
					alert.accept();
				});
				System.out.println("screen jumper webE based click: " + this.getInputLabelName());
				//Thread.sleep(5000);

				stepAdvice.setExecutedJumperElementInThisScreen(Optional.of(this));
				executeEngine.getMyWebDriver().getMultiWindowsHandler().focusOnLatestWindow();
				newScreenMessage = executeEngine.downloadScreenMessage();

				newStepAdvices = executeEngine
						.getStepAdvice(executeEngine.getTestCaseName(),newScreenMessage, stepAdvice);
				if (newStepAdvices.isEmpty()) {
					if (newScreenMessage!=null && !newScreenMessage.equals(message)) {
						executeEngine.getMyWebDriver().getWebDriverInstance().navigate().back();
					}
					String newGuid = RunnerGlobalUtils.parseNextAteAttr(mlHtmlCode, guid, IWebElementHandler.ELEMENT_FIND_ATE_ATTRIBUE_NAME);
					if (!newGuid.isEmpty() && !newGuid.equals(excludedGuid)) {
						return tryClickGuidElementInMachineLearningHtmlCodeUntilSuccess(newGuid, mlHtmlCode, executeEngine, stepAdvice, message, excludedGuid);
					}
				} else {
					if (executeEngine.isSameScreenNamedAdvices(newStepAdvices, executeEngine.getCurrentExecutingScreenStepAdvices())) {
						String newGuid = RunnerGlobalUtils.parseNextAteAttr(mlHtmlCode, guid, IWebElementHandler.ELEMENT_FIND_ATE_ATTRIBUE_NAME);
						if (!newGuid.isEmpty() && !newGuid.equals(excludedGuid)) {
							return tryClickGuidElementInMachineLearningHtmlCodeUntilSuccess(newGuid, mlHtmlCode, executeEngine, stepAdvice, message, excludedGuid);
						}
					}
				}
			} else if (executeEngine.isSameScreenNamedAdvices(newStepAdvices, executeEngine.getCurrentExecutingScreenStepAdvices())) {
					String newGuid = RunnerGlobalUtils.parseNextAteAttr(mlHtmlCode, guid, IWebElementHandler.ELEMENT_FIND_ATE_ATTRIBUE_NAME);
					if (!newGuid.isEmpty() && !newGuid.equals(excludedGuid)) {
						return tryClickGuidElementInMachineLearningHtmlCodeUntilSuccess(newGuid, mlHtmlCode, executeEngine, stepAdvice, message, excludedGuid);
					}

			} else if (!executeEngine.isValidStepPath(stepAdvice, this, newStepAdvices).isPresent()) {
				executeEngine.getMyWebDriver().getWebDriverInstance().navigate().back();
				String newGuid = RunnerGlobalUtils.parseNextAteAttr(mlHtmlCode, guid, IWebElementHandler.ELEMENT_FIND_ATE_ATTRIBUE_NAME);
				if (!newGuid.isEmpty() && !newGuid.equals(excludedGuid)) {
					return tryClickGuidElementInMachineLearningHtmlCodeUntilSuccess(newGuid, mlHtmlCode, executeEngine, stepAdvice, message, excludedGuid);
				}
			}
			return newStepAdvices;
		} catch (Throwable niwde) {

			if (niwde instanceof ElementNotVisibleException || niwde.getMessage().contains("is not clickable")) {
				String newGuid = RunnerGlobalUtils.parseNextAteAttr(mlHtmlCode, guid, IWebElementHandler.ELEMENT_FIND_ATE_ATTRIBUE_NAME);
				if (!newGuid.isEmpty() && !newGuid.equals(excludedGuid) ) {
					return tryClickGuidElementInMachineLearningHtmlCodeUntilSuccess(newGuid, mlHtmlCode, executeEngine, stepAdvice, message, excludedGuid);
				} else {
					return new HashMap<ScreenNamePredictStrategy, ScreenStepsAdvice> ();
				}

			} else {
				if (message != null) {
					String msg1 = "Failed screen url is:  " + message.getDomainProtocol() +
							"://" + message.getDomain() + message.getScreenUrl() + " with error: " + niwde.getMessage();
					System.out.println(msg1);
					LogbackWriter.writeAppInfo(msg1);
				}
				niwde.printStackTrace();

//				try {
//					executeEngine.getMyWebDriver().getMultiWindowsHandler().closeAllWindowsExceptMainWindow();
//				} catch (BrowserUnexpectedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}

//			}
			//throw niwde;
				return new HashMap<ScreenNamePredictStrategy, ScreenStepsAdvice> ();
			}
		}

	}

	@Override
	public Map<ScreenNamePredictStrategy, ScreenStepsAdvice> executeStep(ITestStepExecutionContext executeEngine,  ScreenStepsAdvice stepAdvice) throws Throwable {
		DevToolMessage message = stepAdvice.getDevToolMessage();
		UserInputTrainingRecord uitr = this;
		List<Element> coreElements = new ArrayList<Element>();
		Map<ScreenNamePredictStrategy, ScreenStepsAdvice> newStepAdvices = new HashMap<ScreenNamePredictStrategy, ScreenStepsAdvice>();
		String coreGuid = "";
		if ((coreElements  = this.parseAteCollectableTaggedElements(Arrays
								.asList(WebFormUserInputsCollectorHtmlTerms.USER_COLLECTABLE_TAGS_EXCEPT_ALINK))).size()>0) {
			coreGuid = coreElements.get(0).getAttribute(IWebElementHandler.ELEMENT_FIND_ATE_ATTRIBUE_NAME);
			newStepAdvices = tryClickGuidElementInMachineLearningHtmlCodeUntilSuccess(coreGuid, uitr.getInputMLHtmlCode(), executeEngine, stepAdvice, message, "");
		}
		if (newStepAdvices.isEmpty()) {
			String guid = RunnerGlobalUtils.parseFirstAteAttrByBoundary(uitr.getElementCoreHtmlCode(), IWebElementHandler.ELEMENT_FIND_ATE_ATTRIBUE_NAME);
			newStepAdvices = tryClickGuidElementInMachineLearningHtmlCodeUntilSuccess(guid, uitr.getInputMLHtmlCode(), executeEngine, stepAdvice, message, coreGuid);
		}
		return newStepAdvices;

	}



}
