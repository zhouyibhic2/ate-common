/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.utils;
 
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

//import scala.collection.Iterator;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;

// TODO: Auto-generated Javadoc
/**
 * This class MapLongSerializer defines ....
 * @author Peidong Hu
 *
 */
public class MapLongSerializer extends JsonSerializer<Set<Long>> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void serialize(Set<Long> value, JsonGenerator jgen,
			SerializerProvider provider) throws IOException,
			JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		Set<Long> temp2 = new HashSet<Long>();
		
		for (java.util.Iterator<Long> itr = value.iterator(); itr.hasNext();) {
			Object temp = itr.next();
			if (temp instanceof Integer)
			temp2.add(((Integer)temp).longValue());
			else if (temp instanceof Long)
				temp2.add(((Long) temp).longValue());
			else
				throw new IllegalStateException("The object in the Collection is not a long type number");
		}
		mapper.writeValue(jgen, temp2);
		//System.out.println("abc");
	}

}
