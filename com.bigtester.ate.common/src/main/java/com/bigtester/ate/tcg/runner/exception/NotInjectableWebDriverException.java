/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.runner.exception;

// TODO: Auto-generated Javadoc
/**
 * This class NotInjectableWebDriverException defines ....
 * @author Peidong Hu
 *
 */
public class NotInjectableWebDriverException extends IllegalStateException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4528765286299346413L;

	/**
	 * 
	 */
	public NotInjectableWebDriverException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param s
	 */
	public NotInjectableWebDriverException(String s) {
		super(s);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public NotInjectableWebDriverException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public NotInjectableWebDriverException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
