/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.runner.screenobject.handler;

import java.util.List;
import java.util.Optional;

import org.bigtester.ate.model.page.atewebdriver.IMyWebDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.w3c.dom.Element;

import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord.UserInputType;
import com.bigtester.ate.tcg.runner.model.ITestStepRunner;

// TODO: Auto-generated Javadoc
/**
 * This class IWebElementHandler defines ....
 * @author Peidong Hu
 *
 */
public interface IWebElementHandler {
	//public static final String ELEMENT_FIND_ATE_ATTRIBUE_NAME = "ate-computed-xpath";
	public static final String ELEMENT_FIND_ATE_ATTRIBUE_NAME = "ate-guid";
	public static enum ActionResult {
		NOT_TYPABLE, VALUE_NOT_TYPED_IN_CORRECTLY, SUCCESS, NOT_VISIBLE_OR_ENABLED, FAILED_FOR_UNKNOWN_REASON, NOT_VISIBLE_EXCEPTION
	}
	UserInputType getApplicableUitrType();
	Optional<Element> match(ITestStepRunner runner);
	boolean isTypeable(Element node);
	boolean isReadOnly(Element node);
	boolean isClickable(Element node);
	boolean isVisible(Element node);
	ActionResult doAcceptableAction(IMyWebDriver webDriver, String input, Element node);
	List<Element> match(Element node);
	WebElement findWebE(IMyWebDriver webDriver, String guid);

}
