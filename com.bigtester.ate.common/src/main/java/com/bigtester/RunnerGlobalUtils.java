/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.bigtester.ate.GlobalUtils;
import org.bigtester.ate.constant.ExceptionErrorCode;
import org.bigtester.ate.constant.ExceptionMessage;
import org.bigtester.ate.model.data.exception.RuntimeDataException;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

// TODO: Auto-generated Javadoc
/**
 * This class GlobalUitls defines ....
 * @author Peidong Hu
 *
 */
public class RunnerGlobalUtils {

	/**
	 *
	 */
	private RunnerGlobalUtils() {
		// TODO Auto-generated constructor stub
	}
	public static <T> Predicate<T> distinctByKey(Function<? super T,Object> keyExtractor) {
	    Map<Object,Boolean> seen = new ConcurrentHashMap<>();
	    //isSameHtmlNode = GlobalUtils.isSameHtmlCodeWithoutAteGuid(((INameLabelPredictableObjectNonPointerAssignment) record).getComparableHtmlCode(), ((INameLabelPredictableObjectNonPointerAssignment) record2).getComparableHtmlCode());
	    return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}
	public static String parseFirstAteGuid(String htmlSource) throws SAXException, IOException, ParserConfigurationException {
		Element node =  DocumentBuilderFactory
		    .newInstance()
		    .newDocumentBuilder()
		    .parse(new ByteArrayInputStream(htmlSource.getBytes()))
		    .getDocumentElement();
		return node.getAttribute("ate-guid");
	}
	public static String parseLeftRightBoundryData(String str, String left, String right, int index) throws RuntimeDataException {


		String[] values = StringUtils.substringsBetween(str, left, right);

		if (values.length < index+1 || -1 == index) {
			index = values.length -1;
		}
		return values[index];


	}

	public static String parseFirstAteGuidByBoundary(String mlHtmlCode) {

		//String preGuidSubString = mlHtmlCode.substring(0, mlHtmlCode.indexOf(guid));
		Optional<String[]> values = Optional.ofNullable(StringUtils.substringsBetween(mlHtmlCode, "ate-guid=\"", "\""));
		String[] str  = {""};
		String newGuidWithQuote = values.orElse(str)[0];
		//String newGuid = newGuidWithQuote.substring(0, newGuidWithQuote.length());
		return newGuidWithQuote;
	}
	public static String parseFirstAteAttrByBoundary(String mlHtmlCode, String ateAttrName) {

		//String preGuidSubString = mlHtmlCode.substring(0, mlHtmlCode.indexOf(guid));
		Optional<String[]> values = Optional.ofNullable(StringUtils.substringsBetween(mlHtmlCode, ateAttrName + "=\"", "\""));
		String[] str  = {""};
		String newGuidWithQuote = StringEscapeUtils.unescapeXml(values.orElse(str)[0]);
		//String newGuid = newGuidWithQuote.substring(0, newGuidWithQuote.length());
		return newGuidWithQuote;
	}

//	public static long parseGuidIndex(String mlHtmlCode, String guid) {
//
//		//String preGuidSubString = mlHtmlCode.substring(0, mlHtmlCode.indexOf(guid));
//		String[] values = StringUtils.substringsBetween(mlHtmlCode, "ate-guid=", " ");
//
//		String newGuidWithQuote = values[0];
//		String newGuid = newGuidWithQuote.substring(1, newGuidWithQuote.length()-1);
//		return newGuid;
//	}

	public static String parsePreviousAteGuid(String mlHtmlCode, String guid) {

			String preGuidSubString = mlHtmlCode.substring(0, mlHtmlCode.indexOf(guid));
			String[] values = StringUtils.substringsBetween(preGuidSubString, "ate-guid=\"", "\"");
			String newGuid = "";
			if (values!=null && values.length>1) {
				newGuid = values[values.length-1];
				//newGuid = newGuidWithQuote.substring(1, newGuidWithQuote.length()-1);
			}
		return newGuid;
	}

	public static String parsePreviousAteAttr(String mlHtmlCode, String currentValue, String attrName) {

		String preGuidSubString = mlHtmlCode.substring(0, mlHtmlCode.indexOf(currentValue));
		String[] values = StringUtils.substringsBetween(preGuidSubString, attrName + "=\"", "\"");
		String newGuid = "";
		if (values!=null && values.length>1) {
			newGuid = values[values.length-1];
			//newGuid = newGuidWithQuote.substring(1, newGuidWithQuote.length()-1);
		}
	return StringEscapeUtils.unescapeXml(newGuid);
}

	public static String parseNextAteGuid(String mlHtmlCode, String guid) {

		String nextGuidSubString = mlHtmlCode.substring(mlHtmlCode.indexOf(guid) + guid.length(), mlHtmlCode.length());
		String[] values = StringUtils.substringsBetween(nextGuidSubString, "ate-guid=\"", "\"");
		String newGuid = "";
		if (values!=null && values.length>0) {
			newGuid = values[0];
			//newGuid = newGuidWithQuote.substring(1, newGuidWithQuote.length()-1);
		}
	return newGuid;
}
public static String parseNextAteAttr(String mlHtmlCode, String currentValue, String attrName) {

		String nextGuidSubString = mlHtmlCode.substring(mlHtmlCode.indexOf(currentValue) + currentValue.length(), mlHtmlCode.length());
		String[] values = StringUtils.substringsBetween(nextGuidSubString, attrName + "=\"", "\"");
		String newGuid = "";
		if (values!=null && values.length>0) {
			newGuid = values[0];
			//newGuid = newGuidWithQuote.substring(1, newGuidWithQuote.length()-1);
		}
	return StringEscapeUtils.unescapeXml(newGuid);
}
	public static String readStringFromURL(URL url) {
		String retVal = "";
		Optional<InputStream> in = Optional.empty();
		try {
			in = Optional.of(url.openStream());
			retVal = IOUtils.toString( in.get());
		} catch (IOException e) {
			retVal = "";
		} finally {
			if (in.isPresent())
				IOUtils.closeQuietly(in.get());
		}
		return retVal;
	}
}
