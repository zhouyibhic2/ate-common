/**
 *
 */
package com.bigtester.tebloud.machine.fasttext.config;

/**
 * @author peidong
 *
 */
public class TrainingParams {
	private int dim = 20;
	private int lrUpdateRate = 5;
	private int ws = 5;
	private int neg = 5;
	private float lr = 1.43f;
	private int epoch = 65;
	private int thread = 8;
	private int wordNgrams = 1;
	private String loss = "hs";
	/**
	 * @return the dim
	 */
	public int getDim() {
		return dim;
	}
	/**
	 * @param dim the dim to set
	 */
	public void setDim(int dim) {
		this.dim = dim;
	}
	/**
	 * @return the lrUpdateRate
	 */
	public int getLrUpdateRate() {
		return lrUpdateRate;
	}
	/**
	 * @param lrUpdateRate the lrUpdateRate to set
	 */
	public void setLrUpdateRate(int lrUpdateRate) {
		this.lrUpdateRate = lrUpdateRate;
	}
	/**
	 * @return the ws
	 */
	public int getWs() {
		return ws;
	}
	/**
	 * @param ws the ws to set
	 */
	public void setWs(int ws) {
		this.ws = ws;
	}
	/**
	 * @return the neg
	 */
	public int getNeg() {
		return neg;
	}
	/**
	 * @param neg the neg to set
	 */
	public void setNeg(int neg) {
		this.neg = neg;
	}
	/**
	 * @return the lr
	 */
	public float getLr() {
		return lr;
	}
	/**
	 * @param lr the lr to set
	 */
	public void setLr(float lr) {
		this.lr = lr;
	}
	/**
	 * @return the epoch
	 */
	public int getEpoch() {
		return epoch;
	}
	/**
	 * @param epoch the epoch to set
	 */
	public void setEpoch(int epoch) {
		this.epoch = epoch;
	}
	/**
	 * @return the thread
	 */
	public int getThread() {
		return thread;
	}
	/**
	 * @param thread the thread to set
	 */
	public void setThread(int thread) {
		this.thread = thread;
	}
	/**
	 * @return the wordNgrams
	 */
	public int getWordNgrams() {
		return wordNgrams;
	}
	/**
	 * @param wordNgrams the wordNgrams to set
	 */
	public void setWordNgrams(int wordNgrams) {
		this.wordNgrams = wordNgrams;
	}
	/**
	 * @return the loss
	 */
	public String getLoss() {
		return loss;
	}
	/**
	 * @param loss the loss to set
	 */
	public void setLoss(String loss) {
		this.loss = loss;
	}

}
