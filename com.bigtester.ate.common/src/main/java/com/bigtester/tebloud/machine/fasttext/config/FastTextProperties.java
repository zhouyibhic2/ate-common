/**
 *
 */
package com.bigtester.tebloud.machine.fasttext.config;

import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author peidong
 *
 */
//@ConfigurationProperties(ignoreUnknownFields = false, prefix = "fasttext")
public class FastTextProperties {
    @NotNull
    private TrainingParams trainingParams;

	/**
	 * @return the trainingParams
	 */
	public TrainingParams getTrainingParams() {
		return trainingParams;
	}

	/**
	 * @param trainingParams the trainingParams to set
	 */
	public void setTrainingParams(TrainingParams trainingParams) {
		this.trainingParams = trainingParams;
	}



}