/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.tcg.config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;



// TODO: Auto-generated Javadoc
/**
 * The Class WebMvcConfig.
 */
@Component
public class RunningModeProperties {


	public static enum RunningMode {
		SINGLE_APP_TESTING, USECASE_APP, SINGLE_INDUSTRY_APP_TEST, CROSS_INDUSTRY_APP_TEST
	}

	@Value("${runningMode:SINGLE_APP_TESTING}")
	private String runningMode = "SINGLE_APP_TESTING";

	public String getRunningMode() {
		return runningMode;
	}


	@Value("${usecaseAPPTrainingEventName}")
	private String usecaseAPPTrainingEventName = "$set";

	public String getUsecaseAPPTrainingEventName() {
		return usecaseAPPTrainingEventName;
	}

}
