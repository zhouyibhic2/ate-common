/**
 *
 */
package com.bigtester.tebloud.tcg.config;

/**
 * @author peidong
 *
 */
public class PioProperties {
	private String decisionTreeChannelName;

	/**
	 * @return the decisionTreeChannelName
	 */
	public String getDecisionTreeChannelName() {
		return decisionTreeChannelName;
	}

	/**
	 * @param decisionTreeChannelName the decisionTreeChannelName to set
	 */
	public void setDecisionTreeChannelName(String decisionTreeChannelName) {
		this.decisionTreeChannelName = decisionTreeChannelName;
	}
}
