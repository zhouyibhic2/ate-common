/**
 *
 */
package com.bigtester.tebloud.tcg.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import com.bigtester.tebloud.machine.fasttext.config.FastTextProperties;

/**
 * @author peidong
 *
 */
@ConfigurationProperties(ignoreUnknownFields = false, prefix="tebloud")

public class TebloudApplicationProperties {
	private FastTextProperties fasttext;
	private PioProperties pio;

	private String fastTextServerHost;
	private String fasttextDockerContainerName;
	private boolean enableDecisionTreeInPrediction;


	/**
	 * @return the fasttext
	 */
	public FastTextProperties getFasttext() {
		return fasttext;
	}
	/**
	 * @param fasttext the fasttext to set
	 */
	public void setFasttext(FastTextProperties fasttext) {
		this.fasttext = fasttext;
	}
	/**
	 * @return the pio
	 */
	public PioProperties getPio() {
		return pio;
	}
	/**
	 * @param pio the pio to set
	 */
	public void setPio(PioProperties pio) {
		this.pio = pio;
	}
	/**
	 * @return the fastTextServerHost
	 */
	public String getFastTextServerHost() {
		return fastTextServerHost;
	}
	/**
	 * @param fastTextServerHost the fastTextServerHost to set
	 */
	public void setFastTextServerHost(String fastTextServerHost) {
		this.fastTextServerHost = fastTextServerHost;
	}
	/**
	 * @return the fasttextDockerContainerName
	 */
	public String getFasttextDockerContainerName() {
		return fasttextDockerContainerName;
	}
	/**
	 * @param fasttextDockerContainerName the fasttextDockerContainerName to set
	 */
	public void setFasttextDockerContainerName(String fasttextDockerContainerName) {
		this.fasttextDockerContainerName = fasttextDockerContainerName;
	}

	/**
	 * @return the enableDecisionTreeInPrediction
	 */
	public boolean isEnableDecisionTreeInPrediction() {
		return enableDecisionTreeInPrediction;
	}
	/**
	 * @param enableDecisionTreeInPrediction the enableDecisionTreeInPrediction to set
	 */
	public void setEnableDecisionTreeInPrediction(boolean enableDecisionTreeInPrediction) {
		this.enableDecisionTreeInPrediction = enableDecisionTreeInPrediction;
	}
}
