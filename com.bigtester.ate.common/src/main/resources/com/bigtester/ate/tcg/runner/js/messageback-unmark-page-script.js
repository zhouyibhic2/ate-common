// document.querySelector('button').addEventListener('click', function() {
//  chrome.extension.sendMessage({action: 'message', content:"Changed by page"}, function(message){});
// });
/*document.querySelector('button').addEventListener('click', function() {
 sendObjectToDevTools({content: "Changed by page"});
 });*/
//alert(ate_jQuery_3_0_0.fn.jquery);

var ate_unmarker_script_execution_ended = false;
function sendObjectToDevTools(message) {
    // The callback here can be used to execute something on receipt
    chrome.extension.sendMessage(message, function (message) {
    });
}
function appliedProperty(element, propString) {
    var notAppliedMap = {"form": ["height", "width"]};
    var retVal = true;
    Object.keys(notAppliedMap).forEach(function(elementTag){
        if (element.tagName.toLowerCase() == elementTag) {
            if (ate_jQuery_3_0_0.inArray(element.tagName.toLowerCase(), notAppliedMap[elementTag])) {
                retVal = false;
            }
        }
    })
    return retVal;

}


function ate_remove_all_markers(document) {
    var ate_allElements = ate_jQuery_3_0_0(ate_get_body_equavelent_element(document)).find("*");
    ate_jQuery_3_0_0.each(ate_allElements, function(elemIndex, elem) {
        elem.removeAttribute("ate-invisible");
        elem.removeAttribute("ate-computed-width");
        elem.removeAttribute("ate-computed-height");
        elem.removeAttribute("ate-computed-leftTopCornerX");
        elem.removeAttribute("ate-computed-leftTopCornerY");
        elem.removeAttribute("ate-guid");
        elem.removeAttribute("ate-computed-xpath");

    });
}

function ate_removeJSStypleAndCloneDoc(doc) {

    var cloned = ate_jQuery_3_0_0(doc).find("html").clone()[0];
    var arr_scripts = ate_jQuery_3_0_0(cloned).find("script");
    var arr_styles = ate_jQuery_3_0_0(cloned).find("style");
    var arr_links = ate_jQuery_3_0_0(cloned).find("link");
    ate_jQuery_3_0_0.each(arr_scripts,function(eachIndex, eachValue) {
        eachValue.remove();
    })
    ate_jQuery_3_0_0.each(arr_styles,function(eachIndex, eachValue) {
        eachValue.remove();
    })
    ate_jQuery_3_0_0.each(arr_links,function(eachIndex, eachValue) {
        eachValue.remove();
    })
    return cloned;
}


function ate_get_body_equavelent_element(doc) {
    if (typeof doc.body != "undefined") {
        //document.body either return frameset or body element in chrome
        return doc.body;
    }
    else if (typeof doc.getElementsByTagName("frameset") != "undefined") {
        return doc.getElementsByTagName("frameset")[0];
    } else {
        return doc.getElementsByTagName("body")[0];
    }
}


ate_parseAllFrames = function(rootDomDoc) {
    var iframeElements=[];
    var frameElements=[];
    var allFrameNodes=[];
    var frameDoc;

    iframeElements = Array.prototype.slice.call(rootDomDoc.getElementsByTagName("iframe"), 0);
    frameElements = Array.prototype.slice.call(rootDomDoc.getElementsByTagName("frame"), 0);

    allFrameNodes = iframeElements.concat(frameElements);
    return allFrameNodes;
}

ate_unmarkAllDocumentsOnPage = function (thisDocument) {
    ate_remove_all_markers(thisDocument);
    var allFrameNodes = ate_parseAllFrames(thisDocument);
    for (var i = 0; i < allFrameNodes.length; i++) {

        frameDoc = allFrameNodes[i].contentWindow.document;
        ate_unmarkAllDocumentsOnPage(frameDoc.documentElement);
    }
}


function getAllElementsInBody(bodyElement)
{
    ate_ml_allElements_in_page = bodyElement.getElementsByTagName("*");


};

ate_unmarkAllDocumentsOnPage (document);

ate_global_devTool_back_message = {
    content: {
        screenWidth: window.screen.availWidth,
        topDocumentHeight:ate_jQuery_3_0_0(document).height(),
        topDocumentWidth:ate_jQuery_3_0_0(document).width(),
        pages: {},
        //allClickables: ate_ml_allClickables_in_page,
        screenUrl: window.location.href.replace(/^.*\/\/[^\/]+/, ''),
        domain: window.location.host,
        domainProtocol: window.location.protocol,
        domainPort: window.location.port,

    }
};

function getMarkedPagesInJson() {
    if (ate_global_devTool_back_message !== 'undefined') {
        return JSON.stringify(ate_global_devTool_back_message);
    } else {
        return "";
    }
}
if (typeof ate_global_webdriver_executed_js === 'undefined')
    sendObjectToDevTools(ate_global_devTool_back_message);

var ate_unmarker_script_execution_ended = true;
/*else
 //this return can't be accepted by chrome browser in chrome extension injection.
 //we will add this statement in java code.
 return getMarkedPagesInJson();
 */
//http://stackoverflow.com/questions/17727977/how-to-get-all-text-from-all-tags-in-one-array to get the text on the page. need to iterate the frames
