/**
 *
 */
package com.bigtester.ate.tcg.utils;

import org.testng.annotations.Test;

import com.bigtester.ate.tcg.controller.WebFormUserInputsCollectorHtmlTerms;

/**
 * @author peidong
 *
 */
public class GlobalUtilsTest {
	//@Test
	public void removeTagAndAttributeNamesAndDigitsTest() {
		String testStr = "<a href=\"http://peidong-3442:8888/wp-admin/post.php?post=36&amp;action=edit\" aria-label=\"Edit “my first wp post title”\" ate-guid=\"ef734f66-aa63-444f-a8be-92a3c9ac8edf\" ate-computed-width=\"123\" ate-computed-height=\"14\" ate-computed-lefttopcornerx=\"625\" ate-computed-lefttopcornery=\"701\" ate-computed-xpath=\"/html/body/div/div[2]/div[2]/div/div[4]/div[2]/div/div[2]/div/div/div/div/ul/li/div/a\">my first wp post title</a>";
		String result = GlobalUtils.removeTagAndAttributeNamesAndDigits(GlobalUtils.replaceGuidId(testStr));
		System.out.println(result);
	}

	@Test
	public void cleanHtmlFragmentTest() {
		String attributesToRemove = WebFormUserInputsCollectorHtmlTerms.PREVIOUS_PROCESSED_ATTR_NAME
				+ "|"
				+ WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME
				+ "|ate-guid|ate-computed-height|ate-computed-lefttopcornerx|ate-computed-lefttopcornery|ate-computed-width|ate-computed-xpath";

		String testStr = "<a href=\"http://peidong-3442:8888/wp-admin/post.php?post=36&amp;action=edit\" aria-label=\"Edit “my first wp post title”\" ate-guid=\"ef734f66-aa63-444f-a8be-92a3c9ac8edf\" ate-computed-width=\"123\" ate-computed-height=\"14\" ate-computed-lefttopcornerx=\"625\" ate-computed-lefttopcornery=\"701\" ate-computed-xpath=\"/html/body/div/div[2]/div[2]/div/div[4]/div[2]/div/div[2]/div/div/div/div/ul/li/div/a\">my first wp post title</a>";
		String result = GlobalUtils.cleanHtmlFragment(testStr, attributesToRemove);
		System.out.println(result);
	}

}
